#!/bin/bash
rm -f legendary.zip
zip -r legendary.zip bin/
zip -r legendary.zip lib/
zip -r legendary.zip rules.pdf
zip -r legendary.zip resources/db/clearDB.txt
zip -r legendary.zip resources/images/
zip -r legendary.zip README.txt
zip -r legendary.zip Server.bat
zip -r legendary.zip Server.sh
zip -r legendary.zip Legendary.bat
zip -r legendary.zip Legendary.sh

