package client.preloader;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

import client.definitions.ClientVariables;

public class Preloader extends JFrame implements ClientVariables{
		
	private static final long serialVersionUID = -5415205937326886538L;
	
	private int currentFile = ZERO;
	private int totalFiles = NEGATIVE_ONE;
	JProgressBar dialogText = null;
	private static Preloader preloader = null;
	
	public static Preloader getInstance(){
		if(preloader == null){
			new Preloader();
		}
		return preloader;
	}
	
	private Preloader(){
		Preloader.preloader = this;		
		
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run(){
				if(totalFiles == NEGATIVE_ONE){
					totalFiles = ONE;
				}
				
				preloader.setTitle(PRELOADER_TITLE);
				preloader.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				
				setPanel();
				
				preloader.setPreferredSize(new Dimension(PRELOADER_WIDTH, PRELOADER_HEIGHT));
				preloader.pack();
				preloader.setLocationRelativeTo(null);
				
				preloader.setVisible(true);
			}
		});
	}
	
	private void setPanel(){
		JPanel outer = new JPanel();
		outer.setLayout(new BorderLayout());
		JPanel upper = new JPanel();
		upper.setLayout(new FlowLayout());
		JLabel label = new JLabel(PRELOADER_MESSAGE);
		label.setFont(FONT_PLAIN_40);
		
		upper.add(label);
		outer.add(upper, BorderLayout.NORTH);
				
		dialogText = new JProgressBar(ZERO, ONE);
		dialogText.setPreferredSize(new Dimension(PRELOADER_LOADER_WIDTH, PRELOADER_LOADER_HEIGHT));
		dialogText.setStringPainted(true);
		outer.add(dialogText, BorderLayout.SOUTH);
		this.add(outer);
		start();
	}
	
	@SuppressWarnings("rawtypes")
	private void start(){
		SwingWorker worker = new SwingWorker(){

			@Override
			protected Object doInBackground() throws Exception {
				try{
					while(true){
						dialogText.setMaximum(totalFiles);
						dialogText.setValue(currentFile);
						if(preloader == null){
							break;
						}
						Thread.sleep(PRELOADER_SLEEP);
					}
				}
				catch(Exception e){
					e.printStackTrace();
				}
				return null;
			}
			
		};
		worker.execute();
	}
	
	public void setTotalFiles(int size){
		totalFiles = size;
	}
	
	public void incCurrent(){
		++currentFile;
	}
	
	public void stop(){
		this.dispose();
		preloader = null;
	}
}
