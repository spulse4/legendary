package client.join;

import java.util.List;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import client.base.*;
import client.definitions.ClientVariables;
import shared.definitions.PlayerHeader;

@SuppressWarnings("serial")
public class PlayerWaitingView extends OverlayView implements ClientVariables{

	private JLabel label;
	private JButton startButton;
	private JButton heroButton;
	private JButton mastermindButton;
	private JButton villiansButton;
	private JButton schemeButton;
	private JPanel southPanel;
	private JPanel settingsPanel;
	private JPanel startPanel;
	private JPanel center;

	public PlayerWaitingView(){
		this.setOpaque(true);
		this.setLayout(new BorderLayout());
		this.setBorder(BorderFactory.createLineBorder(Color.black, VIEW_BORDER_WIDTH));
		
		label = new JLabel(PLAYER_WAITING_TITLE);
		setFont(label, TEXT_SIZE_LABEL);
		this.add(label, BorderLayout.NORTH);
		
		center = new JPanel();
		center.setLayout(new BoxLayout(center, BoxLayout.Y_AXIS));
		this.add(center, BorderLayout.CENTER);	
		
		settingsPanel = new JPanel();
		settingsPanel.setLayout(new BoxLayout(settingsPanel, BoxLayout.X_AXIS));
		Font font = FONT_PLAIN_20;
		
		heroButton = new JButton(PLAYER_WAITING_EDIT_HEROES);
		heroButton.setFont(font);
		heroButton.setPreferredSize(new Dimension(PLAYER_WAITING_BUTTON_WIDTH, PLAYER_WAITING_BUTTON_HEIGHT));
		heroButton.addActionListener(actionListener);

		mastermindButton = new JButton(PLAYER_WAITING_EDIT_MASTERMIND);
		mastermindButton.setFont(font);
		mastermindButton.setPreferredSize(new Dimension(PLAYER_WAITING_BUTTON_WIDTH, PLAYER_WAITING_BUTTON_HEIGHT));
		mastermindButton.addActionListener(actionListener);

		villiansButton = new JButton(PLAYER_WAITING_EDIT_VILLIANS);
		villiansButton.setFont(font);
		villiansButton.setPreferredSize(new Dimension(PLAYER_WAITING_BUTTON_WIDTH, PLAYER_WAITING_BUTTON_HEIGHT));
		villiansButton.addActionListener(actionListener);

		schemeButton = new JButton(PLAYER_WAITING_EDIT_SCHEME);
		schemeButton.setFont(font);
		schemeButton.setPreferredSize(new Dimension(PLAYER_WAITING_BUTTON_WIDTH, PLAYER_WAITING_BUTTON_HEIGHT));
		schemeButton.addActionListener(actionListener);
		
		settingsPanel.add(mastermindButton);
		settingsPanel.add(Box.createRigidArea(new Dimension(PLAYER_WAITING_RIGID_AREA, ZERO)));
		settingsPanel.add(schemeButton);
		settingsPanel.add(Box.createRigidArea(new Dimension(PLAYER_WAITING_RIGID_AREA, ZERO)));
		settingsPanel.add(heroButton);
		settingsPanel.add(Box.createRigidArea(new Dimension(PLAYER_WAITING_RIGID_AREA, ZERO)));
		settingsPanel.add(villiansButton);
				
		startPanel = new JPanel();
		startPanel.setLayout(new BoxLayout(startPanel, BoxLayout.X_AXIS));
				
		startPanel.add(Box.createHorizontalGlue());
						
		startButton = new JButton(PLAYER_WAITING_START_WITH_CURRENT_PLAYERS);
		startButton.addActionListener(actionListener);
		setFont(startButton, TEXT_SIZE_BUTTON);
		startPanel.add(startButton);
		
		southPanel = new JPanel();
		southPanel.setLayout(new BoxLayout(southPanel, BoxLayout.Y_AXIS));
		southPanel.add(settingsPanel);
		southPanel.add(Box.createRigidArea(new Dimension(ZERO, PLAYER_WAITING_RIGID_AREA/TWO)));
		southPanel.add(startPanel);
		this.add(southPanel, BorderLayout.SOUTH);
	}

	public PlayerWaitingController getPlayerWaitingController(){
		return (PlayerWaitingController)super.getController();
	}

	private ActionListener actionListener = new ActionListener(){
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == startButton) {			
				getPlayerWaitingController().startGame();
			}
			else if(e.getSource() == heroButton){
				getPlayerWaitingController().editHeros();
			}
			else if(e.getSource() == mastermindButton){
				getPlayerWaitingController().editMastermind();
			}
			else if(e.getSource() == villiansButton){
				getPlayerWaitingController().editVillians();
			}
			else if(e.getSource() == schemeButton){
				getPlayerWaitingController().editScheme();
			}
		}	
	};

	public void setPlayers(List<PlayerHeader> value){
		String labelText = PLAYER_WAITING_TITLE;
		this.closeModal();
		if(value.size() != MAX_PLAYERS){
			labelText = (PLAYER_WAITING_MESSAGE_1 + (MAX_PLAYERS - value.size()) + PLAYER_WAITING_MESSAGE_2);
			startButton.setEnabled(true);
		}
		
		label.setText(labelText);
		
		center.removeAll();
		
		for(int i = ZERO; i < value.size(); i++){
			String builtString = (i + ONE) + SPACE + value.get(i).getName();
			JPanel playerPanel = new JPanel();
			playerPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
			playerPanel.setPreferredSize(new Dimension(PLAYER_WAITING_PANEL_WIDTH, PLAYER_WAITING_PANEL_HEIGHT));
			Color color = null;
			switch(i){
			case ZERO:
				color = new Color(PURPLE_RED, PURPLE_GREEN, PURPLE_BLUE);
				break;
			case ONE:
				color = Color.RED;
				break;
			case TWO:
				color = Color.CYAN;
				break;
			case THREE:
				color = Color.YELLOW;
				break;
			case FOUR:
				color = Color.GREEN;
			}
			
			playerPanel.setBackground(color);
			JLabel playerLabel = new JLabel(builtString, SwingConstants.LEFT);
			setFont(playerLabel, TEXT_SIZE_LABEL);
			playerPanel.add(playerLabel);
			center.add(playerPanel);
			
			Dimension minSize = new Dimension(PLAYER_WAITING_RIGID_AREA/FOUR, PLAYER_WAITING_RIGID_AREA/TWO);
			Dimension prefSize = new Dimension(PLAYER_WAITING_RIGID_AREA/FOUR, PLAYER_WAITING_RIGID_AREA/TWO);
			Dimension maxSize = new Dimension(Short.MAX_VALUE, PLAYER_WAITING_RIGID_AREA/TWO);
			center.add(new Box.Filler(minSize, prefSize, maxSize));			
		}
		this.showModal();
	}
	
	private void setFont(JComponent comp, int size){
		Font font = comp.getFont();
		font = font.deriveFont(font.getStyle(), size);
		comp.setFont(font);;
	}
}

