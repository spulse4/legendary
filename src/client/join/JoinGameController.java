package client.join;

import shared.definitions.GameHeader;

import client.base.*;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import client.misc.*;
import client.poller.GamePoller;
import client.poller.JoinGamePoller;

public class JoinGameController extends Controller implements ClientVariables{
	
	private MessageView messageView;
	private NewGameView newGameView;
	
	public JoinGameController(IView view){
		super(view);

		this.newGameView = new NewGameView();
		this.messageView = new MessageView();
		
		newGameView.setController(this);
	}
	
	public JoinGameView getJoinGameView(){
		return (JoinGameView)super.getView();
	}
	
	public NewGameView getNewGameView(){
		return newGameView;
	}
		
	public void start(){		
		ClientManager.getInstance().setPoller(new JoinGamePoller((JoinGameView)getView()));
	}

	public void startCreateNewGame(){
		newGameView.showModal();
	}

	public void cancelCreateNewGame(){
		newGameView.closeModal();
		getJoinGameView().showModal();
	}

	public void createNewGame(){
		String title = newGameView.getTitle();

		int id = ClientManager.getInstance().getServer().createGame(title);
		if(id != -1){
			newGameView.closeModal();
		}
		else{
			messageView.setTitle(JOIN_GAME_EXISTS_TITLE);
			messageView.setMessage(JOIN_GAME_EXISTS_MESSAGE);
			messageView.showModal();
		}
		newGameView.setTitle(EMPTY);
	}

	public void joinGame(GameHeader game){
		ClientManager.getInstance().setGame(game);
				
		getJoinGameView().closeModal();
		
		int gameID = game.getGameID();
		int playerID = ClientManager.getInstance().getUser().getId();
		String playerName = ClientManager.getInstance().getUser().getName();
		
		int index = ClientManager.getInstance().getServer().joinGame(gameID, playerID, playerName);

		if(index != -1){
			ClientManager.getInstance().setPlayerIndex(index);
			ClientManager.getInstance().getPoller().stop();
			startPlayerWaiting();
			ClientManager.getInstance().setPoller(new GamePoller());
		}
		else{
			messageView.setTitle(JOIN_COULD_NOT_JOIN_GAME_TITLE);
			messageView.setMessage(JOIN_COULD_NOT_JOIN_GAME_MESSAGE);
			ClientManager.getInstance().setGame(null);
			getJoinGameView().showModal();
			messageView.showModal();
		}
	}

	public void cancelJoinGame(){
		ClientManager.getInstance().setGame(null);
		getJoinGameView().closeModal();
		getJoinGameView().showModal();
	}
	
	private void startPlayerWaiting(){
		PlayerWaitingController cont = new PlayerWaitingController(new PlayerWaitingView());
		cont.start();
	}
}

