package client.join;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import client.base.*;
import client.definitions.ClientVariables;

import javax.swing.border.Border;

@SuppressWarnings("serial")
public class NewGameView extends OverlayView implements ClientVariables{
    
    private JLabel lblNewGameSettings = null;
    private JButton createButton = null;
    private JButton cancelButton = null;
    private JPanel buttonPanel = null;
    
    private JTextField txtTitle = null;

    public NewGameView(){
        this.setOpaque(true);
        this.setLayout(new BorderLayout());
        this.setBorder(BorderFactory.createLineBorder(Color.black, VIEW_BORDER_WIDTH));

        lblNewGameSettings = new JLabel(NEW_GAME_MESSAGE);
        Font labelFont = lblNewGameSettings.getFont();
        labelFont = labelFont.deriveFont(labelFont.getStyle(), TEXT_SIZE_LABEL);
        lblNewGameSettings.setFont(labelFont);
        this.add(lblNewGameSettings, BorderLayout.PAGE_START);

        this.add(initInternalComponents(), BorderLayout.CENTER);
        
        cancelButton = new JButton(BUTTON_CANCEL);
        cancelButton.addActionListener(actionListener);
        Font buttonFont = cancelButton.getFont();
        buttonFont = buttonFont.deriveFont(buttonFont.getStyle(), TEXT_SIZE_BUTTON);
        cancelButton.setFont(buttonFont);

        createButton = new JButton(CREATE_GAME);
        createButton.addActionListener(actionListener);
        createButton.setFont(buttonFont);

        buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
        buttonPanel.add(createButton);
        buttonPanel.add(cancelButton);        
        this.add(buttonPanel, BorderLayout.SOUTH);                
    }
    
    public JoinGameController getJoinGameController(){
        return (JoinGameController) super.getController();
    }
    
    private JComponent initInternalComponents(){
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
        
        JLabel lblTitle = new JLabel(TITLE_STRING);
        txtTitle = new JTextField();
        
        
        mainPanel.add(lblTitle);
        mainPanel.add(txtTitle);
        
        mainPanel.setBorder(createBufferBorder());
        
        return mainPanel;        
    }
    
    private Border createBufferBorder(){
        Border innerBuffer = BorderFactory.createEmptyBorder(NEW_GAME_BUFFER, NEW_GAME_BUFFER, NEW_GAME_BUFFER, NEW_GAME_BUFFER);
        Border outerBuffer = BorderFactory.createEmptyBorder(NEW_GAME_BUFFER, NEW_GAME_BUFFER, NEW_GAME_BUFFER, NEW_GAME_BUFFER);
        Border etching = BorderFactory.createEtchedBorder();

        Border outerCompound = BorderFactory.createCompoundBorder(outerBuffer, etching);
        Border wholeCompound = BorderFactory.createCompoundBorder(outerCompound, innerBuffer);

        return wholeCompound;
    }

    private ActionListener actionListener = new ActionListener(){
        @Override
        public void actionPerformed(ActionEvent e){

            if (e.getSource() == createButton){
                getJoinGameController().createNewGame();
            }
            else if (e.getSource() == cancelButton){
                getJoinGameController().cancelCreateNewGame();
            }
        }
    };

    public void setTitle(String value){
        this.txtTitle.setText(value);
    }

    public String getTitle(){
        return txtTitle.getText();
    }
}

