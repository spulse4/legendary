package client.join;

import java.util.ArrayList;
import java.util.List;
import client.base.*;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import client.misc.MessageView;
import client.settings.*;
import shared.definitions.PlayerHeader;
import shared.model.ModelFacade;

public class PlayerWaitingController extends Controller implements ClientVariables{
		
	private List<PlayerHeader> players;
	private HeroController heroes;
	private MastermindController mastermind;
	private VillianController villians;
	private SchemeController schemes;
	private MessageView messageView;
	
	public PlayerWaitingController(IView view){
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
		players = new ArrayList<PlayerHeader>();
		heroes = new HeroController(new HeroView());
		mastermind = new MastermindController(new MastermindView());
		villians = new VillianController(new VillianView());
		schemes = new SchemeController(new SchemeView());
		messageView = new MessageView();
	}

	public PlayerWaitingView getPlayerWaitingView(){
		return (PlayerWaitingView)super.getView();
	}

	public void start(){
		List<PlayerHeader> players = ClientManager.getInstance().getGame().getPlayers();
		getPlayerWaitingView().setPlayers(players);
	}

	public void startGame() {
		if(canStartGame()){
			int gameID = ClientManager.getInstance().getGame().getGameID();
			ClientManager.getInstance().getServer().startGame(gameID);
		}
	}

	@Override
	public void playersChanged() {
		if(compareExisting(ClientManager.getInstance().getGame().getPlayers())){
			return;
		}
		players = ClientManager.getInstance().getGame().getPlayers();
		getPlayerWaitingView().setPlayers(players);
	}
	
	private boolean compareExisting(List<PlayerHeader> newGames){
		if(players.size() != newGames.size()){
			return false;
		}
		for(int i = ZERO; i < players.size(); ++i){
			if(players.get(i).getId() != newGames.get(i).getId()){
				return false;
			}
		}
		return true;
	}

	public void editHeros() {
		heroes.start();
	}

	public void editMastermind() {
		mastermind.start();
	}

	public void editVillians() {
		villians.start();
	}
	
	public void editScheme() {
		schemes.start();
	}
	
	@Override
	public void gameStarted(){
		if(getPlayerWaitingView().isModalShowing()){
			getPlayerWaitingView().closeModal();
		}
	}
	
	private boolean canStartGame(){
		int players = ClientManager.getInstance().getGame().getPlayers().size();
		String mastermind = this.mastermind.getMastermindView().getMastermind();
		String scheme = this.schemes.getSchemeView().getScheme();
		List<String> heroes = this.heroes.getHeroView().getHeroes();
		List<String> villiansTemp = this.villians.getVillianView().getVillians();
		List<String> villians = new ArrayList<String>();
		List<String> henchmen = new ArrayList<String>();
		
		if(mastermind == null){
			messageView.setTitle(PLAYER_WAITING_MASTERMIND);
			messageView.setMessage(PLAYER_WAITING_SELECT_MASTERMIND);
			messageView.showModal();
			return false;
		}
		if(scheme == null){
			messageView.setTitle(PLAYER_WAITING_SCHEME);
			messageView.setMessage(PLAYER_WAITING_SELECT_SCHEME);
			messageView.showModal();
			return false;
		}

		for(String str : villiansTemp){
			if(str.equals(DOOMBOT_LEGION) ||
			   str.equals(HAND_NINJAS) ||
			   str.equals(SAVAGE_LAND_MUTATES) ||
			   str.equals(SENTINEL)){
				henchmen.add(str);
			}
			else{
				villians.add(str);
			}
		}
		
		int numberHeroes = ZERO;
		int numberVillians = ZERO;
		int numberHenchmen = ZERO;
		switch(players){
		case 5:
			numberHeroes = SIX;
			numberVillians = FOUR;
			numberHenchmen = TWO;
			break;
		case 4:
			numberHeroes = FIVE;
			numberVillians = THREE;
			numberHenchmen = TWO;
			break;
		case 3:
			numberHeroes = FIVE;
			numberVillians = THREE;
			numberHenchmen = ONE;
			break;
		case 2:
			numberHeroes = FIVE;
			numberVillians = TWO;
			numberHenchmen = ONE;
			if(scheme.equals(SUPER_HERO_CIVIL_WAR)){
				--numberHeroes;
			}
			break;
		case 1:
			numberHeroes = THREE;
			numberVillians = ONE;
			numberHenchmen = ONE;
			break;
		}
		
		if(scheme.equals(SECRET_INVASION_OF_THE_SKRULL_SHAPESHIFTERS)){
			++numberHeroes;
		}
		
		if(mastermind.equals(DR_DOOM)){
			if(!henchmen.contains(DOOMBOT_LEGION) && players != ONE){
				messageView.setTitle(ERROR_DR_DOOM_TITLE);
				messageView.setMessage(ERROR_DR_DOOM_MESSAGE);
				messageView.showModal();
				return false;
			}
		}
		else if(mastermind.equals(LOKI)){
			if(!villians.contains(CATEGORY_ENEMIES_OF_ASGARD) && players != ONE){
				messageView.setTitle(ERROR_LOKI_TITLE);
				messageView.setMessage(ERROR_LOKI_MESSAGE);
				messageView.showModal();
				return false;
			}
		}
		else if(mastermind.equals(MAGNETO)){
			if(!villians.contains(CATEGORY_BROTHERHOOD) && players != ONE){
				messageView.setTitle(ERROR_MAGNETO_TITLE);
				messageView.setMessage(ERROR_MAGNETO_MESSAGE);
				messageView.showModal();
				return false;
			}
		}
		else if(mastermind.equals(RED_SKULL)){
			if(!villians.contains(CATEGORY_HYDRA) && players != ONE){
				messageView.setTitle(ERROR_RED_SKULL_TITLE);
				messageView.setMessage(ERROR_RED_SKULL_MESSAGE);
				messageView.showModal();
				return false;
			}
		}
		
		if(heroes.size() != numberHeroes){
			messageView.setTitle(ERROR_HERO_MISMATCH_TITLE);
			StringBuilder str = new StringBuilder();
			str.append(ERROR_GENERIC_MESSAGE_1);
			str.append(numberHeroes);
			if(numberHeroes != 1){
				str.append(ERROR_HERO_MISMATCH_MESSAGE_2_PLURAL);
			}
			else{
				str.append(ERROR_HERO_MISMATCH_MESSAGE_2_SINGULAR);
			}
			str.append(heroes.size());
			str.append(ERROR_GENERIC_MESSAGE_3);
			messageView.setMessage(str.toString());
			messageView.showModal();
			return false;
		}
		if(villians.size() != numberVillians){
			messageView.setTitle(ERROR_VILLIAN_MISMATCH_TITLE);
			StringBuilder str = new StringBuilder();
			str.append(ERROR_GENERIC_MESSAGE_1);
			str.append(numberVillians);
			if(numberVillians != 1){
				str.append(ERROR_VILLIAN_MISMATCH_MESSAGE_2_PLURAL);
			}
			else{
				str.append(ERROR_VILLIAN_MISMATCH_MESSAGE_2_SINGULAR);
			}
			str.append(villians.size());
			str.append(ERROR_GENERIC_MESSAGE_3);
			messageView.setMessage(str.toString());
			messageView.showModal();
			return false;
		}
		if(henchmen.size() != numberHenchmen){
			messageView.setTitle(ERROR_HENCHMEN_MISMATCH_TITLE);
			StringBuilder str = new StringBuilder();
			str.append(ERROR_GENERIC_MESSAGE_1);
			str.append(numberHenchmen);
			if(numberHenchmen != 1){
				str.append(ERROR_HENCHMEN_MISMATCH_MESSAGE_2_PLURAL);
			}
			else{
				str.append(ERROR_HENCHMEN_MISMATCH_MESSAGE_2_SINGULAR);
			}
			str.append(henchmen.size());
			str.append(ERROR_GENERIC_MESSAGE_3);
			messageView.setMessage(str.toString());
			messageView.showModal();
			return false;
		}
		return true;
	}
	
	@Override
	public void turnTrackerChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		String mastermind = model.getTurnTracker().getMastermind();
		String scheme = model.getTurnTracker().getScheme();
		List<String> henchmen = model.getTurnTracker().getHenchmen();
		List<String> villians = model.getTurnTracker().getVillians();
		List<String> heroes = model.getTurnTracker().getHeroes();
		
		for(String str : henchmen){
			villians.add(str);
		}
		
		this.mastermind.getMastermindView().setMastermind(mastermind);
		this.schemes.getSchemeView().setScheme(scheme);
		this.heroes.getHeroView().setHeroes(heroes);
		this.villians.getVillianView().setVillians(villians);
	}
}

