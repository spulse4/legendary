package client.join;

import java.awt.*;
import java.awt.event.*;
import java.util.List;

import javax.swing.*;

import client.base.*;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.GameHeader;
import shared.definitions.PlayerHeader;
import shared.definitions.UserHeader;

@SuppressWarnings("serial")
public class JoinGameView extends OverlayView implements ClientVariables{
		
	private JLabel label;
	private JLabel subLabel;

	private JLabel hash;
	private JLabel name;
	private JLabel currentPlayer;
	private JLabel join;

	private JButton createButton;

	private JPanel labelPanel;
	private JPanel gamePanel;
	private JPanel buttonPanel;

	private List<GameHeader> games;
	private UserHeader localPlayer;

	public JoinGameView(){
		this.initialize();
	}
	
	public JoinGameController getJoinGameController(){
		return (JoinGameController)super.getController();
	}

	private void initialize(){
		this.initializeView();
	}

	private void initializeView(){
		this.setOpaque(true);
		this.setLayout(new BorderLayout());
		this.setBorder(BorderFactory.createLineBorder(Color.black, VIEW_BORDER_WIDTH));

		label = new JLabel(JOIN_WELCOME);
		Font labelFont = label.getFont();
		labelFont = labelFont.deriveFont(labelFont.getStyle(), TEXT_SIZE_LABEL);
		label.setFont(labelFont);
		subLabel = new JLabel(JOIN_MESSAGE);
		labelFont = subLabel.getFont();
		labelFont = labelFont.deriveFont(labelFont.getStyle(), TEXT_SIZE_LABEL * TWO / THREE);
		subLabel.setFont(labelFont);

		labelPanel = new JPanel();
		labelPanel.setLayout(new FlowLayout());
		labelPanel.add(label);
		labelPanel.add(subLabel);
		this.add(labelPanel, BorderLayout.NORTH);

		gamePanel = new JPanel();
		gamePanel.setLayout(new GridLayout(ZERO, FOUR));
		hash = new JLabel(HASH);
		labelFont = new Font(labelFont.getFontName(), Font.BOLD, TEXT_SIZE_PANEL);
		hash.setFont(labelFont);
		name = new JLabel(NAME_STRING);
		name.setFont(labelFont);
		currentPlayer = new JLabel(JOIN_CURRENT_PLAYERS);
		currentPlayer.setFont(labelFont);
		join = new JLabel(JOIN);
		join.setFont(labelFont);

		gamePanel.add(hash);
		gamePanel.add(name);
		gamePanel.add(currentPlayer);
		gamePanel.add(join);

		if (games != null && games.size() > ZERO){
			labelFont = labelFont.deriveFont(labelFont.getStyle(), TEXT_SIZE_PANEL);
			for (GameHeader game : games){
				if(game.isFinished()){
					continue;
				}
				if(game.isStarted()){
					boolean contains = false;
					for(PlayerHeader player : game.getPlayers()){
						if(player.getId() == ClientManager.getInstance().getUser().getId()){
							contains = true;
							break;
						}
					}
					if(!contains){
						continue;
					}
				}
				JLabel tmp1 = new JLabel(String.valueOf(game.getGameID()));
				tmp1.setFont(labelFont);
				gamePanel.add(tmp1);
				JLabel tmp2 = new JLabel(game.getName());
				tmp2.setFont(labelFont);
				gamePanel.add(tmp2);
				String players = String.valueOf(game.getPlayers().size()) + JOIN_TOTAL;
				for (int j = 0; j < game.getPlayers().size(); j++){
					if (j < game.getPlayers().size() - 1){
						players = players + game.getPlayers().get(j).getName() + COMMA + SPACE;
					}
					else {
						players = players + game.getPlayers().get(j).getName();
					}
				}
				JLabel tmp3 = new JLabel(players);
				tmp3.setFont(labelFont);
				gamePanel.add(tmp3);
				JButton joinButton = null;
				
				for(PlayerHeader player : game.getPlayers()){
					if(player.getId() == localPlayer.getId()){
						joinButton = new JButton(REJOIN);
						break;
					}
				}
				if(game.getPlayers().size() >= FOUR && joinButton == null){
					joinButton = new JButton(FULL);
					joinButton.setEnabled(false);
				}
				if(joinButton == null){
					joinButton = new JButton(JOIN);
				}
				joinButton.setActionCommand(EMPTY + game.getGameID());
				joinButton.addActionListener(actionListener);
				gamePanel.add(joinButton);
			}
		}

		//Add all the above
		this.add(gamePanel, BorderLayout.CENTER);

		createButton = new JButton(CREATE_GAME);
		Font buttonFont = createButton.getFont();
		buttonFont = buttonFont.deriveFont(buttonFont.getStyle(), TEXT_SIZE_BUTTON);
		createButton.addActionListener(actionListener);
		createButton.setFont(buttonFont);

		buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		buttonPanel.add(createButton);
		this.add(buttonPanel, BorderLayout.SOUTH);
	}

	public void setGames(List<GameHeader> games, UserHeader localPlayer){
		if(checkSame(games)){
			return;
		}
		if(this.isModalShowing()){
			this.closeModal();
		}
		this.games = games;
		this.localPlayer = localPlayer;
		this.removeAll();
		this.initialize();
		this.getComponent(ONE).revalidate();
		this.getComponent(ONE).repaint();
		try{
			Thread.sleep(INTERVAL);
		}
		catch(InterruptedException e){
			e.printStackTrace();
		}
		this.showModal();
	}
	
	private ActionListener actionListener = new ActionListener(){
		@Override
		public void actionPerformed(ActionEvent e){
			if (e.getSource() == createButton){
				getJoinGameController().startCreateNewGame();
			}
			else{
				try{
					int gameId = Integer.parseInt(e.getActionCommand());
					GameHeader game = null;
					for (GameHeader g : games){
						if (g.getGameID() == gameId){
							game = g;
							break;
						}
					}
					getJoinGameController().joinGame(game);
				}
				catch (NumberFormatException ex)
				{
					ex.printStackTrace();
				}
			}
		}
	};
	
	public int getNextIndex(int gameID){
		int index = NEGATIVE_ONE;
		for(GameHeader game : games){
			if(game.getGameID() == gameID){
				int currentSize = game.getPlayers().size();
				if(currentSize == FOUR){
					break;
				}
				index = currentSize + ONE;
			}
		}
		return index;
	}
	
	private boolean checkSame(List<GameHeader> games){
		if(this.games == null){
			return false;
		}
		if(this.games.size() != games.size()){
			return false;
		}
		for(int i = 0; i < this.games.size(); ++i){
			if(this.games.get(i).getGameID() != games.get(i).getGameID()){
				return false;
			}
			List<PlayerHeader> thisPlayers = this.games.get(i).getPlayers();
			List<PlayerHeader> thatPlayers = games.get(i).getPlayers();
			
			if(thisPlayers.size() != thatPlayers.size()){
				return false;
			}
			for(int j = ZERO; j < thisPlayers.size(); ++j){
				if(thisPlayers.get(j).getId() != thatPlayers.get(j).getId()){
					return false;
				}
			}
		}
		return true;
	}
}

