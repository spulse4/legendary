package client.initialize;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientVariables;
import client.main.Client;
import client.main.ClientManager;
import client.misc.MessageView;

public class InitializeController extends Controller implements ClientVariables{

	private Client client;
	private MessageView messageView;
	
	public InitializeController(IView view, Client client) {
		super(view);
		this.client = client;
		messageView = new MessageView();
	}

	private InitializeView getInitializeView(){
		return (InitializeView) super.getView();
	}
	
	public void start(){
		getInitializeView().showModal();
	}
	
	public void submit(String host){
		if(!host.equals("localhost")){
			int periods = ZERO;
			for(int i = ZERO; i < host.length(); ++i){
				if(host.charAt(i) == PERIOD.charAt(ZERO)){
					++periods;
				}
			}
			if(periods != THREE){
				messageView.setTitle("Error");
				messageView.setMessage("Not a valid IP Address");
				messageView.showModal();
				return;
			}
		}
		ClientManager.getInstance().setHost(host);
		getInitializeView().closeModal();
		client.startLogin();
	}
}
