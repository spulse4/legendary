package client.initialize;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.InetAddress;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import client.base.OverlayView;
import client.definitions.ClientVariables;

public class InitializeView extends OverlayView implements ClientVariables{

	private static final long serialVersionUID = 59326422253308089L;

	public InitializeView(){
        this.setOpaque(true);
        this.setLayout(new BorderLayout());
        this.setBorder(BorderFactory.createLineBorder(Color.black, VIEW_BORDER_WIDTH));

        initComponents();
	}
	
	public InitializeController getInitializeController(){
		return (InitializeController) super.getController();
	}
	
	private void initComponents(){
		try{
			JPanel panel = new JPanel();
			panel.setPreferredSize(new Dimension(200, 70));
			panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
			JPanel hostPanel = new JPanel();
			JPanel buttonPanel = new JPanel();
			
			JLabel host = new JLabel("Host:");
			host.setPreferredSize(new Dimension(50, 20));
			
			final JTextField hostField = new JTextField(InetAddress.getLocalHost().getHostAddress());
			hostField.setPreferredSize(new Dimension(140, 20));
			
			JButton submit = new JButton(SUBMIT);
			submit.addActionListener(new ActionListener(){
	
				@Override
				public void actionPerformed(ActionEvent arg0) {
					getInitializeController().submit(hostField.getText());
				}
				
			});
			
			hostPanel.add(host);
			hostPanel.add(hostField);
			buttonPanel.add(submit);
			panel.add(hostPanel);
			panel.add(buttonPanel);
			
			this.add(panel);
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
}
