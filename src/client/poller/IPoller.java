package client.poller;

public interface IPoller{
	
	public void start();
	public void stop();
}
