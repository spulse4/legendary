package client.poller;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import client.communicator.ServerProxy;
import client.definitions.ClientVariables;
import client.join.JoinGameView;
import client.main.ClientManager;
import shared.definitions.GameHeader;

public class JoinGamePoller implements IPoller, ClientVariables{

	private JoinGameView gameView = null;
	private ServerProxy proxy = null;
	private Timer poller = null;

	public JoinGamePoller(JoinGameView gameView){
		this.proxy = ClientManager.getInstance().getServer();
		this.gameView = gameView;
		start();
	}
	
	@Override
	public void start(){
		poller = new Timer(true);
		poller.scheduleAtFixedRate(new TimerTask(){
			@Override
			public void run(){
				List<GameHeader> games = proxy.getGames();
				if(games != null){
					gameView.setGames(games, ClientManager.getInstance().getUser());
				}
			}
		}, ZERO, INTERVAL);
	}

	@Override
	public void stop(){
		poller.cancel();
		poller = null;
	}
}
