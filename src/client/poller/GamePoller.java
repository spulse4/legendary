package client.poller;

import java.util.Timer;
import java.util.TimerTask;

import com.google.gson.JsonObject;

import client.communicator.ServerProxy;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;

public class GamePoller implements IPoller, ClientVariables{

	private int version;
	private ServerProxy proxy = null;
	private Timer poller = null;
	
	public GamePoller(){
		version = NEGATIVE_ONE;
		this.proxy = ClientManager.getInstance().getServer();
		start();
	}
	
	@Override
	public void start(){
		poller = new Timer(true);
		poller.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run(){
				int gameID = ClientManager.getInstance().getGame().getGameID();
				JsonObject model = proxy.getModel(gameID, version);
				if(model.get(MODEL_VERSION) != null){
					version = model.get(MODEL_VERSION).getAsInt();
					ClientManager.getInstance().getModel().updateFromJson(model, ((PlayerHeader)(ClientManager.getInstance().getUser())).getIndex());
				}
			}
		}, ZERO, INTERVAL);
	}

	@Override
	public void stop(){
		poller.cancel();
		poller = null;
	}
}
