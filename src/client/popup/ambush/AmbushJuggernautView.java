package client.popup.ambush;

import client.popup.AbstractPopupView;

public class AmbushJuggernautView extends AbstractPopupView{
	//Each player KOs two Heroes from their discard pile
	
	private static final long serialVersionUID = 3241150527141451530L;
	
	public AmbushJuggernautView(){
		super(JUGGERNAUT_AMBUSH_MESSAGE);
	}
}
