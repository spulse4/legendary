package client.popup.ambush;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class AmbushJuggernautController extends Controller implements ClientVariables{
	//Each player KOs two Heroes from their discard pile

	public AmbushJuggernautController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public AmbushJuggernautView getAmbushJuggernautView(){
		return (AmbushJuggernautView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		
		if(model.getTurnTracker().getState() == State.JUGGERNAUT_AMBUSH){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				List<String> discard = model.getPlayerCards().getDiscard();
				List<ClientCard> cards = new ArrayList<ClientCard>();
				for(String str : discard){
					cards.add(ClientManager.getInstance().getBoard().getCard(str));
				}
				List<Integer> indexes = new ArrayList<Integer>();
				for(int i = ZERO; i < cards.size(); ++i){
					indexes.add(NEGATIVE_ONE);
				}
				getAmbushJuggernautView().update(cards, STATE_JUGGERNAUT_AMBUSH, false, indexes);
			}
		}
		else{
			if(getAmbushJuggernautView().isShowing()){
				getAmbushJuggernautView().closeModal();
			}
		}
	}
}
