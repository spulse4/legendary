package client.popup.ability;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class AbilityStackTheDeckController extends Controller implements ClientVariables{
	//Draw two cards. Then put a card from your hand on top of your deck

	public AbilityStackTheDeckController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public AbilityStackTheDeckView getAbilityStackTheDeckView(){
		return (AbilityStackTheDeckView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.STACK_THE_DECK_ABILITY){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				List<String> hand = model.getPlayerCards().getHand();
				List<ClientCard> cards = new ArrayList<ClientCard>();
				for(String str : hand){
					cards.add(ClientManager.getInstance().getBoard().getCard(str));
				}
				List<Integer> indexes = new ArrayList<Integer>();
				for(int i = ZERO; i < cards.size(); ++i){
					indexes.add(NEGATIVE_ONE);
				}
				getAbilityStackTheDeckView().update(cards, STATE_STACK_THE_DECK_ABILITY, false, indexes);
			}
		}
		else{
			if(getAbilityStackTheDeckView().isShowing()){
				getAbilityStackTheDeckView().closeModal();
			}
		}
	}
}
