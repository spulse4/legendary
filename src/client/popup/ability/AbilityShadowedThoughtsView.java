package client.popup.ability;

import client.popup.AbstractPopupView;

public class AbilityShadowedThoughtsView extends AbstractPopupView{
	//Red: You may play the top card of the Villain Deck. If you do, you get +2 Attack Power

	private static final long serialVersionUID = -195029779396177281L;

	public AbilityShadowedThoughtsView() {
		super(SHADOWED_THOUGHTS_MESSAGE);
	}
}
