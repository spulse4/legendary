package client.popup.ability;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;
import shared.model.PlayerCards;

public class AbilityHypnoticCharmController extends Controller implements ClientVariables{
	//Reveal the top card of your deck. Discard it or put it back.
	//Yellow: Do the same thing to each other player's deck

	public AbilityHypnoticCharmController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public AbilityHypnoticCharmView getAbilityHypnoticCharmView(){
		return (AbilityHypnoticCharmView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		int currentPlayer = model.getTurnTracker().getCurrentPlayer();
		int thisPlayer = ((PlayerHeader)ClientManager.getInstance().getUser()).getIndex();
		if(currentPlayer == thisPlayer && model.getTurnTracker().getState() == State.HYPNOTIC_CHARM_ABILITY){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			int index = effected.get(0);
			PlayerCards card = model.getPlayerCardList().get(index);
			List<ClientCard> cards = new ArrayList<ClientCard>();
			cards.add(ClientManager.getInstance().getBoard().getCard(card.getDraw().get(0)));
			List<Integer> indexes = new ArrayList<Integer>();
			for(int i = ZERO; i < cards.size(); ++i){
				indexes.add(NEGATIVE_ONE);
			}
			getAbilityHypnoticCharmView().update(cards, STATE_HYPNOTIC_CHARM_ABILITY, false, indexes);
		}
		else{
			if(getAbilityHypnoticCharmView().isShowing()){
				getAbilityHypnoticCharmView().closeModal();
			}
		}
	}
}
