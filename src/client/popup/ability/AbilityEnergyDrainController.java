package client.popup.ability;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class AbilityEnergyDrainController extends Controller implements ClientVariables{
	//Red: You may KO a card from your hand or discard pile. If you do, you get +1 Buy Power

	public AbilityEnergyDrainController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public AbilityEnergyDrainView getAbilityEnergyDrainView(){
		return (AbilityEnergyDrainView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.ENERGY_DRAIN_ABILITY){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				List<String> hand = model.getPlayerCards().getHand();
				List<String> discard = model.getPlayerCards().getDiscard();
				List<ClientCard> cards = new ArrayList<ClientCard>();
				for(String str : hand){
					cards.add(ClientManager.getInstance().getBoard().getCard(str));
				}
				for(String str : discard){
					cards.add(ClientManager.getInstance().getBoard().getCard(str));
				}
				List<Integer> indexes = new ArrayList<Integer>();
				for(int i = ZERO; i < cards.size(); ++i){
					indexes.add(NEGATIVE_ONE);
				}
				getAbilityEnergyDrainView().update(cards, STATE_ENERGY_DRAIN_ABILITY, true, indexes);
			}
		}
		else{
			if(getAbilityEnergyDrainView().isShowing()){
				getAbilityEnergyDrainView().closeModal();
			}
		}
	}
}
