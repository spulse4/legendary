package client.popup.ability;

import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class AbilityCoveringFireController extends Controller implements ClientVariables{
	//Black: Choose one: each other player draws a card or each other player discards a card

	public AbilityCoveringFireController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public AbilityCoveringFireView getAbilityCoveringFireView(){
		return (AbilityCoveringFireView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.COVERING_FIRE_ABILITY){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				getAbilityCoveringFireView().acceptDecline(BUTTON_DRAW, BUTTON_DISCARD, STATE_COVERING_FIRE_ABILITY);
			}
		}
		else{
			if(getAbilityCoveringFireView().isShowing()){
				getAbilityCoveringFireView().closeModal();
			}
		}
	}
}