package client.popup.ability;

import client.popup.AbstractPopupView;

public class AbilityEnergyDrainView extends AbstractPopupView{
	//Red: You may KO a card from your hand or discard pile. If you do, you get +1 Buy Power

	private static final long serialVersionUID = -5201298631330557467L;

	public AbilityEnergyDrainView() {
		super(ENERGY_DRAIN_MESSAGE);
	}
}
