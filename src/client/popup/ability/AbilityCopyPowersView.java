package client.popup.ability;

import client.popup.AbstractPopupView;

public class AbilityCopyPowersView extends AbstractPopupView{
	//Play this card as a copy of another Hero you played this turn. This card is both Red and the color you copy

	private static final long serialVersionUID = 3590210133742632666L;

	public AbilityCopyPowersView() {
		super(COPY_POWERS_MESSAGE);
	}
}
