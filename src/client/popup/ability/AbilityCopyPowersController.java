package client.popup.ability;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class AbilityCopyPowersController extends Controller implements ClientVariables{
	//Play this card as a copy of another Hero you played this turn. This card is both Red and the color you copy

	public AbilityCopyPowersController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public AbilityCopyPowersView getAbilityCopyPowersView(){
		return (AbilityCopyPowersView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.COPY_POWERS_ABILITY){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				List<String> played = model.getPlayerCards().getPlayed();
				List<ClientCard> cards = new ArrayList<ClientCard>();
				for(String str : played){
					ClientCard c = ClientManager.getInstance().getBoard().getCard(str);
					if(c.getName().equals(COPY_POWERS)){
						continue;
					}
					cards.add(c);
				}
				List<Integer> indexes = new ArrayList<Integer>();
				for(int i = ZERO; i < cards.size(); ++i){
					indexes.add(NEGATIVE_ONE);
				}
				getAbilityCopyPowersView().update(cards, STATE_COPY_POWERS_ABILITY, false, indexes);
			}
		}
		else{
			if(getAbilityCopyPowersView().isShowing()){
				getAbilityCopyPowersView().closeModal();
			}
		}
	}
}