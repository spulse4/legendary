package client.popup.ability;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class AbilitySpinningCycloneController extends Controller implements ClientVariables{
	//You may move a Villain to a new city space. Rescue any Bystanders captured by that Villain.
	//(If you move a Villain to a city space that already has a Villain, swap them)

	public AbilitySpinningCycloneController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public AbilitySpinningCycloneView getAbilitySpinningCycloneView(){
		return (AbilitySpinningCycloneView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.SPINNING_CYCLONE_ABILITY){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				List<String> villians = new ArrayList<String>();
				List<Integer> bystanders = new ArrayList<Integer>();
				villians.add(ClientManager.getInstance().getModel().getCity().getBridge());
				villians.add(ClientManager.getInstance().getModel().getCity().getStreets());
				villians.add(ClientManager.getInstance().getModel().getCity().getRooftops());
				villians.add(ClientManager.getInstance().getModel().getCity().getBank());
				villians.add(ClientManager.getInstance().getModel().getCity().getSewers());
				List<String> temp = ClientManager.getInstance().getModel().getCity().getBridgeCaptured();
				int count = ZERO;
				for(String str : temp){
					if(str.equals(BYSTANDER)){
						++count;
					}
				}
				bystanders.add(count);
				temp = ClientManager.getInstance().getModel().getCity().getStreetsCaptured();
				count = ZERO;
				for(String str : temp){
					if(str.equals(BYSTANDER)){
						++count;
					}
				}
				bystanders.add(count);
				temp = ClientManager.getInstance().getModel().getCity().getRooftopsCaptured();
				count = ZERO;
				for(String str : temp){
					if(str.equals(BYSTANDER)){
						++count;
					}
				}
				bystanders.add(count);
				temp = ClientManager.getInstance().getModel().getCity().getBankCaptured();
				count = ZERO;
				for(String str : temp){
					if(str.equals(BYSTANDER)){
						++count;
					}
				}
				bystanders.add(count);
				temp = ClientManager.getInstance().getModel().getCity().getSewersCaptured();
				count = ZERO;
				for(String str : temp){
					if(str.equals(BYSTANDER)){
						++count;
					}
				}
				bystanders.add(count);
				getAbilitySpinningCycloneView().spinningCyclone(villians, bystanders);
			}
		}
		else{
			if(getAbilitySpinningCycloneView().isShowing()){
				getAbilitySpinningCycloneView().closeModal();
			}
		}
	}
}
