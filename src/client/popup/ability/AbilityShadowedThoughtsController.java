package client.popup.ability;

import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class AbilityShadowedThoughtsController extends Controller implements ClientVariables{
	//Red: You may play the top card of the Villain Deck. If you do, you get +2 Attack Power

	public AbilityShadowedThoughtsController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public AbilityShadowedThoughtsView getAbilityShadowedThoughtsView(){
		return (AbilityShadowedThoughtsView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.SHADOWED_THOUGHTS_ABILITY){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				getAbilityShadowedThoughtsView().acceptDecline(BUTTON_ACCEPT, BUTTON_DECLINE, STATE_SHADOWED_THOUGHTS_ABILITY);
			}
		}
		else{
			if(getAbilityShadowedThoughtsView().isShowing()){
				getAbilityShadowedThoughtsView().closeModal();
			}
		}
	}
}
