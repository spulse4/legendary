package client.popup.ability;

import client.popup.AbstractPopupView;

public class AbilitySilentSniperView extends AbstractPopupView{
	//Defeat a Villain or Mastermind that has a Bystander

	private static final long serialVersionUID = -4636715610359581377L;

	public AbilitySilentSniperView() {
		super(SILENT_SNIPER_MESSAGE);
	}
}
