package client.popup.ability;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class AbilityBattlefieldPromotionController extends Controller implements ClientVariables{
	//You may KO a SHIELD Hero from your hand or discard pile. If you do, you may gain a SHIELD Officer to your hand.

	public AbilityBattlefieldPromotionController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public AbilityBattlefieldPromotionView getAbilityBattlefieldPromotionView(){
		return (AbilityBattlefieldPromotionView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.BATTLEFIELD_PROMOTION_ABILITY){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				List<String> discard = model.getPlayerCards().getDiscard();
				List<String> hand = model.getPlayerCards().getHand();
				List<ClientCard> cards = new ArrayList<ClientCard>();
				for(String str : discard){
					ClientCard c = ClientManager.getInstance().getBoard().getCard(str);
					if(c.getCategory().equals(CATEGORY_SHIELD)){
						boolean found = false;
						for(ClientCard client : cards){
							if(client.getName().equals(c.getName())){
								found = true;
								break;
							}
						}
						if(!found){
							cards.add(c);
						}
					}
				}
				for(String str : hand){
					ClientCard c = ClientManager.getInstance().getBoard().getCard(str);
					if(c.getCategory().equals(CATEGORY_SHIELD)){
						boolean found = false;
						for(ClientCard client : cards){
							if(client.getName().equals(c.getName())){
								found = true;
								break;
							}
						}
						if(!found){
							cards.add(c);
						}
					}
				}
				List<Integer> indexes = new ArrayList<Integer>();
				for(int i = ZERO; i < cards.size(); ++i){
					indexes.add(NEGATIVE_ONE);
				}
				getAbilityBattlefieldPromotionView().update(cards, STATE_BATTLEFIELD_PROMOTION_ABILITY, true, indexes);
			}
		}
		else{
			if(getAbilityBattlefieldPromotionView().isShowing()){
				getAbilityBattlefieldPromotionView().closeModal();
			}
		}
	}
}
