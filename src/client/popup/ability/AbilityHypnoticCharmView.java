package client.popup.ability;

import client.popup.AbstractPopupView;

public class AbilityHypnoticCharmView extends AbstractPopupView{
	//Reveal the top card of your deck. Discard it or put it back.
	//Yellow: Do the same thing to each other player's deck

	private static final long serialVersionUID = -3587647008992724678L;

	public AbilityHypnoticCharmView() {
		super(HYPNOTIC_CHARM_MESSAGE);
	}
}
