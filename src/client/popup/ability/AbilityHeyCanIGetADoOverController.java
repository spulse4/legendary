package client.popup.ability;

import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class AbilityHeyCanIGetADoOverController extends Controller implements ClientVariables{
	//If this is the first Hero you played this turn, you may discard the rest of your hand and draw four cards

	public AbilityHeyCanIGetADoOverController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public AbilityHeyCanIGetADoOverView getAbilityHeyCanIGetADoOverView(){
		return (AbilityHeyCanIGetADoOverView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.HEY_CAN_I_GET_A_DO_OVER_ABILITY){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				getAbilityHeyCanIGetADoOverView().acceptDecline(BUTTON_ACCEPT, BUTTON_DECLINE, STATE_HEY_CAN_I_GET_A_DO_OVER_ABILITY);
			}
		}
		else{
			if(getAbilityHeyCanIGetADoOverView().isShowing()){
				getAbilityHeyCanIGetADoOverView().closeModal();
			}
		}
	}
}
