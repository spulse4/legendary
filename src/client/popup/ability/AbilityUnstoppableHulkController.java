package client.popup.ability;

import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class AbilityUnstoppableHulkController extends Controller implements ClientVariables{
	//You may KO a Wound from your hand or discard pile. If you do, you get +2 Attack Power

	public AbilityUnstoppableHulkController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public AbilityUnstoppableHulkView getAbilityUnstoppableHulkView(){
		return (AbilityUnstoppableHulkView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.UNSTOPPABLE_HULK_ABILITY){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				getAbilityUnstoppableHulkView().acceptDecline(BUTTON_ACCEPT, BUTTON_DECLINE, STATE_UNSTOPPABLE_HULK_ABILITY);
			}
		}
		else{
			if(getAbilityUnstoppableHulkView().isShowing()){
				getAbilityUnstoppableHulkView().closeModal();
			}
		}
	}
}
