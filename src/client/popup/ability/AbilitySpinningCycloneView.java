package client.popup.ability;

import client.popup.AbstractPopupView;

public class AbilitySpinningCycloneView extends AbstractPopupView{
	//You may move a Villain to a new city space. Rescue any Bystanders captured by that Villain.
	//(If you move a Villain to a city space that already has a Villain, swap them)

	private static final long serialVersionUID = -7240907956260234710L;

	public AbilitySpinningCycloneView() {
		super(SPINNING_CYCLONE_MESSAGE);
	}
}
