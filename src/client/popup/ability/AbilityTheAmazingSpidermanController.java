package client.popup.ability;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class AbilityTheAmazingSpidermanController extends Controller implements ClientVariables{
	//Reveal the top three cards of your deck. Put any that cost 2 or less into your hand. Put the rest back in any order

	public AbilityTheAmazingSpidermanController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public AbilityTheAmazingSpidermanView getAbilityTheAmazingSpidermanView(){
		return (AbilityTheAmazingSpidermanView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		int currentPlayer = model.getTurnTracker().getCurrentPlayer();
		int thisPlayer = ((PlayerHeader)ClientManager.getInstance().getUser()).getIndex();
		if(currentPlayer == thisPlayer && model.getTurnTracker().getState() == State.THE_AMAZING_SPIDERMAN_ABILITY){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			int size = effected.size();
			List<String> draw = model.getPlayerCardList().get(thisPlayer).getDraw();
			List<ClientCard> cards = new ArrayList<ClientCard>();
			for(int i = ZERO; i < size; ++i){
				cards.add(ClientManager.getInstance().getBoard().getCard(draw.get(i)));
			}
			List<Integer> indexes = new ArrayList<Integer>();
			for(int i = ZERO; i < cards.size(); ++i){
				indexes.add(NEGATIVE_ONE);
			}
			getAbilityTheAmazingSpidermanView().update(cards, STATE_THE_AMAZING_SPIDERMAN_ABILITY, false, indexes);
		}
		else{
			if(getAbilityTheAmazingSpidermanView().isShowing()){
				getAbilityTheAmazingSpidermanView().closeModal();
			}
		}
	}
}
