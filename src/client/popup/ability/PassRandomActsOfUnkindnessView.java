package client.popup.ability;

import client.popup.AbstractPopupView;

public class PassRandomActsOfUnkindnessView extends AbstractPopupView{
	//You may gain a Wound to your hand. Then each player passes a card from their hand to the player on their left

	private static final long serialVersionUID = -596610276727310225L;

	public PassRandomActsOfUnkindnessView() {
		super(RANDOM_ACTS_OF_UNKINDNESS_MESSAGE);
	}
}
