package client.popup.ability;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class AbilitySilentSniperController extends Controller implements ClientVariables{
	//Defeat a Villain or Mastermind that has a Bystander

	public AbilitySilentSniperController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public AbilitySilentSniperView getAbilitySilentSniperView(){
		return (AbilitySilentSniperView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.SILENT_SNIPER_ABILITY){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				List<ClientCard> cards = new ArrayList<ClientCard>();
				List<Integer> indexes = new ArrayList<Integer>();
				if(model.getCity().getSewersCaptured().contains(BYSTANDER)){
					cards.add(ClientManager.getInstance().getBoard().getCard(model.getCity().getSewers()));
					indexes.add(SEWERS_NUMBER);
				}
				if(model.getCity().getBankCaptured().contains(BYSTANDER)){
					cards.add(ClientManager.getInstance().getBoard().getCard(model.getCity().getBank()));
					indexes.add(BANK_NUMBER);
				}
				if(model.getCity().getRooftopsCaptured().contains(BYSTANDER)){
					cards.add(ClientManager.getInstance().getBoard().getCard(model.getCity().getRooftops()));
					indexes.add(ROOFTOPS_NUMBER);
				}
				if(model.getCity().getStreetsCaptured().contains(BYSTANDER)){
					cards.add(ClientManager.getInstance().getBoard().getCard(model.getCity().getStreets()));
					indexes.add(STREETS_NUMBER);
				}
				if(model.getCity().getBridgeCaptured().contains(BYSTANDER)){
					cards.add(ClientManager.getInstance().getBoard().getCard(model.getCity().getBridge()));
					indexes.add(BRIDGE_NUMBER);
				}
				if(model.getCity().getMastermindCaptured().contains(BYSTANDER)){
					cards.add(ClientManager.getInstance().getBoard().getCard(model.getTurnTracker().getMastermind()));
					indexes.add(MASTERMIND_NUMBER);
				}
				getAbilitySilentSniperView().update(cards, STATE_SILENT_SNIPER_ABILITY, false, indexes);
			}
		}
		else{
			if(getAbilitySilentSniperView().isShowing()){
				getAbilitySilentSniperView().closeModal();
			}
		}
	}
}
