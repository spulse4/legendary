package client.popup.ability;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class DiscardCoveringFireController extends Controller implements ClientVariables{
	//Black: Choose one: each other player draws a card or each other player discards a card

	public DiscardCoveringFireController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public DiscardCoveringFireView getDiscardCoveringFireView(){
		return (DiscardCoveringFireView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.COVERING_FIRE_DISCARD){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				List<String> hand = model.getPlayerCards().getHand();
				List<ClientCard> cards = new ArrayList<ClientCard>();
				for(String str : hand){
					cards.add(ClientManager.getInstance().getBoard().getCard(str));
				}
				List<Integer> indexes = new ArrayList<Integer>();
				for(int i = ZERO; i < cards.size(); ++i){
					indexes.add(NEGATIVE_ONE);
				}
				getDiscardCoveringFireView().update(cards, STATE_COVERING_FIRE_DISCARD, false, indexes);
			}
		}
		else{
			if(getDiscardCoveringFireView().isShowing()){
				getDiscardCoveringFireView().closeModal();
			}
		}
	}
}
