package client.popup.ability;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class AbilityPureFuryController extends Controller implements ClientVariables{
	//Defeat any Villain or Mastermind whose Hit Points is less than the number of SHIELD Heroes in the KO pile

	public AbilityPureFuryController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public AbilityPureFuryView getAbilityPureFuryView(){
		return (AbilityPureFuryView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.PURE_FURY_ABILITY){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				int shield = model.getKoPile().getShield();
				String sewers = model.getCity().getSewers();
				String bank = model.getCity().getBank();
				String rooftops = model.getCity().getRooftops();
				String streets = model.getCity().getStreets();
				String bridge = model.getCity().getBridge();
				String mastermind = model.getTurnTracker().getMastermind();
				List<ClientCard> cards = new ArrayList<ClientCard>();
				List<Integer> indexes = new ArrayList<Integer>();
				if(sewers != null){
					ClientCard c = ClientManager.getInstance().getBoard().getCard(sewers);
					if(c.getHitPoints() < shield){
						cards.add(c);
						indexes.add(SEWERS_NUMBER);
					}
				}
				if(bank != null){
					ClientCard c = ClientManager.getInstance().getBoard().getCard(bank);
					if(c.getHitPoints() < shield){
						cards.add(c);
						indexes.add(BANK_NUMBER);
					}
				}
				if(rooftops != null){
					ClientCard c = ClientManager.getInstance().getBoard().getCard(rooftops);
					if(c.getHitPoints() < shield){
						cards.add(c);
						indexes.add(ROOFTOPS_NUMBER);
					}
				}
				if(streets != null){
					ClientCard c = ClientManager.getInstance().getBoard().getCard(streets);
					if(c.getHitPoints() < shield){
						cards.add(c);
						indexes.add(STREETS_NUMBER);
					}
				}
				if(bridge != null){
					ClientCard c = ClientManager.getInstance().getBoard().getCard(bridge);
					if(c.getHitPoints() < shield){
						cards.add(c);
						indexes.add(BRIDGE_NUMBER);
					}
				}
				ClientCard c = ClientManager.getInstance().getBoard().getCard(mastermind);
				if(c.getHitPoints() < shield){
					cards.add(c);
					indexes.add(MASTERMIND_NUMBER);
				}
				getAbilityPureFuryView().update(cards, STATE_PURE_FURY_ABILITY, false, indexes);
			}
		}
		else{
			if(getAbilityPureFuryView().isShowing()){
				getAbilityPureFuryView().closeModal();
			}
		}
	}
}
