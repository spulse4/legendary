package client.popup.ability;

import client.popup.AbstractPopupView;

public class AbilityHereHoldThisForASecondView extends AbstractPopupView{
	//A Villain of your choice captures a Bystander

	private static final long serialVersionUID = 5198698811248400635L;

	public AbilityHereHoldThisForASecondView() {
		super(HERE_HOLD_THIS_FOR_A_SECOND_MESSAGE);
	}
}
