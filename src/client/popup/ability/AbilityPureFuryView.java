package client.popup.ability;

import client.popup.AbstractPopupView;

public class AbilityPureFuryView extends AbstractPopupView{
	//Defeat any Villain or Mastermind whose Hit Points is less than the number of SHIELD Heroes in the KO pile

	private static final long serialVersionUID = -3278748716754680388L;

	public AbilityPureFuryView() {
		super(PURE_FURY_MESSAGE);
	}
}
