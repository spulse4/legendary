package client.popup.ability;

import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class AbilityRandomActsOfUnkindnessController extends Controller implements ClientVariables{
	//You may gain a Wound to your hand. Then each player passes a card from their hand to the player on their left

	public AbilityRandomActsOfUnkindnessController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public AbilityRandomActsOfUnkindnessView getAbilityRandomActsOfUnkindnessView(){
		return (AbilityRandomActsOfUnkindnessView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.RANDOM_ACTS_OF_UNKINDNESS_ABILITY){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				getAbilityRandomActsOfUnkindnessView().acceptDecline(BUTTON_ACCEPT, BUTTON_DECLINE, STATE_RANDOM_ACTS_OF_UNKINDNESS_ABILITY);
			}
		}
		else{
			if(getAbilityRandomActsOfUnkindnessView().isShowing()){
				getAbilityRandomActsOfUnkindnessView().closeModal();
			}
		}
	}
}
