package client.popup.ability;

import client.popup.AbstractPopupView;

public class AbilityTheAmazingSpidermanView extends AbstractPopupView{
	//Reveal the top three cards of your deck. Put any that cost 2 or less into your hand. Put the rest back in any order

	private static final long serialVersionUID = 1399792399721754607L;

	public AbilityTheAmazingSpidermanView() {
		super(THE_AMAZING_SPIDERMAN_MESSAGE);
	}
}
