package client.popup.ability;

import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class AbilityHealingFactorController extends Controller implements ClientVariables{
	//You may KO a Wound from your hand or discard pile. If you do, draw a card

	public AbilityHealingFactorController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public AbilityHealingFactorView getAbilityHealingFactorView(){
		return (AbilityHealingFactorView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.HEALING_FACTOR_ABILITY){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				getAbilityHealingFactorView().acceptDecline(BUTTON_KO, BUTTON_DECLINE, STATE_HEALING_FACTOR_ABILITY);
			}
		}
		else{
			if(getAbilityHealingFactorView().isShowing()){
				getAbilityHealingFactorView().closeModal();
			}
		}
	}
}
