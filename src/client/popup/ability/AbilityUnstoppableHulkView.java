package client.popup.ability;

import client.popup.AbstractPopupView;

public class AbilityUnstoppableHulkView extends AbstractPopupView{
	//You may KO a Wound from your hand or discard pile. If you do, you get +2 Attack Power

	private static final long serialVersionUID = 4686763934591613893L;

	public AbilityUnstoppableHulkView() {
		super(UNSTOPPABLE_HULK_MESSAGE);
	}
}
