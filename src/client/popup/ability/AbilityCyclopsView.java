package client.popup.ability;

import client.popup.AbstractPopupView;

public class AbilityCyclopsView extends AbstractPopupView{
	//To play this card, you must discard a card from your hand

	private static final long serialVersionUID = 2459667719792251588L;

	public AbilityCyclopsView() {
		super(CYCLOPS_MESSAGE);
	}
}
