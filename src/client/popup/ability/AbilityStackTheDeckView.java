package client.popup.ability;

import client.popup.AbstractPopupView;

public class AbilityStackTheDeckView extends AbstractPopupView{
	//Draw two cards. Then put a card from your hand on top of your deck

	private static final long serialVersionUID = -3317736240885274054L;

	public AbilityStackTheDeckView() {
		super(STACK_THE_DECK_MESSAGE);
	}
}
