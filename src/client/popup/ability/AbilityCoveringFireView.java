package client.popup.ability;

import client.popup.AbstractPopupView;

public class AbilityCoveringFireView extends AbstractPopupView{
	//Black: Choose one: each other player draws a card or each other player discards a card

	private static final long serialVersionUID = 6960961258759274207L;

	public AbilityCoveringFireView() {
		super(COVERING_FIRE_MESSAGE);
	}
}
