package client.popup.ability;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class PassRandomActsOfUnkindnessController extends Controller implements ClientVariables{
	//You may gain a Wound to your hand. Then each player passes a card from their hand to the player on their left

	public PassRandomActsOfUnkindnessController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public PassRandomActsOfUnkindnessView getPassRandomActsOfUnkindnessView(){
		return (PassRandomActsOfUnkindnessView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.RANDOM_ACTS_OF_UNKINDNESS_PASS){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				List<String> hand = model.getPlayerCards().getHand();
				List<ClientCard> cards = new ArrayList<ClientCard>();
				for(String str : hand){
					cards.add(ClientManager.getInstance().getBoard().getCard(str));
				}
				List<Integer> indexes = new ArrayList<Integer>();
				for(int i = ZERO; i < cards.size(); ++i){
					indexes.add(NEGATIVE_ONE);
				}
				getPassRandomActsOfUnkindnessView().update(cards, STATE_RANDOM_ACTS_OF_UNKINDNESS_PASS, false, indexes);
			}
		}
		else{
			if(getPassRandomActsOfUnkindnessView().isShowing()){
				getPassRandomActsOfUnkindnessView().closeModal();
			}
		}
	}
}
