package client.popup.ability;

import client.popup.AbstractPopupView;

public class AbilityRandomActsOfUnkindnessView extends AbstractPopupView{
	//You may gain a Wound to your hand. Then each player passes a card from their hand to the player on their left

	private static final long serialVersionUID = -2020969450116340414L;

	public AbilityRandomActsOfUnkindnessView() {
		super(RANDOM_ACTS_OF_UNKINDNESS_MESSAGE);
	}
}
