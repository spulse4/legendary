package client.popup.ability;

import client.popup.AbstractPopupView;

public class AbilityBattlefieldPromotionView extends AbstractPopupView{
	//You may KO a SHIELD Hero from your hand or discard pile. If you do, you may gain a SHIELD Officer to your hand.

	private static final long serialVersionUID = 1L;

	public AbilityBattlefieldPromotionView() {
		super(BATTLEFIELD_PROMOTION_MESSAGE);
	}
}
