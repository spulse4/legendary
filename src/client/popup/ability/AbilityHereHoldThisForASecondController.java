package client.popup.ability;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class AbilityHereHoldThisForASecondController extends Controller implements ClientVariables{
	//A Villain of your choice captures a Bystander

	public AbilityHereHoldThisForASecondController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public AbilityHereHoldThisForASecondView getAbilityHereHoldThisForASecondView(){
		return (AbilityHereHoldThisForASecondView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.HERE_HOLD_THIS_FOR_A_SECOND_ABILITY){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				String sewers = model.getCity().getSewers();
				String bank = model.getCity().getBank();
				String rooftops = model.getCity().getRooftops();
				String streets = model.getCity().getStreets();
				String bridge = model.getCity().getBridge();
				List<ClientCard> cards = new ArrayList<ClientCard>();
				List<Integer> indexes = new ArrayList<Integer>();
				if(sewers != null){
					cards.add(ClientManager.getInstance().getBoard().getCard(sewers));
					indexes.add(SEWERS_NUMBER);
				}
				if(bank != null){
					cards.add(ClientManager.getInstance().getBoard().getCard(bank));
					indexes.add(BANK_NUMBER);
				}
				if(rooftops != null){
					cards.add(ClientManager.getInstance().getBoard().getCard(rooftops));
					indexes.add(ROOFTOPS_NUMBER);
				}
				if(streets != null){
					cards.add(ClientManager.getInstance().getBoard().getCard(streets));
					indexes.add(STREETS_NUMBER);
				}
				if(bridge != null){
					cards.add(ClientManager.getInstance().getBoard().getCard(bridge));
					indexes.add(BRIDGE_NUMBER);
				}
				cards.add(ClientManager.getInstance().getBoard().getCard(model.getTurnTracker().getMastermind()));
				indexes.add(MASTERMIND_NUMBER);
				getAbilityHereHoldThisForASecondView().update(cards, STATE_HERE_HOLD_THIS_FOR_A_SECOND_ABILITY, false, indexes);
			}
		}
		else{
			if(getAbilityHereHoldThisForASecondView().isShowing()){
				getAbilityHereHoldThisForASecondView().closeModal();
			}
		}
	}
}
