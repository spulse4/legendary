package client.popup.ability;

import client.popup.AbstractPopupView;

public class AbilityDangerousRescueView extends AbstractPopupView{
	//Red: You may KO a card from your hand or discard pile. If you do, rescue a Bystander.

	private static final long serialVersionUID = 7650345709952568560L;

	public AbilityDangerousRescueView() {
		super(DANGEROUS_RESCUE_MESSAGE);
	}
}
