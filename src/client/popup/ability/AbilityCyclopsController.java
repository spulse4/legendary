package client.popup.ability;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class AbilityCyclopsController extends Controller implements ClientVariables{
	//To play this card, you must discard a card from your hand

	public AbilityCyclopsController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public AbilityCyclopsView getAbilityCyclopsView(){
		return (AbilityCyclopsView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.CYCLOPS_ABILITY){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				List<String> hand = model.getPlayerCards().getHand();
				List<ClientCard> cards = new ArrayList<ClientCard>();
				for(String str : hand){
					cards.add(ClientManager.getInstance().getBoard().getCard(str));
				}
				List<Integer> indexes = new ArrayList<Integer>();
				for(int i = ZERO; i < cards.size(); ++i){
					indexes.add(NEGATIVE_ONE);
				}
				getAbilityCyclopsView().update(cards, STATE_CYCLOPS_ABILITY, false, indexes);
			}
		}
		else{
			if(getAbilityCyclopsView().isShowing()){
				getAbilityCyclopsView().closeModal();
			}
		}
	}
}

