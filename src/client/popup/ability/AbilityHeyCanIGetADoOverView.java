package client.popup.ability;

import client.popup.AbstractPopupView;

public class AbilityHeyCanIGetADoOverView extends AbstractPopupView{
	//If this is the first Hero you played this turn, you may discard the rest of your hand and draw four cards

	private static final long serialVersionUID = 2435831230416884723L;

	public AbilityHeyCanIGetADoOverView() {
		super(HEY_CAN_I_GET_A_DO_OVER_MESSAGE);
	}
}
