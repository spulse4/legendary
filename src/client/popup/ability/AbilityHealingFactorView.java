package client.popup.ability;

import client.popup.AbstractPopupView;

public class AbilityHealingFactorView extends AbstractPopupView{
	//You may KO a Wound from your hand or discard pile. If you do, draw a card

	private static final long serialVersionUID = -3691686195015244774L;

	public AbilityHealingFactorView() {
		super(HEALING_FACTOR_MESSAGE);
	}
}
