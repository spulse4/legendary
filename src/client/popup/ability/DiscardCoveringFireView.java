package client.popup.ability;

import client.popup.AbstractPopupView;

public class DiscardCoveringFireView extends AbstractPopupView{
	//Black: Choose one: each other player draws a card or each other player discards a card

	private static final long serialVersionUID = -5910970371900566601L;

	public DiscardCoveringFireView() {
		super(COVERING_FIRE_MESSAGE);
	}
}
