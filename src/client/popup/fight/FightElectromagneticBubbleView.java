package client.popup.fight;

import client.popup.AbstractPopupView;

public class FightElectromagneticBubbleView extends AbstractPopupView{
	//Choose one of your XMen Heroes. When you draw a new hand of cards at the end of this turn, add that Hero to your hand as a seventh card

	private static final long serialVersionUID = 1370211142865060417L;

	public FightElectromagneticBubbleView() {
		super(ELECTROMAGNETIC_BUBBLE_FIGHT_MESSAGE);
	}
}
