package client.popup.fight;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class FightMelterController extends Controller implements ClientVariables{
	//Each player reveals the top card of their deck. For each card, you choose to KO it or put it back

	public FightMelterController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public FightMelterView getFightMelterView(){
		return (FightMelterView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		int currentPlayer = model.getTurnTracker().getCurrentPlayer();
		int thisPlayer = ((PlayerHeader)ClientManager.getInstance().getUser()).getIndex();
		if(model.getTurnTracker().getState() == State.MELTER_FIGHT && currentPlayer == thisPlayer){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			String card = model.getPlayerCardList().get(effected.get(ZERO)).getDraw().get(ZERO);
			List<ClientCard> cards = new ArrayList<ClientCard>();
			List<Integer> indexes = new ArrayList<Integer>();
			cards.add(ClientManager.getInstance().getBoard().getCard(card));
			indexes.add(NEGATIVE_ONE);
			getFightMelterView().update(cards, STATE_MELTER_FIGHT, false, indexes);
		}
		else{
			if(getFightMelterView().isShowing()){
				getFightMelterView().closeModal();
			}
		}
	}
}
