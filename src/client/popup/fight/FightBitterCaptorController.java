package client.popup.fight;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class FightBitterCaptorController extends Controller implements ClientVariables{
	//Recruit an XMen Hero from the HQ for free

	public FightBitterCaptorController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public FightBitterCaptorView getFightBitterCaptorView(){
		return (FightBitterCaptorView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.BITTER_CAPTOR_FIGHT){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				ClientCard hq1 = ClientManager.getInstance().getBoard().getCard(model.getHeadQuarters().getHQ1());
				ClientCard hq2 = ClientManager.getInstance().getBoard().getCard(model.getHeadQuarters().getHQ2());
				ClientCard hq3 = ClientManager.getInstance().getBoard().getCard(model.getHeadQuarters().getHQ3());
				ClientCard hq4 = ClientManager.getInstance().getBoard().getCard(model.getHeadQuarters().getHQ4());
				ClientCard hq5 = ClientManager.getInstance().getBoard().getCard(model.getHeadQuarters().getHQ5());
				List<ClientCard> cards = new ArrayList<ClientCard>();
				List<Integer> indexes = new ArrayList<Integer>();
				if(hq1.getCategory().equals(CATEGORY_X_MEN)){
					cards.add(hq1);
					indexes.add(HQ_1_NUMBER);
				}
				if(hq2.getCategory().equals(CATEGORY_X_MEN)){
					cards.add(hq2);
					indexes.add(HQ_2_NUMBER);
				}
				if(hq3.getCategory().equals(CATEGORY_X_MEN)){
					cards.add(hq3);
					indexes.add(HQ_3_NUMBER);
				}
				if(hq4.getCategory().equals(CATEGORY_X_MEN)){
					cards.add(hq4);
					indexes.add(HQ_4_NUMBER);
				}
				if(hq5.getCategory().equals(CATEGORY_X_MEN)){
					cards.add(hq5);
					indexes.add(HQ_5_NUMBER);
				}
				getFightBitterCaptorView().update(cards, STATE_BITTER_CAPTOR_FIGHT, false, indexes);
			}
		}
		else{
			if(getFightBitterCaptorView().isShowing()){
				getFightBitterCaptorView().closeModal();
			}
		}
	}
}
