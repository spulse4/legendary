package client.popup.fight;

import client.popup.AbstractPopupView;

public class DiscardMonarchsDecreeView extends AbstractPopupView{
	//Choose one: each other player draws a card or each other player discards a card

	private static final long serialVersionUID = -9007787092769801836L;

	public DiscardMonarchsDecreeView() {
		super(MONARCHS_DECREE_FIGHT_MESSAGE);
	}
}
