package client.popup.fight;

import client.popup.AbstractPopupView;

public class FightManiacalTyrantView extends AbstractPopupView{
	//KO up to four cards from your discard pile

	private static final long serialVersionUID = 8669056730025916808L;

	public FightManiacalTyrantView() {
		super(MANIACAL_TYRANT_FIGHT_MESSAGE);
	}
}
