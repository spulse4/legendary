package client.popup.fight;

import client.popup.AbstractPopupView;

public class FightVanishingIllusionsView extends AbstractPopupView{
	//Each other player KOs a Villain from their Victory Pile

	private static final long serialVersionUID = 195540342040798084L;

	public FightVanishingIllusionsView() {
		super("Each other player KOs a Villain from their Victory Pile");
	}
}
