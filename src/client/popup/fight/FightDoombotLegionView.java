package client.popup.fight;

import client.popup.AbstractPopupView;

public class FightDoombotLegionView extends AbstractPopupView{
	//Look at the top two cards of your deck. KO one of them and put the other back

	private static final long serialVersionUID = 3093758631433502989L;

	public FightDoombotLegionView() {
		super(DOOMBOT_LEGION_FIGHT_MESSAGE);
	}
}
