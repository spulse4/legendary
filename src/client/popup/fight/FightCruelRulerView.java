package client.popup.fight;

import client.popup.AbstractPopupView;

public class FightCruelRulerView extends AbstractPopupView{
	//Defeat a Villain in the City for free

	private static final long serialVersionUID = 7526416708544086827L;

	public FightCruelRulerView() {
		super(CRUEL_RULER_FIGHT_MESSAGE);
	}
}
