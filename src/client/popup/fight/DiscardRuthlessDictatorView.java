package client.popup.fight;

import client.popup.AbstractPopupView;

public class DiscardRuthlessDictatorView extends AbstractPopupView{
	//Look at the top three cards of your deck. KO one, discard one and put one back on top of your deck

	private static final long serialVersionUID = -7579799108834815009L;

	public DiscardRuthlessDictatorView() {
		super(RUTHLESS_DICTATOR_FIGHT_MESSAGE);
	}
}
