package client.popup.fight;

import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class FightHydraKidnappersController extends Controller implements ClientVariables{
	//You may gain a SHIELD Officer

	public FightHydraKidnappersController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public FightHydraKidnappersView getFightHydraKidnappersView(){
		return (FightHydraKidnappersView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.HYDRA_KIDNAPPERS_FIGHT){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				getFightHydraKidnappersView().acceptDecline(BUTTON_ACCEPT, BUTTON_DECLINE, STATE_HYDRA_KIDNAPPERS_FIGHT);
			}
		}
		else{
			if(getFightHydraKidnappersView().isShowing()){
				getFightHydraKidnappersView().closeModal();
			}
		}
	}
}
