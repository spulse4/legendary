package client.popup.fight;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class FightVanishingIllusionsController extends Controller implements ClientVariables{
	//Each other player KOs a Villain from their Victory Pile

	public FightVanishingIllusionsController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public FightVanishingIllusionsView getFightVanishingIllusionsView(){
		return (FightVanishingIllusionsView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.VANISHING_ILLUSIONS_FIGHT){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				List<String> victoryPile = model.getPlayerCards().getVictoryPile();
				List<ClientCard> cards = new ArrayList<ClientCard>();
				for(String str : victoryPile){
					cards.add(ClientManager.getInstance().getBoard().getCard(str));
				}
				List<Integer> indexes = new ArrayList<Integer>();
				for(int i = ZERO; i < cards.size(); ++i){
					indexes.add(NEGATIVE_ONE);
				}
				getFightVanishingIllusionsView().update(cards, STATE_VANISHING_ILLUSIONS_FIGHT, false, indexes);
			}
		}
		else{
			if(getFightVanishingIllusionsView().isShowing()){
				getFightVanishingIllusionsView().closeModal();
			}
		}
	}
}
