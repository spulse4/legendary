package client.popup.fight;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class FightDoombotLegionController extends Controller implements ClientVariables{
	//Look at the top two cards of your deck. KO one of them and put the other back

	public FightDoombotLegionController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public FightDoombotLegionView getFightDoombotLegionView(){
		return (FightDoombotLegionView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.DOOMBOT_LEGION_FIGHT){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				List<String> draw = model.getPlayerCards().getDraw();
				List<ClientCard> cards = new ArrayList<ClientCard>();
				for(int i = ZERO; i < TWO; ++i){
					cards.add(ClientManager.getInstance().getBoard().getCard(draw.get(i)));
				}
				List<Integer> indexes = new ArrayList<Integer>();
				for(int i = ZERO; i < cards.size(); ++i){
					indexes.add(NEGATIVE_ONE);
				}
				getFightDoombotLegionView().update(cards, STATE_DOOMBOT_LEGION_FIGHT, false, indexes);
			}
		}
		else{
			if(getFightDoombotLegionView().isShowing()){
				getFightDoombotLegionView().closeModal();
			}
		}
	}
}
