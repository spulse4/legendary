package client.popup.fight;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class FightManiacalTyrantController extends Controller implements ClientVariables{
	//KO up to four cards from your discard pile

	public FightManiacalTyrantController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public FightManiacalTyrantView getFightManiacalTyrantView(){
		return (FightManiacalTyrantView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.MANIACAL_TYRANT_FIGHT){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				List<String> discard = model.getPlayerCards().getDiscard();
				List<ClientCard> cards = new ArrayList<ClientCard>();
				for(String str : discard){
					cards.add(ClientManager.getInstance().getBoard().getCard(str));
				}
				List<Integer> indexes = new ArrayList<Integer>();
				for(int i = ZERO; i < cards.size(); ++i){
					indexes.add(NEGATIVE_ONE);
				}
				getFightManiacalTyrantView().update(cards, STATE_MANIACAL_TYRANT_FIGHT, true, indexes);
			}
		}
		else{
			if(getFightManiacalTyrantView().isShowing()){
				getFightManiacalTyrantView().closeModal();
			}
		}
	}
}
