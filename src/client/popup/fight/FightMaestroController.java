package client.popup.fight;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class FightMaestroController extends Controller implements ClientVariables{
	//For each of your Green Heroes, KO one of your Heroes

	public FightMaestroController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public FightMaestroView getFightMaestroView(){
		return (FightMaestroView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.MAESTRO_FIGHT){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				List<String> discard = model.getPlayerCards().getDiscard();
				List<String> draw = model.getPlayerCards().getDraw();
				List<String> hand = model.getPlayerCards().getHand();
				List<String> played = model.getPlayerCards().getPlayed();
				List<ClientCard> cards = new ArrayList<ClientCard>();
				for(String str : discard){
					cards.add(ClientManager.getInstance().getBoard().getCard(str));
				}
				for(String str : draw){
					cards.add(ClientManager.getInstance().getBoard().getCard(str));
				}
				for(String str : hand){
					cards.add(ClientManager.getInstance().getBoard().getCard(str));
				}
				for(String str : played){
					cards.add(ClientManager.getInstance().getBoard().getCard(str));
				}
				List<Integer> indexes = new ArrayList<Integer>();
				for(int i = ZERO; i < cards.size(); ++i){
					indexes.add(NEGATIVE_ONE);
				}
				getFightMaestroView().update(cards, STATE_MAESTRO_FIGHT, false, indexes);
			}
		}
		else{
			if(getFightMaestroView().isShowing()){
				getFightMaestroView().closeModal();
			}
		}
	}
}
