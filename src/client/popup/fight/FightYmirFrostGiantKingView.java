package client.popup.fight;

import client.popup.AbstractPopupView;

public class FightYmirFrostGiantKingView extends AbstractPopupView{
	//Choose a player. That player KOs any number of Wounds from their hand and discard pile

	private static final long serialVersionUID = 7613673192704157168L;

	public FightYmirFrostGiantKingView() {
		super(YMIR_FROST_GIANT_KING_FIGHT_MESSAGE);
	}
}
