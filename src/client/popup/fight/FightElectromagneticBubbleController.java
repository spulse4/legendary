package client.popup.fight;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class FightElectromagneticBubbleController extends Controller implements ClientVariables{
	//Choose one of your XMen Heroes. When you draw a new hand of cards at the end of this turn, add that Hero to your hand as a seventh card

	public FightElectromagneticBubbleController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public FightElectromagneticBubbleView getFightElectromagneticBubbleView(){
		return (FightElectromagneticBubbleView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.ELECTROMAGNETIC_BUBBLE_FIGHT){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				List<String> discard = model.getPlayerCards().getDiscard();
				List<String> draw = model.getPlayerCards().getDraw();
				List<String> hand = model.getPlayerCards().getHand();
				List<String> played = model.getPlayerCards().getPlayed();
				List<ClientCard> cards = new ArrayList<ClientCard>();
				for(String str : discard){
					ClientCard c = ClientManager.getInstance().getBoard().getCard(str);
					if(c.getCategory().equals(CATEGORY_X_MEN)){
						cards.add(c);
					}
				}
				for(String str : draw){
					ClientCard c = ClientManager.getInstance().getBoard().getCard(str);
					if(c.getCategory().equals(CATEGORY_X_MEN)){
						cards.add(c);
					}
				}
				for(String str : hand){
					ClientCard c = ClientManager.getInstance().getBoard().getCard(str);
					if(c.getCategory().equals(CATEGORY_X_MEN)){
						cards.add(c);
					}
				}
				for(String str : played){
					ClientCard c = ClientManager.getInstance().getBoard().getCard(str);
					if(c.getCategory().equals(CATEGORY_X_MEN)){
						cards.add(c);
					}
				}
				List<Integer> indexes = new ArrayList<Integer>();
				for(int i = ZERO; i < cards.size(); ++i){
					indexes.add(NEGATIVE_ONE);
				}
				getFightElectromagneticBubbleView().update(cards, STATE_ELECTROMAGNETIC_BUBBLE_FIGHT, false, indexes);
			}
		}
		else{
			if(getFightElectromagneticBubbleView().isShowing()){
				getFightElectromagneticBubbleView().closeModal();
			}
		}
	}
}
