package client.popup.fight;

import client.popup.AbstractPopupView;

public class FightMaestroView extends AbstractPopupView{
	//For each of your Green Heroes, KO one of your Heroes

	private static final long serialVersionUID = -8573740927739084076L;

	public FightMaestroView() {
		super(MAESTRO_FIGHT_MESSAGE);
	}
}
