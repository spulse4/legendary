package client.popup.fight;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class FightDarkTechnologyController extends Controller implements ClientVariables{
	//You may recruit a Black or Blue Hero from the HQ for free

	public FightDarkTechnologyController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public FightDarkTechnologyView getFightDarkTechnologyView(){
		return (FightDarkTechnologyView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.DARK_TECHNOLOGY_FIGHT){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				ClientCard hq1 = ClientManager.getInstance().getBoard().getCard(model.getHeadQuarters().getHQ1());
				ClientCard hq2 = ClientManager.getInstance().getBoard().getCard(model.getHeadQuarters().getHQ2());
				ClientCard hq3 = ClientManager.getInstance().getBoard().getCard(model.getHeadQuarters().getHQ3());
				ClientCard hq4 = ClientManager.getInstance().getBoard().getCard(model.getHeadQuarters().getHQ4());
				ClientCard hq5 = ClientManager.getInstance().getBoard().getCard(model.getHeadQuarters().getHQ5());
				List<ClientCard> cards = new ArrayList<ClientCard>();
				List<Integer> indexes = new ArrayList<Integer>();
				if(hq1.getColors().contains(COLOR_BLACK) ||
				   hq1.getColors().contains(COLOR_BLUE)){
					cards.add(hq1);
					indexes.add(HQ_1_NUMBER);
				}
				if(hq2.getColors().contains(COLOR_BLACK) ||
				   hq2.getColors().contains(COLOR_BLUE)){
					cards.add(hq2);
					indexes.add(HQ_2_NUMBER);
				}
				if(hq3.getColors().contains(COLOR_BLACK) ||
				   hq3.getColors().contains(COLOR_BLUE)){
					cards.add(hq3);
					indexes.add(HQ_3_NUMBER);
				}
				if(hq4.getColors().contains(COLOR_BLACK) ||
				   hq4.getColors().contains(COLOR_BLUE)){
					cards.add(hq4);
					indexes.add(HQ_4_NUMBER);
				}
				if(hq5.getColors().contains(COLOR_BLACK) ||
				   hq5.getColors().contains(COLOR_BLUE)){
					cards.add(hq5);
					indexes.add(HQ_5_NUMBER);
				}
				getFightDarkTechnologyView().update(cards, STATE_DARK_TECHNOLOGY_FIGHT, false, indexes);
			}
		}
		else{
			if(getFightDarkTechnologyView().isShowing()){
				getFightDarkTechnologyView().closeModal();
			}
		}
	}
}
