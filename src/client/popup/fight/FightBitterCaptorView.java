package client.popup.fight;

import client.popup.AbstractPopupView;

public class FightBitterCaptorView extends AbstractPopupView{
	//Recruit an XMen Hero from the HQ for free

	private static final long serialVersionUID = -3947303072949434701L;

	public FightBitterCaptorView() {
		super(BITTER_CAPTOR_FIGHT_MESSAGE);
	}
}
