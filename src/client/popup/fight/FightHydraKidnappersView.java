package client.popup.fight;

import client.popup.AbstractPopupView;

public class FightHydraKidnappersView extends AbstractPopupView{
	//You may gain a SHIELD Officer

	private static final long serialVersionUID = -7306558513355994828L;

	public FightHydraKidnappersView() {
		super(HYDRA_KIDNAPPERS_FIGHT_MESSAGE);
	}
}
