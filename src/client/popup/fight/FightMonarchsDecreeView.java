package client.popup.fight;

import client.popup.AbstractPopupView;

public class FightMonarchsDecreeView extends AbstractPopupView{
	//Choose one: each other player draws a card or each other player discards a card

	private static final long serialVersionUID = -2333227695950627650L;

	public FightMonarchsDecreeView() {
		super(MONARCHS_DECREE_FIGHT_MESSAGE);
	}
}
