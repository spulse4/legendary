package client.popup.fight;

import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class DiscardYmirFrostGiantKingController extends Controller implements ClientVariables{
	//Choose a player. That player KOs any number of Wounds from their hand and discard pile

	public DiscardYmirFrostGiantKingController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public DiscardYmirFrostGiantKingView getDiscardYmirFrostGiantKingView(){
		return (DiscardYmirFrostGiantKingView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.YMIR_FROST_GIANT_KING_DISCARD){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				getDiscardYmirFrostGiantKingView().ymirFrostGiantKingDiscard();
			}
		}
		else{
			if(getDiscardYmirFrostGiantKingView().isShowing()){
				getDiscardYmirFrostGiantKingView().closeModal();
			}
		}
	}
}
