package client.popup.fight;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class FightSuperSkrullController extends Controller implements ClientVariables{
	//Each player KOs one of their Heroes

	public FightSuperSkrullController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public FightSuperSkrullView getFightSuperSkrullView(){
		return (FightSuperSkrullView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.SUPER_SKRULL_FIGHT){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				List<String> discard = model.getPlayerCards().getDiscard();
				List<String> draw = model.getPlayerCards().getDraw();
				List<String> hand = model.getPlayerCards().getHand();
				List<String> played = model.getPlayerCards().getPlayed();
				List<ClientCard> cards = new ArrayList<ClientCard>();
				for(String str : discard){
					cards.add(ClientManager.getInstance().getBoard().getCard(str));
				}
				for(String str : draw){
					cards.add(ClientManager.getInstance().getBoard().getCard(str));
				}
				for(String str : hand){
					cards.add(ClientManager.getInstance().getBoard().getCard(str));
				}
				for(String str : played){
					cards.add(ClientManager.getInstance().getBoard().getCard(str));
				}
				List<Integer> indexes = new ArrayList<Integer>();
				for(int i = ZERO; i < cards.size(); ++i){
					indexes.add(NEGATIVE_ONE);
				}
				getFightSuperSkrullView().update(cards, STATE_SUPER_SKRULL_FIGHT, false, indexes);
			}
		}
		else{
			if(getFightSuperSkrullView().isShowing()){
				getFightSuperSkrullView().closeModal();
			}
		}
	}
}
