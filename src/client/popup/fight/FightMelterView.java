package client.popup.fight;

import client.popup.AbstractPopupView;

public class FightMelterView extends AbstractPopupView{
	//Each player reveals the top card of their deck. For each card, you choose to KO it or put it back

	private static final long serialVersionUID = 8286517960467414645L;

	public FightMelterView() {
		super(MELTER_FIGHT_MESSAGE);
	}
}
