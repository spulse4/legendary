package client.popup.fight;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class FightWhirlwindController extends Controller implements ClientVariables{
	//If you fight Whirlwind on the Rooftops or Bridge, KO two of your Heroes

	public FightWhirlwindController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public FightWhirlwindView getFightWhirlwindView(){
		return (FightWhirlwindView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.WHIRLWIND_FIGHT){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				List<String> discard = model.getPlayerCards().getDiscard();
				List<String> draw = model.getPlayerCards().getDraw();
				List<String> hand = model.getPlayerCards().getHand();
				List<String> played = model.getPlayerCards().getPlayed();
				List<ClientCard> cards = new ArrayList<ClientCard>();
				for(String str : discard){
					if(str.equals(WOUND)){
						continue;
					}
					cards.add(ClientManager.getInstance().getBoard().getCard(str));
				}
				for(String str : draw){
					if(str.equals(WOUND)){
						continue;
					}
					cards.add(ClientManager.getInstance().getBoard().getCard(str));
				}
				for(String str : hand){
					if(str.equals(WOUND)){
						continue;
					}
					cards.add(ClientManager.getInstance().getBoard().getCard(str));
				}
				for(String str : played){
					if(str.equals(WOUND)){
						continue;
					}
					cards.add(ClientManager.getInstance().getBoard().getCard(str));
				}
				List<Integer> indexes = new ArrayList<Integer>();
				for(int i = ZERO; i < cards.size(); ++i){
					indexes.add(NEGATIVE_ONE);
				}
				getFightWhirlwindView().update(cards, STATE_WHIRLWIND_FIGHT, false, indexes);
			}
		}
		else{
			if(getFightWhirlwindView().isShowing()){
				getFightWhirlwindView().closeModal();
			}
		}
	}
}
