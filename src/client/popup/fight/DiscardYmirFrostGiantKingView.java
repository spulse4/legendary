package client.popup.fight;

import client.popup.AbstractPopupView;

public class DiscardYmirFrostGiantKingView extends AbstractPopupView{
	//Choose a player. That player KOs any number of Wounds from their hand and discard pile

	private static final long serialVersionUID = -4654736564101070688L;

	public DiscardYmirFrostGiantKingView() {
		super(YMIR_FROST_GIANT_KING_FIGHT_MESSAGE);
	}
}
