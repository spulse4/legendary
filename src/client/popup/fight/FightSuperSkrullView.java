package client.popup.fight;

import client.popup.AbstractPopupView;

public class FightSuperSkrullView extends AbstractPopupView{
	//Each player KOs one of their Heroes

	private static final long serialVersionUID = -7432779509009917583L;

	public FightSuperSkrullView() {
		super(SUPER_SKRULL_FIGHT_MESSAGE);
	}
}
