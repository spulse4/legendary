package client.popup.fight;

import client.popup.AbstractPopupView;

public class FightPaibokThePowerSkrullView extends AbstractPopupView{
	//Choose a hero in the HQ for each player. Each player gains that hero

	private static final long serialVersionUID = -5331992789927586420L;

	public FightPaibokThePowerSkrullView() {
		super(PAIBOK_THE_POWER_SKRULL_FIGHT_MESSAGE);
	}
}
