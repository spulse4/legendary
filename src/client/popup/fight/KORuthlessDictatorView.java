package client.popup.fight;

import client.popup.AbstractPopupView;

public class KORuthlessDictatorView extends AbstractPopupView{
	//Look at the top three cards of your deck. KO one, discard one and put one back on top of your deck

	private static final long serialVersionUID = 7008443073072194148L;

	public KORuthlessDictatorView() {
		super(RUTHLESS_DICTATOR_FIGHT_MESSAGE);
	}
}
