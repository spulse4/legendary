package client.popup.fight;

import client.popup.AbstractPopupView;

public class FightSentinelView extends AbstractPopupView{
	//KO one of your Heroes

	private static final long serialVersionUID = 3709598004062835243L;

	public FightSentinelView() {
		super(SENTINEL_FIGHT_MESSAGE);
	}
}
