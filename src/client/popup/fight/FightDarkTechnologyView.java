package client.popup.fight;

import client.popup.AbstractPopupView;

public class FightDarkTechnologyView extends AbstractPopupView{
	//You may recruit a Black or Blue Hero from the HQ for free

	private static final long serialVersionUID = -1415006877277320079L;

	public FightDarkTechnologyView() {
		super(DARK_TECHNOLOGY_FIGHT_MESSAGE);
	}
}
