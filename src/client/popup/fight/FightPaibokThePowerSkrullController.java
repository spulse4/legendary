package client.popup.fight;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class FightPaibokThePowerSkrullController extends Controller implements ClientVariables{
	//Choose a hero in the HQ for each player. Each player gains that hero

	public FightPaibokThePowerSkrullController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public FightPaibokThePowerSkrullView getFightPaibokThePowerSkrullView(){
		return (FightPaibokThePowerSkrullView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.PAIBOK_THE_POWER_SKRULL_FIGHT){
			int currentPlayer = model.getTurnTracker().getCurrentPlayer();
			int thisPlayer = ((PlayerHeader)ClientManager.getInstance().getUser()).getIndex();
			if(currentPlayer == thisPlayer){
				ClientCard hq1 = ClientManager.getInstance().getBoard().getCard(model.getHeadQuarters().getHQ1());
				ClientCard hq2 = ClientManager.getInstance().getBoard().getCard(model.getHeadQuarters().getHQ2());
				ClientCard hq3 = ClientManager.getInstance().getBoard().getCard(model.getHeadQuarters().getHQ3());
				ClientCard hq4 = ClientManager.getInstance().getBoard().getCard(model.getHeadQuarters().getHQ4());
				ClientCard hq5 = ClientManager.getInstance().getBoard().getCard(model.getHeadQuarters().getHQ5());
				List<ClientCard> cards = new ArrayList<ClientCard>();
				List<Integer> indexes = new ArrayList<Integer>();
				cards.add(hq1);
				indexes.add(HQ_1_NUMBER);
				cards.add(hq2);
				indexes.add(HQ_2_NUMBER);
				cards.add(hq3);
				indexes.add(HQ_3_NUMBER);
				cards.add(hq4);
				indexes.add(HQ_4_NUMBER);
				cards.add(hq5);
				indexes.add(HQ_5_NUMBER);
				getFightPaibokThePowerSkrullView().update(cards, STATE_PAIBOK_THE_POWER_SKRULL_FIGHT, false, indexes);
			}
		}
		else{
			if(getFightPaibokThePowerSkrullView().isShowing()){
				getFightPaibokThePowerSkrullView().closeModal();
			}
		}
	}
}
