package client.popup.fight;

import client.popup.AbstractPopupView;

public class FightWhirlwindView extends AbstractPopupView{
	//If you fight Whirlwind on the Rooftops or Bridge, KO two of your Heroes

	private static final long serialVersionUID = -6402882696057352796L;

	public FightWhirlwindView() {
		super(WHIRLWIND_FIGHT_MESSAGE);
	}
}
