package client.popup.fight;

import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class FightMonarchsDecreeController extends Controller implements ClientVariables{
	//Choose one: each other player draws a card or each other player discards a card

	public FightMonarchsDecreeController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public FightMonarchsDecreeView getFightMonarchsDecreeView(){
		return (FightMonarchsDecreeView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.MONARCHS_DECREE_FIGHT){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				getFightMonarchsDecreeView().acceptDecline(BUTTON_DRAW, BUTTON_DISCARD, STATE_MONARCHS_DECREE_FIGHT);
			}
		}
		else{
			if(getFightMonarchsDecreeView().isShowing()){
				getFightMonarchsDecreeView().closeModal();
			}
		}
	}
}
