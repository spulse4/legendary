package client.popup.fight;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class DiscardRuthlessDictatorController extends Controller implements ClientVariables{
	//Look at the top three cards of your deck. KO one, discard one and put one back on top of your deck

	public DiscardRuthlessDictatorController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public DiscardRuthlessDictatorView getDiscardRuthlessDictatorView(){
		return (DiscardRuthlessDictatorView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.RUTHLESS_DICTATOR_DISCARD){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				List<String> draw = model.getPlayerCards().getDraw();
				List<ClientCard> cards = new ArrayList<ClientCard>();
				List<Integer> indexes = new ArrayList<Integer>();
				for(int i = ZERO; i < TWO; ++i){
					cards.add(ClientManager.getInstance().getBoard().getCard(draw.get(i)));
					indexes.add(i);
				}
				getDiscardRuthlessDictatorView().update(cards, STATE_RUTHLESS_DICTATOR_DISCARD, false, indexes);
			}
		}
		else{
			if(getDiscardRuthlessDictatorView().isShowing()){
				getDiscardRuthlessDictatorView().closeModal();
			}
		}
	}
}
