package client.popup.fight;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class FightCruelRulerController extends Controller implements ClientVariables{
	//Defeat a Villain in the City for free

	public FightCruelRulerController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public FightCruelRulerView getFightCruelRulerView(){
		return (FightCruelRulerView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.CRUEL_RULER_FIGHT){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				String sewers = model.getCity().getSewers();
				String bank = model.getCity().getBank();
				String rooftops = model.getCity().getRooftops();
				String streets = model.getCity().getStreets();
				String bridge = model.getCity().getBridge();
				List<ClientCard> cards = new ArrayList<ClientCard>();
				List<Integer> indexes = new ArrayList<Integer>();
				if(sewers != null){
					cards.add(ClientManager.getInstance().getBoard().getCard(sewers));
					indexes.add(SEWERS_NUMBER);
				}
				if(bank != null){
					cards.add(ClientManager.getInstance().getBoard().getCard(bank));
					indexes.add(BANK_NUMBER);
				}
				if(rooftops != null){
					cards.add(ClientManager.getInstance().getBoard().getCard(rooftops));
					indexes.add(ROOFTOPS_NUMBER);
				}
				if(streets != null){
					cards.add(ClientManager.getInstance().getBoard().getCard(streets));
					indexes.add(STREETS_NUMBER);
				}
				if(bridge != null){
					cards.add(ClientManager.getInstance().getBoard().getCard(bridge));
					indexes.add(BRIDGE_NUMBER);
				}
				getFightCruelRulerView().update(cards, STATE_CRUEL_RULER_FIGHT, false, indexes);
			}
		}
		else{
			if(getFightCruelRulerView().isShowing()){
				getFightCruelRulerView().closeModal();
			}
		}
	}
}
