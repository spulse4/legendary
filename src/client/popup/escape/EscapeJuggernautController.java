package client.popup.escape;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class EscapeJuggernautController extends Controller implements ClientVariables{
	//Each player KOs two Heroes from their hand

	public EscapeJuggernautController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public EscapeJuggernautView getEscapeJuggernautView(){
		return (EscapeJuggernautView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.JUGGERNAUT_ESCAPE){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				List<String> hand = model.getPlayerCards().getHand();
				List<ClientCard> cards = new ArrayList<ClientCard>();
				for(String str : hand){
					cards.add(ClientManager.getInstance().getBoard().getCard(str));
				}
				List<Integer> indexes = new ArrayList<Integer>();
				for(int i = ZERO; i < cards.size(); ++i){
					indexes.add(NEGATIVE_ONE);
				}
				getEscapeJuggernautView().update(cards, STATE_JUGGERNAUT_ESCAPE, false, indexes);
			}
		}
		else{
			if(getEscapeJuggernautView().isShowing()){
				getEscapeJuggernautView().closeModal();
			}
		}
	}
}
