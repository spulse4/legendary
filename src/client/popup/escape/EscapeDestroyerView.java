package client.popup.escape;

import client.popup.AbstractPopupView;

public class EscapeDestroyerView extends AbstractPopupView{
	//Each player KOs two of their Heroes

	private static final long serialVersionUID = -1025066802926268851L;

	public EscapeDestroyerView() {
		super(DESTROYER_ESCAPE_MESSAGE);
	}
}
