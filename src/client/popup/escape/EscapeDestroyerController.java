package client.popup.escape;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class EscapeDestroyerController extends Controller implements ClientVariables{
	//Each player KOs two of their Heroes

	public EscapeDestroyerController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public EscapeDestroyerView getEscapeDestroyerView(){
		return (EscapeDestroyerView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.DESTROYER_ESCAPE){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				List<String> discard = model.getPlayerCards().getDiscard();
				List<String> draw = model.getPlayerCards().getDraw();
				List<String> hand = model.getPlayerCards().getHand();
				List<String> played = model.getPlayerCards().getPlayed();
				List<ClientCard> cards = new ArrayList<ClientCard>();
				for(String str : discard){
					ClientCard card = ClientManager.getInstance().getBoard().getCard(str);
					if(card.getName().equals(WOUND)){
						continue;
					}
					cards.add(card);
				}
				for(String str : draw){
					ClientCard card = ClientManager.getInstance().getBoard().getCard(str);
					if(card.getName().equals(WOUND)){
						continue;
					}
					cards.add(card);
				}
				for(String str : hand){
					ClientCard card = ClientManager.getInstance().getBoard().getCard(str);
					if(card.getName().equals(WOUND)){
						continue;
					}
					cards.add(card);
				}
				for(String str : played){
					ClientCard card = ClientManager.getInstance().getBoard().getCard(str);
					if(card.getName().equals(WOUND)){
						continue;
					}
					cards.add(card);
				}
				List<Integer> indexes = new ArrayList<Integer>();
				for(int i = ZERO; i < cards.size(); ++i){
					indexes.add(NEGATIVE_ONE);
				}
				getEscapeDestroyerView().update(cards, STATE_DESTROYER_ESCAPE, false, indexes);
			}
		}
		else{
			if(getEscapeDestroyerView().isShowing()){
				getEscapeDestroyerView().closeModal();
			}
		}
	}
}
