package client.popup.escape;

import client.popup.AbstractPopupView;

public class EscapeJuggernautView extends AbstractPopupView{
	//Each player KOs two Heroes from their hand

	private static final long serialVersionUID = -6394257881250689841L;

	public EscapeJuggernautView() {
		super(JUGGERNAUT_ESCAPE_MESSAGE);
	}
}
