package client.popup;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.hand.PlayerCardPanel;
import client.main.ClientManager;
import client.popup.ability.AbilitySpinningCycloneView;
import shared.definitions.PlayerHeader;
import shared.model.PlayerCards;

public class AbstractPopupView extends AbstractPopupFunctions implements ClientVariables{

	private static final long serialVersionUID = 3517151486452311412L;
	
	private int spinningCycloneFrom = NEGATIVE_ONE;
	private int spinningCycloneTo = NEGATIVE_ONE;
	private AbstractPopupView view;
	protected List<JLabel> labels;
	protected JPanel labelPanel;
	protected JScrollPane scroll1;

	public AbstractPopupView(String messagesString){
		int multiplier = ONE;
		if(this.getClass() == AbilitySpinningCycloneView.class){
			multiplier = TWO;
		}
		view = this;
		List<String> messages = new ArrayList<String>();
		
		StringBuilder str = new StringBuilder();
		FontMetrics metric = this.getFontMetrics(FONT_PLAIN_12);

		Scanner scanner = new Scanner(messagesString);

		while(scanner.hasNext()){
			String current = str.toString();
			String next = scanner.next();
			if(current.length() == ZERO){
				str.append(next);
			}
			else{
				str.append(SPACE + next);
			}
			
			if(metric.stringWidth(str.toString()) > (POPUP_TEXT_WIDTH * multiplier)){
				messages.add(current);
				str.setLength(ZERO);
				str.append(next);
			}
		}
		scanner.close();

		messages.add(str.toString());
		
        this.setOpaque(true);
        this.setLayout(new BorderLayout());
        this.setBorder(BorderFactory.createLineBorder(Color.black, VIEW_BORDER_WIDTH));
        
		scroll1 = new JScrollPane();
		scroll1.setPreferredSize(new Dimension(ZERO, POPUP_HEIGHT));
		scroll1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scroll1.getVerticalScrollBar().setUnitIncrement(SCROLL_SPEED);
		
		labelPanel = new JPanel();
		labelPanel.setLayout(new BoxLayout(labelPanel, BoxLayout.Y_AXIS));

		for(String message : messages){
			JLabel label = new JLabel(message);
			label.setFont(FONT_PLAIN_12);
			label.setAlignmentX(Component.CENTER_ALIGNMENT);
			labelPanel.add(label);
		}
		this.add(labelPanel, BorderLayout.NORTH);
		this.add(scroll1, BorderLayout.CENTER);
	}
	
	public void update(List<ClientCard> cards, String function, boolean declineable, List<Integer> indexes){
		if(this.isModalShowing()){
			this.closeModal();
		}
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		panel.setPreferredSize(new Dimension(ZERO, HAND_INITIAL_HEIGHT_NOT_PLAYED + (HAND_HEIGHT_PER_CARD * cards.size())));
		for(int i = ZERO; i < cards.size(); ++i){
			panel.add(new PlayerCardPanel(cards.get(i), null, function, indexes.get(i)));
			panel.add(Box.createRigidArea(new Dimension(ZERO, CARD_RIGID_AREA)));
		}
		scroll1 = new JScrollPane(panel);
		scroll1.setPreferredSize(new Dimension(ZERO, POPUP_HEIGHT));
		scroll1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scroll1.getVerticalScrollBar().setUnitIncrement(SCROLL_SPEED);
		JPanel declinePanel = new JPanel();
		JButton decline = getButtonForClose(function);
		declinePanel.add(decline);
		
		this.setPreferredSize(new Dimension(POPUP_HEIGHT, POPUP_HEIGHT));
		
		this.removeAll();
		this.add(labelPanel, BorderLayout.NORTH);
		this.add(scroll1, BorderLayout.CENTER);
		if(declineable){
			this.add(declinePanel, BorderLayout.SOUTH);
			this.setPreferredSize(new Dimension(POPUP_HEIGHT, POPUP_HEIGHT + POPUP_DECLINE_HEIGHT));
		}
		this.showModal();
	}
	
	public void acceptDecline(String message1, String message2, String function){
		if(this.isModalShowing()){
			this.closeModal();
		}
		JPanel panel = new JPanel();
		
		panel.setPreferredSize(new Dimension(ZERO, POPUP_ACCEPT_HEIGHT / FOUR));
		
		JButton accept = getButton(message1, function, true);
		accept.setFont(FONT_PLAIN_20);
		accept.setPreferredSize(new Dimension(BUTTON_2_LENGTH, BUTTON_HEIGHT * TWO));
		JButton decline = getButton(message2, function, false);
		decline.setFont(FONT_PLAIN_20);
		decline.setPreferredSize(new Dimension(BUTTON_2_LENGTH, BUTTON_HEIGHT * TWO));
		
		panel.add(accept);
		panel.add(decline);
		
		scroll1 = new JScrollPane(panel);
		scroll1.setPreferredSize(new Dimension(ZERO, POPUP_HEIGHT));
		scroll1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scroll1.getVerticalScrollBar().setUnitIncrement(SCROLL_SPEED);
		int difference = ZERO;
		if(scroll1.getComponentCount() == ONE){
			difference = NEGATIVE_ONE * FIFTEEN;
		}
		else if(scroll1.getComponentCount() == THREE){
			difference = FIFTEEN;
		}
		this.setPreferredSize(new Dimension(POPUP_WIDTH, (POPUP_HEIGHT / THREE) + difference));
		
		this.removeAll();
		this.add(labelPanel, BorderLayout.NORTH);
		this.add(scroll1, BorderLayout.CENTER);
		this.showModal();	
	}
	
	public void spinningCyclone(List<String> villians, List<Integer> bystanders){
		
		JPanel upper = new JPanel();
		JPanel lower = new JPanel();
		JPanel submitPanel = new JPanel();
		JPanel buttons = new JPanel();
		buttons.setLayout(new BoxLayout(buttons, BoxLayout.Y_AXIS));
		
		final JButton bridgeFrom;
		final JButton streetsFrom;
		final JButton rooftopsFrom;
		final JButton bankFrom;
		final JButton sewersFrom;
		
		if(villians.get(ZERO) != null){
			bridgeFrom = new JButton(HTML_BODY + HTML_H3 + BRIDGE + COLON + SPACE + villians.get(ZERO) + HTML_BR + 
				 BYSTANDER + COLON + SPACE + bystanders.get(ZERO) + HTML_H3_CLOSE + HTML_BODY_CLOSE);
		}
		else{
			bridgeFrom = null;
		}
		if(villians.get(ONE) != null){
			streetsFrom = new JButton(HTML_BODY + HTML_H3 + STREETS + COLON + SPACE + villians.get(ONE) + HTML_BR + 
				 BYSTANDER + COLON + SPACE + bystanders.get(ONE) + HTML_H3_CLOSE + HTML_BODY_CLOSE);
		}
		else{
			streetsFrom = null;
		}
		if(villians.get(TWO) != null){
			rooftopsFrom = new JButton(HTML_BODY + HTML_H3 + ROOFTOPS + COLON + SPACE + villians.get(TWO) + HTML_BR + 
				 BYSTANDER + COLON + SPACE + bystanders.get(TWO) + HTML_H3_CLOSE + HTML_BODY_CLOSE);
		}
		else{
			rooftopsFrom = null;
		}
		if(villians.get(THREE) != null){
			bankFrom = new JButton(HTML_BODY + HTML_H3 + BANK + COLON + SPACE + villians.get(THREE) + HTML_BR + 
				 BYSTANDER + COLON + SPACE + bystanders.get(THREE) + HTML_H3_CLOSE + HTML_BODY_CLOSE);
		}
		else{
			bankFrom = null;
		}
		if(villians.get(FOUR) != null){
			sewersFrom = new JButton(HTML_BODY + HTML_H3 + SEWERS + COLON + SPACE + villians.get(FOUR) + HTML_BR + 
				 BYSTANDER + COLON + SPACE + bystanders.get(FOUR) + HTML_H3_CLOSE + HTML_BODY_CLOSE);
		}
		else{
			sewersFrom = null;
		}

		final JButton bridgeTo;
		final JButton streetsTo;
		final JButton rooftopsTo;
		final JButton bankTo;
		final JButton sewersTo;
		
		if(villians.get(ZERO) != null){
			bridgeTo = new JButton(HTML_BODY + HTML_H3 + BRIDGE + COLON + SPACE + villians.get(ZERO) + HTML_H3_CLOSE + HTML_BODY_CLOSE);
		}
		else{
			bridgeTo = new JButton(HTML_BODY + HTML_H3 + BRIDGE + COLON + SPACE + EMPTY_STRING + HTML_H3_CLOSE + HTML_BODY_CLOSE);
		}
		if(villians.get(ONE) != null){
			streetsTo = new JButton(HTML_BODY + HTML_H3 + STREETS + COLON + SPACE + villians.get(ONE) + HTML_H3_CLOSE + HTML_BODY_CLOSE);
		}
		else{
			streetsTo = new JButton(HTML_BODY + HTML_H3 + BRIDGE + COLON + SPACE + EMPTY_STRING + HTML_H3_CLOSE + HTML_BODY_CLOSE);
		}
		if(villians.get(TWO) != null){
			rooftopsTo = new JButton(HTML_BODY + HTML_H3 + ROOFTOPS + COLON + SPACE + villians.get(TWO) + HTML_H3_CLOSE + HTML_BODY_CLOSE);
		}
		else{
			rooftopsTo = new JButton(HTML_BODY + HTML_H3 + BRIDGE + COLON + SPACE + EMPTY_STRING + HTML_H3_CLOSE + HTML_BODY_CLOSE);
		}
		if(villians.get(THREE) != null){
			bankTo = new JButton(HTML_BODY + HTML_H3 + BANK + COLON + SPACE + villians.get(THREE) + HTML_H3_CLOSE + HTML_BODY_CLOSE);
		}
		else{
			bankTo = new JButton(HTML_BODY + HTML_H3 + BRIDGE + COLON + SPACE + EMPTY_STRING + HTML_H3_CLOSE + HTML_BODY_CLOSE);
		}
		if(villians.get(FOUR) != null){
			sewersTo = new JButton(HTML_BODY + HTML_H3 + SEWERS + COLON + SPACE + villians.get(FOUR) + HTML_H3_CLOSE + HTML_BODY_CLOSE);
		}
		else{
			sewersTo = new JButton(HTML_BODY + HTML_H3 + BRIDGE + COLON + SPACE + EMPTY_STRING + HTML_H3_CLOSE + HTML_BODY_CLOSE);
		}

		final JButton submit = new JButton(HTML_BODY + HTML_H3 + SUBMIT + HTML_H3_CLOSE + HTML_BODY_CLOSE);
		submit.setEnabled(false);
		
		ActionListener action = new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int count = ZERO;
				if(bridgeFrom != null){
					++count;
				}
				if(streetsFrom != null){
					++count;
				}
				if(rooftopsFrom != null){
					++count;
				}
				if(bankFrom != null){
					++count;
				}
				if(sewersFrom != null){
					++count;
				}
				if(count == ONE){
					if(bridgeFrom != null){
						spinningCycloneFrom = BRIDGE_NUMBER;
					}
					else if(streetsFrom != null){
						spinningCycloneFrom = STREETS_NUMBER;
					}
					else if(rooftopsFrom != null){
						spinningCycloneFrom = ROOFTOPS_NUMBER;
					}
					else if(bankFrom != null){
						spinningCycloneFrom = BANK_NUMBER;
					}
					else{
						spinningCycloneFrom = SEWERS_NUMBER;
					}
				}
				else{
					if(arg0.getSource() == bridgeFrom){
						if(spinningCycloneFrom == NEGATIVE_ONE){
							spinningCycloneFrom = BRIDGE_NUMBER;
						}
						else{
							spinningCycloneFrom = NEGATIVE_ONE;
						}
						toggleButton(streetsFrom);
						toggleButton(rooftopsFrom);
						toggleButton(bankFrom);
						toggleButton(sewersFrom);
					}
					else if(arg0.getSource() == streetsFrom){
						if(spinningCycloneFrom == NEGATIVE_ONE){
							spinningCycloneFrom = STREETS_NUMBER;
						}
						else{
							spinningCycloneFrom = NEGATIVE_ONE;
						}
						toggleButton(bridgeFrom);
						toggleButton(rooftopsFrom);
						toggleButton(bankFrom);
						toggleButton(sewersFrom);
					}
					else if(arg0.getSource() == rooftopsFrom){
						if(spinningCycloneFrom == NEGATIVE_ONE){
							spinningCycloneFrom = ROOFTOPS_NUMBER;
						}
						else{
							spinningCycloneFrom = NEGATIVE_ONE;
						}
						toggleButton(bridgeFrom);
						toggleButton(streetsFrom);
						toggleButton(bankFrom);
						toggleButton(sewersFrom);
					}
					else if(arg0.getSource() == bankFrom){
						if(spinningCycloneFrom == NEGATIVE_ONE){
							spinningCycloneFrom = BANK_NUMBER;
						}
						else{
							spinningCycloneFrom = NEGATIVE_ONE;
						}
						toggleButton(bridgeFrom);
						toggleButton(streetsFrom);
						toggleButton(rooftopsFrom);
						toggleButton(sewersFrom);
					}
					else if(arg0.getSource() == sewersFrom){
						if(spinningCycloneFrom == NEGATIVE_ONE){
							spinningCycloneFrom = SEWERS_NUMBER;
						}
						else{
							spinningCycloneFrom = NEGATIVE_ONE;
						}
						toggleButton(bridgeFrom);
						toggleButton(streetsFrom);
						toggleButton(rooftopsFrom);
						toggleButton(bankFrom);
					}
				}
				if(arg0.getSource() == bridgeTo){
					if(spinningCycloneTo == NEGATIVE_ONE){
						spinningCycloneTo = BRIDGE_NUMBER;
					}
					else{
						spinningCycloneTo = NEGATIVE_ONE;
					}
					toggleButton(streetsTo);
					toggleButton(rooftopsTo);
					toggleButton(bankTo);
					toggleButton(sewersTo);
				}
				else if(arg0.getSource() == streetsTo){
					if(spinningCycloneTo == NEGATIVE_ONE){
						spinningCycloneTo = STREETS_NUMBER;
					}
					else{
						spinningCycloneTo = NEGATIVE_ONE;
					}
					toggleButton(bridgeTo);
					toggleButton(rooftopsTo);
					toggleButton(bankTo);
					toggleButton(sewersTo);
				}
				else if(arg0.getSource() == rooftopsTo){
					if(spinningCycloneTo == NEGATIVE_ONE){
						spinningCycloneTo = ROOFTOPS_NUMBER;
					}
					else{
						spinningCycloneTo = NEGATIVE_ONE;
					}
					toggleButton(bridgeTo);
					toggleButton(streetsTo);
					toggleButton(bankTo);
					toggleButton(sewersTo);
				}
				else if(arg0.getSource() == bankTo){
					if(spinningCycloneTo == NEGATIVE_ONE){
						spinningCycloneTo = BANK_NUMBER;
					}
					else{
						spinningCycloneTo = NEGATIVE_ONE;
					}
					toggleButton(bridgeTo);
					toggleButton(streetsTo);
					toggleButton(rooftopsTo);
					toggleButton(sewersTo);
				}
				else if(arg0.getSource() == sewersTo){
					if(spinningCycloneTo == NEGATIVE_ONE){
						spinningCycloneTo = SEWERS_NUMBER;
					}
					else{
						spinningCycloneTo = NEGATIVE_ONE;
					}
					toggleButton(bridgeTo);
					toggleButton(streetsTo);
					toggleButton(rooftopsTo);
					toggleButton(bankTo);
				}
				else if(arg0.getSource() == submit){
					int gameID = ClientManager.getInstance().getModel().getGameID();
					ClientManager.getInstance().getServer().spinningCycloneAbility(gameID, spinningCycloneFrom, spinningCycloneTo);
					spinningCycloneFrom = NEGATIVE_ONE;
					spinningCycloneTo = NEGATIVE_ONE;
					view.closeModal();
				}
				if(spinningCycloneFrom != NEGATIVE_ONE && spinningCycloneTo != NEGATIVE_ONE){
					submit.setEnabled(true);
				}
				else{
					submit.setEnabled(false);
				}
			}
		};			
		
		if(bridgeFrom != null){
			bridgeFrom.addActionListener(action);
			upper.add(bridgeFrom);
		}
		if(streetsFrom != null){
			streetsFrom.addActionListener(action);
			upper.add(streetsFrom);
		}
		if(rooftopsFrom != null){
			rooftopsFrom.addActionListener(action);
			upper.add(rooftopsFrom);
		}
		if(bankFrom != null){
			bankFrom.addActionListener(action);
			upper.add(bankFrom);
		}
		if(sewersFrom != null){
			sewersFrom.addActionListener(action);
			upper.add(sewersFrom);
		}
		if(bridgeTo != null){
			bridgeTo.addActionListener(action);
			lower.add(bridgeTo);
		}
		if(streetsTo != null){
			streetsTo.addActionListener(action);
			lower.add(streetsTo);
		}
		if(rooftopsTo != null){
			rooftopsTo.addActionListener(action);
			lower.add(rooftopsTo);
		}
		if(bankTo != null){
			bankTo.addActionListener(action);
			lower.add(bankTo);
		}
		if(sewersTo != null){
			sewersTo.addActionListener(action);
			lower.add(sewersTo);
		}
		
		submit.addActionListener(action);
		submitPanel.add(submit);
		
		buttons.add(upper);
		buttons.add(lower);
		buttons.add(submitPanel);
		
		this.removeAll();
		this.add(labelPanel, BorderLayout.NORTH);
		this.add(buttons, BorderLayout.CENTER);
		this.showModal();
	}
	
	public void ymirFrostGiantKingFight(){
		JPanel panel = new JPanel();
		int playerCount = ClientManager.getInstance().getModel().getPlayerCardList().size();
		int thisPlayer = ((PlayerHeader)ClientManager.getInstance().getUser()).getIndex();
		for(int i = ZERO; i < playerCount; ++i){
			final int playerID = i;
			JButton button;
			if(i == thisPlayer){
				button = new JButton(HTML_BODY + HTML_H3 + ME + HTML_H3_CLOSE + HTML_BODY_CLOSE);
			}
			else{
				button = new JButton(HTML_BODY + HTML_H3 + DIALOG_PLAYER_TITLE + i + HTML_H3_CLOSE + HTML_BODY_CLOSE);
			}
			button.addActionListener(new ActionListener(){

				@Override
				public void actionPerformed(ActionEvent e) {
					int gameID = ClientManager.getInstance().getGame().getGameID();
					ClientManager.getInstance().getServer().ymirFrostGiantKingFight(gameID, playerID);
				}
				
			});
			panel.add(button);
		}
		this.removeAll();
		this.add(labelPanel, BorderLayout.NORTH);
		this.add(panel, BorderLayout.CENTER);
		this.showModal();
	}
	
	public void ymirFrostGiantKingDiscard(){
		PlayerCards cards = ClientManager.getInstance().getModel().getPlayerCards();
		List<String> hand = cards.getHand();
		List<String> discard = cards.getDiscard();
		int count = ZERO;
		for(String str : hand){
			if(str.equals(WOUND)){
				++count;
			}
		}
		for(String str : discard){
			if(str.equals(WOUND)){
				++count;
			}
		}
		SpinnerModel model = new SpinnerNumberModel(count, ZERO, count, ONE);
		final JSpinner spinner = new JSpinner(model);
		((JSpinner.DefaultEditor) spinner.getEditor()).getTextField().setEditable(false);
		JPanel panel = new JPanel();
		panel.add(new JLabel(POPUP_DISCARD));
		panel.add(spinner);
		panel.add(new JLabel(POPUP_WOUNDS));
		
		JPanel buttonPanel = new JPanel();
		JButton button = new JButton(BUTTON_ACCEPT);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				int playerIndex = ((PlayerHeader)ClientManager.getInstance().getUser()).getIndex();
				int number = (Integer)spinner.getValue();
				ClientManager.getInstance().getServer().ymirFrostGiantKingDiscard(gameID, playerIndex, number);
			}
			
		});

		buttonPanel.add(button);
		
		this.removeAll();
		this.add(labelPanel, BorderLayout.NORTH);
		this.add(panel, BorderLayout.CENTER);
		this.add(buttonPanel, BorderLayout.SOUTH);
		this.showModal();
	}
	
	private void toggleButton(JButton button){
		if(button == null){
			return;
		}
		if(button.isEnabled()){
			button.setEnabled(false);
		}
		else{
			button.setEnabled(true);
		}
	}
		
	private JButton getButton(String message, String function, boolean accept){
		JButton button = null;
		switch(function){
		case STATE_COVERING_FIRE_ABILITY:
			button = coveringFireAbility(accept);
			break;
		case STATE_HEALING_FACTOR_ABILITY:
			button = healingFactorAbility(accept);
			break;
		case STATE_HEY_CAN_I_GET_A_DO_OVER_ABILITY:
			button = heyCanIGetADoOverAbility(accept);
			break;
		case STATE_HYDRA_KIDNAPPERS_FIGHT:
			button = hydraKidnappersFight(accept);
			break;
		case STATE_HYPNOTIC_CHARM_ABILITY:
			button = hypnoticCharmAbility(accept);
			break;
		case STATE_MELTER_FIGHT:
			button = melterFight(accept);
			break;
		case STATE_MONARCHS_DECREE_FIGHT:
			button = monarchsDecreeFight(accept);
			break;
		case STATE_RANDOM_ACTS_OF_UNKINDNESS_ABILITY:
			button = randomActsOfUnkindnessAbility(accept);
			break;
		case STATE_SHADOWED_THOUGHTS_ABILITY:
			button = shadowedThoughtsAbility(accept);
			break;
		case STATE_UNSTOPPABLE_HULK_ABILITY:
			button = unstoppableHulkAbility(accept);
			break;
		}
		
		button.setFont(FONT_PLAIN_20);
		
		return button;
	}
	
	private JButton coveringFireAbility(final boolean draw){
		JButton button;
		if(draw){
			button = new JButton(BUTTON_DRAW);
		}
		else{
			button = new JButton(BUTTON_DISCARD);
		}
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().coveringFireAbility(gameID, draw);
				view.closeModal();
			}
			
		});
		return button;
	}
	private JButton healingFactorAbility(final boolean accept){
		JButton button;
		if(accept){
			button = new JButton(BUTTON_KO);
		}
		else{
			button = new JButton(BUTTON_DECLINE);
		}
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().healingFactorAbility(gameID, accept);
				view.closeModal();
			}
			
		});
		return button;
	}
	private JButton heyCanIGetADoOverAbility(final boolean accept){
		JButton button;
		if(accept){
			button = new JButton(BUTTON_DISCARD);
		}
		else{
			button = new JButton(BUTTON_DECLINE);
		}
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().heyCanIGetADoOverAbility(gameID, accept);
				view.closeModal();
			}
			
		});
		return button;
	}
	private JButton hydraKidnappersFight(final boolean accept){
		JButton button;
		if(accept){
			button = new JButton(BUTTON_ACCEPT);
		}
		else{
			button = new JButton(BUTTON_DECLINE);
		}
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().hydraKidnappersFight(gameID, accept);
				view.closeModal();
			}
			
		});
		return button;
	}
	private JButton hypnoticCharmAbility(final boolean discard){
		JButton button;
		if(discard){
			button = new JButton(BUTTON_DISCARD);
		}
		else{
			button = new JButton(BUTTON_RETURN);
		}
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().hypnoticCharmAbility(gameID, ((PlayerHeader)ClientManager.getInstance().getUser()).getIndex(), discard);
				view.closeModal();
			}
			
		});
		return button;
	}
	private JButton melterFight(final boolean ko){
		JButton button;
		if(ko){
			button = new JButton(BUTTON_KO);
		}
		else{
			button = new JButton(BUTTON_RETURN);
		}
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().melterFight(gameID, ((PlayerHeader)ClientManager.getInstance().getUser()).getIndex(), ko);
				view.closeModal();
			}
			
		});
		return button;
	}
	private JButton monarchsDecreeFight(final boolean draw){
		JButton button;
		if(draw){
			button = new JButton(BUTTON_DRAW);
		}
		else{
			button = new JButton(BUTTON_DISCARD);
		}
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().monarchsDecreeFight(gameID, draw);
				view.closeModal();
			}
			
		});
		return button;
	}
	private JButton randomActsOfUnkindnessAbility(final boolean accept){
		JButton button;
		if(accept){
			button = new JButton(BUTTON_ACCEPT);
		}
		else{
			button = new JButton(BUTTON_DECLINE);
		}
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().randomActsOfUnkindnessAbility(gameID, accept);
				view.closeModal();
			}
			
		});
		return button;
	}
	private JButton shadowedThoughtsAbility(final boolean accept){
		JButton button;
		if(accept){
			button = new JButton(BUTTON_ACCEPT);
		}
		else{
			button = new JButton(BUTTON_DECLINE);
		}
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().shadowedThoughtsAbility(gameID, accept);
				view.closeModal();
			}
			
		});
		return button;
	}
	private JButton unstoppableHulkAbility(final boolean accept){
		JButton button;
		if(accept){
			button = new JButton(BUTTON_ACCEPT);
		}
		else{
			button = new JButton(BUTTON_DECLINE);
		}
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().unstoppableHulkAbility(gameID, accept);
				view.closeModal();
			}
			
		});
		return button;
	}
}
