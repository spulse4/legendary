package client.popup.masterstrike;

import client.popup.AbstractPopupView;

public class MasterstrikeMagnetoView extends AbstractPopupView{
	//Each player reveals an X-Men Hero or discards down to four cards

	private static final long serialVersionUID = 4950663108079537909L;

	public MasterstrikeMagnetoView(){
		super(MAGNETO_MASTERSTRIKE_MESSAGE);
	}
}
