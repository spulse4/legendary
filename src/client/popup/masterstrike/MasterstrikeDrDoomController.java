package client.popup.masterstrike;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class MasterstrikeDrDoomController extends Controller implements ClientVariables{
	//Each player with exactly 6 cards in hand reveals a Black Hero or puts 2 cards from their hand on top of their deck

	public MasterstrikeDrDoomController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}

	public MasterstrikeDrDoomView getMasterStrikeDrDoomView(){
		return (MasterstrikeDrDoomView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		
		if(model.getTurnTracker().getState() == State.DR_DOOM_MASTER_STRIKE){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				List<String> hand = model.getPlayerCards().getHand();
				List<ClientCard> cards = new ArrayList<ClientCard>();
				for(String str : hand){
					cards.add(ClientManager.getInstance().getBoard().getCard(str));
				}
				List<Integer> indexes = new ArrayList<Integer>();
				for(int i = ZERO; i < cards.size(); ++i){
					indexes.add(NEGATIVE_ONE);
				}
				getMasterStrikeDrDoomView().update(cards, STATE_DR_DOOM_MASTERSTRIKE, false, indexes);
			}
		}
		else{
			if(getMasterStrikeDrDoomView().isShowing()){
				getMasterStrikeDrDoomView().closeModal();
			}
		}
	}
}
