package client.popup.masterstrike;

import client.popup.AbstractPopupView;

public class MasterstrikeRedSkullView extends AbstractPopupView{
	//Each player KOs a Hero from their hand

	private static final long serialVersionUID = 4668111848573457519L;

	public MasterstrikeRedSkullView(){
		super(RED_SKULL_MASTERSTRIKE_MESSAGE);
	}
}
