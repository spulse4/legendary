package client.popup.masterstrike;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;

public class MasterstrikeMagnetoController extends Controller implements ClientVariables{
	//Each player reveals an X-Men Hero or discards down to four cards

	public MasterstrikeMagnetoController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}

	public MasterstrikeMagnetoView getMasterStrikeMagnetoView(){
		return (MasterstrikeMagnetoView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		
		if(model.getTurnTracker().getState() == State.MAGNETO_MASTER_STRIKE){
			List<Integer> effected = model.getTurnTracker().getState().getEffected(model.getGameID());
			if(effected.contains(((PlayerHeader)ClientManager.getInstance().getUser()).getIndex())){
				List<String> hand = model.getPlayerCards().getHand();
				List<ClientCard> cards = new ArrayList<ClientCard>();
				for(String str : hand){
					cards.add(ClientManager.getInstance().getBoard().getCard(str));
				}
				List<Integer> indexes = new ArrayList<Integer>();
				for(int i = ZERO; i < cards.size(); ++i){
					indexes.add(NEGATIVE_ONE);
				}
				getMasterStrikeMagnetoView().update(cards, STATE_MAGNETO_MASTERSTRIKE, false, indexes);
			}
		}
		else{
			if(getMasterStrikeMagnetoView().isShowing()){
				getMasterStrikeMagnetoView().closeModal();
			}
		}
	}
}
