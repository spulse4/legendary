package client.popup.masterstrike;

import client.popup.AbstractPopupView;

public class MasterstrikeDrDoomView extends AbstractPopupView{
	//Each player with exactly 6 cards in hand reveals a Black Hero or puts 2 cards from their hand on top of their deck

	private static final long serialVersionUID = 2965711688978235034L;

	public MasterstrikeDrDoomView(){
		super(DR_DOOM_MASTERSTRIKE_MESSAGE);
	}
}
