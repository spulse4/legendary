package client.popup;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import client.base.OverlayView;
import client.definitions.ClientVariables;
import client.main.ClientManager;

public class AbstractPopupFunctions extends OverlayView implements ClientVariables{

	private static final long serialVersionUID = 6463026537221692182L;
	
	private AbstractPopupFunctions view;

	protected JButton getButtonForClose(String function){
		view = this;
		JButton decline = new JButton(BUTTON_DECLINE);
		decline.setFont(FONT_PLAIN_20);
		decline.setPreferredSize(new Dimension(BUTTON_2_LENGTH, BUTTON_HEIGHT * TWO));
		
		switch(function){
		case STATE_BATTLEFIELD_PROMOTION_ABILITY:
			battlefieldPromotionAbility(decline);
			break;
		case STATE_DANGEROUS_RESCUE_ABILITY:
			dangerousRescueAbility(decline);
			break;
		case STATE_ENERGY_DRAIN_ABILITY:
			energyDrainAbility(decline);
			break;
		case STATE_MANIACAL_TYRANT_FIGHT:
			maniacalTyrantAbility(decline);
			break;
		}
		return decline;
	}
	
	private void battlefieldPromotionAbility(JButton decline){
		decline.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().battlefieldPromotionAbility(gameID, EMPTY);
				view.closeModal();
			}
			
		});
	}
	private void dangerousRescueAbility(JButton decline){
		decline.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().dangerousRescueAbility(gameID, EMPTY);
				view.closeModal();
			}
			
		});
	}
	private void energyDrainAbility(JButton decline){
		decline.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().energyDrainAbility(gameID, EMPTY);
				view.closeModal();
			}
			
		});
	}
	private void maniacalTyrantAbility(JButton decline){
		decline.setText(STOP);
		decline.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().maniacalTyrantFight(gameID, EMPTY);
				view.closeModal();
			}
			
		});
	}
}
