package client.popup.endGame;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JPanel;

import client.main.ClientManager;
import client.popup.AbstractPopupView;

public class EndGameLoseView extends AbstractPopupView{

	private static final long serialVersionUID = -7373683676064921215L;

	private class SPanel extends JPanel{

		private static final long serialVersionUID = -3440458894720154166L;

		private BufferedImage image;

		public void setImage(){
			try{
				image = javax.imageio.ImageIO.read(new File(LOSE_PATH));
			}
			catch(IOException e){
				e.printStackTrace();
			}
		}

		@Override
		public void paintComponent(Graphics g){
			super.paintComponent(g);
			Graphics2D g2 = (Graphics2D)g;
			g2.drawImage(image.getScaledInstance(500, 470, Image.SCALE_SMOOTH), ZERO, ZERO, null);
		}
		
	};
	
	public EndGameLoseView(){
		super(LOST_MESSAGE);
				
		this.removeAll();
		this.setPreferredSize(new Dimension(500, 500));		
		this.setLayout(new BorderLayout());
		
		SPanel topPanel = new SPanel();
		
		topPanel.setImage();
		JPanel bottomPanel = new JPanel();
		JButton button = new JButton("Return to Join Game View");
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				ClientManager.getInstance().reset();
			}
			
		});
		
		button.setPreferredSize(new Dimension((int)button.getPreferredSize().getWidth(), 30));
		bottomPanel.add(button);
		
		this.add(topPanel, BorderLayout.CENTER);
		this.add(bottomPanel, BorderLayout.SOUTH);
	}
}
