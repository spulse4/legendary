package client.popup.endGame;

import client.base.Controller;
import client.base.IView;
import client.main.ClientManager;
import shared.definitions.State;

public class EndGameLoseController extends Controller{

	public EndGameLoseController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}

	public EndGameLoseView getEndGameLoseView(){
		return (EndGameLoseView)getView();
	}
	
	@Override
	public void modelChanged(){
		if(ClientManager.getInstance().getModel().getTurnTracker().getState() == State.LOST){
			getEndGameLoseView().showModal();
		}
	}
}
