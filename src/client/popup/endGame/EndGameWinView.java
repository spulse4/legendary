package client.popup.endGame;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

import client.main.ClientManager;
import client.popup.AbstractPopupView;
import shared.model.PlayerCards;

public class EndGameWinView extends AbstractPopupView{
	
	private static final long serialVersionUID = 452971177762257206L;

	public EndGameWinView(){
		super(WIN_MESSAGE);
	}
	
	public void go(){
		this.removeAll();
		this.setLayout(new BorderLayout());
		JPanel centerPanel = new JPanel();

		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
		List<PlayerCards> cards = ClientManager.getInstance().getModel().getPlayerCardList();
		
		int max = ZERO;
		for(int i = ZERO; i < cards.size(); ++i){
			int points = cards.get(i).getPoints();
			if(points > max){
				max = points;
			}
		}
		for(PlayerCards card : cards){
			int points = card.getPoints();
			centerPanel.add(Box.createRigidArea(new Dimension(ZERO, FIVE)));
			if(points == max){
				centerPanel.add(card.getPanel(true));
			}
			else{
				centerPanel.add(card.getPanel(false));
			}
		}
				
		centerPanel.add(Box.createRigidArea(new Dimension(ZERO, FIVE)));
		JPanel panel = new JPanel();
		JButton button = new JButton("Return to Join Game View");
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				ClientManager.getInstance().reset();
			}
			
		});
		
		button.setPreferredSize(new Dimension((int)button.getPreferredSize().getWidth(), 30));
		panel.add(button);
		this.add(centerPanel, BorderLayout.CENTER);
		this.add(panel, BorderLayout.SOUTH);
	}
}
