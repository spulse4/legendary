package client.popup.endGame;

import client.base.Controller;
import client.base.IView;
import client.main.ClientManager;
import shared.definitions.State;

public class EndGameWinController extends Controller{

	public EndGameWinController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}

	public EndGameWinView getEndGameWinView(){
		return (EndGameWinView)getView();
	}

	@Override
	public void modelChanged(){
		if(ClientManager.getInstance().getModel().getTurnTracker().getState() == State.WON){
			getEndGameWinView().go();
			getEndGameWinView().showModal();
		}
	}
}
