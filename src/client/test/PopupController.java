package client.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import client.base.Controller;
import client.base.IView;
import client.main.ClientManager;
import shared.definitions.State;
import shared.model.ModelFacade;

public class PopupController extends Controller{

	public PopupController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public PopupView getPopupView(){
		return (PopupView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		ModelFacade model = ClientManager.getInstance().getModel();
		if(model.getTurnTracker().getState() == State.TEST){
			String message = model.getMessageString();
			List<String> messages = new ArrayList<String>();
			Scanner myScanner = new Scanner(message);
			myScanner.useDelimiter("::");
			while(myScanner.hasNext()){
				messages.add(myScanner.next());
			}
			myScanner.close();
			getPopupView().start(messages, model.getGameID());
		}
	}
}
