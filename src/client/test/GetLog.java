package client.test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;

import client.definitions.ClientVariables;

public class GetLog implements ClientVariables{

	public static void main(String[] args){
		try{
			File log = new File("send/logs.zip");
			if(log.exists()){
				Files.delete(log.toPath());
			}

			String URL_PREFIX = HTTP + HOST + COLON + PORT + "/test/getLog";
			URL url = new URL(URL_PREFIX);
			HttpURLConnection connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod(POST);
			connection.setDoOutput(true);
			connection.connect();

			connection.getResponseCode();
			InputStream input = connection.getInputStream();
			int len = ZERO;
			
			byte[] buffer = new byte[BYTE_SIZE];
			StringBuilder string = new StringBuilder();
			while(-1 != (len = input.read(buffer))){
				string.append(new String(buffer, ZERO, len));
			}
			
			FileWriter saveIt = new FileWriter("send/logs.zip");
			saveIt.write(string.toString());
			saveIt.close();
		}
		catch(MalformedURLException e){
			e.printStackTrace();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
}
