package client.test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class AddTestCase {

	public static void main(String[] args){
		int current = 0;
		while(new File("test/model-" + current + ".txt").exists()){
			++current;
		}
		--current;
		while(current > -1){
			File oldFile = new File("test/model-" + current + ".txt");
			File newFile = new File("test/model-" + (current + 1) + ".txt");
			try{
				Files.move(oldFile.toPath(), newFile.toPath());
			}
			catch(IOException e){
				e.printStackTrace();
			}
			--current;
		}
	}
}
