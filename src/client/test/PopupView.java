package client.test;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import client.base.OverlayView;
import client.main.ClientManager;

public class PopupView extends OverlayView{

	private static final long serialVersionUID = -7672856678387113813L;

	public PopupView(){}
	
	public void start(List<String> messages, final int gameID){
		final JDialog dialog = new JDialog();
		JPanel select = new JPanel();
		select.setLayout(new BoxLayout(select, BoxLayout.Y_AXIS));
		for(String message : messages){
			select.add(new JLabel(message));	
		}
		JButton button = new JButton("close");
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				dialog.dispose();
				ClientManager.getInstance().getServer().attackCard(gameID, 0);
			}
			
		});
		
		select.add(button);
		int wid = 400;
		int hei = 400;
		dialog.setSize(wid, hei);
		dialog.setTitle("Test");
		dialog.getContentPane().setBackground(Color.WHITE);
		dialog.setLocationRelativeTo(null);
		dialog.add(select);
		dialog.getRootPane().setBorder(BorderFactory.createMatteBorder(4, 4, 4, 4, Color.BLACK));
		dialog.setModal(true);
		dialog.setVisible(true);
	}
	
	public PopupController getPopupController(){
		return (PopupController)super.getController();
	}
}
