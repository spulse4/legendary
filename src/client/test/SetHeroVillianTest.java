package client.test;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import client.definitions.ClientVariables;

public class SetHeroVillianTest implements ClientVariables{

	public static void main(String[] args){
		String URL_PREFIX;
		URL url;
		HttpURLConnection connection;
		BufferedOutputStream toServer;
		File myFile;
		byte[] mybytearray;
		FileInputStream fis;
		BufferedInputStream bis;
		try{
			URL_PREFIX = HTTP + HOST + COLON + PORT + "/test/setHero";
			url = new URL(URL_PREFIX);
			connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod(POST);
			connection.setDoOutput(true);
			connection.connect();
			toServer = new BufferedOutputStream(connection.getOutputStream());
			myFile = new File("send/hand.txt");
			mybytearray = new byte[(int) myFile.length()];
	        fis = new FileInputStream(myFile);
	        bis = new BufferedInputStream(fis);
	        bis.read(mybytearray, 0, mybytearray.length);
            toServer.write(mybytearray, 0, mybytearray.length);
            bis.close();
            toServer.flush();
			if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
				System.out.println("sent hand.txt");
			}
			else{
				System.out.println("failed to send hand.txt");
			}
            toServer.close();

            
            
    		URL_PREFIX = HTTP + HOST + COLON + PORT + "/test/setVillian";
			url = new URL(URL_PREFIX);
			connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod(POST);
			connection.setDoOutput(true);
			connection.connect();
			toServer = new BufferedOutputStream(connection.getOutputStream());
			myFile = new File("send/villian.txt");
			mybytearray = new byte[(int) myFile.length()];
	        fis = new FileInputStream(myFile);
	        bis = new BufferedInputStream(fis);
	        bis.read(mybytearray, 0, mybytearray.length);
            toServer.write(mybytearray, 0, mybytearray.length);
            bis.close();
            toServer.flush();
			if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
				System.out.println("sent villian.txt");
			}
			else{
				System.out.println("failed to send villian.txt");
			}
            toServer.close();
		}
		catch(MalformedURLException e){
			e.printStackTrace();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
}
