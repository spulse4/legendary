package client.legendary;

import java.awt.*;
import javax.swing.*;

import client.definitions.ClientVariables;
import client.hand.PlayerHandController;
import client.hand.PlayerHandView;


@SuppressWarnings("serial")
public class RightPanel extends JPanel implements ClientVariables{

	private PlayerHandController playerHand;
	
	public RightPanel(JPanel legendary){
		this.setLayout(new BorderLayout());
		this.setPreferredSize(new Dimension(HAND_WIDTH, HAND_HEIGHT));
						
		playerHand = new PlayerHandController(new PlayerHandView(legendary));       	
		this.add((PlayerHandView)playerHand.getView(), BorderLayout.CENTER);
	}

	public PlayerHandController getHandController(){
		return playerHand;
	}
}


