package client.legendary;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

import client.definitions.ClientVariables;

public class TitlePanel extends JPanel implements ClientVariables{
		
	private static final long serialVersionUID = -8059989678241438413L;
	
	private final String TITLE = LEGENDARY;
	
	private JLabel titleLabel;
	
	public TitlePanel(){
		this.setLayout(new BorderLayout());
		this.setBorder(new EmptyBorder(TITLE_BORDER, TITLE_BORDER, TITLE_BORDER, TITLE_BORDER));
		
		titleLabel = new JLabel(TITLE);
		titleLabel.setOpaque(true);
		
		Font font = titleLabel.getFont();
		Font newFont = font.deriveFont(font.getStyle(), FONT_SIZE_48);
		titleLabel.setFont(newFont);
		
		this.add(titleLabel, BorderLayout.CENTER);
	}
}

