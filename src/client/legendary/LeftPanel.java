package client.legendary;

import java.awt.*;
import javax.swing.*;

import client.chat.*;
import client.definitions.ClientVariables;


@SuppressWarnings("serial")
public class LeftPanel extends JPanel implements ClientVariables{
	
	private JPanel panel;
	private ChatView chatView;
	private ChatController chatController;
	
	public LeftPanel(){
		
		this.setLayout(new BorderLayout());
		
		panel = new JPanel();
		Font font = panel.getFont();
		Font newFont = font.deriveFont(font.getStyle(), FONT_SIZE_20);
		panel.setFont(newFont);
				
		chatView = new ChatView();
        chatController = new ChatController(this, chatView);
        chatView.setController(chatController);
        
        panel.add(chatView);
		
		this.add(chatView, BorderLayout.CENTER);

		this.setPreferredSize(new Dimension(CHAT_WIDTH, CHAT_HEIGHT));
	}

	public ChatView getChatView(){
		return chatView;
	}
}


