package client.legendary;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import client.definitions.ClientVariables;
import client.main.ClientManager;
import shared.definitions.*;


@SuppressWarnings("serial")
public class GameStatePanel extends JPanel implements ClientVariables{
		
	private JButton button;
	
	public GameStatePanel(){
		this.setLayout(new FlowLayout());
		this.setOpaque(false);
		new GameStateController(this);
		
		button = new JButton();
		
		Font font = button.getFont();
		Font newFont = font.deriveFont(font.getStyle(), FONT_SIZE_20);
		button.setFont(newFont);
		
		button.setPreferredSize(new Dimension(GAME_STATE_WIDTH, GAME_STATE_HEIGHT));
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				int playerIndex = ((PlayerHeader)ClientManager.getInstance().getUser()).getIndex();
				ClientManager.getInstance().getServer().endTrun(gameID, playerIndex);
			}
		});
		
		this.add(button);
		
		updateGameState(GAME_STATE_INITIAL_MESSAGE, false);
	}
	
	public void updateGameState(String stateMessage, boolean enable){
		button.setText(stateMessage);
		button.setEnabled(enable);
	}
}

