package client.legendary;

import java.awt.BorderLayout;

import javax.swing.JPanel;

public class LegendaryPanel extends JPanel{

	private static final long serialVersionUID = -6175660942243346087L;

	private TitlePanel titlePanel;
	private LeftPanel leftPanel;
	private MidPanel midPanel;
	private RightPanel rightPanel;

	public LegendaryPanel(){
		this.setLayout(new BorderLayout());
		
		titlePanel = new TitlePanel();
		midPanel = new MidPanel(this);
		leftPanel = new LeftPanel();
		rightPanel = new RightPanel(this);
		leftPanel.setVisible(false);
		
		this.add(titlePanel, BorderLayout.NORTH);
		this.add(leftPanel, BorderLayout.WEST);
		this.add(midPanel, BorderLayout.CENTER);
		this.add(rightPanel, BorderLayout.EAST);
	}
	
	public void toggleChat(boolean visibility){
		leftPanel.setVisible(visibility);
	}
	
	public void toggleHand(boolean visibility){
		rightPanel.setVisible(visibility);
	}
}
