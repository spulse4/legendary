package client.legendary;

import client.base.Controller;
import client.definitions.ClientVariables;
import client.login.LoginView;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;

public class GameStateController extends Controller implements ClientVariables{
		
	GameStatePanel panel;
	
	public GameStateController(GameStatePanel panel){
		//This is just to appease the Controller class. LoginView here does nothing
		super(new LoginView());
		ClientManager.getInstance().getModel().registerObserver(this);
		this.panel = panel;
	}
	
	@Override
	public void turnTrackerChanged(){
		int thisPlayer = ((PlayerHeader)ClientManager.getInstance().getUser()).getIndex();
		
		int currentPlayer = ClientManager.getInstance().getModel().getTurnTracker().getCurrentPlayer();
		
		if(currentPlayer == thisPlayer){
			panel.updateGameState(GAME_STATE_END_TURN, true);
		}
		else{
			panel.updateGameState(GAME_STATE_WAITING + (currentPlayer + ONE), false);
		}
	}
}
