package client.legendary;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

import client.board.BoardController;
import client.board.BoardView;
import client.definitions.ClientVariables;
import client.images.RotatedIcon;
import client.images.TextIcon;
import client.legendary.GameStatePanel;
import client.main.ClientManager;

@SuppressWarnings("serial")
public class MidPanel extends JPanel implements ClientVariables{
		
	private GameStatePanel gameStatePanel = null;
	private JButton hideButton = null;
	private JButton hideButton2 = null;
	private boolean visible = false;
	private boolean visible2 = true;
	protected LegendaryPanel legendary = null;
	private static BackgroundPanel panel;
	
	public MidPanel(LegendaryPanel legendaryPanel){
		this.legendary = legendaryPanel;
		
		this.setLayout(new BorderLayout());
		BoardController cont = new BoardController(new BoardView());
		
		gameStatePanel = new GameStatePanel();
		hideButton = new JButton();
		hideButton.setFont(FONT_PLAIN_10);
		TextIcon label = new TextIcon(hideButton, MIDDLE_CHAT_HIDE, TextIcon.Layout.HORIZONTAL);
		RotatedIcon rotated = new RotatedIcon(label, RotatedIcon.Rotate.DOWN);
		hideButton.setIcon(rotated);
		hideButton.setPreferredSize(new Dimension(MIDDLE_HIDE_BUTTON_WIDTH, ZERO));//changed from 1
		hideButton.setBackground(Color.WHITE);
		hideButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(visible){
					visible = false;
					TextIcon label = new TextIcon(hideButton, MIDDLE_CHAT_SHOW, TextIcon.Layout.HORIZONTAL);
					RotatedIcon rotated = new RotatedIcon(label, RotatedIcon.Rotate.DOWN);
					hideButton.setIcon(rotated);
					legendary.toggleChat(visible);
				}
				else{
					visible = true;
					hideButton.setBackground(Color.WHITE);
					TextIcon label = new TextIcon(hideButton, MIDDLE_CHAT_HIDE, TextIcon.Layout.HORIZONTAL);
					RotatedIcon rotated = new RotatedIcon(label, RotatedIcon.Rotate.DOWN);
					hideButton.setIcon(rotated);
					legendary.toggleChat(visible);
				}
			}
			
		});
		ClientManager.getInstance().setChatButton(hideButton);
	
		hideButton2 = new JButton();
		hideButton2.setFont(FONT_PLAIN_10);
		label = new TextIcon(hideButton2, MIDDLE_HAND_SHOW, TextIcon.Layout.HORIZONTAL);
		rotated = new RotatedIcon(label, RotatedIcon.Rotate.UP);
		hideButton2.setIcon(rotated);
		hideButton2.setPreferredSize(new Dimension(MIDDLE_HIDE_BUTTON_WIDTH, ZERO));//changed from 1
		hideButton2.setBackground(Color.WHITE);
		hideButton2.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(visible2){
					visible2 = false;
					TextIcon label = new TextIcon(hideButton, MIDDLE_HAND_SHOW, TextIcon.Layout.HORIZONTAL);
					RotatedIcon rotated = new RotatedIcon(label, RotatedIcon.Rotate.UP);
					hideButton2.setIcon(rotated);
					legendary.toggleHand(visible2);
				}
				else{
					visible2 = true;
					TextIcon label = new TextIcon(hideButton, MIDDLE_HAND_HIDE, TextIcon.Layout.HORIZONTAL);
					RotatedIcon rotated = new RotatedIcon(label, RotatedIcon.Rotate.UP);
					hideButton2.setIcon(rotated);
					legendary.toggleHand(visible2);
				}
			}
			
		});
		ClientManager.getInstance().setHandButton(hideButton2);

		panel = new BackgroundPanel(cont.getBoardView());
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.add((BoardView)cont.getBoardView());
		panel.add(gameStatePanel);
		
		this.add(panel, BorderLayout.CENTER);
		this.add(hideButton, BorderLayout.WEST);
		this.add(hideButton2, BorderLayout.EAST);

		this.setPreferredSize(new Dimension(MIDDLE_WIDTH, MIDDLE_HEIGHT));
	}
	
	public static BackgroundPanel getPanel(){
		return panel;
	}
}

