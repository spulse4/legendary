package client.legendary;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import client.board.BoardComponent;
import client.board.BoardView;
import client.definitions.ClientVariables;

public class BackgroundPanel extends JPanel implements ClientVariables{
	
	private static final long serialVersionUID = 5707609961831876113L;

	private BoardComponent board;
	
	public BackgroundPanel(BoardView boardView){
		super();
		board = boardView.getBoard();
	}
	
	@Override
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		
		Graphics2D g2 = (Graphics2D)g;
		
		BufferedImage img = board.getImage(BOARD);
		if(img != null){
			g2.drawImage(board.getImage(BOARD), ZERO, ZERO, this.getWidth(), this.getHeight(), null);
		}
		else{
			g2.setColor(Color.WHITE);
			g2.fillRect(ZERO, ZERO, this.getWidth(), this.getHeight());
		}
	}
}
