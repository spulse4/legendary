package client.settings;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import client.definitions.ClientVariables;

public class HeroView extends AbstractSettingsView implements ClientVariables{

	private static final long serialVersionUID = 5999758114971747963L;
	
	public HeroView(){
        this.setOpaque(true);
        this.setLayout(new BorderLayout());
        this.setBorder(BorderFactory.createLineBorder(Color.black, VIEW_BORDER_WIDTH));
		cards = new HashMap<String, NameCard>();
		cards.put(BLACK_WIDOW, new NameCard(BLACK_WIDOW_FIXED, BLACK_WIDOW, this));
		cards.put(CAPTAIN_AMERICA, new NameCard(CAPTAIN_AMERICA_FIXED, CAPTAIN_AMERICA, this));
		cards.put(HAWKEYE, new NameCard(HAWKEYE_FIXED, HAWKEYE, this));
		cards.put(HULK, new NameCard(HULK_FIXED, HULK, this));
		cards.put(IRON_MAN, new NameCard(IRON_MAN_FIXED, IRON_MAN, this));
		cards.put(NICK_FURY, new NameCard(NICK_FURY_FIXED, NICK_FURY, this));
		cards.put(THOR, new NameCard(THOR_FIXED, THOR, this));
		cards.put(DEADPOOL, new NameCard(DEADPOOL_FIXED, DEADPOOL, this));
		cards.put(SPIDERMAN, new NameCard(SPIDERMAN_FIXED, SPIDERMAN, this));
		cards.put(CYCLOPS, new NameCard(CYCLOPS_FIXED, CYCLOPS, this));
		cards.put(EMMA_FROST, new NameCard(EMMA_FROST_FIXED, EMMA_FROST, this));
		cards.put(GAMBIT, new NameCard(GAMBIT_FIXED, GAMBIT, this));
		cards.put(ROGUE, new NameCard(ROGUE_FIXED, ROGUE, this));
		cards.put(STORM, new NameCard(STORM_FIXED, STORM, this));
		cards.put(WOLVERINE, new NameCard(WOLVERINE_FIXED, WOLVERINE, this));
		
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.add(cards.get(BLACK_WIDOW));
		panel.add(cards.get(CAPTAIN_AMERICA));
		panel.add(cards.get(HAWKEYE));
		panel.add(cards.get(HULK));
		panel.add(cards.get(IRON_MAN));
		panel.add(cards.get(NICK_FURY));
		panel.add(cards.get(THOR));
		panel.add(cards.get(DEADPOOL));
		panel.add(cards.get(SPIDERMAN));
		panel.add(cards.get(CYCLOPS));
		panel.add(cards.get(EMMA_FROST));
		panel.add(cards.get(GAMBIT));
		panel.add(cards.get(ROGUE));
		panel.add(cards.get(STORM));
		panel.add(cards.get(WOLVERINE));
		
		JScrollPane scroll1 = new JScrollPane(panel);
		scroll1.setPreferredSize(new Dimension(SELECT_SCROLL_WIDTH, SELECT_SCROLL_HEIGHT));
		scroll1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scroll1.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scroll1.getVerticalScrollBar().setUnitIncrement(SCROLL_SPEED);
		
		this.add(scroll1, BorderLayout.CENTER);
		JButton setHeroes = new JButton(SELECT_HEROES);
		setHeroes.setFont(FONT_PLAIN_20);
		setHeroes.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				getHeroController().sendHeroes();
				getHeroController().close();
			}
			
		});
		JButton cancel = new JButton(BUTTON_CANCEL);
		cancel.setFont(FONT_PLAIN_20);
		cancel.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				List<String> cardList = getHeroController().getCards();
				for(String str : cards.keySet()){
					cards.get(str).setSelected(false);
				}
				for(String str : cardList){
					cards.get(str).setSelected(true);
				}
				getHeroController().close();
			}
			
		});
		JPanel buttons = new JPanel();
		buttons.setLayout(new FlowLayout());
		buttons.add(setHeroes);
		buttons.add(cancel);
		this.add(buttons, BorderLayout.SOUTH);
	}
	
	public HeroController getHeroController(){
		return (HeroController)super.getController();
	}
	
	public void setHeroes(List<String> heroes) {
		for(String str : cards.keySet()){
			cards.get(str).setSelected(false);
		}
		for(String str : heroes){
			cards.get(str).setSelected(true);
		}
	}

	public List<String> getHeroes() {
		List<String> heroes = new ArrayList<String>();
		for(String str : cards.keySet()){
			if(cards.get(str).isSelected()){
				heroes.add(str);
			}
		}
		return heroes;
	}
}
