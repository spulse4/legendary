package client.settings;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import client.definitions.ClientVariables;

public class MastermindView extends AbstractSettingsView implements ClientVariables{

	private static final long serialVersionUID = 4236335000976035169L;
	
	public MastermindView(){
        this.setOpaque(true);
        this.setLayout(new BorderLayout());
        this.setBorder(BorderFactory.createLineBorder(Color.black, VIEW_BORDER_WIDTH));
		cards = new HashMap<String, NameCard>();
		cards.put(DR_DOOM, new NameCard(DR_DOOM_FIXED, DR_DOOM, this));
		cards.put(LOKI, new NameCard(LOKI_FIXED, LOKI, this));
		cards.put(MAGNETO, new NameCard(MAGNETO_FIXED, MAGNETO, this));
		cards.put(RED_SKULL, new NameCard(RED_SKULL_FIXED, RED_SKULL, this));

		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.add(cards.get(DR_DOOM));
		panel.add(cards.get(LOKI));
		panel.add(cards.get(MAGNETO));
		panel.add(cards.get(RED_SKULL));
		
		JScrollPane scroll1 = new JScrollPane(panel);
		scroll1.setPreferredSize(new Dimension(SELECT_SCROLL_WIDTH, SELECT_SCROLL_HEIGHT));
		scroll1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scroll1.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scroll1.getVerticalScrollBar().setUnitIncrement(SCROLL_SPEED);
		
		this.add(scroll1, BorderLayout.CENTER);
		JButton setMastermind = new JButton(SELECT_MASTERMIND);
		setMastermind.setFont(FONT_PLAIN_20);
		setMastermind.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				getMastermindController().sendMastermind();
				getMastermindController().close();
			}
			
		});
		JButton cancel = new JButton(BUTTON_CANCEL);
		cancel.setFont(FONT_PLAIN_20);
		cancel.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String card = getMastermindController().getCard();
				for(String str : cards.keySet()){
					cards.get(str).setSelected(false);
				}
				if(card != null){
					cards.get(card).setSelected(true);
				}
				getMastermindController().close();
			}
			
		});
		JPanel buttons = new JPanel();
		buttons.setLayout(new FlowLayout());
		buttons.add(setMastermind);
		buttons.add(cancel);
		this.add(buttons, BorderLayout.SOUTH);
	}
	
	public MastermindController getMastermindController(){
		return (MastermindController)super.getController();
	}

	public void setMastermind(String mastermind) {
		for(String str : cards.keySet()){
			cards.get(str).setSelected(false);
		}
		if(mastermind == null){
			return;
		}
		cards.get(mastermind).setSelected(true);
	}

	public String getMastermind() {
		for(String str : cards.keySet()){
			if(cards.get(str).isSelected()){
				return str;
			}
		}
		return null;
	}

	@Override
	public void clicked(String name) {
		for(String str : cards.keySet()){
			cards.get(str).setSelected(false);
		}
		cards.get(name).setSelected(true);
	}
}
