package client.settings;

import client.base.Controller;
import client.base.IView;
import client.main.ClientManager;

public class SchemeController extends Controller{

	private String card;

	public SchemeController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}

	public SchemeView getSchemeView() {
		return (SchemeView)super.getView();
	}

	public void start() {
		if(getSchemeView().isModalShowing()){
			getSchemeView().closeModal();
		}
		getSchemeView().showModal();
		card = getSchemeView().getScheme();
	}

	public void setScheme(String scheme) {
		getSchemeView().setScheme(scheme);
	}

	public void sendScheme() {
		int gameID = ClientManager.getInstance().getGame().getGameID();
		String scheme = getSchemeView().getScheme();
		ClientManager.getInstance().getServer().setScheme(gameID, scheme);
	}
	
	public String getCard(){
		return card;
	}

	public void close() {
		getSchemeView().closeModal();
	}

	@Override
	public void turnTrackerChanged(){
		setScheme(ClientManager.getInstance().getModel().getTurnTracker().getScheme());
	}

	@Override
	public void gameStarted(){
		if(getSchemeView().isModalShowing()){
			getSchemeView().closeModal();
		}
	}
}