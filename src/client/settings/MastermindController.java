package client.settings;

import client.base.Controller;
import client.base.IView;
import client.main.ClientManager;

public class MastermindController extends Controller{

	private String card;

	public MastermindController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}

	public MastermindView getMastermindView() {
		return (MastermindView)super.getView();
	}

	public void start() {
		if(getMastermindView().isModalShowing()){
			getMastermindView().closeModal();
		}
		getMastermindView().showModal();
		card = getMastermindView().getMastermind();
	}

	public void setMastermind(String mastermind) {
		getMastermindView().setMastermind(mastermind);
	}

	public void sendMastermind() {
		int gameID = ClientManager.getInstance().getGame().getGameID();
		String mastermind = getMastermindView().getMastermind();
		ClientManager.getInstance().getServer().setMastermind(gameID, mastermind);
	}
	
	public String getCard(){
		return card;
	}

	public void close() {
		getMastermindView().closeModal();
	}

	@Override
	public void turnTrackerChanged(){
		setMastermind(ClientManager.getInstance().getModel().getTurnTracker().getMastermind());
	}

	@Override
	public void gameStarted(){
		if(getMastermindView().isModalShowing()){
			getMastermindView().closeModal();
		}
	}
}
