package client.settings;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import client.definitions.ClientVariables;

public class VillianView extends AbstractSettingsView implements ClientVariables{

	private static final long serialVersionUID = 4786909451457195206L;
	
	public VillianView(){
        this.setOpaque(true);
        this.setLayout(new BorderLayout());
        this.setBorder(BorderFactory.createLineBorder(Color.black, VIEW_BORDER_WIDTH));
		cards = new HashMap<String, NameCard>();
		cards.put(CATEGORY_BROTHERHOOD, new NameCard(BROTHERHOOD_FIXED + VILLIAN_FIXED, CATEGORY_BROTHERHOOD, this));
		cards.put(CATEGORY_ENEMIES_OF_ASGARD, new NameCard(ENEMIES_OF_ASGARD_FIXED + VILLIAN_FIXED, CATEGORY_ENEMIES_OF_ASGARD, this));
		cards.put(CATEGORY_HYDRA, new NameCard(HYDRA_FIXED + VILLIAN_FIXED, CATEGORY_HYDRA, this));
		cards.put(CATEGORY_MASTERS_OF_EVIL, new NameCard(MASTERS_OF_EVIL_FIXED + VILLIAN_FIXED, CATEGORY_MASTERS_OF_EVIL, this));
		cards.put(CATEGORY_RADIATION, new NameCard(RADIATION_FIXED+ VILLIAN_FIXED, CATEGORY_RADIATION, this));
		cards.put(CATEGORY_SKRULLS, new NameCard(SKRULLS_FIXED + VILLIAN_FIXED, CATEGORY_SKRULLS, this));
		cards.put(CATEGORY_SPIDER_FOES, new NameCard(SPIDER_FOES_FIXED + VILLIAN_FIXED, CATEGORY_SPIDER_FOES, this));
		cards.put(DOOMBOT_LEGION, new NameCard(DOOMBOT_LEGION_FIXED + HENCHMAN_FIXED, DOOMBOT_LEGION, this));
		cards.put(HAND_NINJAS, new NameCard(HAND_NINJAS_FIXED + HENCHMAN_FIXED, HAND_NINJAS, this));
		cards.put(SAVAGE_LAND_MUTATES, new NameCard(SAVAGE_LAND_MUTATES_FIXED + HENCHMAN_FIXED, SAVAGE_LAND_MUTATES, this));
		cards.put(SENTINEL, new NameCard(SENTINEL_FIXED + HENCHMAN_FIXED, SENTINEL, this));

		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.add(cards.get(CATEGORY_BROTHERHOOD));
		panel.add(cards.get(CATEGORY_ENEMIES_OF_ASGARD));
		panel.add(cards.get(CATEGORY_HYDRA));
		panel.add(cards.get(CATEGORY_MASTERS_OF_EVIL));
		panel.add(cards.get(CATEGORY_RADIATION));
		panel.add(cards.get(CATEGORY_SKRULLS));
		panel.add(cards.get(CATEGORY_SPIDER_FOES));
		panel.add(cards.get(DOOMBOT_LEGION));
		panel.add(cards.get(HAND_NINJAS));
		panel.add(cards.get(SAVAGE_LAND_MUTATES));
		panel.add(cards.get(SENTINEL));
		
		JScrollPane scroll1 = new JScrollPane(panel);
		scroll1.setPreferredSize(new Dimension(SELECT_SCROLL_WIDTH, SELECT_SCROLL_HEIGHT));
		scroll1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scroll1.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scroll1.getVerticalScrollBar().setUnitIncrement(SCROLL_SPEED);
		
		this.add(scroll1, BorderLayout.CENTER);
		JButton setVillians = new JButton(SELECT_VILLIANS);
		setVillians.setFont(FONT_PLAIN_20);
		setVillians.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				getVillianController().sendVillians();
				getVillianController().close();
			}
			
		});
		JButton cancel = new JButton(BUTTON_CANCEL);
		cancel.setFont(FONT_PLAIN_20);
		cancel.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				List<String> cardList = getVillianController().getCards();
				for(String str : cards.keySet()){
					cards.get(str).setSelected(false);
				}
				for(String str : cardList){
					cards.get(str).setSelected(true);
				}
				getVillianController().close();
			}
			
		});
		JPanel buttons = new JPanel();
		buttons.setLayout(new FlowLayout());
		buttons.add(setVillians);
		buttons.add(cancel);
		this.add(buttons, BorderLayout.SOUTH);
	}
	
	public VillianController getVillianController(){
		return (VillianController)super.getController();
	}

	public void setVillians(List<String> villians) {
		for(String str : cards.keySet()){
			cards.get(str).setSelected(false);
		}
		for(String str : villians){
			cards.get(str).setSelected(true);
		}
	}

	public List<String> getVillians() {
		List<String> villians = new ArrayList<String>();
		for(String str : cards.keySet()){
			if(cards.get(str).isSelected()){
				villians.add(str);
			}
		}
		return villians;
	}
}