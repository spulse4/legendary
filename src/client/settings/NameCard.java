package client.settings;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Scanner;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import client.definitions.ClientVariables;

public class NameCard extends JPanel implements ClientVariables{

	private static final long serialVersionUID = 1222194104184726085L;

	private String name;
	private String rawName;
	private boolean selected;
	private ISettingView view;
	
	public NameCard(String n, String rn, ISettingView v){
		this.name = n;
		this.rawName = rn;
		selected = false;
		this.view = v;
		int height = SELECT_HEIGHT_PER_ROW;
		int length = n.length();
		while(length > ZERO){
			if(length > SELECT_MAX_LENGTH_PER_ROW){
				height += SELECT_HEIGHT_PER_ROW;
			}
			length -= SELECT_MAX_LENGTH_PER_ROW;
		}
		Dimension dim = new Dimension(SELECT_WIDTH, height);
		this.setMinimumSize(dim);
		this.setPreferredSize(dim);
		this.setMaximumSize(dim);
		this.setBackground(Color.WHITE);
        this.setBorder(BorderFactory.createLineBorder(Color.black, ONE));
		this.addMouseListener(new MouseListener(){

			@Override
			public void mouseClicked(MouseEvent e) {
				view.clicked(rawName);
			}

			@Override
			public void mouseEntered(MouseEvent e) {}

			@Override
			public void mouseExited(MouseEvent e) {}

			@Override
			public void mousePressed(MouseEvent e) {}

			@Override
			public void mouseReleased(MouseEvent e) {}
		});
	}
	
	public void setSelected(boolean selected){
		this.selected = selected;
		if(this.selected){
			this.setBackground(Color.GRAY);
		}
		else{
			this.setBackground(Color.WHITE);
		}
	}
	
	public boolean isSelected(){
		return selected;
	}
	
	@Override 
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		
		Graphics2D g2 = (Graphics2D)g;
		StringBuilder str = new StringBuilder();
		int diff = SELECT_INITIAL_DIFF;
		Scanner myScanner = new Scanner(name);
		String next = EMPTY;
		while(myScanner.hasNext()){
			if(str.length() == ZERO){
				str.append(next);
			}
			else{
				str.append(SPACE + next);
			}
			next = myScanner.next();
			if(str.length() + ONE + next.length() > SELECT_MAX_LENGTH_PER_ROW){
				g2.setFont(FONT_PLAIN_20);
				g2.setColor(Color.BLACK);
				g2.drawString(str.toString(), SELECT_BUFFER, diff);
				diff += SELECT_MAX_LENGTH_PER_ROW;
				str.setLength(ZERO);
			}
		}
		myScanner.close();

		if(str.length() == ZERO){
			str.append(next);
		}
		else{
			str.append(SPACE + next);
		}
		g2.setFont(FONT_PLAIN_20);
		g2.setColor(Color.BLACK);
		g2.drawString(str.toString(), SELECT_BUFFER, diff);
	}
}
