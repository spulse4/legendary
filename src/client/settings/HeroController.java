package client.settings;

import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.main.ClientManager;

public class HeroController extends Controller{
	
	private List<String> cards;
	
	public HeroController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}

	public HeroView getHeroView() {
		return (HeroView)super.getView();
	}

	public void start() {
		if(getHeroView().isModalShowing()){
			getHeroView().closeModal();
		}
		getHeroView().showModal();
		cards = getHeroView().getHeroes();
	}

	public void setHeroes(List<String> heroes) {
		getHeroView().setHeroes(heroes);
	}
	
	public void sendHeroes(){
		int gameID = ClientManager.getInstance().getGame().getGameID();
		List<String> heroes = getHeroView().getHeroes();
		ClientManager.getInstance().getServer().setHeroes(gameID, heroes);
	}
	
	public List<String> getCards(){
		return cards;
	}

	public void close() {
		getHeroView().closeModal();
	}

	@Override
	public void turnTrackerChanged(){
		setHeroes(ClientManager.getInstance().getModel().getTurnTracker().getHeroes());
	}

	@Override
	public void gameStarted(){
		if(getHeroView().isModalShowing()){
			getHeroView().closeModal();
		}
	}
}
