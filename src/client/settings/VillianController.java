package client.settings;

import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.main.ClientManager;

public class VillianController extends Controller{

	private List<String> cards;

	public VillianController(IView view) {
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
	}

	public VillianView getVillianView() {
		return (VillianView)super.getView();
	}

	public void start() {
		if(getVillianView().isModalShowing()){
			getVillianView().closeModal();
		}
		getVillianView().showModal();
		cards = getVillianView().getVillians();
	}

	public void setVillians(List<String> villians) {
		getVillianView().setVillians(villians);
	}

	public void sendVillians(){
		int gameID = ClientManager.getInstance().getGame().getGameID();
		List<String> villians = getVillianView().getVillians();
		ClientManager.getInstance().getServer().setVillians(gameID, villians);
	}

	public List<String> getCards(){
		return cards;
	}

	public void close() {
		getVillianView().closeModal();
	}

	@Override
	public void turnTrackerChanged(){
		List<String> villians = ClientManager.getInstance().getModel().getTurnTracker().getVillians();
		List<String> henchmen = ClientManager.getInstance().getModel().getTurnTracker().getHenchmen();
		villians.addAll(henchmen);
		
		setVillians(villians);
	}
	
	@Override
	public void gameStarted(){
		if(getVillianView().isModalShowing()){
			getVillianView().closeModal();
		}
	}
}