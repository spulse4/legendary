package client.settings;

import java.util.Map;

import client.base.OverlayView;

public abstract class AbstractSettingsView extends OverlayView implements ISettingView{

	private static final long serialVersionUID = -7539605616264345240L;

	protected Map<String, NameCard> cards;
	
	public void clicked(String name) {
		boolean selected = cards.get(name).isSelected();
		if(selected){
			cards.get(name).setSelected(false);
		}
		else{
			cards.get(name).setSelected(true);
		}
	}
}
