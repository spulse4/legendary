package client.settings;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import client.definitions.ClientVariables;

public class SchemeView extends AbstractSettingsView implements ClientVariables{

	private static final long serialVersionUID = 8507679531624027979L;

	public SchemeView(){
        this.setOpaque(true);
        this.setLayout(new BorderLayout());
        this.setBorder(BorderFactory.createLineBorder(Color.black, VIEW_BORDER_WIDTH));
		cards = new HashMap<String, NameCard>();
		cards.put(MIDTOWN_BANK_ROBBERY, new NameCard(MIDTOWN_BANK_ROBBERY_FIXED, MIDTOWN_BANK_ROBBERY, this));
		cards.put(NEGATIVE_ZONE_PRISON_BREAKOUT, new NameCard(NEGATIVE_ZONE_PRISON_BREAKOUT_FIXED, NEGATIVE_ZONE_PRISON_BREAKOUT, this));
		cards.put(PORTALS_TO_THE_DARK_DIMENSION, new NameCard(PORTALS_TO_THE_DARK_DIMENSION_FIXED, PORTALS_TO_THE_DARK_DIMENSION, this));
		cards.put(REPLACE_EARTHS_LEADERS_WITH_KILLBOTS, new NameCard(REPLACE_EARTHS_LEADERS_WITH_KILLBOTS_FIXED, REPLACE_EARTHS_LEADERS_WITH_KILLBOTS, this));
		cards.put(SECRET_INVASION_OF_THE_SKRULL_SHAPESHIFTERS, new NameCard(SECRET_INVASION_OF_THE_SKRULL_SHAPESHIFTERS_FIXED, SECRET_INVASION_OF_THE_SKRULL_SHAPESHIFTERS, this));
		cards.put(SUPER_HERO_CIVIL_WAR, new NameCard(SUPER_HERO_CIVIL_WAR_FIXED, SUPER_HERO_CIVIL_WAR, this));
		cards.put(THE_LEGACY_VIRUS, new NameCard(THE_LEGACY_VIRUS_FIXED, THE_LEGACY_VIRUS, this));
		cards.put(UNLEASH_THE_POWER_OF_THE_COSMIC_CUBE, new NameCard(UNLEASH_THE_POWER_OF_THE_COSMIC_CUBE_FIXED, UNLEASH_THE_POWER_OF_THE_COSMIC_CUBE, this));

		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.add(cards.get(MIDTOWN_BANK_ROBBERY));
		panel.add(cards.get(NEGATIVE_ZONE_PRISON_BREAKOUT));
		panel.add(cards.get(PORTALS_TO_THE_DARK_DIMENSION));
		panel.add(cards.get(REPLACE_EARTHS_LEADERS_WITH_KILLBOTS));
		panel.add(cards.get(SECRET_INVASION_OF_THE_SKRULL_SHAPESHIFTERS));
		panel.add(cards.get(SUPER_HERO_CIVIL_WAR));
		panel.add(cards.get(THE_LEGACY_VIRUS));
		panel.add(cards.get(UNLEASH_THE_POWER_OF_THE_COSMIC_CUBE));
		
		JScrollPane scroll1 = new JScrollPane(panel);
		scroll1.setPreferredSize(new Dimension(SELECT_SCROLL_WIDTH, SELECT_SCROLL_HEIGHT));
		scroll1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scroll1.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scroll1.getVerticalScrollBar().setUnitIncrement(SCROLL_SPEED);
		
		this.add(scroll1, BorderLayout.CENTER);
		JButton setScheme = new JButton(SELECT_SCHEME);
		setScheme.setFont(FONT_PLAIN_20);
		setScheme.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				getSchemeController().sendScheme();
				getSchemeController().close();
			}
			
		});
		JButton cancel = new JButton(BUTTON_CANCEL);
		cancel.setFont(FONT_PLAIN_20);
		cancel.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String card = getSchemeController().getCard();
				for(String str : cards.keySet()){
					cards.get(str).setSelected(false);
				}
				if(card != null){
					cards.get(card).setSelected(true);
				}
				getSchemeController().close();
			}
			
		});
		JPanel buttons = new JPanel();
		buttons.setLayout(new FlowLayout());
		buttons.add(setScheme);
		buttons.add(cancel);
		this.add(buttons, BorderLayout.SOUTH);
	}
	
	public SchemeController getSchemeController(){
		return (SchemeController)super.getController();
	}
	
	public void setScheme(String scheme) {
		for(String str : cards.keySet()){
			cards.get(str).setSelected(false);
		}
		if(scheme == null){
			return;
		}
		cards.get(scheme).setSelected(true);
	}

	public String getScheme() {
		for(String str : cards.keySet()){
			if(cards.get(str).isSelected()){
				return str;
			}
		}
		return null;
	}

	@Override
	public void clicked(String name) {
		for(String str : cards.keySet()){
			cards.get(str).setSelected(false);
		}
		cards.get(name).setSelected(true);
	}
}
