package client.main;

import java.io.FileNotFoundException;
import javax.swing.JFrame;

import client.definitions.ClientVariables;
import client.initialize.*;
import client.legendary.LegendaryPanel;
import client.login.*;
import client.preloader.Preloader;

public class Client extends JFrame implements ClientVariables{
	
	private static final long serialVersionUID = -5760265456256341253L;
	
	private LegendaryPanel legendaryPanel;
	private LoginController login;
	
	public static void main(String[] args) throws FileNotFoundException{	
		if(args.length == TWO){
			ClientManager.getInstance().setHost(args[ZERO]);
			ClientManager.getInstance().setPort(Integer.valueOf(args[ONE]));
		}
		else{
			ClientManager.getInstance().setHost(HOST);
			ClientManager.getInstance().setPort(PORT);
		}
		Preloader.getInstance();
		new Client();
		Preloader.getInstance().stop();
	}
	
	public Client(){
		ClientManager.getInstance().setMain(this);
		client.base.OverlayView.setWindow(this);
		
		this.setTitle(LEGENDARY);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		legendaryPanel = new LegendaryPanel();
		this.setContentPane(legendaryPanel);
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		
		if(!ClientManager.getInstance().resetting()){
			display();
		}
		else{
			pack();
		}
		new InitializeController(new InitializeView(), this).start();
	}
	
	public void startLogin(){
		login = new LoginController(new LoginView());
		try{
			Thread.sleep(INTERVAL);
		}
		catch(InterruptedException e){
			e.printStackTrace();
		}
		login.start();
	}
	
	private void display(){
		pack();
		setVisible(true);
	}
	
	public LoginController getLogin(){
		return login;
	}
}
