package client.main;

import java.util.List;
import java.util.Random;

import javax.swing.JButton;

import client.board.BoardComponent;
import client.board.LegendaryMap;
import client.communicator.ServerProxy;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.poller.IPoller;
import shared.definitions.GameHeader;
import shared.definitions.PlayerHeader;
import shared.definitions.UserHeader;
import shared.model.ModelFacade;

public class ClientManager implements ClientVariables{
    
	private static ClientManager instance;
	private int port;
	private int playerID;
	private int playerIndex;
	private boolean resetting = false;
	private String host;
	private String name;
	private Client main = null;
	private GameHeader game = null;
	private IPoller poller = null;
	private JButton chatButton = null;
	private JButton handButton = null;
	private BoardComponent board = null;
	private Random random;
	
	public static ClientManager getInstance(){
		if(instance == null){
			instance = new ClientManager();
		}
		return instance;
	}
	
	private ClientManager(){
		port = PORT;
		host = HOST;
		playerID = NEGATIVE_ONE;
		playerIndex = NEGATIVE_ONE;
		random = new Random(System.currentTimeMillis());
	}
	
	public void setPort(int port){
		this.port = port;
	}
		
	public void setHost(String host){
		this.host = host;
	}
	
	public String getURL(){
		return HTTP + host + COLON + port;
	}
	
	public void setMain(Client main){
		this.main = main;
	}
	
	public ServerProxy getServer(){
		return ServerProxy.getInstance();
	}
	
	public void setInfo(int playerID, String name){
		this.playerID = playerID;
		this.name = name;
	}
	
	public void setPlayerIndex(int playerIndex){
		this.playerIndex = playerIndex;
	}
		
	public void setGame(GameHeader game){
		this.game = game;
	}
	
	public GameHeader getGame(){
		return game;
	}
	
	public UserHeader getUser(){
		if(playerIndex == -1){
			return new UserHeader(playerID, name);
		}
		return new PlayerHeader(playerIndex, playerID, name);
	}
	
	public void setPoller(IPoller poller){
		this.poller = poller;
	}
	
	public IPoller getPoller(){
		return poller;
	}
	
	public ModelFacade getModel(){
		return ModelFacade.getInstance();
	}
	
	public void updatePlayers(List<PlayerHeader> players){
		game.setPlayers(players);
	}
	
	public void setChatButton(JButton chatButton){
		this.chatButton = chatButton;
	}
	
	public JButton getChatButton(){
		return chatButton;
	}
	
	public void setHandButton(JButton handButton){
		this.handButton = handButton;
	}
	
	public JButton getHandButton(){
		return handButton;
	}
	
	public void setBoard(BoardComponent board){
		this.board = board;
	}
	
	public BoardComponent getBoard(){
		return board;
	}
	
	public Random getRandom(){
		return random;
	}

	public void reset() {
		resetting = true;
		poller.stop();
		LegendaryMap<String, ClientCard> map = board.getImages();
		main.dispose();
		main = new Client();
		board.setImages(map);
		main.getLogin().startJoinGame();
		
		main.setVisible(true);
		resetting = false;
	}
	
	public boolean resetting(){
		return resetting;
	}
}
