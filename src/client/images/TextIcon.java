package client.images;

import java.awt.*;
import java.beans.*;
import java.util.*;
import javax.swing.*;

import client.definitions.ClientVariables;

public class TextIcon implements Icon, PropertyChangeListener, ClientVariables{
		
	public enum Layout{
		HORIZONTAL,
		VERTICAL;
	}

	private JComponent component;
	private Layout layout;
	private String text;
	private Font font;
	private Color foreground;
	private int padding;
	private int iconWidth;
	private int iconHeight;
	private String[] strings;
	private int[] stringWidths;

	public TextIcon(JComponent component, String text){
		this(component, text, Layout.HORIZONTAL);
	}

	public TextIcon(JComponent component, String text, Layout layout){
		this.component = component;
		this.layout = layout;
		setText(text);

		component.addPropertyChangeListener(FONT, this);
	}

	public Layout getLayout(){
		return layout;
	}

	public String getText(){
		return text;
	}

	public void setText(String text){
		this.text = text;

		calculateIconDimensions();
	}

	public Font getFont(){
		if(font == null)
			return component.getFont();
		else
			return font;
	}

	public void setFont(Font font){
		this.font = font;

		calculateIconDimensions();
	}

	public Color getForeground(){
		if(foreground == null)
			return component.getForeground();
		else
			return foreground;
	}

	public void setForeground(Color foreground){
		this.foreground = foreground;
		component.repaint();
	}

	public int getPadding(){
		return padding;
	}

	public void setPadding(int padding){
		this.padding = padding;

		calculateIconDimensions();
	}

	private void calculateIconDimensions(){
		Font font = getFont();
		FontMetrics fm = component.getFontMetrics(font);

		if(layout == Layout.HORIZONTAL){
		 	iconWidth = fm.stringWidth(text) +(padding * TWO);
		 	iconHeight = fm.getHeight();
		}
		else if(layout == Layout.VERTICAL){
			int maxWidth = ZERO;
			strings = new String[text.length()];
			stringWidths = new int[text.length()];

			for(int i = ZERO; i < text.length(); i++){
				strings[i] = text.substring(i, i + ONE);
				stringWidths[i] = fm.stringWidth(strings[i]);
				maxWidth = Math.max(maxWidth, stringWidths[i]);
			}

			iconWidth = maxWidth +((fm.getLeading() + TWO) * TWO);

			iconHeight =(fm.getHeight() - fm.getDescent()) * text.length();
			iconHeight += padding * TWO;
		}

		component.revalidate();
	}

	@Override
	public int getIconWidth(){
		return iconWidth;
	}

	@Override
	public int getIconHeight(){
		return iconHeight;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void paintIcon(Component c, Graphics g, int x, int y){
		Graphics2D g2 =(Graphics2D)g.create();

		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Map map =(Map)(toolkit.getDesktopProperty(DESKTOP_HINTS));

		if(map != null){
		    g2.addRenderingHints(map);
		}
		else
			g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
				RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

		g2.setFont(getFont());
		g2.setColor(getForeground());
		FontMetrics fm = g2.getFontMetrics();

		if(layout == Layout.HORIZONTAL){
			g2.translate(x, y +	fm.getAscent());
			g2.drawString(text, padding, ZERO);
		}
		else if(layout == Layout.VERTICAL){
			int offsetY = fm.getAscent() - fm.getDescent() + padding;
			int incrementY = fm.getHeight() - fm.getDescent();

			for(int i = ZERO; i < text.length(); i++){
				int offsetX = Math.round((getIconWidth() - stringWidths[i]) / FLOAT_TWO);
				g2.drawString(strings[i], x + offsetX, y + offsetY);
				offsetY += incrementY;
			}
		}

		g2.dispose();
	}

	public void propertyChange(PropertyChangeEvent e){
		if(font == null){
			calculateIconDimensions();
		}
	}
}
