package client.chat;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import client.base.*;
import client.definitions.ClientVariables;
import client.legendary.LeftPanel;
import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.model.Message;

public class ChatController extends Controller implements ClientVariables{

	private LeftPanel chatPanel;
	private List<Message> currentMessages;
	
	public ChatController(LeftPanel chatPanel, IView view){
		super(view);
		this.chatPanel = chatPanel;
		currentMessages = new ArrayList<Message>();
		ClientManager.getInstance().getModel().registerObserver(this);
	}

	public ChatView getChatView(){
		return (ChatView)super.getView();
	}

	public void sendMessage(String message){
		int gameID = ClientManager.getInstance().getGame().getGameID();
		int playerIndex = ((PlayerHeader)ClientManager.getInstance().getUser()).getIndex();
		ClientManager.getInstance().getServer().sendChat(gameID, playerIndex, message);
	}

	@Override
	public void modelChanged(){
		List<Message> messages = ClientManager.getInstance().getModel().getChat().getMessages();
		if(compare(messages)){
			return;
		}
		currentMessages.clear();
		for(Message m : messages){
			currentMessages.add(m);
		}
		List<LogEntry> entries = new ArrayList<LogEntry>();
		for(Message m : messages){
			int index = m.getIndex();
			String message = m.getMessage();
			Color color = null;
			switch(index){
			case ZERO:
				color = new Color(PURPLE_RED, PURPLE_GREEN, PURPLE_BLUE);
				break;
			case ONE:
				color = Color.RED;
				break;
			case TWO:
				color = Color.CYAN;
				break;
			case THREE:
				color = Color.YELLOW;
				break;
			case FOUR:
				color = Color.GREEN;
			}
			entries.add(new LogEntry(color, message));
		}
		getChatView().setEntries(entries);
		if(!chatPanel.isVisible()){
			ClientManager.getInstance().getChatButton().setBackground(Color.YELLOW);
		}
	}
	
	private boolean compare(List<Message> messages){
		if(messages.size() != currentMessages.size()){
			return false;
		}
		for(int i = ZERO; i < messages.size(); ++i){
			if(messages.get(i).getIndex() != currentMessages.get(i).getIndex()){
				return false;
			}
			if(!messages.get(i).getMessage().equals(currentMessages.get(i).getMessage())){
				return false;
			}
		}
		return true;
	}
}

