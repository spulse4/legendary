package client.chat;

import java.awt.Color;

public class LogEntry{
	
	private Color color;	
	private String message;
	
	public LogEntry(Color color, String message){
		this.color = color;
		this.message = message;
	}
	
	public Color getColor(){
		return color;
	}
	
	public void setColor(Color color){
		this.color = color;
	}
	
	public String getMessage(){
		return message;
	}
	
	public void setMessage(String message){
		this.message = message;
	}
}

