package client.chat;

import java.util.List;
import java.util.*;
import java.awt.*;
import java.awt.font.*;
import java.awt.geom.*;

import javax.swing.*;

import client.definitions.ClientVariables;
import client.utils.*;

@SuppressWarnings("serial")
public class LogComponent extends JComponent implements ClientVariables{	
	
	private Font font;
	
	private List<LogEntry> entries;
	
	public LogComponent(){		
		this.setBackground(Color.white);
		this.setOpaque(true);
		
		Font tmpFont = new JLabel(EMPTY).getFont();
		font = tmpFont.deriveFont(tmpFont.getStyle(), FONT_SIZE_24);
		
		setEntries(null);
	}
	
	public void setEntries(List<LogEntry> entries){		
		if(entries == null || entries.size() == ZERO){
			this.entries = new ArrayList<LogEntry>();
			this.entries.add(new LogEntry(Color.WHITE, LOG_COMPONENT_NO_MESSAGES));
		}
		else{
			this.entries = entries;
		}
		
		if (this.getWidth() > ZERO){
			updateSize(this.getWidth());
		}
	}
	
	private void updateSize(int width){
		int height = this.getPreferredHeight(width);		
		Dimension newSize = new Dimension(width, height);
		this.setPreferredSize(newSize);
		this.setSize(newSize);
	}
	
	private int getPreferredHeight(int width){
		Graphics2D g2 = ImageUtils.DEFAULT_IMAGE.createGraphics();
		
		int prefHeight = draw(g2, width);
		
		return prefHeight;		
	}

	@Override
	protected void paintComponent(Graphics g){		
		super.paintComponent(g);
		
		Graphics2D g2 = (Graphics2D)g;
		g2.setColor(Color.LIGHT_GRAY);
		
		g2.fillRect(ZERO, ZERO, this.getWidth(), this.getHeight());
		
		draw(g2, this.getWidth());
	}
	
	private int draw(Graphics2D g2, int width){		
		int y = ZERO;
		
		FontMetrics fontMetrics = this.getFontMetrics(font);
		FontRenderContext fontContext = fontMetrics.getFontRenderContext();
		
		g2.setFont(font);
		
		for (LogEntry entry : entries){		
			List<String> lines = wrapText(fontContext, entry.getMessage(), width);
			int rectHeight = LOG_COMPONENT_TOP_MARGIN + LOG_COMPONENT_BOTTOM_MARGIN + lines.size()
							 * fontMetrics.getHeight();
			
			g2.setColor(entry.getColor());
			g2.fillRect(ZERO, y, this.getWidth(), rectHeight);
			
			g2.setColor(Color.white);
			g2.drawRect(ZERO, y, this.getWidth(), rectHeight);
			
			g2.setColor(Color.black);
			
			y += LOG_COMPONENT_TOP_MARGIN + fontMetrics.getAscent();
			
			for (int i = ZERO; i < lines.size(); ++i){
				
				if(i > ZERO){
					y += fontMetrics.getHeight();
				}
				
				g2.drawString(lines.get(i), LOG_COMPONENT_LEFT_MARGIN, y);
			}
			
			y += fontMetrics.getDescent() + LOG_COMPONENT_BOTTOM_MARGIN;
		}
		
		return y;
	}
	
	private List<String> wrapText(FontRenderContext context, String text, int width){
		int MAX_WIDTH = width - LOG_COMPONENT_LEFT_MARGIN - LOG_COMPONENT_RIGHT_MARGIN;
		
		List<String> result = new ArrayList<String>();
		
		try(Scanner scanner = new Scanner(text)){			
			scanner.useDelimiter("\\s+");
			
			String line = EMPTY;
			
			while(scanner.hasNext()){				
				String word = scanner.next();
				
				if(line.length() == ZERO){
					// Each line must have at least one word (even if
					// it doesn't fit)
					line = word;
				}
				else{
					// Check to see if the line can fit another word
					String newLine = line + SPACE + word;
					
					Rectangle2D bounds = font.getStringBounds(newLine, context);
					if(bounds.getWidth() <= MAX_WIDTH){
						line = newLine;
					}
					else{
						result.add(line);
						line = word;
					}
				}
			}
			
			if(line.length() > ZERO){
				result.add(line.toString());
			}
		}
		
		return result;
	}
}

