package client.chat;

import client.base.PanelView;
import client.definitions.ClientVariables;

import java.awt.*;
import java.awt.event.*;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

@SuppressWarnings("serial")
public class ChatView extends PanelView implements ClientVariables{
    
    private LogComponent chatPanel;
    private JScrollPane chatScrollPane;
    private JPanel inputPanel;
    private JButton sendChatBtn;
    private PlaceholderTextField chatTextInput;

    public ChatView(){
        // Create the components
        chatPanel = new LogComponent();
        chatScrollPane = new JScrollPane(chatPanel);
		chatScrollPane.getVerticalScrollBar().setUnitIncrement(SCROLL_SPEED);
        inputPanel = new JPanel();
        sendChatBtn = new JButton(CHAT_SEND);
        chatTextInput = new PlaceholderTextField();
        chatTextInput.setPlaceholder(CHAT_SEND_A_MESSAGE);
        chatTextInput.setPreferredSize(new Dimension(CHAT_INPUT_WIDTH, CHAT_INPUT_HEIGHT));
        
        // Register the listeners
        EventListener listener = new EventListener();
        sendChatBtn.addActionListener(listener);
        chatTextInput.addKeyListener(listener);
        
        // Create the layout and add the components
        inputPanel.setLayout(new GridBagLayout());
        
        // Send Chat Button
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = ZERO;
        constraints.gridy = ZERO;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = CHAT_CHAT_INPUT_WEIGHT_X;
        constraints.weighty = CHAT_WEIGHT_Y;
        constraints.insets = new Insets(CHAT_INSETS, CHAT_INSETS, CHAT_INSETS, CHAT_INSETS);
        inputPanel.add(sendChatBtn, constraints);

        // Chat Input Field
        constraints = new GridBagConstraints();
        constraints.gridx = ONE;
        constraints.gridy = ZERO;
        constraints.gridwidth = TWO;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = CHAT_CHAT_INPUT_WEIGHT_X;
        constraints.weighty = CHAT_WEIGHT_Y;
        constraints.insets = new Insets(CHAT_INSETS, ZERO, CHAT_INSETS, ZERO);
        inputPanel.add(chatTextInput, constraints);
        
        setLayout(new BorderLayout());
        add(chatScrollPane, BorderLayout.CENTER);
        add(inputPanel, BorderLayout.PAGE_END);
        
        setBackground(Color.white);
    }

    public ChatController getChatController(){
        return (ChatController)super.getController();
    }

    public void setEntries(final List<LogEntry> entries){
        chatPanel.setEntries(entries);
    }
    
    private void sendMessage(){
        String message = chatTextInput.getText();
        if (!message.isEmpty()){
            getChatController().sendMessage(message);
            
             chatTextInput.setText(EMPTY);
        }
    }
    
    private class EventListener extends KeyAdapter implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e){
            sendMessage();
        }
        
        @Override
        public void keyReleased(KeyEvent e){
           if (e.getKeyCode() == KeyEvent.VK_ENTER){
                sendMessage();
            }
        }
    }
}


