package client.communicator;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import client.definitions.ClientVariables;
import client.main.ClientManager;

public class Communicator implements ClientVariables{
	
	private static Communicator communicator = null;
	
	public static Communicator getInstance(){
		if(communicator == null){
			communicator = new Communicator();
		}
		return communicator;
	}
	
	private Communicator(){}
	
	public JsonObject sendRequest(JsonObject o){
		try{
			String urlString = ClientManager.getInstance().getURL() + o.get(URL).getAsString();
			URL url = new URL(urlString);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod(o.get(REQUEST_TYPE).getAsString());
			con.setDoOutput(true);
			con.connect();
			
			OutputStreamWriter output = new OutputStreamWriter(con.getOutputStream());
			output.write(o.toString());
			output.flush();

			if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
				InputStream input = con.getInputStream();
				int len = ZERO;
				
				byte[] buffer = new byte[BYTE_SIZE];
				StringBuilder string = new StringBuilder();
				while(-1 != (len = input.read(buffer))){
					string.append(new String(buffer, ZERO, len));
				}
				JsonParser parser = new JsonParser();
				return parser.parse(string.toString()).getAsJsonObject();
			}
			else{
				return null;
			}
		}
		catch(IOException e){
			e.printStackTrace();
		}
		
		return null;
	}
}
