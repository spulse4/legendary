package client.communicator;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import client.definitions.ClientVariables;
import shared.definitions.GameHeader;
import shared.server.IServer;

public class ServerProxy implements IServer, ClientVariables{

	private static ServerProxy proxy = null;
	private Communicator communicator;

	public static ServerProxy getInstance(){
		if(proxy == null){
			proxy = new ServerProxy();
		}
		return proxy;
	}
		
	private ServerProxy(){
		communicator = Communicator.getInstance();
	}
	
	//ability-------------------------------------------------------------------
	public void battlefieldPromotionAbility(int gameID, String card){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(CARD, card);
		o.addProperty(URL, ABILITY_BATTLEFIELD_PROMOTION_ABILITY);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void copyPowersAbility(int gameID, String card){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(CARD, card);
		o.addProperty(URL, ABILITY_COPY_POWERS_ABILITY);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void coveringFireAbility(int gameID, boolean draw){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(DRAW, draw);
		o.addProperty(URL, ABILITY_COVERING_FIRE_ABILITY);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void coveringFireDiscard(int gameID, int playerIndex, String card){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(PLAYER_INDEX, playerIndex);
		o.addProperty(CARD, card);
		o.addProperty(URL, ABILITY_COVERING_FIRE_DISCARD);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void cyclopsAbility(int gameID, String card){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(CARD, card);
		o.addProperty(URL, ABILITY_CYCLOPS_ABILITY);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void dangerousRescueAbility(int gameID, String card){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(CARD, card);
		o.addProperty(URL, ABILITY_DANGEROUS_RESCUE_ABILITY);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void energyDrainAbility(int gameID, String card){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(CARD, card);
		o.addProperty(URL, ABILITY_ENERGY_DRAIN_ABILITY);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void healingFactorAbility(int gameID, boolean accept){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(ACCEPT, accept);
		o.addProperty(URL, ABILITY_HEALING_FACTOR_ABILITY);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void hereHoldThisForASecondAbility(int gameID, int location){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(LOCATION, location);
		o.addProperty(URL, ABILITY_HERE_HOLD_THIS_FOR_A_SECOND_ABILITY);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void heyCanIGetADoOverAbility(int gameID, boolean accept){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(ACCEPT, accept);
		o.addProperty(URL, ABILITY_HEY_CAN_I_GET_A_DO_OVER_ABILITY);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void hypnoticCharmAbility(int gameID, int playerIndex, boolean discard){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(PLAYER_INDEX, playerIndex);
		o.addProperty(DISCARD, discard);
		o.addProperty(URL, ABILITY_HYPNOTIC_CHARM_ABILITY);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void pureFuryAbility(int gameID, int cardIndex){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(CARD_INDEX, cardIndex);
		o.addProperty(URL, ABILITY_PURE_FURY_ABILITY);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void randomActsOfUnkindnessAbility(int gameID, boolean accept){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(ACCEPT, accept);
		o.addProperty(URL, ABILITY_RANDOM_ACTS_OF_UNKINDNESS_ABILITY);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void randomActsOfUnkindnessPass(int gameID, int playerIndex, String card){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(PLAYER_INDEX, playerIndex);
		o.addProperty(CARD, card);
		o.addProperty(URL, ABILITY_RANDOM_ACTS_OF_UNKINDNESS_PASS);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void shadowedThoughtsAbility(int gameID, boolean accept){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(ACCEPT, accept);
		o.addProperty(URL, ABILITY_SHADOWED_THOUGHTS_ABILITY);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void silentSniperAbility(int gameID, int location){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(LOCATION, location);
		o.addProperty(URL, ABILITY_SILENT_SNIPER_ABILITY);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void spinningCycloneAbility(int gameID, int startingLocation, int endingLocation){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(STARTING_LOCATION, startingLocation);
		o.addProperty(ENDING_LOCATION, endingLocation);
		o.addProperty(URL, ABILITY_SPINNING_CYCLONE_ABILITY);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void stackTheDeckAbility(int gameID, String card){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(CARD, card);
		o.addProperty(URL, ABILITY_STACK_THE_DECK_ABILITY);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void theAmazingSpidermanAbility(int gameID, String card){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(CARD, card);
		o.addProperty(URL, ABILITY_THE_AMAZING_SPIDERMAN_ABILITY);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void unstoppableHulkAbility(int gameID, boolean accept){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(ACCEPT, accept);
		o.addProperty(URL, ABILITY_UNSTOPPABLE_HULK_ABILITY);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	//ambush--------------------------------------------------------------------
	public void juggernautAmbush(int gameID, int playerIndex, String card){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(PLAYER_INDEX, playerIndex);
		o.addProperty(CARD, card);
		o.addProperty(URL, AMBUSH_JUGGERNAUT_AMBUSH);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	//chat----------------------------------------------------------------------
	public void sendChat(int gameID, int playerIndex, String message){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(PLAYER_INDEX, playerIndex);
		o.addProperty(MESSAGE, message);
		o.addProperty(URL, CHAT_SEND_CHAT);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	//escape--------------------------------------------------------------------
	public void destroyerEscape(int gameID, int playerIndex, String card){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(PLAYER_INDEX, playerIndex);
		o.addProperty(CARD, card);
		o.addProperty(URL, ESCAPE_DESTROYER_ESCAPE);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void juggernautEscape(int gameID, int playerIndex, String card){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(PLAYER_INDEX, playerIndex);
		o.addProperty(CARD, card);
		o.addProperty(URL, ESCAPE_JUGGERNAUT_ESCAPE);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	//fight---------------------------------------------------------------------
	public void bitterCaptorFight(int gameID, int location){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(LOCATION, location);
		o.addProperty(URL, FIGHT_BITTER_CAPTOR_FIGHT);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void cruelRulerFight(int gameID, int location){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(LOCATION, location);
		o.addProperty(URL, FIGHT_CRUEL_RULER_FIGHT);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void darkTechnologyFight(int gameID, int location){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(LOCATION, location);
		o.addProperty(URL, FIGHT_DARK_TECHNOLOGY_FIGHT);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void doombotLegionFight(int gameID, String card){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(CARD, card);
		o.addProperty(URL, FIGHT_DOOMBOT_LEGION_FIGHT);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void electromagneticBubbleFight(int gameID, String card){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(CARD, card);
		o.addProperty(URL, FIGHT_ELECTROMAGNETIC_BUBBLE_FIGHT);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void hydraKidnappersFight(int gameID, boolean accept){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(ACCEPT, accept);
		o.addProperty(URL, FIGHT_HYDRA_KIDNAPPERS_FIGHT);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void maestroFight(int gameID, String card){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(CARD, card);
		o.addProperty(URL, FIGHT_MAESTRO_FIGHT);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void maniacalTyrantFight(int gameID, String card){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(CARD, card);
		o.addProperty(URL, FIGHT_MANIACAL_TYRANT_FIGHT);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void melterFight(int gameID, int playerIndex, boolean ko){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(PLAYER_INDEX, playerIndex);
		o.addProperty(KO, ko);
		o.addProperty(URL, FIGHT_MELTER_FIGHT);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void monarchsDecreeFight(int gameID, boolean draw){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(DRAW, draw);
		o.addProperty(URL, FIGHT_MONARCHS_DECREE_FIGHT);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void monarchsDecreeDiscard(int gameID, int playerIndex, String card){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(PLAYER_INDEX, playerIndex);
		o.addProperty(CARD, card);
		o.addProperty(URL, FIGHT_MONARCHS_DECREE_DISCARD);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void paibokThePowerSkrullFight(int gameID, int playerIndex, int location){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(PLAYER_INDEX, playerIndex);
		o.addProperty(LOCATION, location);
		o.addProperty(URL, FIGHT_PAIBOK_THE_POWER_SKRULL_FIGHT);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void ruthlessDictatorDiscard(int gameID, String card){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(CARD, card);
		o.addProperty(URL, FIGHT_RUTHLESS_DICTATOR_DISCARD);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void ruthlessDictatorKO(int gameID, String card){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(CARD, card);
		o.addProperty(URL, FIGHT_RUTHLESS_DICTATOR_KO);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void sentinelFight(int gameID, String card){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(CARD, card);
		o.addProperty(URL, FIGHT_SENTINEL_FIGHT);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void superSkrullFight(int gameID, int playerIndex, String card){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(PLAYER_INDEX, playerIndex);
		o.addProperty(CARD, card);
		o.addProperty(URL, FIGHT_SUPER_SKRULL_FIGHT);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void vanishingIllusionsFight(int gameID, int playerIndex, String card){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(PLAYER_INDEX, playerIndex);
		o.addProperty(CARD, card);
		o.addProperty(URL, FIGHT_VANISHING_ILLUSIONS_FIGHT);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void whirlwindFight(int gameID, String card){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(CARD, card);
		o.addProperty(URL, FIGHT_WHIRLWIND_FIGHT);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void ymirFrostGiantKingFight(int gameID, int playerIndex){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(PLAYER_INDEX, playerIndex);
		o.addProperty(URL, FIGHT_YMIR_FROST_GIANT_KING_FIGHT);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void ymirFrostGiantKingDiscard(int gameID, int playerIndex, int number){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(PLAYER_INDEX, playerIndex);
		o.addProperty(NUMBER, number);
		o.addProperty(URL, FIGHT_YMIR_FROST_GIANT_KING_DISCARD);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	//game----------------------------------------------------------------------
	@Override
	public int createGame(String title){
		JsonObject o = new JsonObject();
		o.addProperty(TITLE, title);
		o.addProperty(URL, GAME_CREATE);
		o.addProperty(REQUEST_TYPE, POST);
		JsonObject returned = communicator.sendRequest(o);
		if(returned == null){
			return NEGATIVE_ONE;
		}
		return returned.get(GAME_ID).getAsInt();
	}	
	@SuppressWarnings("unchecked")	
	@Override
	public List<GameHeader> getGames(){
		JsonObject o = new JsonObject();
		o.addProperty(URL, GAME_GET);
		o.addProperty(REQUEST_TYPE, POST);
		JsonObject returned = communicator.sendRequest(o);
		if(returned == null){
			return null;
		}
		Gson gson = new Gson();
		List<GameHeader> games = new ArrayList<GameHeader>();
		ArrayList<Object> temp = gson.fromJson(returned.get(GAMES), ArrayList.class);
		for(Object obj : temp){
			GameHeader game = gson.fromJson(gson.toJson(obj), GameHeader.class);
			games.add(game);
		}
		return games;
	}
	@Override
	public JsonObject getModel(int gameID, int version){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(VERSION_ID, version);
		o.addProperty(URL, GAME_GET_MODEL);
		o.addProperty(REQUEST_TYPE, POST);
		return communicator.sendRequest(o);
	}
	@Override
	public int joinGame(int gameID, int playerID, String playerName){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(PLAYER_ID, playerID);
		o.addProperty(PLAYER_NAME, playerName);
		o.addProperty(URL, GAME_JOIN);
		o.addProperty(REQUEST_TYPE, POST);
		JsonObject returned = communicator.sendRequest(o);
		if(returned == null){
			return NEGATIVE_ONE;
		}
		return returned.get(INDEX).getAsInt();
	}	
	@Override
	public void setHeroes(int gameID, List<String> heroes) {
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.add(HEROES, new Gson().toJsonTree(heroes));
		o.addProperty(URL, GAME_SET_HEROES);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	@Override
	public void setMastermind(int gameID, String mastermind) {
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(MASTERMIND, mastermind);
		o.addProperty(URL, GAME_SET_MASTERMIND);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	@Override
	public void setScheme(int gameID, String scheme) {
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(SCHEME, scheme);
		o.addProperty(URL, GAME_SET_SCHEME);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	@Override
	public void setVillians(int gameID, List<String> villians) {
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.add(VILLIANS, new Gson().toJsonTree(villians));
		o.addProperty(URL, GAME_SET_VILLIANS);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	@Override
	public void startGame(int gameID){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(URL, GAME_START);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	//masterstrike--------------------------------------------------------------
	public void drDoomMasterstrike(int gameID, int playerIndex, String card){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(PLAYER_INDEX, playerIndex);
		o.addProperty(CARD, card);
		o.addProperty(URL, MASTERSTRIKE_DR_DOOM_MASTERSTRIKE);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void magnetoMasterstrike(int gameID, int playerIndex, String card){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(PLAYER_INDEX, playerIndex);
		o.addProperty(CARD, card);
		o.addProperty(URL, MASTERSTRIKE_MAGNETO_MASTERSTRIKE);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	public void redSkullMasterstrike(int gameID, int playerIndex, String card){
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(PLAYER_INDEX, playerIndex);
		o.addProperty(CARD, card);
		o.addProperty(URL, MASTERSTRIKE_RED_SKULL_MASTERSTRIKE);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	//moves---------------------------------------------------------------------
	@Override
	public void attackCard(int gameID, int cardIndex) {
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(CARD_INDEX, cardIndex);
		o.addProperty(URL, MOVES_ATTACK_CARD);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	@Override
	public void buyCard(int gameID, int playerIndex, int cardIndex) {
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(PLAYER_INDEX, playerIndex);
		o.addProperty(CARD_INDEX, cardIndex);
		o.addProperty(URL, MOVES_BUY_CARD);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	@Override
	public void endTrun(int gameID, int playerIndex) {
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(PLAYER_INDEX, playerIndex);
		o.addProperty(URL, MOVES_END_TURN);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	@Override
	public void playCard(int gameID, String name) {
		JsonObject o = new JsonObject();
		o.addProperty(GAME_ID, gameID);
		o.addProperty(NAME, name);
		o.addProperty(URL, MOVES_PLAY_CARD);
		o.addProperty(REQUEST_TYPE, POST);
		communicator.sendRequest(o);
	}
	//users---------------------------------------------------------------------
	@Override
	public int login(String username, String password) {
		JsonObject o = new JsonObject();
		o.addProperty(USERNAME, username);
		o.addProperty(PASSWORD, password);
		o.addProperty(URL, USER_LOGIN);
		o.addProperty(REQUEST_TYPE, POST);
		JsonObject returned = communicator.sendRequest(o);
		if(returned == null){
			return NEGATIVE_ONE;
		}
		return returned.get(PLAYER_ID).getAsInt();
	}
	@Override
	public int register(String username, String password){
		JsonObject o = new JsonObject();
		o.addProperty(USERNAME, username);
		o.addProperty(PASSWORD, password);
		o.addProperty(URL, USER_REGISTER);
		o.addProperty(REQUEST_TYPE, POST);
		JsonObject returned = communicator.sendRequest(o);
		if(returned == null){
			return NEGATIVE_ONE;
		}
		return returned.get(PLAYER_ID).getAsInt();
	}
}
