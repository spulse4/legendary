package client.base;

public interface IOverlayView extends IView{
	
	void showModal();
	
	void closeModal();
	
	boolean isModalShowing();
}

