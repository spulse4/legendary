package client.base;

public interface IView{
	
	void setController(IController controller);
	
	IController getController();
}

