package client.base;

public interface IController{
	
	IView getView();
	
	void playersChanged();
	
	void turnTrackerChanged();
	
	void gameStarted();
	
	void modelChanged();
}

