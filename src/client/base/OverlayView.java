package client.base;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;

@SuppressWarnings("serial")
public class OverlayView extends PanelView implements IOverlayView{

	private static Component defaultGlassPane;
	private static JFrame window;
	private static List<OverlayInfo> overlayStack;
	
	public static void setWindow(JFrame window){
		OverlayView.window = window;
		defaultGlassPane = window.getGlassPane();
		overlayStack = new ArrayList<OverlayInfo>();
	}
	
	public OverlayView(){
		super();
	}
	
	public void showModal(){
		if(isModalShowing()){
			return;
		}
		// Open the new overlay
		JPanel overlayPanel = new JPanel();
		overlayPanel.setLayout(new BorderLayout());
		overlayPanel.setOpaque(false);
		
		// Discard all mouse and keyboard events
		MouseAdapter mouseAdapter = new MouseAdapter(){};
		overlayPanel.addMouseListener(mouseAdapter);
		overlayPanel.addMouseMotionListener(mouseAdapter);
		overlayPanel.addMouseWheelListener(mouseAdapter);
		
		KeyAdapter keyAdapter = new KeyAdapter(){};
		overlayPanel.addKeyListener(keyAdapter);
		
		Dimension winSize = window.getContentPane().getSize();
		Dimension prefSize = this.getPreferredSize();
		
		int widthDiff = (int)(winSize.getWidth() - prefSize.getWidth());
		int heightDiff = (int)(winSize.getHeight() - prefSize.getHeight());
		
		overlayPanel.add(this, BorderLayout.CENTER);
		if(widthDiff / 2 > 0){
			overlayPanel.add(Box.createRigidArea(new Dimension(widthDiff / 2, 0)),
							 BorderLayout.WEST);
			overlayPanel.add(Box.createRigidArea(new Dimension(widthDiff / 2, 0)),
							 BorderLayout.EAST);
		}
		if(heightDiff / 2 > 0){
			overlayPanel.add(Box.createRigidArea(new Dimension(0,
															   heightDiff / 2)),
							 BorderLayout.NORTH);
			overlayPanel.add(Box.createRigidArea(new Dimension(0,
															   heightDiff / 2)),
							 BorderLayout.SOUTH);
		}
		
		if(overlayStack.size() > 0){
			for(OverlayInfo info : overlayStack){
				info.getOverlayPanel().setVisible(false);
			}
		}
		
		window.setGlassPane(overlayPanel);
		overlayPanel.setVisible(true);
		overlayStack.add(new OverlayInfo(this, overlayPanel));
	}
	
	public void closeModal(){
		if(overlayStack.size() > 0){
			for(OverlayInfo info : overlayStack){
				if(info.getOverlayView() == this){
					overlayStack.remove(overlayStack.indexOf(info));
					info.getOverlayPanel().setVisible(false);
					break;
				}
			}
			
			if(overlayStack.size() > 0){
				window.setGlassPane(overlayStack.get(overlayStack.size() - 1).getOverlayPanel());
				overlayStack.get(overlayStack.size() - 1).getOverlayPanel().setVisible(true);
			}
			else{
				window.setGlassPane(defaultGlassPane);
				window.getGlassPane().setVisible(false);
			}
		}
	}
	
	@Override
	public boolean isModalShowing(){		
		for (OverlayInfo info : overlayStack){
			if(info.getOverlayView() == this){
				return true;
			}
		}
		
		return false;
	}
	
	private static class OverlayInfo{
		private OverlayView overlayView;
		private JPanel overlayPanel;
		
		public OverlayInfo(OverlayView overlayView, JPanel overlayPanel){
			setOverlayView(overlayView);
			setOverlayPanel(overlayPanel);
		}
		
		public OverlayView getOverlayView(){
			return overlayView;
		}
		
		public void setOverlayView(OverlayView overlayView){
			this.overlayView = overlayView;
		}
		
		public JPanel getOverlayPanel(){
			return overlayPanel;
		}
		
		public void setOverlayPanel(JPanel overlayPanel){
			this.overlayPanel = overlayPanel;
		}
	}
}

