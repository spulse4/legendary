package client.base;

public abstract class Controller implements IController{
	
	private IView view;
	
	protected Controller(IView view){
		setView(view);
		getView().setController(this);
	}
	
	private void setView(IView view){
		this.view = view;
	}
	
	@Override
	public IView getView(){
		return this.view;
	}
	
	@Override
	public void playersChanged(){}
	
	@Override
	public void turnTrackerChanged(){}
	
	@Override
	public void gameStarted(){}
	
	@Override
	public void modelChanged(){}
}

