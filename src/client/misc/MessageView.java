package client.misc;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import client.base.*;
import client.definitions.ClientVariables;

@SuppressWarnings("serial")
public class MessageView extends OverlayView implements ClientVariables{

	private JLabel label;
	private JButton closeButton;
	private JPanel buttonPanel;
	private JLabel message;
	
	public MessageView(){
		this.setOpaque(true);
		this.setLayout(new BorderLayout());
		this.setBorder(BorderFactory.createLineBorder(Color.black, VIEW_BORDER_WIDTH));
		
		label = new JLabel(MESSAGE_VIEW);
		Font labelFont = label.getFont();
		labelFont = labelFont.deriveFont(labelFont.getStyle(), TEXT_SIZE_LABEL);
		label.setFont(labelFont);
		this.add(label, BorderLayout.NORTH);
		
		message = new JLabel(MESSAGE_BODY);
		this.add(message, BorderLayout.CENTER);
		
		closeButton = new JButton(BUTTON_CANCEL);
		closeButton.addActionListener(actionListener);
		Font buttonFont = closeButton.getFont();
		buttonFont = buttonFont.deriveFont(buttonFont.getStyle(), TEXT_SIZE_BUTTON);
		closeButton.setFont(buttonFont);
		
		buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));		
		buttonPanel.add(closeButton);		
		this.add(buttonPanel, BorderLayout.SOUTH);
	}

	private ActionListener actionListener = new ActionListener(){
		@Override
		public void actionPerformed(ActionEvent e){
			
			if (e.getSource() == closeButton){
				closeModal();
			}
		}	
	};

	public void setTitle(String title){
		label.setText(title);
	}

	public void setMessage(String message){
		this.message.setText(String.format(MESSAGE_HTML, MESSAGE_SIZE, message));
	}
}


