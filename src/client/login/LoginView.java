package client.login;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import client.base.*;
import client.definitions.ClientVariables;
import java.awt.GridLayout;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

@SuppressWarnings({"serial"})
public class LoginView extends OverlayView implements ClientVariables{
    
    private SignInPanel signInPanel = null;
    private RegisterPanel registerPanel = null;

    public LoginView(){
        this.setOpaque(true);
        this.setLayout(new BorderLayout());
        this.setBorder(BorderFactory.createLineBorder(Color.black, VIEW_BORDER_WIDTH));

        initComponents();
    }

     public LoginController getLoginController(){
        return (LoginController) super.getController();
    }

    private void initComponents(){
        JComponent left = initLeftComponents();
        JComponent middle = initMiddleComponents();
        JComponent right = initRightComponents();

        this.setLayout(new GridLayout(ONE, THREE));

        this.add(left);
        this.add(middle);
        this.add(right);
    }

    private JComponent initLeftComponents(){
    	JPanel left = new JPanel();
    	left.setLayout(new BorderLayout());
        JPanel leftPanel = new JPanel(new GridLayout(TWO, ONE));

        leftPanel.setBorder(createBufferBorder());

        JLabel lblTitle = new JLabel(HTML_BODY + LEGENDARY + HTML_BODY_CLOSE);
        Font labelFont = lblTitle.getFont();
        labelFont = labelFont.deriveFont(labelFont.getStyle(), BIG_LABEL_TEXT_SIZE);
        lblTitle.setFont(labelFont);

        leftPanel.add(lblTitle);

        left.add(leftPanel, BorderLayout.CENTER);
        JLabel label = new JLabel(VERSION, SwingConstants.LEFT);
        label.setFont(FONT_PLAIN_15);
        left.add(label, BorderLayout.SOUTH);
        return left;
    }

    private JComponent initMiddleComponents(){
        signInPanel = new SignInPanel();
        signInPanel.setBorder(createBufferBorder());
        return signInPanel;
    }

    private JComponent initRightComponents(){
        registerPanel = new RegisterPanel();
        registerPanel.setBorder(createBufferBorder());
        return registerPanel;
    }

    private Border createBufferBorder(){
        final int BUFFER_SPACE = 15;
        Border innerBuffer = BorderFactory.createEmptyBorder(BUFFER_SPACE, BUFFER_SPACE, BUFFER_SPACE, BUFFER_SPACE);
        Border outerBuffer = BorderFactory.createEmptyBorder(BUFFER_SPACE, BUFFER_SPACE, BUFFER_SPACE, BUFFER_SPACE);
        Border etching = BorderFactory.createEtchedBorder();

        Border outerCompound = BorderFactory.createCompoundBorder(outerBuffer, etching);
        Border wholeCompound = BorderFactory.createCompoundBorder(outerCompound, innerBuffer);

        return wholeCompound;
    }

    public String getLoginUsername(){
        return signInPanel.txtUsername.getText();
    }

    public String getLoginPassword(){
        return signInPanel.txtPassword.getText();
    }

    public String getRegisterUsername(){
        return registerPanel.txtUsername.getText();
    }

    public String getRegisterPassword(){
        return registerPanel.txtPassword.getText();
    }

    public String getRegisterPasswordRepeat(){
        return registerPanel.txtPasswordAgain.getText();
    }

    private class SignInPanel extends JPanel{
        private JLabel lblLogin = null;
        private JLabel lblUsername = null;
        private JTextField txtUsername = null;
        private JLabel lblPassword = null;
        private JTextField txtPassword = null;
        private JButton btnSignIn = null;

        public SignInPanel(){
            initComponents();
            initLayout();
            initEventListeners();
        }

        private void initComponents(){
            lblLogin = new JLabel(LOGIN);
            Font labelFont = lblLogin.getFont();
            labelFont = labelFont.deriveFont(labelFont.getStyle(), TEXT_SIZE_LABEL);
            lblLogin.setFont(labelFont);

            final int NUM_TXT_COLS = 16;

            lblUsername = new JLabel(USERNAME);
            txtUsername = new JTextField(NUM_TXT_COLS);
            lblPassword = new JLabel(PASSWORD_STRING);
            txtPassword = new JPasswordField(NUM_TXT_COLS);

            btnSignIn = new JButton(LOGIN);
        }

        private void initLayout(){
            this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

            this.add(lblLogin);

            this.add(Box.createVerticalGlue());

            JPanel internalInputBox = new JPanel(new GridLayout(FOUR, ONE));
            //Change the font on the user entry labels.
            Font smallTextFont = lblUsername.getFont();
            smallTextFont = smallTextFont.deriveFont(smallTextFont.getStyle(), SMALL_LABEL_TEXT_SIZE);
            lblUsername.setFont(smallTextFont);
            txtUsername.setFont(smallTextFont);
            lblPassword.setFont(smallTextFont);
            txtPassword.setFont(smallTextFont);

            internalInputBox.add(lblUsername);
            internalInputBox.add(txtUsername);
            internalInputBox.add(lblPassword);
            internalInputBox.add(txtPassword);
            this.add(internalInputBox);

            this.add(Box.createVerticalGlue());

            Font btnFont = btnSignIn.getFont();
            btnFont = btnFont.deriveFont(btnFont.getStyle(), TEXT_SIZE_BUTTON);
            btnSignIn.setFont(btnFont);
            this.add(btnSignIn);

        }

        private void initEventListeners(){
            btnSignIn.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e){
                    getLoginController().signIn();
                }
            });
        }
    }

    private class RegisterPanel extends JPanel{

        private JLabel lblRegister = null;
        private JLabel lblUsername = null;
        private JTextField txtUsername = null;
        private JLabel lblPassword = null;
        private JTextField txtPassword = null;
        private JLabel lblPasswordAgain = null;
        private JTextField txtPasswordAgain = null;
        private JButton btnRegister = null;

        public RegisterPanel(){
            initComponents();
            initTooltips();
            initLayout();
            initEventListeners();
        }

        private void initComponents(){
            lblRegister = new JLabel(REGISTER);
            Font labelFont = lblRegister.getFont();
            labelFont = labelFont.deriveFont(labelFont.getStyle(), TEXT_SIZE_LABEL);
            lblRegister.setFont(labelFont);

            final int NUM_TXT_COLS = 16;

            lblUsername = new JLabel(USERNAME_STRING);
            txtUsername = new JTextField(NUM_TXT_COLS);
            lblPassword = new JLabel(PASSWORD_STRING);
            txtPassword = new JPasswordField(NUM_TXT_COLS);
            lblPasswordAgain = new JLabel(PASSWORD_AGAIN);
            txtPasswordAgain = new JPasswordField(NUM_TXT_COLS);

            btnRegister = new JButton(REGISTER);
        }

        private void initTooltips(){
            txtUsername.setToolTipText(TOOL_TIP_USERNAME);
            txtPassword.setToolTipText(TOOL_TIP_PASSWORD);
            txtPasswordAgain.setToolTipText(TOOL_TIP_PASSWORD_AGAIN);
        }

        private void initLayout(){
            this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

            this.add(lblRegister);

            this.add(Box.createVerticalGlue());

            JPanel internalInputBox = new JPanel(new GridLayout(SIX, ONE));
            //Change the font on the user entry labels.
            Font smallTextFont = lblUsername.getFont();
            smallTextFont = smallTextFont.deriveFont(smallTextFont.getStyle(), SMALL_LABEL_TEXT_SIZE);
            lblUsername.setFont(smallTextFont);
            txtUsername.setFont(smallTextFont);
            lblPassword.setFont(smallTextFont);
            txtPassword.setFont(smallTextFont);
            lblPasswordAgain.setFont(smallTextFont);
            txtPasswordAgain.setFont(smallTextFont);

            internalInputBox.add(lblUsername);
            internalInputBox.add(txtUsername);
            internalInputBox.add(lblPassword);
            internalInputBox.add(txtPassword);
            internalInputBox.add(lblPasswordAgain);
            internalInputBox.add(txtPasswordAgain);
            this.add(internalInputBox);

            this.add(Box.createVerticalGlue());

            Font btnFont = btnRegister.getFont();
            btnFont = btnFont.deriveFont(btnFont.getStyle(), TEXT_SIZE_BUTTON);
            btnRegister.setFont(btnFont);

            this.add(btnRegister);
        }

        private void initEventListeners(){
            btnRegister.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e){
                    getLoginController().register();
                }
            });

            TextFieldValidator usernameValidator = new TextFieldValidator(txtUsername){
                @Override
                public boolean validateContents(String username){
                    final int MIN_UNAME_LENGTH = LOGIN_MIN_LENGTH;
                    final int MAX_UNAME_LENGTH = LOGIN_MAX_LENGTH;

                    if (username.length() < MIN_UNAME_LENGTH
                            || username.length() > MAX_UNAME_LENGTH){
                        return false;
                    }
                    else{
                        for (char c : username.toCharArray()){
                            if (!Character.isLetterOrDigit(c)
                                    && c != UNDERSCORE && c != DASH){
                                return false;
                            }
                        }
                    }

                    return true;
                }
            };

            TextFieldValidator passValidator = new TextFieldValidator(txtPassword){
                @Override
                public boolean validateContents(String input){

                    for (char c : input.toCharArray()){
                        if (!Character.isLetterOrDigit(c)
                                && c != UNDERSCORE && c != DASH){
                            return false;
                        }
                    }
                    return true;
                }
            };

            TextFieldValidator passAgainValidator = new TextFieldValidator(txtPasswordAgain){
                @Override
                public boolean validateContents(String input){
                    return input.equals(txtPassword.getText());
                }
            };
            
            txtUsername.addFocusListener(usernameValidator);
            txtUsername.getDocument().addDocumentListener(usernameValidator);
            
            txtPassword.addFocusListener(passValidator);
            txtPassword.getDocument().addDocumentListener(passValidator);
            
            txtPasswordAgain.addFocusListener(passAgainValidator);
            txtPasswordAgain.getDocument().addDocumentListener(passAgainValidator);

        }
    }

    private static abstract class TextFieldValidator implements DocumentListener, FocusListener{

        public abstract boolean validateContents(String input);

        private JTextField textFieldValidate = null;
        private Border originalBorder = null;
        private Border redBorder = null;

        public TextFieldValidator(JTextField textFieldValidate){
            this.textFieldValidate = textFieldValidate;
            originalBorder = textFieldValidate.getBorder();
            redBorder = BorderFactory.createLineBorder(Color.RED, TWO);
        }

        @Override
        public void focusGained(FocusEvent e){
            validateInput();
        }

        @Override
        public void focusLost(FocusEvent e){
            validateInput();
        }

        @Override
        public void insertUpdate(DocumentEvent e){
            validateInput();
        }

        @Override
        public void removeUpdate(DocumentEvent e){
            validateInput();
        }

        @Override
        public void changedUpdate(DocumentEvent e){
            validateInput();
        }

        private void validateInput(){
            String contents = textFieldValidate.getText();

            if (validateContents(contents)){
                textFieldValidate.setBorder(originalBorder);
            }
            else{
                Border errorBorder = BorderFactory.createCompoundBorder(originalBorder, redBorder);
                textFieldValidate.setBorder(errorBorder);
            }
        }
    }
}

