package client.login;

import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import client.base.*;
import client.definitions.ClientVariables;
import client.join.JoinGameController;
import client.join.JoinGameView;
import client.main.ClientManager;
import client.misc.*;

public class LoginController extends Controller implements ClientVariables{
	
	private MessageView messageView;
	private JoinGameController join;
	
	public LoginController(IView view){
		super(view);
		messageView = new MessageView();
	}

	private LoginView getLoginView(){
		return (LoginView)super.getView();
	}

	public void start(){
		getLoginView().showModal();
	}

	public void signIn(){
		String username = getLoginView().getLoginUsername();
		String password = getLoginView().getLoginPassword();
		try{
			if(!password.equals(EMPTY)){
				password = encrypt(password);
			}
			int id = ClientManager.getInstance().getServer().login(username, password);
			if(id != NEGATIVE_ONE){
				ClientManager.getInstance().setInfo(id, username);
				startJoinGame();
			}
			else{
				messageView.setTitle(ERROR_GENERIC_TITLE);
				messageView.setMessage(ERROR_INVALID_CREDENTIALS);
				messageView.showModal();
			}
			return;
		}
		catch(Exception e){
			messageView.setTitle(ERROR_GENERIC_TITLE);
			messageView.setMessage(ERROR_ENCRYPTION);
			messageView.showModal();
		}
	}

	public void register(){
		String username = getLoginView().getRegisterUsername();
		String password = getLoginView().getRegisterPassword();
		String password2 = getLoginView().getRegisterPasswordRepeat();
		try{
			if(!password.equals(password2)){
				messageView.setTitle(ERROR_GENERIC_TITLE);
				messageView.setMessage(ERROR_PASSWORD_MISMATCH);
				messageView.showModal();
			}
			else{
				if(!password.equals(EMPTY)){
					password = encrypt(password);
				}				
				int id = ClientManager.getInstance().getServer().register(username, password);
				if(id != NEGATIVE_ONE){
					ClientManager.getInstance().setInfo(id, username);
					startJoinGame();
				}
				else{
					messageView.setTitle(ERROR_GENERIC_TITLE);
					messageView.setMessage(ERROR_USERNAME_ALREADY_USED);
					messageView.showModal();
				}
			}
			return;
		}
		catch(Exception e){
			messageView.setTitle(ERROR_GENERIC_TITLE);
			messageView.setMessage(ERROR_ENCRYPTION);
			messageView.showModal();

		}
	}
	
	private String encrypt(String input) throws Exception{
		String key = ENCRYPTION_KEY;
		Key aesKey = new SecretKeySpec(key.getBytes(), ENCRYPTION_TYPE);
		Cipher cipher = Cipher.getInstance(ENCRYPTION_TYPE);
		cipher.init(Cipher.ENCRYPT_MODE,  aesKey);
		byte[] encrypted = cipher.doFinal(input.getBytes());
		
		return new String(encrypted);
	}
	
	public void startJoinGame(){
		getLoginView().closeModal();
		join = new JoinGameController(new JoinGameView());
		join.start();
	}
	
	public JoinGameController getJoinGameController(){
		return join;
	}
}

