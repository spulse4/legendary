package client.definitions;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import client.main.ClientManager;

public class ClientCard extends ButtonSetter implements ClientVariables{
		
	public ClientCard(String name, String path){
 		super(name, path);
	}
		
	public JPanel getPanel(final JDialog dialog){
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));		

		JPanel namePanel = new JPanel();
		namePanel.setLayout(new BorderLayout());
		JPanel categoryPanel = new JPanel();
		categoryPanel.setLayout(new BorderLayout());
		JPanel descriptionPanel = new JPanel();
		descriptionPanel.setLayout(new BorderLayout());
		JPanel buyPanel = new JPanel();
		buyPanel.setLayout(new BorderLayout());
		JPanel attackPanel = new JPanel();
		attackPanel.setLayout(new BorderLayout());
		JPanel pricePanel = new JPanel();
		pricePanel.setLayout(new BorderLayout());
		JPanel ambushPanel = new JPanel();
		ambushPanel.setLayout(new BorderLayout());
		JPanel fightPanel = new JPanel();
		fightPanel.setLayout(new BorderLayout());
		JPanel escapePanel = new JPanel();
		escapePanel.setLayout(new BorderLayout());
		JPanel colorPanel = new JPanel();
		colorPanel.setLayout(new BorderLayout());
		JPanel pointsPanel = new JPanel();
		pointsPanel.setLayout(new BorderLayout());
		JPanel hitPointsPanel = new JPanel();
		hitPointsPanel.setLayout(new BorderLayout());
		JPanel setupPanel = new JPanel();
		setupPanel.setLayout(new BorderLayout());
		JPanel specialRulesPanel = new JPanel();
		specialRulesPanel.setLayout(new BorderLayout());
		JPanel twistPanel = new JPanel();
		twistPanel.setLayout(new BorderLayout());
		JPanel evilWinsPanel = new JPanel();
		evilWinsPanel.setLayout(new BorderLayout());
	
		JLabel first;
		JPanel second;	
		
		int lines = ZERO;
		
		schemeSetup();
		
		if(!name.equals(NA)){			
			JPanel left = new JPanel();
			left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));
			first = new JLabel(NAME_STRING);
			first.setFont(FONT_BOLD_20);
			left.add(first);
			left.setPreferredSize(new Dimension(CARD_PANEL_LEFT_BUFFER, ZERO));
			second = getLabels(fixedName);
			lines += second.getComponentCount();
			
			namePanel.add(left, BorderLayout.LINE_START);
			namePanel.add(second, BorderLayout.CENTER);
			panel.add(namePanel);
		}        
		if(!category.equals(NA)){
			JPanel left = new JPanel();
			left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));
			first = new JLabel(CARD_CATEGORY);
			first.setFont(FONT_BOLD_20);
			left.add(first);
			left.setPreferredSize(new Dimension(CARD_PANEL_LEFT_BUFFER, ZERO));
			StringBuilder str = new StringBuilder();
			str.append(Character.toUpperCase(category.charAt(ZERO)));
			str.append(category.substring(ONE));
			second = getLabels(str.toString());
			lines += second.getComponentCount();
			
			categoryPanel.add(left, BorderLayout.LINE_START);
			categoryPanel.add(second, BorderLayout.CENTER);
			panel.add(categoryPanel);
		}
		if(!colors.get(0).equals(NA)){
			JPanel left = new JPanel();
			left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));
			first = new JLabel(CARD_COLOR);
			first.setFont(FONT_BOLD_20);
			left.add(first);
			left.setPreferredSize(new Dimension(CARD_PANEL_LEFT_BUFFER, ZERO));
			StringBuilder str = new StringBuilder();
			List<String> colorsStrings = new ArrayList<String>();
			for(int i = ZERO; i < colors.size(); ++i){
				if(!colorsStrings.contains(colors.get(i))){
					String color = colors.get(i);
					StringBuilder string = new StringBuilder();
					string.append(Character.toUpperCase(color.charAt(ZERO)));
					string.append(color.substring(ONE));
					str.append(string.toString() + SPACE);
					colorsStrings.add(colors.get(i));
				}
			}
			str.setLength(str.length() - ONE);
			second = getLabels(str.toString());
			lines += second.getComponentCount();
			
			colorPanel.add(left, BorderLayout.LINE_START);
			colorPanel.add(second, BorderLayout.CENTER);
			panel.add(colorPanel);
		}		
		if(!description.equals(NA)){
			JPanel left = new JPanel();
			left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));
			first = new JLabel(CARD_DESCRIPTION);
			first.setFont(FONT_BOLD_20);
			left.add(first);
			left.setPreferredSize(new Dimension(CARD_PANEL_LEFT_BUFFER, ZERO));
			second = getLabels(description);
			lines += second.getComponentCount();
			
			descriptionPanel.add(left, BorderLayout.LINE_START);
			descriptionPanel.add(second, BorderLayout.CENTER);
			panel.add(descriptionPanel);
		}
		if(buyPower != NEGATIVE_ONE){
			JPanel left = new JPanel();
			left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));
			first = new JLabel(CARD_BUY_POWER);
			first.setFont(FONT_BOLD_20);
			left.add(first);
			left.setPreferredSize(new Dimension(CARD_PANEL_LEFT_BUFFER, ZERO));
			second = getLabels(Integer.toString(buyPower));
			lines += second.getComponentCount();
			
			buyPanel.add(left, BorderLayout.LINE_START);
			buyPanel.add(second, BorderLayout.CENTER);
			panel.add(buyPanel);
		}
		if(attackPower != NEGATIVE_ONE){
			JPanel left = new JPanel();
			left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));
			first = new JLabel(CARD_ATTACK_POWER);
			first.setFont(FONT_BOLD_20);
			left.add(first);
			left.setPreferredSize(new Dimension(CARD_PANEL_LEFT_BUFFER, ZERO));
			second = getLabels(Integer.toString(attackPower));
			lines += second.getComponentCount();
			
			attackPanel.add(left, BorderLayout.LINE_START);
			attackPanel.add(second, BorderLayout.CENTER);
			panel.add(attackPanel);
		}
		if(price != NEGATIVE_ONE){
			JPanel left = new JPanel();
			left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));
			first = new JLabel(CARD_PRICE);
			first.setFont(FONT_BOLD_20);
			left.add(first);
			left.setPreferredSize(new Dimension(CARD_PANEL_LEFT_BUFFER, ZERO));
			second = getLabels(Integer.toString(price));
			lines += second.getComponentCount();
			
			pricePanel.add(left, BorderLayout.LINE_START);
			pricePanel.add(second, BorderLayout.CENTER);
			panel.add(pricePanel);
		}
		if(!ambush.equals(NA)){
			JPanel left = new JPanel();
			left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));
			first = new JLabel(CARD_AMBUSH);
			first.setFont(FONT_BOLD_20);
			left.add(first);
			left.setPreferredSize(new Dimension(CARD_PANEL_LEFT_BUFFER, ZERO));
			second = getLabels(ambush);
			lines += second.getComponentCount();
			
			ambushPanel.add(left, BorderLayout.LINE_START);
			ambushPanel.add(second, BorderLayout.CENTER);
			panel.add(ambushPanel);
		}
		if(!fight.equals(NA)){
			JPanel left = new JPanel();
			left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));
			first = new JLabel(CARD_FIGHT);
			first.setFont(FONT_BOLD_20);
			left.add(first);
			left.setPreferredSize(new Dimension(CARD_PANEL_LEFT_BUFFER, ZERO));
			second = getLabels(fight);
			lines += second.getComponentCount();
			
			fightPanel.add(left, BorderLayout.LINE_START);
			fightPanel.add(second, BorderLayout.CENTER);
			panel.add(fightPanel);
		}
		if(!escape.equals(NA)){
			JPanel left = new JPanel();
			left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));
			first = new JLabel(CARD_ESCAPE);
			first.setFont(FONT_BOLD_20);
			left.add(first);
			left.setPreferredSize(new Dimension(CARD_PANEL_LEFT_BUFFER, ZERO));
			second = getLabels(escape);
			lines += second.getComponentCount();
			
			escapePanel.add(left, BorderLayout.LINE_START);
			escapePanel.add(second, BorderLayout.CENTER);
			panel.add(escapePanel);
		}
		if(points != NEGATIVE_ONE){
			JPanel left = new JPanel();
			left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));
			first = new JLabel(CARD_POINTS);
			first.setFont(FONT_BOLD_20);
			left.add(first);
			left.setPreferredSize(new Dimension(CARD_PANEL_LEFT_BUFFER, ZERO));
			second = getLabels(Integer.toString(points));
			lines += second.getComponentCount();
			
			pointsPanel.add(left, BorderLayout.LINE_START);
			pointsPanel.add(second, BorderLayout.CENTER);
			panel.add(pointsPanel);
		}
		if(hitPoints != NEGATIVE_ONE){
			JPanel left = new JPanel();
			left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));
			first = new JLabel(CARD_HIT_POINTS);
			first.setFont(FONT_BOLD_20);
			left.add(first);
			left.setPreferredSize(new Dimension(CARD_PANEL_LEFT_BUFFER, ZERO));
			if(ClientManager.getInstance().getModel().getTurnTracker().getCardsInEffect().contains(LIGHTNING_BOLT) &&
					   index.equals(Integer.toString(ROOFTOPS_NUMBER))){
						second = getLabels(Integer.toString(hitPoints - 2));
			}
			else if(ClientManager.getInstance().getModel().getTurnTracker().getCardsInEffect().contains(TIDAL_WAVE) &&
					 index.equals(Integer.toString(BRIDGE_NUMBER))){
						second = getLabels(Integer.toString(hitPoints - 2));
			}
			else if(ClientManager.getInstance().getModel().getTurnTracker().getCardsInEffect().contains(TIDAL_WAVE_MASTERMIND) &&
					index.equals(Integer.toString(MASTERMIND_NUMBER))){
						second = getLabels(Integer.toString(hitPoints - 2));
			}
			else{
				second = getLabels(Integer.toString(hitPoints));
			}
			lines += second.getComponentCount();
			
			hitPointsPanel.add(left, BorderLayout.LINE_START);
			hitPointsPanel.add(second, BorderLayout.CENTER);
			panel.add(hitPointsPanel);
		}
		if(!setup.equals(NA)){
			JPanel left = new JPanel();
			left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));
			first = new JLabel(CARD_SETUP);
			first.setFont(FONT_BOLD_20);
			left.add(first);
			left.setPreferredSize(new Dimension(CARD_PANEL_LEFT_BUFFER, ZERO));
			second = getLabels(setup);
			lines += second.getComponentCount();
			
			setupPanel.add(left, BorderLayout.LINE_START);
			setupPanel.add(second, BorderLayout.CENTER);
			panel.add(setupPanel);
		}
		if(!specialRules.equals(NA)){
			JPanel left = new JPanel();
			left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));
			first = new JLabel(CARD_SPECIAL_RULES);
			first.setFont(FONT_BOLD_20);
			left.add(first);
			left.setPreferredSize(new Dimension(CARD_PANEL_LEFT_BUFFER, ZERO));
			second = getLabels(specialRules);
			lines += second.getComponentCount();
			
			specialRulesPanel.add(left, BorderLayout.LINE_START);
			specialRulesPanel.add(second, BorderLayout.CENTER);
			panel.add(specialRulesPanel);
		}
		if(!twist.equals(NA)){
			JPanel left = new JPanel();
			left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));
			first = new JLabel(CARD_TWIST);
			first.setFont(FONT_BOLD_20);
			left.add(first);
			left.setPreferredSize(new Dimension(CARD_PANEL_LEFT_BUFFER, ZERO));
			second = getLabels(twist);
			lines += second.getComponentCount();
			
			twistPanel.add(left, BorderLayout.LINE_START);
			twistPanel.add(second, BorderLayout.CENTER);
			panel.add(twistPanel);
		}
		if(!evilWins.equals(NA)){
			JPanel left = new JPanel();
			left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));
			first = new JLabel(CARD_EVIL_WINS);
			first.setFont(FONT_BOLD_20);
			left.add(first);
			left.setPreferredSize(new Dimension(CARD_PANEL_LEFT_BUFFER, ZERO));
			second = getLabels(evilWins);
			lines += second.getComponentCount();
			
			evilWinsPanel.add(left, BorderLayout.LINE_START);
			evilWinsPanel.add(second, BorderLayout.CENTER);
			panel.add(evilWinsPanel);
		}
						
		JPanel buttonPanel = super.getButtonPanel(dialog);
		panel.add(buttonPanel);

		panel.setMinimumSize(new Dimension(ZERO, CARD_INITIAL_LINE_HEIGHT + (CARD_HEIGHT_PER_LINE * lines) + CARD_EXTRA_BUFFER));
		panel.add(Box.createRigidArea(new Dimension(ZERO, CARD_RIGID_AREA)));
		
		schemeTearDown();
		
        return panel;
	}
	
	public void type(String type){
		this.type = type;
	}
	
	private JPanel getLabels(String string){
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setAlignmentX(JPanel.LEFT_ALIGNMENT);
		
		FontMetrics fm = new JLabel(EMPTY).getFontMetrics(FONT_PLAIN_20);
		StringBuilder str = new StringBuilder();
		
		Scanner myScanner = new Scanner(string);
		String next = EMPTY;
		while(myScanner.hasNext()){
			if(str.length() == 0){
				str.append(next);
			}
			else{
				str.append(SPACE + next);
			}
			next = myScanner.next();
			next = next.replaceAll(AT, COMMA);
			if((fm.stringWidth(str.toString() + SPACE + next) > 280) || (next.equals(NEXT_LINE))){
				if(next.equals(NEXT_LINE)){
					next = EMPTY;
				}
				JLabel second = new JLabel(str.toString());
				second.setFont(FONT_PLAIN_20);
				panel.add(second);
				str.setLength(0);
			}
		}
		myScanner.close();
		if(str.length() == 0){
			str.append(next);
		}
		else{
			str.append(SPACE + next);
		}
		JLabel second = new JLabel(str.toString());
		second.setFont(FONT_PLAIN_20);
		panel.add(second);
		return panel;
	}

	public ClientCard duplicate(){
		ClientCard card = new ClientCard(name, path);
		card.setColor(ZERO, colors.get(ZERO));
		card.setColor(ONE, colors.get(ONE));
		card.setColor(TWO, colors.get(TWO));
		card.setColor(THREE, colors.get(THREE));
		card.setAttackPower(attackPower);
		card.setBuyPower(buyPower);
		return card;
	}
}
