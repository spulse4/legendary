package client.definitions;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import client.main.ClientManager;
import shared.definitions.PlayerHeader;
import shared.model.ModelFacade;

public class ButtonSetter extends ClientCardFunctions implements ClientVariables{
		
	private static final String CLASS = "clinet.definitions.ButtonSetter: ";
	
	public ButtonSetter(String name, String path) {
		super(name, path);
	}
	
	protected JPanel getButtonPanel(final JDialog dialog){
		if(type != null){
			ModelFacade model = null;
			JPanel buttonPanel = new JPanel();
			buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
			JButton button1 = null;
			JButton button2 = cancel(dialog, BUTTON_CANCEL);
			JButton button3 = null;
			switch(type){
			case STATE_ATTACK:
				button1 = attack(dialog);
				break;
			case STATE_BATTLEFIELD_PROMOTION_ABILITY:
				button1 = battlefieldPromotionAbility(dialog);
				break;
			case STATE_BITTER_CAPTOR_FIGHT:
				button1 = bitterCaptorFight(dialog);
				break;
			case STATE_BUY:
				button1 = buy(dialog);
				break;
			case STATE_COPY_POWERS_ABILITY:
				button1 = copyPowersAbility(dialog);
				break;
			case STATE_COVERING_FIRE_ABILITY:
				button1 = coveringFireAbility(dialog, true);
				button3 = coveringFireAbility(dialog, false);
				break;
			case STATE_COVERING_FIRE_DISCARD:
				button1 = coveringFireDiscard(dialog);
				break;
			case STATE_CRUEL_RULER_FIGHT:
				button1 = cruelRulerFight(dialog);
				break;
			case STATE_CYCLOPS_ABILITY:
				button1 = cyclopsAbility(dialog);
				break;
			case STATE_DANGEROUS_RESCUE_ABILITY:
				button1 = dangerousRescueAbility(dialog);
				break;
			case STATE_DARK_TECHNOLOGY_FIGHT:
				button1 = darkTechnologyFight(dialog);
				break;
			case STATE_DESTROYER_ESCAPE:
				button1 = destroyerEscape(dialog);
				break;
			case STATE_DOOMBOT_LEGION_FIGHT:
				button1 = doombotLegionFight(dialog);
				break;
			case STATE_DR_DOOM_MASTERSTRIKE:
				button1 = drDoomMasterstrike(dialog);
				break;
			case STATE_ELECTROMAGNETIC_BUBBLE_FIGHT:
				button1 = electromagneticBubbleFight(dialog);
				break;
			case STATE_ENERGY_DRAIN_ABILITY:
				button1 = energyDrainAbility(dialog);
				break;
			case STATE_HEALING_FACTOR_ABILITY:
				button1 = healingFactorAbility(dialog, true);
				button3 = healingFactorAbility(dialog, false);
				break;
			case STATE_HERE_HOLD_THIS_FOR_A_SECOND_ABILITY:
				button1 = hereHoldThisForASecondAbility(dialog);
				break;
			case STATE_HEY_CAN_I_GET_A_DO_OVER_ABILITY:
				button1 = heyCanIGetADoOverAbility(dialog, true);
				button3 = heyCanIGetADoOverAbility(dialog, false);
				break;
			case STATE_HYDRA_KIDNAPPERS_FIGHT:
				button1 = hydraKidnappersFight(dialog, true);
				button3 = hydraKidnappersFight(dialog, false);
				break;
			case STATE_HYPNOTIC_CHARM_ABILITY:
				model = ClientManager.getInstance().getModel();
				dialog.setTitle(DIALOG_PLAYER_TITLE + (model.getTurnTracker().getState().getEffected(model.getGameID()).get(ZERO) + ONE));
				button1 = hypnoticCharmAbility(dialog, true);
				button2 = null;
				button3 = hypnoticCharmAbility(dialog, false);
				break;
			case STATE_JUGGERNAUT_AMBUSH:
				button1 = juggernautAmbush(dialog);
				break;
			case STATE_JUGGERNAUT_ESCAPE:
				button1 = juggernautEscape(dialog);
				break;
			case STATE_MAESTRO_FIGHT:
				button1 = maestroFight(dialog);
				break;
			case STATE_MAGNETO_MASTERSTRIKE:
				button1 = magnetoMasterstrike(dialog);
				break;
			case STATE_MANIACAL_TYRANT_FIGHT:
				button1 = maniacalTyrantFight(dialog);
				break;
			case STATE_MELTER_FIGHT:
				model = ClientManager.getInstance().getModel();
				dialog.setTitle(DIALOG_PLAYER_TITLE + (model.getTurnTracker().getState().getEffected(model.getGameID()).get(ZERO) + ONE));
				button1 = melterFight(dialog, true);
				button2 = null;
				button3 = melterFight(dialog, false);
				break;
			case STATE_MONARCHS_DECREE_DISCARD:
				button1 = monarchsDecreeDiscard(dialog);
				break;
			case STATE_MONARCHS_DECREE_FIGHT:
				button1 = monarchsDecreeFight(dialog, true);
				button3 = monarchsDecreeFight(dialog, false);
				break;
			case STATE_PAIBOK_THE_POWER_SKRULL_FIGHT:
				model = ClientManager.getInstance().getModel();
				dialog.setTitle(DIALOG_PLAYER_TITLE + (model.getTurnTracker().getState().getEffected(model.getGameID()).get(ZERO) + ONE));
				button1 = paibokThePowerSkrullFight(dialog);
				break;
			case STATE_PURE_FURY_ABILITY:
				button1 = pureFuryAbility(dialog);
				break;
			case STATE_RANDOM_ACTS_OF_UNKINDNESS_ABILITY:
				button1 = randomActsOfUnkindnessAbility(dialog, true);
				button3 = randomActsOfUnkindnessAbility(dialog, false);
				break;
			case STATE_RANDOM_ACTS_OF_UNKINDNESS_PASS:
				button1 = randomActsOfUnkindnessPass(dialog);
				break;
			case STATE_RED_SKULL_MASTERSTRIKE:
				button1 = redSkullMasterstrike(dialog);
				break;
			case STATE_RUTHLESS_DICTATOR_DISCARD:
				button1 = ruthlessDictatorDiscard(dialog);
				break;
			case STATE_RUTHLESS_DICTATOR_KO:
				button1 = ruthlessDictatorKO(dialog);
				break;
			case STATE_SENTINEL_FIGHT:
				button1 = sentinelFight(dialog);
				break;
			case STATE_SHADOWED_THOUGHTS_ABILITY:
				button1 = shadowedThoughtsAbility(dialog, true);
				button3 = shadowedThoughtsAbility(dialog, false);
				break;
			case STATE_SILENT_SNIPER_ABILITY:
				button1 = silentSniperAbility(dialog);
				break;
			case STATE_STACK_THE_DECK_ABILITY:
				button1 = stackTheDeckAbility(dialog);
				break;
			case STATE_SUPER_SKRULL_FIGHT:
				button1 = superSkrullFight(dialog);
				break;
			case STATE_THE_AMAZING_SPIDERMAN_ABILITY:
				button1 = theAmazingSpidermanAbility(dialog);
				break;
			case STATE_UNSTOPPABLE_HULK_ABILITY:
				button1 = unstoppableHulkAbility(dialog, true);
				button3 = unstoppableHulkAbility(dialog, false);
				break;
			case STATE_USE:
				button1 = use(dialog);
				break;
			case STATE_VANISHING_ILLUSIONS_FIGHT:
				button1 = vanishingIllusionsFight(dialog);
				break;
			case STATE_WHIRLWIND_FIGHT:
				button1 = whirlwindFight(dialog);
				break;
			}
			if(button1 == null && button2 == null && button3 == null){
				System.out.println(CLASS + ONE + COLON + SPACE + NULL_STRING);
				System.out.println(CLASS + TWO + COLON + SPACE + NULL_STRING);
				System.out.println(CLASS + THREE + COLON + SPACE + NULL_STRING);
				System.exit(SYSTEM_EXIT);
			}
			else if(button1 == null && button2 == null && button3 != null){
				button3.setPreferredSize(new Dimension(BUTTON_2_LENGTH ,BUTTON_HEIGHT));				
				button3.setFont(FONT_PLAIN_10);
				buttonPanel.add(button3);
			}
			else if(button1 == null && button2 != null && button3 == null){
				button2.setPreferredSize(new Dimension(BUTTON_2_LENGTH ,BUTTON_HEIGHT));				
				button2.setFont(FONT_PLAIN_10);
				buttonPanel.add(button2);
			}
			else if(button1 == null && button2 != null && button3 != null){
				button3.setPreferredSize(new Dimension(BUTTON_2_LENGTH, BUTTON_HEIGHT));
				button3.setFont(FONT_PLAIN_10);
				button2.setPreferredSize(new Dimension(BUTTON_2_LENGTH ,BUTTON_HEIGHT));				
				button2.setFont(FONT_PLAIN_10);
				buttonPanel.add(Box.createRigidArea(new Dimension(BUTTON_2_SPACE, ZERO)));
				buttonPanel.add(button3);
				buttonPanel.add(Box.createRigidArea(new Dimension(BUTTON_2_SPACE, ZERO)));
				buttonPanel.add(button2);
				buttonPanel.add(Box.createRigidArea(new Dimension(BUTTON_2_SPACE, ZERO)));
			}
			else if(button1 != null && button2 == null && button3 == null){
				button1.setPreferredSize(new Dimension(BUTTON_2_LENGTH ,BUTTON_HEIGHT));				
				button1.setFont(FONT_PLAIN_10);
				buttonPanel.add(button1);
			}
			else if(button1 != null && button2 == null && button3 != null){
				button1.setPreferredSize(new Dimension(BUTTON_2_LENGTH, BUTTON_HEIGHT));
				button1.setFont(FONT_PLAIN_10);
				button3.setPreferredSize(new Dimension(BUTTON_2_LENGTH ,BUTTON_HEIGHT));				
				button3.setFont(FONT_PLAIN_10);
				buttonPanel.add(Box.createRigidArea(new Dimension(BUTTON_2_SPACE, ZERO)));
				buttonPanel.add(button1);
				buttonPanel.add(Box.createRigidArea(new Dimension(BUTTON_2_SPACE, ZERO)));
				buttonPanel.add(button3);
				buttonPanel.add(Box.createRigidArea(new Dimension(BUTTON_2_SPACE, ZERO)));
			}
			else if(button1 != null && button2 != null && button3 == null){
				button1.setPreferredSize(new Dimension(BUTTON_2_LENGTH, BUTTON_HEIGHT));
				button1.setFont(FONT_PLAIN_10);
				button2.setPreferredSize(new Dimension(BUTTON_2_LENGTH ,BUTTON_HEIGHT));				
				button2.setFont(FONT_PLAIN_10);
				buttonPanel.add(Box.createRigidArea(new Dimension(BUTTON_2_SPACE, ZERO)));
				buttonPanel.add(button1);
				buttonPanel.add(Box.createRigidArea(new Dimension(BUTTON_2_SPACE, ZERO)));
				buttonPanel.add(button2);
				buttonPanel.add(Box.createRigidArea(new Dimension(BUTTON_2_SPACE, ZERO)));
			}
			else if(button1 != null && button2 != null && button3 != null){
				button1.setPreferredSize(new Dimension(BUTTON_3_LENGTH, BUTTON_2_LENGTH));
				button1.setFont(FONT_PLAIN_10);
				button2.setPreferredSize(new Dimension(BUTTON_3_LENGTH, BUTTON_2_LENGTH));
				button2.setFont(FONT_PLAIN_10);
				button3.setPreferredSize(new Dimension(BUTTON_3_LENGTH, BUTTON_2_LENGTH));
				button3.setFont(FONT_PLAIN_10);
				buttonPanel.add(Box.createRigidArea(new Dimension(BUTTON_3_SPACE, ZERO)));
				buttonPanel.add(button1);
				buttonPanel.add(Box.createRigidArea(new Dimension(BUTTON_3_SPACE, ZERO)));
				buttonPanel.add(button3);
				buttonPanel.add(Box.createRigidArea(new Dimension(BUTTON_3_SPACE, ZERO)));
				buttonPanel.add(button2);
				buttonPanel.add(Box.createRigidArea(new Dimension(BUTTON_3_SPACE, ZERO)));
			}
			
			return buttonPanel;
		}
		else{
			JPanel buttonPanel = new JPanel();
			buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
			JButton button1 = cancel(dialog, BUTTON_CANCEL);

			button1.setPreferredSize(new Dimension(BUTTON_2_LENGTH, BUTTON_HEIGHT));
			buttonPanel.add(button1);
			return buttonPanel;
		}
	}
	
	private JButton attack(final JDialog dialog){
		JButton button = new JButton(BUTTON_ATTACK);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().attackCard(gameID, Integer.valueOf(index));
				dialog.dispose();
			}
			
		});
		if(ClientManager.getInstance().getModel().getTurnTracker().getCardsInEffect().contains(LIGHTNING_BOLT) &&
		   index.equals(Integer.toString(ROOFTOPS_NUMBER))){
			if(ClientManager.getInstance().getModel().getPlayerCards().getPendingAttackPoints() < hitPoints - 2){
				button.setEnabled(false);
			}
		}
		else if(ClientManager.getInstance().getModel().getTurnTracker().getCardsInEffect().contains(TIDAL_WAVE) &&
				 index.equals(Integer.toString(BRIDGE_NUMBER))){
			if(ClientManager.getInstance().getModel().getPlayerCards().getPendingAttackPoints() < hitPoints - 2){
				button.setEnabled(false);
			}
		}
		else if(ClientManager.getInstance().getModel().getTurnTracker().getCardsInEffect().contains(TIDAL_WAVE_MASTERMIND) &&
				index.equals(Integer.toString(MASTERMIND_NUMBER))){
			if(ClientManager.getInstance().getModel().getPlayerCards().getPendingAttackPoints() < hitPoints - 2){
				button.setEnabled(false);
			}
		}
		else{
			if(ClientManager.getInstance().getModel().getPlayerCards().getPendingAttackPoints() < hitPoints){
				button.setEnabled(false);
			}
		}
		return button;
	}
	private JButton battlefieldPromotionAbility(final JDialog dialog){
		JButton button = new JButton(BUTTON_KO);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().battlefieldPromotionAbility(gameID, name);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton bitterCaptorFight(final JDialog dialog){
		JButton button = new JButton(BUTTON_BUY);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().bitterCaptorFight(gameID, Integer.valueOf(index));
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton buy(final JDialog dialog){
		JButton button = new JButton(BUTTON_BUY);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().buyCard(gameID, ((PlayerHeader)ClientManager.getInstance().getUser()).getIndex(), Integer.valueOf(index));
				dialog.dispose();
			}
			
		});
		if(ClientManager.getInstance().getModel().getPlayerCards().getPendingBuyPoints() < price){
			button.setEnabled(false);
		}
		return button;
	}
	private JButton cancel(final JDialog dialog, String message){
		JButton button = new JButton(message);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton copyPowersAbility(final JDialog dialog){
		JButton button = new JButton(BUTTON_COPY);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().copyPowersAbility(gameID, name);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton coveringFireAbility(final JDialog dialog, final boolean draw){
		JButton button;
		if(draw){
			button = new JButton(BUTTON_DRAW);
		}
		else{
			button = new JButton(BUTTON_DISCARD);
		}
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().coveringFireAbility(gameID, draw);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton coveringFireDiscard(final JDialog dialog){
		JButton button = new JButton(BUTTON_DISCARD);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().coveringFireDiscard(gameID, ((PlayerHeader)ClientManager.getInstance().getUser()).getIndex(), name);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton cruelRulerFight(final JDialog dialog){
		JButton button = new JButton(BUTTON_ATTACK);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().cruelRulerFight(gameID, Integer.valueOf(index));
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton cyclopsAbility(final JDialog dialog){
		JButton button = new JButton(BUTTON_DISCARD);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().cyclopsAbility(gameID, name);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton dangerousRescueAbility(final JDialog dialog){
		JButton button = new JButton(BUTTON_KO);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().dangerousRescueAbility(gameID, name);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton darkTechnologyFight(final JDialog dialog){
		JButton button = new JButton(BUTTON_BUY);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().darkTechnologyFight(gameID, Integer.valueOf(index));
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton destroyerEscape(final JDialog dialog){
		JButton button = new JButton(BUTTON_KO);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().destroyerEscape(gameID, ((PlayerHeader)ClientManager.getInstance().getUser()).getIndex(), name);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton doombotLegionFight(final JDialog dialog){
		JButton button = new JButton(BUTTON_KO);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().doombotLegionFight(gameID, name);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton drDoomMasterstrike(final JDialog dialog){
		JButton button = new JButton(BUTTON_RETURN);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().drDoomMasterstrike(gameID, ((PlayerHeader)ClientManager.getInstance().getUser()).getIndex(), name);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton electromagneticBubbleFight(final JDialog dialog){
		JButton button = new JButton(BUTTON_CHOOSE);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().electromagneticBubbleFight(gameID, name);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton energyDrainAbility(final JDialog dialog){
		JButton button = new JButton(BUTTON_KO);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().energyDrainAbility(gameID, name);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton healingFactorAbility(final JDialog dialog, final boolean accept){
		JButton button;
		if(accept){
			button = new JButton(BUTTON_KO);
		}
		else{
			button = new JButton(BUTTON_DECLINE);
		}
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().healingFactorAbility(gameID, accept);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton hereHoldThisForASecondAbility(final JDialog dialog){
		JButton button = new JButton(BUTTON_CHOOSE);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().hereHoldThisForASecondAbility(gameID, Integer.valueOf(index));
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton heyCanIGetADoOverAbility(final JDialog dialog, final boolean accept){
		JButton button;
		if(accept){
			button = new JButton(BUTTON_DISCARD);
		}
		else{
			button = new JButton(BUTTON_DECLINE);
		}
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().heyCanIGetADoOverAbility(gameID, accept);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton hydraKidnappersFight(final JDialog dialog, final boolean accept){
		JButton button;
		if(accept){
			button = new JButton(BUTTON_ACCEPT);
		}
		else{
			button = new JButton(BUTTON_DECLINE);
		}
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().hydraKidnappersFight(gameID, accept);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton hypnoticCharmAbility(final JDialog dialog, final boolean discard){
		JButton button;
		if(discard){
			button = new JButton(BUTTON_DISCARD);
		}
		else{
			button = new JButton(BUTTON_RETURN);
		}
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().hypnoticCharmAbility(gameID, ((PlayerHeader)ClientManager.getInstance().getUser()).getIndex(), discard);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton juggernautAmbush(final JDialog dialog){
		JButton button = new JButton(BUTTON_KO);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().juggernautAmbush(gameID, ((PlayerHeader)ClientManager.getInstance().getUser()).getIndex(), name);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton juggernautEscape(final JDialog dialog){
		JButton button = new JButton(BUTTON_KO);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().juggernautEscape(gameID, ((PlayerHeader)ClientManager.getInstance().getUser()).getIndex(), name);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton maestroFight(final JDialog dialog){
		JButton button = new JButton(BUTTON_KO);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().maestroFight(gameID, name);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton magnetoMasterstrike(final JDialog dialog){
		JButton button = new JButton(BUTTON_DISCARD);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().magnetoMasterstrike(gameID, ((PlayerHeader)ClientManager.getInstance().getUser()).getIndex(), name);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton maniacalTyrantFight(final JDialog dialog){
		JButton button = new JButton(BUTTON_KO);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().maniacalTyrantFight(gameID, name);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton melterFight(final JDialog dialog, final boolean ko){
		JButton button;
		if(ko){
			button = new JButton(BUTTON_KO);
		}
		else{
			button = new JButton(BUTTON_RETURN);
		}
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				ModelFacade model = ClientManager.getInstance().getModel();
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().melterFight(gameID, model.getTurnTracker().getState().getEffected(gameID).get(ZERO), ko);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton monarchsDecreeDiscard(final JDialog dialog){
		JButton button = new JButton(BUTTON_DISCARD);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().monarchsDecreeDiscard(gameID, ((PlayerHeader)ClientManager.getInstance().getUser()).getIndex(), name);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton monarchsDecreeFight(final JDialog dialog, final boolean draw){
		JButton button;
		if(draw){
			button = new JButton(BUTTON_DRAW);
		}
		else{
			button = new JButton(BUTTON_DISCARD);
		}
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().monarchsDecreeFight(gameID, draw);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton paibokThePowerSkrullFight(final JDialog dialog){
		JButton button = new JButton(BUTTON_CHOOSE);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				int playerIndex = ClientManager.getInstance().getModel().getTurnTracker().getState().getEffected(gameID).get(ZERO);
				ClientManager.getInstance().getServer().paibokThePowerSkrullFight(gameID, playerIndex, Integer.valueOf(index));
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton pureFuryAbility(final JDialog dialog){
		JButton button = new JButton(BUTTON_ATTACK);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().pureFuryAbility(gameID, Integer.valueOf(index));
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton randomActsOfUnkindnessAbility(final JDialog dialog, final boolean accept){
		JButton button;
		if(accept){
			button = new JButton(BUTTON_ACCEPT);
		}
		else{
			button = new JButton(BUTTON_DECLINE);
		}
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().randomActsOfUnkindnessAbility(gameID, accept);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton randomActsOfUnkindnessPass(final JDialog dialog){
		JButton button = new JButton(BUTTON_PASS);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().randomActsOfUnkindnessPass(gameID, ((PlayerHeader)ClientManager.getInstance().getUser()).getIndex(), name);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton redSkullMasterstrike(final JDialog dialog){
		JButton button = new JButton(BUTTON_KO);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().redSkullMasterstrike(gameID, ((PlayerHeader)ClientManager.getInstance().getUser()).getIndex(), name);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton ruthlessDictatorDiscard(final JDialog dialog){
		JButton button = new JButton(BUTTON_DISCARD);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().ruthlessDictatorDiscard(gameID, name);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton ruthlessDictatorKO(final JDialog dialog){
		JButton button = new JButton(BUTTON_KO);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().ruthlessDictatorKO(gameID, name);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton sentinelFight(final JDialog dialog){
		JButton button = new JButton(BUTTON_KO);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().sentinelFight(gameID, name);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton shadowedThoughtsAbility(final JDialog dialog, final boolean accept){
		JButton button;
		if(accept){
			button = new JButton(BUTTON_ACCEPT);
		}
		else{
			button = new JButton(BUTTON_DECLINE);
		}
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().shadowedThoughtsAbility(gameID, accept);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton silentSniperAbility(final JDialog dialog){
		JButton button = new JButton(BUTTON_ATTACK);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().silentSniperAbility(gameID, Integer.valueOf(index));
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton stackTheDeckAbility(final JDialog dialog){
		JButton button = new JButton(BUTTON_RETURN);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().stackTheDeckAbility(gameID, name);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton superSkrullFight(final JDialog dialog){
		JButton button = new JButton(BUTTON_KO);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().superSkrullFight(gameID, ((PlayerHeader)ClientManager.getInstance().getUser()).getIndex(), name);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton theAmazingSpidermanAbility(final JDialog dialog){
		JButton button = new JButton(BUTTON_RETURN);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().theAmazingSpidermanAbility(gameID, name);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton unstoppableHulkAbility(final JDialog dialog, final boolean accept){
		JButton button;
		if(accept){
			button = new JButton(BUTTON_ACCEPT);
		}
		else{
			button = new JButton(BUTTON_DECLINE);
		}
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().unstoppableHulkAbility(gameID, accept);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton use(final JDialog dialog){
		JButton button = new JButton(BUTTON_USE);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().playCard(gameID, index);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton vanishingIllusionsFight(final JDialog dialog){
		JButton button = new JButton(BUTTON_KO);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().vanishingIllusionsFight(gameID, ((PlayerHeader)ClientManager.getInstance().getUser()).getIndex(), name);
				dialog.dispose();
			}
			
		});
		return button;
	}
	private JButton whirlwindFight(final JDialog dialog){
		JButton button = new JButton(BUTTON_KO);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int gameID = ClientManager.getInstance().getGame().getGameID();
				ClientManager.getInstance().getServer().whirlwindFight(gameID, name);
				dialog.dispose();
			}
			
		});
		return button;
	}
}