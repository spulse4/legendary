package client.definitions;

import shared.definitions.SharedVariables;

public interface ClientVariables extends SharedVariables{

	public final static int INTERVAL = 1000;
	public final static int MAX_PLAYERS = 5;
	public final static int VIEW_BORDER_WIDTH = 10;
	
	public final static int BOARD_COMPONENT_CAPTURED_DIFF_Y = 10;
	public final static int BOARD_COMPONENT_INITIAL_BYSTANDERS = 30;
	public final static int BOARD_COMPONENT_INITIAL_ESCAPED_VILLIANS = 0;
	public final static int BOARD_COMPONENT_INITIAL_HEROES = 0;
	public final static int BOARD_COMPONENT_INITIAL_KOS = 0;
	public final static int BOARD_COMPONENT_INITIAL_MASTERMINDS = 5;
	public final static int BOARD_COMPONENT_INITIAL_OFFICERS = 30;
	public final static int BOARD_COMPONENT_INITIAL_SCHEMES = 0;
	public final static int BOARD_COMPONENT_INITIAL_VILLIANS = 0;
	public final static int BOARD_COMPONENT_INITIAL_WOUNDS = 30;
	public final static int BOARD_COMPONENT_PORTAL_SIZE = 5;
	public final static int BOARD_COMPONENT_SHRUNKEN_IMAGE_BORDER_WIDTH_BOTTOM = 2;
	public final static int BOARD_COMPONENT_SHRUNKEN_IMAGE_BORDER_WIDTH_TOP = 3;
	public final static int BOARD_COMPONENT_SHRUNKEN_IMAGE_EXTRA_Y = 50;
	public final static int BOARD_COMPONENT_SHRUNKEN_IMAGE_DIFF_X = 100;
	public final static int BOARD_COMPONENT_SHRUNKEN_IMAGE_DIFF_Y = 290;
	
	public final static int BUTTON_2_LENGTH = 125;
	public final static int BUTTON_2_SPACE = 50;
	public final static int BUTTON_3_LENGTH = 100;
	public final static int BUTTON_3_SPACE = 25;
	public final static int BUTTON_HEIGHT = 20;
	
	public final static int BUTTON_PANEL_BUTTON_HEIGHT = 40;
	public final static int BUTTON_PANEL_BUTTON_WIDTH = 85;
	public final static int BUTTON_PANEL_HEIGHT = 50;
	public final static int BUTTON_PANEL_VICTORY_BUTTON_WIDTH = 100;
	
	public final static int CARD_EXTRA_BUFFER = 50;
	public final static int CARD_HEIGHT_PER_LINE = 26;
	public final static int CARD_INITIAL_LINE_HEIGHT = 33;
	public final static int CARD_PANEL_LEFT_BUFFER = 140;
	public final static int CARD_RIGID_AREA = 5;
	
	public final static int CARD_PANEL_HEIGHT = 90;
	public final static int CARD_PANEL_PLAYER_CARD_BUFFER = 10;
	public final static int CARD_PANEL_PLAYER_CARD_ROW_1 = 22;
	public final static int CARD_PANEL_PLAYER_CARD_ROW_2 = 52;
	public final static int CARD_PANEL_PLAYER_CARD_ROW_3 = 82;
	public final static int CARD_PANEL_WIDTH = 270;
	
	public final static int CHAT_HEIGHT = 700;
	public final static int CHAT_INPUT_WIDTH = 260;
	public final static int CHAT_INPUT_HEIGHT = 20;
	public final static int CHAT_INSETS = 5;
	public final static int CHAT_WIDTH = 350;
	
	public final static int DIALOG_BORDER_WIDTH = 4;
	public final static int DIALOG_CLICKED_WIDTH = 450;
	public final static int DIALOG_ESCAPED_HEIGHT = 350;
	public final static int DIALOG_ESCAPED_WIDTH = 240;
	public final static int DIALOG_KO_HEIGHT = 310;
	public final static int DIALOG_KO_WIDTH = 180;
	public final static int DIALOG_NO_PEEKING_HEIGHT = 160;
	public final static int DIALOG_NO_PEEKING_WIDTH = 200;
	public final static int DIALOG_STANDARD_HEIGHT = 400;
	public final static int DIALOG_STANDARD_WIDTH = 320;

	public final static int FONT_SIZE_20 = 20;
	public final static int FONT_SIZE_24 = 24;
	public final static int FONT_SIZE_48 = 48;
	
	public final static int GAME_STATE_HEIGHT = 50;
	public final static int GAME_STATE_WIDTH = 400;
	
	public final static int HAND_HEIGHT = 700;
	public final static int HAND_HEIGHT_PER_CARD = 95;
	public final static int HAND_INITIAL_HEIGHT_NOT_PLAYED = 61;
	public final static int HAND_INITIAL_HEIGHT_PLAYED = 106;
	public final static int HAND_WIDTH = 300;
	
	public final static int LOG_COMPONENT_BOTTOM_MARGIN = 3;
	public final static int LOG_COMPONENT_LEFT_MARGIN = 5;
	public final static int LOG_COMPONENT_RIGHT_MARGIN = 5;
	public final static int LOG_COMPONENT_TOP_MARGIN = 3;
	
	public final static int LOGIN_MAX_LENGTH = 15;
	public final static int LOGIN_MIN_LENGTH = 3;
	
	public final static int MESSAGE_SIZE = 500;
	
	public final static int MIDDLE_HEIGHT = 700;
	public final static int MIDDLE_HIDE_BUTTON_WIDTH = 12;
	public final static int MIDDLE_WIDTH = 800;
	
	public final static int NEW_GAME_BUFFER = 7;
	
	public final static int PLAYER_HAND_VIEW_WIDTH = 350;
	
	public final static int PLAYER_WAITING_BUTTON_HEIGHT = 30;
	public final static int PLAYER_WAITING_BUTTON_WIDTH = 50;
	public final static int PLAYER_WAITING_PANEL_HEIGHT = 50;
	public final static int PLAYER_WAITING_PANEL_WIDTH = 200;
	public final static int PLAYER_WAITING_RIGID_AREA = 20;
	
	public final static int POPUP_ACCEPT_HEIGHT = 100;
	public final static int POPUP_DECLINE_HEIGHT = 40;
	public final static int POPUP_HEIGHT = 310;
	public final static int POPUP_TEXT_WIDTH = 280;
	public final static int POPUP_WIDTH = 350;
	
	public final static int PRELOADER_HEIGHT = 130;
	public final static int PRELOADER_LOADER_HEIGHT = 25;
	public final static int PRELOADER_LOADER_WIDTH = 250;
	public final static int PRELOADER_SLEEP = 10;
	public final static int PRELOADER_WIDTH = 400;
	
	public final static int PURPLE_BLUE = 212;
	public final static int PURPLE_GREEN = 140;
	public final static int PURPLE_RED = 157;
	
	public final static int ROTATION_ONE_EIGHTY = 180;
	public final static int ROTATION_NEGATIVE_NINETY = -90;
	public final static int ROTATION_NINETY = 90;
	
	public final static int SCROLL_SPEED = 20;
	
	public final static int SELECT_BUFFER = 5;
	public final static int SELECT_INITIAL_DIFF = 22;
	public final static int SELECT_HEIGHT_PER_ROW = 30;
	public final static int SELECT_MAX_LENGTH_PER_ROW = 30;
	public final static int SELECT_SCROLL_HEIGHT = 350;
	public final static int SELECT_SCROLL_WIDTH = 300;
	public final static int SELECT_WIDTH = 400;
	
	public final static int TEXT_SIZE_LABEL = 40;
	public final static int TEXT_SIZE_PANEL = 14;
	public final static int TEXT_SIZE_BUTTON = 28;
	
	public final static int TITLE_BORDER = 10;
	
	public final static float BIG_LABEL_TEXT_SIZE = TEXT_SIZE_LABEL * 1.5F;
	public final static float COLOR_SATURATION_5 = 0.5f;
	public final static float COLOR_SATURATION_8 = 0.8f;
	public final static float FLOAT_TWO = 2.0f;
	public final static float SMALL_LABEL_TEXT_SIZE = 16.0f;
	
	public final static double CHAT_CHAT_INPUT_WEIGHT_X = 0.9;
	public final static double CHAT_SEND_CHAT_WEIGHT_X = 0.1;
	public final static double CHAT_WEIGHT_Y = 1.0;
	
	public final static char DASH = '-';
	public final static char UNDERSCORE = '_';

	public final static String VERSION = " version 1.0.0";
	
	public final static String AT = "@";
	public final static String CARDS_IN_THE_DRAW_PILE = " cards in draw pile";
	public final static String CREATE_GAME = "Create Game";
	public final static String FONT = "font";
	public final static String FULL = "Full";
	public final static String HASH = "#";
	public final static String HOST = "localhost";
	public final static String HTTP = "http://";
	public final static String JOIN = "Join";
	public final static String LEGENDARY = "Legendary";
	public final static String LOGIN = "Login";
	public final static String ME = "Me";
	public final static String NAME_STRING = "Name";
	public final static String PASSWORD_STRING = "Password";
	public final static String PASSWORD_AGAIN = "Password (Again)";
	public final static String REGISTER = "Register";
	public final static String REJOIN = "Re-Join";
	public final static String SUBMIT = "Submit";
	public final static String TITLE_STRING = "Title:";
	public final static String USERNAME_STRING = "Username";
	public final static String DESKTOP_HINTS = "awt.font.desktophints";
		
	public final static String CARD_AMBUSH = "Ambush: ";
	public final static String CARD_ATTACK_POWER = "Attack Power: ";
	public final static String CARD_BUY_POWER = "Buy Power: ";
	public final static String CARD_CATEGORY = "Category: ";
	public final static String CARD_COLOR = "Color: ";
	public final static String CARD_DESCRIPTION = "Description: ";
	public final static String CARD_ESCAPE = "Escape: ";
	public final static String CARD_EVIL_WINS = "Evil Wins: ";
	public final static String CARD_FIGHT = "Fight: ";
	public final static String CARD_HIT_POINTS = "Hit Points: ";
	public final static String CARD_NAME = "Name: ";
	public final static String CARD_POINTS = "Points: ";
	public final static String CARD_PRICE = "Price: ";
	public final static String CARD_SETUP = "Setup: ";
	public final static String CARD_SPECIAL_RULES = "Special Rules: ";
	public final static String CARD_TWIST = "Twist: ";
	
	public final static String CARD_INFO_HYDRA = "Hydra";
	public final static String CARD_INFO_XMEN = "X Men";
	
	public final static String CARD_PANEL_ATTACK = "Attack: ";
	public final static String CARD_PANEL_BUY = "Buy: ";
	
	public final static String CARD_TYPE_ATTACK = "attack";
	public final static String CARD_TYPE_BUY = "buy";
	public final static String CARD_TYPE_DISCARD = "discard";
	public final static String CARD_TYPE_KO = "ko";
	public final static String CARD_TYPE_TOP = "top";	
	public final static String CARD_TYPE_USE = "use";
	
	public final static String CHAT_SEND = "Send";
	public final static String CHAT_SEND_A_MESSAGE = "Send a message!";
	
	public final static String ENCRYPTION_KEY = "Bar12345Bar12345";
	public final static String ENCRYPTION_TYPE = "AES";
	
	public final static String ERROR_DR_DOOM_MESSAGE = "Dr Doom must lead Doombot Legion henchmen";
	public final static String ERROR_DR_DOOM_TITLE = "Dr Doom";
	public final static String ERROR_ENCRYPTION = "Something went wrong with encryption";
	public final static String ERROR_GENERIC_MESSAGE_1 = "With your current number of players, you need to use ";
	public final static String ERROR_GENERIC_MESSAGE_3 = " selected.";
	public final static String ERROR_GENERIC_TITLE = "Fail";
	public final static String ERROR_HENCHMEN_MISMATCH_MESSAGE_2_PLURAL = " henchmen. You have ";
	public final static String ERROR_HENCHMEN_MISMATCH_MESSAGE_2_SINGULAR = " henchman. You have ";
	public final static String ERROR_HENCHMEN_MISMATCH_TITLE = "Henchmen Mismatch";
	public final static String ERROR_HERO_MISMATCH_MESSAGE_2_PLURAL = " heroes. You have ";
	public final static String ERROR_HERO_MISMATCH_MESSAGE_2_SINGULAR = " hero. You have ";
	public final static String ERROR_HERO_MISMATCH_TITLE = "Hero Mismatch";
	public final static String ERROR_INVALID_CREDENTIALS = "Invalid Credentials";
	public final static String ERROR_LOKI_MESSAGE = "Loki must lead Enemies of Asgard villians";
	public final static String ERROR_LOKI_TITLE = "Loki";
	public final static String ERROR_MAGNETO_MESSAGE = "Magneto must lead Brotherhood villians";
	public final static String ERROR_MAGNETO_TITLE = "Magneto";
	public final static String ERROR_PASSWORD_MISMATCH = "Passwords did not match";
	public final static String ERROR_RED_SKULL_MESSAGE = "Red Skull must lead HYDRA villians";
	public final static String ERROR_RED_SKULL_TITLE = "Red Skull";
	public final static String ERROR_USERNAME_ALREADY_USED = "Username already exists";
	public final static String ERROR_VILLIAN_MISMATCH_MESSAGE_2_PLURAL = " villians. You have ";
	public final static String ERROR_VILLIAN_MISMATCH_MESSAGE_2_SINGULAR = " villian. You have ";
	public final static String ERROR_VILLIAN_MISMATCH_TITLE = "Villian Mismatch";
	
	public final static String FILE_COLOR = RESOURCES_DIRECTORY + "images/Colors/";
	public final static String FILE_EXTENSION_PNG = "png";
	public final static String FILE_PNG = ".png";
	public final static String FILE_RESOURCES = "resources/";
	
	public final static String GAME_STATE_END_TURN = "End Turn";
	public final static String GAME_STATE_INITIAL_MESSAGE = "Waiting for other Players";
	public final static String GAME_STATE_WAITING = "Waiting for Player ";
	
	public final static String HAND_ATTACK_POINTS = "Current Attack Points: ";
	public final static String HAND_BUY_POINTS = "Current Buy Points: ";
	public final static String HAND_IN_HAND = "Cards Currently in Hand";
	public final static String HAND_PLAYED = "Cards Played This Turn";
	
	public final static String HTML_BODY = "<html><body>";
	public final static String HTML_BODY_CLOSE = "</body></html>";
	public final static String HTML_BR = "<br>";
	public final static String HTML_CENTER = "<center>";
	public final static String HTML_CENTER_CLOSE = "</center>";
	public final static String HTML_H1 = "<h1>";
	public final static String HTML_H1_CLOSE = "</h1>";
	public final static String HTML_H3 = "<h3>";
	public final static String HTML_H3_CLOSE = "</h3>";
	
	public final static String JOIN_COULD_NOT_JOIN_GAME_MESSAGE = "Could not join game";
	public final static String JOIN_COULD_NOT_JOIN_GAME_TITLE = "Could not join the game";
	public final static String JOIN_CURRENT_PLAYERS = "Current Players";
	public final static String JOIN_GAME_EXISTS_MESSAGE = "A game with that title already exists";
	public final static String JOIN_GAME_EXISTS_TITLE = "Game already exists";
	public final static String JOIN_MESSAGE = "Join an existing game, or create your own";
	public final static String JOIN_TOTAL = "/4 : ";
	public final static String JOIN_WELCOME = "Welcome to the game hub";
	
	public final static String LOCATION_BRIDGE_FIXED = "Bridge";
	public final static String LOCATION_STREETS_FIXED = "Streets";
	public final static String LOCATION_ROOFTOPS_FIXED = "Rooftops";
	public final static String LOCATION_BANK_FIXED = "Bank";
	public final static String LOCATION_SEWERS_FIXED = "Sewers";

	public final static String LOG_COMPONENT_NO_MESSAGES = "No Messages";

	public final static String MESSAGE_BODY = "Message Body";
	public final static String MESSAGE_HTML = "<html><div style=\"width:%dpx;\">%s</div><html>";
	public final static String MESSAGE_VIEW = "Message View";
	
	public final static String MIDDLE_CHAT_HIDE = "Hide Messages";
	public final static String MIDDLE_CHAT_SHOW = "Show Messages";
	public final static String MIDDLE_HAND_HIDE = "Hide Hand";
	public final static String MIDDLE_HAND_SHOW = "Show Hand";
	
	public final static String NEW_GAME_MESSAGE = "New Game Settings";
	
	public final static String PLAYER_WAITING_EDIT_HEROES = "Edit Heroes";
	public final static String PLAYER_WAITING_EDIT_MASTERMIND = "Edit Mastermind";
	public final static String PLAYER_WAITING_EDIT_SCHEME = "Edit Scheme";
	public final static String PLAYER_WAITING_EDIT_VILLIANS = "Edit Villians";
	public final static String PLAYER_WAITING_MASTERMIND = "Mastermind";
	public final static String PLAYER_WAITING_MESSAGE_1 = "Waiting for Players: up to ";
	public final static String PLAYER_WAITING_MESSAGE_2 = " more may join";
	public final static String PLAYER_WAITING_SCHEME = "Scheme";
	public final static String PLAYER_WAITING_SELECT_MASTERMIND = "You must select 1 Mastermind";
	public final static String PLAYER_WAITING_SELECT_SCHEME = "You must select 1 Scheme Card";
	public final static String PLAYER_WAITING_START_WITH_CURRENT_PLAYERS = "Start game with current players";
	public final static String PLAYER_WAITING_TITLE = "Player Waiting View";
	
	public final static String POPUP_DISCARD = "Discard";
	public final static String POPUP_WOUNDS = "Wounds";
	public final static String PRELOADER_MESSAGE = "Loading Images";
	public final static String PRELOADER_TITLE = "Legendary Loading";
	
	public final static String SELECT_HEROES = "Set Selected Heroes";
	public final static String SELECT_MASTERMIND = "Set Selected Mastermind";
	public final static String SELECT_SCHEME = "Set Selected Scheme";
	public final static String SELECT_VILLIANS = "Set Selected Villians";
	
	public final static String STATE_ATTACK = "attack";
	public final static String STATE_BATTLEFIELD_PROMOTION_ABILITY = "battlefieldPromotionAbility";
	public final static String STATE_BITTER_CAPTOR_FIGHT = "bitterCaptorFight";
	public final static String STATE_BUY = "buy";
	public final static String STATE_COPY_POWERS_ABILITY = "copyPowersAbility";
	public final static String STATE_COVERING_FIRE_ABILITY = "coveringFireAbility";
	public final static String STATE_COVERING_FIRE_DISCARD = "coveringFireDiscard";
	public final static String STATE_CRUEL_RULER_FIGHT = "cruelRulerFight";
	public final static String STATE_CYCLOPS_ABILITY = "cyclopsAbility";
	public final static String STATE_DANGEROUS_RESCUE_ABILITY = "dangerousRescueAbility";
	public final static String STATE_DARK_TECHNOLOGY_FIGHT = "darkTechnologyFight";
	public final static String STATE_DESTROYER_ESCAPE = "destroyerEscape";
	public final static String STATE_DOOMBOT_LEGION_FIGHT = "doombotLegionFight";
	public final static String STATE_DR_DOOM_MASTERSTRIKE = "drDoomMasterstrike";
	public final static String STATE_ELECTROMAGNETIC_BUBBLE_FIGHT = "electromagneticBubbleFight";
	public final static String STATE_ENERGY_DRAIN_ABILITY = "energyDrainAbility";
	public final static String STATE_HEALING_FACTOR_ABILITY = "healingFactorAbility";
	public final static String STATE_HERE_HOLD_THIS_FOR_A_SECOND_ABILITY = "hereHoldThisForASecondAbility";
	public final static String STATE_HEY_CAN_I_GET_A_DO_OVER_ABILITY = "heyCanIGetADoOverAbility";
	public final static String STATE_HYDRA_KIDNAPPERS_FIGHT = "hydraKidnappersFight";
	public final static String STATE_HYPNOTIC_CHARM_ABILITY = "hypnoticCharmAbility";
	public final static String STATE_JUGGERNAUT_AMBUSH = "juggernautAmbush";
	public final static String STATE_JUGGERNAUT_ESCAPE = "juggernautEscape";
	public final static String STATE_MAESTRO_FIGHT = "maestroFight";
	public final static String STATE_MAGNETO_MASTERSTRIKE = "magnetoMasterstrike";
	public final static String STATE_MANIACAL_TYRANT_FIGHT = "maniacalTyrantFight";
	public final static String STATE_MELTER_FIGHT = "melterFight";
	public final static String STATE_MONARCHS_DECREE_DISCARD = "monarchsDecreeDiscard";
	public final static String STATE_MONARCHS_DECREE_FIGHT = "monarchsDecreeFight";
	public final static String STATE_PAIBOK_THE_POWER_SKRULL_FIGHT = "paibokThePowerSkrullFight";
	public final static String STATE_PLAYED = "played";
	public final static String STATE_PURE_FURY_ABILITY = "pureFuryAbility";
	public final static String STATE_RANDOM_ACTS_OF_UNKINDNESS_ABILITY = "randomActsOfUnkindnessAbility";
	public final static String STATE_RANDOM_ACTS_OF_UNKINDNESS_PASS = "randomActsOfUnkindnessPass";
	public final static String STATE_RED_SKULL_MASTERSTRIKE = "redSkullMasterstrike";
	public final static String STATE_RUTHLESS_DICTATOR_DISCARD = "ruthlessDictatorDiscard";
	public final static String STATE_RUTHLESS_DICTATOR_KO = "ruthlessDictatorKO";
	public final static String STATE_SENTINEL_FIGHT = "sentinelFight";
	public final static String STATE_SHADOWED_THOUGHTS_ABILITY = "shadowedThoughtsAbility";
	public final static String STATE_SILENT_SNIPER_ABILITY = "silentSniperAbility";
	public final static String STATE_STACK_THE_DECK_ABILITY = "stackTheDeckAbility";
	public final static String STATE_SUPER_SKRULL_FIGHT = "superSkrullFight";
	public final static String STATE_THE_AMAZING_SPIDERMAN_ABILITY = "theAmazingSpidermanAbility";
	public final static String STATE_UNSTOPPABLE_HULK_ABILITY = "unstoppableHulkAbility";
	public final static String STATE_USE = "use";
	public final static String STATE_VANISHING_ILLUSIONS_FIGHT = "vanishingIllusionsFight";
	public final static String STATE_WHIRLWIND_FIGHT = "whirlwindFight";
	
	public final static String TITLES_CAPTURED_CARDS = "Captured Cards";
	public final static String TITLES_DISCARD_PILE = "Discard Pile";
	public final static String TITLES_DRAW_PILE = "Draw Pile";
	public final static String TITLES_ESCAPED = "escaped";
	public final static String TITLES_KO = "ko";
	public final static String TITLES_NO_PEEKING = "No Peeking";
	public final static String TITLES_VICTORY_PILE = "Victory Pile";
	
	public final static String TOOL_TIP_PASSWORD = "Please match the requested format. The password must be five or more characters: letters, digits, underscore, or dash.";
	public final static String TOOL_TIP_PASSWORD_AGAIN = "Make sure the two passwords match!";
	public final static String TOOL_TIP_USERNAME = "The username must be between three and seven characters: letters, digits, underscore, or dash.";
	
	//cardCategory
	public final static String CSV_CATEGORY_HYDRA = "HYDRA";
	public final static String CSV_CATEGORY_XMEN = "XMen";
	
	//cardImagePath
	public final static String A_DAY_UNLIKE_ANY_OTHER_PATH = RESOURCES_DIRECTORY + "images/Heros/CaptainAmerica/ADayUnlikeAnyOther.png";
	public final static String ABOMINATION_PATH = RESOURCES_DIRECTORY + "images/Villians/Radiation/Abomination.png";
	public final static String AGENT_PATH = RESOURCES_DIRECTORY + "images/Other/Agent/Agent.png";
	public final static String ARC_REACTOR_PATH = RESOURCES_DIRECTORY + "images/Heros/IronMan/ArcReactor.png";
	public final static String ASTONISHING_STRENGTH_PATH = RESOURCES_DIRECTORY + "images/Heros/SpiderMan/AstonishingStrength.png";
	public final static String AVENGERS_ASSEMBLE_PATH = RESOURCES_DIRECTORY + "images/Heros/CaptainAmerica/AvengersAssemble.png";
	public final static String BACK_PATH = RESOURCES_DIRECTORY + "images/Other/Back/Back.png";
	public final static String BARON_ZEMO_PATH = RESOURCES_DIRECTORY + "images/Villians/MastersOfEvil/BaronZemo.png";
	public final static String BATTLEFIELD_PROMOTION_PATH = RESOURCES_DIRECTORY + "images/Heros/NickFury/BattlefieldPromotion.png";
	public final static String BERSERKER_RAGE_PATH = RESOURCES_DIRECTORY + "images/Heros/Wolverine/BerserkerRage.png";
	public final static String BITTER_CAPTOR_PATH = RESOURCES_DIRECTORY + "images/Masterminds/Magneto/BitterCaptor.png";
	public final static String BLOB_PATH = RESOURCES_DIRECTORY + "images/Villians/Brotherhood/Blob.png";
	public final static String BOARD_PATH = RESOURCES_DIRECTORY + "images/Other/Board/board.jpg";
	public final static String BORROWED_BRAWN_PATH = RESOURCES_DIRECTORY + "images/Heros/Rogue/BorrowedBrawn.png";
	public final static String BYSTANDER_PATH = RESOURCES_DIRECTORY + "images/Other/Bystander/Bystander.png";
	public final static String CALL_LIGHTNING_PATH = RESOURCES_DIRECTORY + "images/Heros/Thor/CallLightning.png";
	public final static String CARD_SHARK_PATH = RESOURCES_DIRECTORY + "images/Heros/Gambit/CardShark.png";
	public final static String CITY_PATH = RESOURCES_DIRECTORY + "images/Other/Board/City.png";
	public final static String COPY_POWERS_PATH = RESOURCES_DIRECTORY + "images/Heros/Rogue/CopyPowers.png";
	public final static String COVERING_FIRE_PATH = RESOURCES_DIRECTORY + "images/Heros/Hawkeye/CoveringFire.png";
	public final static String COVERT_OPERATION_PATH = RESOURCES_DIRECTORY + "images/Heros/BlackWidow/CovertOperation.png";
	public final static String CRAZED_RAMPAGE_PATH = RESOURCES_DIRECTORY + "images/Heros/Hulk/CrazedRampage.png";
	public final static String CRUEL_RULER_PATH = RESOURCES_DIRECTORY + "images/Masterminds/Loki/CruelRuler.png";
	public final static String CRUSHING_SHOCKWAVE_PATH = RESOURCES_DIRECTORY + "images/Masterminds/Magneto/CrushingShockwave.png";
	public final static String DANGEROUS_RESCUE_PATH = RESOURCES_DIRECTORY + "images/Heros/BlackWidow/DangerousRescue.png";
	public final static String DARK_TECHNOLOGY_PATH = RESOURCES_DIRECTORY + "images/Masterminds/DrDoom/DarkTechnology.png";
	public final static String DESTROYER_PATH = RESOURCES_DIRECTORY + "images/Villians/EnemiesOfAsgard/Destroyer.png";
	public final static String DETERMINATION_PATH = RESOURCES_DIRECTORY + "images/Heros/Cyclops/Determination.png";
	public final static String DIAMOND_FORM_PATH = RESOURCES_DIRECTORY + "images/Heros/EmmaFrost/DiamondForm.png";
	public final static String DIVING_BLOCK_PATH = RESOURCES_DIRECTORY + "images/Heros/CaptainAmerica/DivingBlock.png";
	public final static String DOCTOR_OCTOPUS_PATH = RESOURCES_DIRECTORY + "images/Villians/SpiderFoes/DoctorOctopus.png";
	public final static String DOOMBOT_LEGION_PATH = RESOURCES_DIRECTORY + "images/Henchmen/DoombotLegion/DoombotLegion.png";
	public final static String DR_DOOM_PATH = RESOURCES_DIRECTORY + "images/Masterminds/DrDoom/DrDoom.png";
	public final static String ELECTROMAGNETIC_BUBBLE_PATH = RESOURCES_DIRECTORY + "images/Masterminds/Magneto/ElectromagneticBubble.png";
	public final static String ENCHANTRESS_PATH = RESOURCES_DIRECTORY + "images/Villians/EnemiesOfAsgard/Enchantress.png";
	public final static String ENDLESS_ARMIES_OF_HYDRA_PATH = RESOURCES_DIRECTORY + "images/Villians/Hydra/EndlessArmiesOfHydra.png";
	public final static String ENDLESS_INVENTION_PATH = RESOURCES_DIRECTORY + "images/Heros/IronMan/EndlessInvention.png";
	public final static String ENDLESS_RESOURCES_PATH = RESOURCES_DIRECTORY + "images/Masterminds/RedSkull/EndlessResources.png";
	public final static String ENERGY_DRAIN_PATH = RESOURCES_DIRECTORY + "images/Heros/Rogue/EnergyDrain.png";
	public final static String FRENZIED_SLASHING_PATH = RESOURCES_DIRECTORY + "images/Heros/Wolverine/FrenziedSlashing.png";
	public final static String FROST_GIANT_PATH = RESOURCES_DIRECTORY + "images/Villians/EnemiesOfAsgard/FrostGiant.png";
	public final static String GATHERING_STORMCLOUDS_PATH = RESOURCES_DIRECTORY + "images/Heros/Storm/GatheringStormclouds.png";
	public final static String GOD_OF_THUNDER_PATH = RESOURCES_DIRECTORY + "images/Heros/Thor/GodOfThunder.png";
	public final static String GREAT_RESPONSIBILITY_PATH = RESOURCES_DIRECTORY + "images/Heros/SpiderMan/GreatResponsibility.png";
	public final static String GREEN_GOBLIN_PATH = RESOURCES_DIRECTORY + "images/Villians/SpiderFoes/GreenGoblin.png";
	public final static String GROWING_ANGER_PATH = RESOURCES_DIRECTORY + "images/Heros/Hulk/GrowingAnger.png";
	public final static String HAND_NINJAS_PATH = RESOURCES_DIRECTORY + "images/Henchmen/HandNinjas/HandNinjas.png";
	public final static String HEALING_FACTOR_PATH = RESOURCES_DIRECTORY + "images/Heros/Wolverine/HealingFactor.png";
	public final static String HERE_HOLD_THIS_FOR_A_SECOND_PATH = RESOURCES_DIRECTORY + "images/Heros/Deadpool/HereHoldThisForASecond.png";
	public final static String HEY_CAN_I_GET_A_DO_OVER_PATH = RESOURCES_DIRECTORY + "images/Heros/Deadpool/HeyCanIGetADoOver.png";
	public final static String HIGH_STAKES_JACKPOT_PATH = RESOURCES_DIRECTORY + "images/Heros/Gambit/HighStakesJackpot.png";
	public final static String HIGH_TECH_WEAPONRY_PATH = RESOURCES_DIRECTORY + "images/Heros/NickFury/HighTechWeaponry.png";
	public final static String HQ_PATH = RESOURCES_DIRECTORY + "images/Other/Board/HQ.png";
	public final static String HULK_SMASH_PATH = RESOURCES_DIRECTORY + "images/Heros/Hulk/HulkSmash.png";
	public final static String HYDRA_CONSPIRACY_PATH = RESOURCES_DIRECTORY + "images/Masterminds/RedSkull/HydraConspiracy.png";
	public final static String HYDRA_KIDNAPPERS_PATH = RESOURCES_DIRECTORY + "images/Villians/Hydra/HydraKidnappers.png";
	public final static String HYPNOTIC_CHARM_PATH = RESOURCES_DIRECTORY + "images/Heros/Gambit/HypnoticCharm.png";
	public final static String IMPOSSIBLE_TRICK_SHOT_PATH = RESOURCES_DIRECTORY + "images/Heros/Hawkeye/ImpossibleTrickShot.png";
	public final static String JUGGERNAUT_PATH = RESOURCES_DIRECTORY + "images/Villians/Brotherhood/Juggernaut.png";
	public final static String KEEN_SENSES_PATH = RESOURCES_DIRECTORY + "images/Heros/Wolverine/KeenSenses.png";
	public final static String LEGENDARY_COMMANDER_PATH = RESOURCES_DIRECTORY + "images/Heros/NickFury/LegendaryCommander.png";
	public final static String LIGHTNING_BOLT_PATH = RESOURCES_DIRECTORY + "images/Heros/Storm/LightningBolt.png";
	public final static String LOKI_PATH = RESOURCES_DIRECTORY + "images/Masterminds/Loki/Loki.png";
	public final static String LOSE_PATH = RESOURCES_DIRECTORY + "images/Other/EndGame/lose.png";
	public final static String MAESTRO_PATH = RESOURCES_DIRECTORY + "images/Villians/Radiation/Maestro.png";
	public final static String MAGNETO_PATH = RESOURCES_DIRECTORY + "images/Masterminds/Magneto/Magneto.png";
	public final static String MANIACAL_TYRANT_PATH = RESOURCES_DIRECTORY + "images/Masterminds/Loki/ManiacalTyrant.png";
	public final static String MASTERSTRIKE_PATH = RESOURCES_DIRECTORY + "images/Other/MasterStrike/MasterStrike.png";
	public final static String MELTER_PATH = RESOURCES_DIRECTORY + "images/Villians/MastersOfEvil/Melter.png";
	public final static String MENTAL_DISCIPLINE_PATH = RESOURCES_DIRECTORY + "images/Heros/EmmaFrost/MentalDiscipline.png";
	public final static String MIDTOWN_BANK_ROBBERY_PATH = RESOURCES_DIRECTORY + "images/Schemes/MidtownBankRobbery/MidtownBankRobbery.png";
	public final static String MISSION_ACCOMPLISHED_PATH = RESOURCES_DIRECTORY + "images/Heros/BlackWidow/MissionAccomplished.png";
	public final static String MONARCHS_DECREE_PATH = RESOURCES_DIRECTORY + "images/Masterminds/DrDoom/MonarchsDecree.png";
	public final static String MYSTIQUE_PATH = RESOURCES_DIRECTORY + "images/Villians/Brotherhood/Mystique.png";
	public final static String NEGABLAST_GRENADES_PATH = RESOURCES_DIRECTORY + "images/Masterminds/RedSkull/NegablastGrenades.png";
	public final static String NEGATIVE_ZONE_PRISON_BREAKOUT_PATH = RESOURCES_DIRECTORY + "images/Schemes/NegativeZonePrisonBreakout/NegativeZonePrisonBreakout.png";
	public final static String ODDBALL_PATH = RESOURCES_DIRECTORY + "images/Heros/Deadpool/Oddball.png";
	public final static String ODINSON_PATH = RESOURCES_DIRECTORY + "images/Heros/Thor/Odinson.png";
	public final static String OFFICER_PATH = RESOURCES_DIRECTORY + "images/Other/Officer/Officer.png";
	public final static String OPTIC_BLAST_PATH = RESOURCES_DIRECTORY + "images/Heros/Cyclops/OpticBlast.png";
	public final static String PAIBOK_THE_POWER_SKRULL_PATH = RESOURCES_DIRECTORY + "images/Villians/Skrulls/PaibokThePowerSkrull.png";
	public final static String PERFECT_TEAMWORK_PATH = RESOURCES_DIRECTORY + "images/Heros/CaptainAmerica/PerfectTeamwork.png";
	public final static String PORTALS_TO_THE_DARK_DIMENSION_PATH = RESOURCES_DIRECTORY + "images/Schemes/PortalsToTheDarkDimension/PortalsToTheDarkDimension.png";
	public final static String PSYCHIC_LINK_PATH = RESOURCES_DIRECTORY + "images/Heros/EmmaFrost/PsychicLink.png";
	public final static String PURE_FURY_PATH = RESOURCES_DIRECTORY + "images/Heros/NickFury/PureFury.png";
	public final static String QUANTUM_BREAKTHROUGH_PATH = RESOURCES_DIRECTORY + "images/Heros/IronMan/QuantumBreakthrough.png";
	public final static String QUICK_DRAW_PATH = RESOURCES_DIRECTORY + "images/Heros/Hawkeye/QuickDraw.png";
	public final static String RANDOM_ACTS_OF_UNKINDNESS_PATH = RESOURCES_DIRECTORY + "images/Heros/Deadpool/RandomActsOfUnkindness.png";
	public final static String RED_SKULL_PATH = RESOURCES_DIRECTORY + "images/Masterminds/RedSkull/RedSkull.png";
	public final static String REPLACE_EARTHS_LEADERS_WITH_KILLBOTS_PATH = RESOURCES_DIRECTORY + "images/Schemes/ReplaceEarthsLeadersWithKillbots/ReplaceEarthsLeadersWithKillbots.png";
	public final static String REPULSOR_RAYS_PATH = RESOURCES_DIRECTORY + "images/Heros/IronMan/RepulsorRays.png";
	public final static String RUTHLESS_DICTATOR_PATH = RESOURCES_DIRECTORY + "images/Masterminds/RedSkull/RuthlessDictator.png";
	public final static String SABRETOOTH_PATH = RESOURCES_DIRECTORY + "images/Villians/Brotherhood/Sabretooth.png";
	public final static String SAVAGE_LAND_MUTATES_PATH = RESOURCES_DIRECTORY + "images/Henchmen/SavageLandMutates/SavageLandMutates.png";
	public final static String SCHEME_TWIST_PATH = RESOURCES_DIRECTORY + "images/Other/SchemeTwist/SchemeTwist.png";
	public final static String SECRET_INVASION_OF_THE_SKRULL_SHAPESHIFTERS_PATH = RESOURCES_DIRECTORY + "images/Schemes/SecretInvasionOfTheSkrullShapeshifters/SecretInvasionOfTheSkrullShapeshifters.png";
	public final static String SECRETS_OF_TIME_TRAVEL_PATH = RESOURCES_DIRECTORY + "images/Masterminds/DrDoom/SecretsOfTimeTravel.png";
	public final static String SENTINEL_PATH = RESOURCES_DIRECTORY + "images/Henchmen/Sentinel/Sentinel.png";
	public final static String SHADOWED_THOUGHTS_PATH = RESOURCES_DIRECTORY + "images/Heros/EmmaFrost/ShadowedThoughts.png";
	public final static String SILENT_SNIPER_PATH = RESOURCES_DIRECTORY + "images/Heros/BlackWidow/SilentSniper.png";
	public final static String SKRULL_QUEEN_VERANKE_PATH = RESOURCES_DIRECTORY + "images/Villians/Skrulls/SkrullQueenVeranke.png";
	public final static String SKRULL_SHAPESHIFTERS_PATH = RESOURCES_DIRECTORY + "images/Villians/Skrulls/SkrullShapeshifters.png";
	public final static String SPINNING_CYCLONE_PATH = RESOURCES_DIRECTORY + "images/Heros/Storm/SpinningCyclone.png";
	public final static String STACK_THE_DECK_PATH = RESOURCES_DIRECTORY + "images/Heros/Gambit/StackTheDeck.png";
	public final static String STEAL_ABILITIES_PATH = RESOURCES_DIRECTORY + "images/Heros/Rogue/StealAbilities.png";
	public final static String SUPER_HERO_CIVIL_WAR_PATH = RESOURCES_DIRECTORY + "images/Schemes/SuperHeroCivilWar/SuperHeroCivilWar.png";
	public final static String SUPER_SKRULL_PATH = RESOURCES_DIRECTORY + "images/Villians/Skrulls/SuperSkrull.png";
	public final static String SUPREME_HYDRA_PATH = RESOURCES_DIRECTORY + "images/Villians/Hydra/SupremeHydra.png";
	public final static String SURGE_OF_POWER_PATH = RESOURCES_DIRECTORY + "images/Heros/Thor/SurgeOfPower.png";
	public final static String TEAM_PLAYER_PATH = RESOURCES_DIRECTORY + "images/Heros/Hawkeye/TeamPlayer.png";
	public final static String THE_AMAZING_SPIDERMAN_PATH = RESOURCES_DIRECTORY + "images/Heros/SpiderMan/TheAmazingSpiderman.png";
	public final static String THE_LEADER_PATH = RESOURCES_DIRECTORY + "images/Villians/Radiation/TheLeader.png";
	public final static String THE_LEGACY_VIRUS_PATH = RESOURCES_DIRECTORY + "images/Schemes/TheLegacyVirus/TheLegacyVirus.png";
	public final static String THE_LIZARD_PATH = RESOURCES_DIRECTORY + "images/Villians/SpiderFoes/TheLizard.png";
	public final static String TIDAL_WAVE_PATH = RESOURCES_DIRECTORY + "images/Heros/Storm/TidalWave.png";
	public final static String TREASURES_OF_LATVERIA_PATH = RESOURCES_DIRECTORY + "images/Masterminds/DrDoom/TreasuresOfLatveria.png";
	public final static String TROOPER_PATH = RESOURCES_DIRECTORY + "images/Other/Trooper/Trooper.png";
	public final static String ULTRON_PATH = RESOURCES_DIRECTORY + "images/Villians/MastersOfEvil/Ultron.png";
	public final static String UNENDING_ENERGY_PATH = RESOURCES_DIRECTORY + "images/Heros/Cyclops/UnendingEnergy.png";
	public final static String UNLEASH_THE_POWER_OF_THE_COSMIC_CUBE_PATH = RESOURCES_DIRECTORY + "images/Schemes/UnleashThePowerOfTheCosmicCube/UnleashThePowerOfTheCosmicCube.png";
	public final static String UNSTOPPABLE_HULK_PATH = RESOURCES_DIRECTORY + "images/Heros/Hulk/UnstoppableHulk.png";
	public final static String VANISHING_ILLUSIONS_PATH = RESOURCES_DIRECTORY + "images/Masterminds/Loki/VanishingIllusions.png";
	public final static String VENOM_PATH = RESOURCES_DIRECTORY + "images/Villians/SpiderFoes/Venom.png";
	public final static String VIPER_PATH = RESOURCES_DIRECTORY + "images/Villians/Hydra/Viper.png";
	public final static String WIN_PATH = RESOURCES_DIRECTORY + "images/Other/EndGame/win.png";
	public final static String WEB_SHOOTERS_PATH = RESOURCES_DIRECTORY + "images/Heros/SpiderMan/WebShooters.png";
	public final static String WHIRLWIND_PATH = RESOURCES_DIRECTORY + "images/Villians/MastersOfEvil/Whirlwind.png";
	public final static String WHISPERS_AND_LIES_PATH = RESOURCES_DIRECTORY + "images/Masterminds/Loki/WhispersAndLies.png";
	public final static String WOUND_PATH = RESOURCES_DIRECTORY + "images/Other/Wound/Wound.png";
	public final static String X_MEN_UNITED_PATH = RESOURCES_DIRECTORY + "images/Heros/Cyclops/XMenUnited.png";
	public final static String XAVIERS_NEMESIS_PATH = RESOURCES_DIRECTORY + "images/Masterminds/Magneto/XaviersNemesis.png";
	public final static String YMIR_FROST_GIANT_KING_PATH = RESOURCES_DIRECTORY + "images/Villians/EnemiesOfAsgard/YmirFrostGiantKing.png";
	public final static String ZZZAX_PATH = RESOURCES_DIRECTORY + "images/Villians/Radiation/Zzzax.png";
	
	//cardMessage
	public final static String BATTLEFIELD_PROMOTION_MESSAGE = "You may KO a SHIELD Hero from your hand or discard pile. If you do, you may gain a SHIELD Officer to your hand.";
	public final static String BITTER_CAPTOR_FIGHT_MESSAGE = "Recruit an XMen Hero from the HQ for free";
	public final static String COPY_POWERS_MESSAGE = "Play this card as a copy of another Hero you played this turn. This card is both Red and the color you copy";
	public final static String COVERING_FIRE_MESSAGE = "Black: Choose one: each other player draws a card or each other player discards a card";
	public final static String CRUEL_RULER_FIGHT_MESSAGE = "Defeat a Villain in the City for free";
	public final static String CYCLOPS_MESSAGE = "To play this card, you must discard a card from your hand";
	public final static String DANGEROUS_RESCUE_MESSAGE = "Red: You may KO a card from your hand or discard pile. If you do, rescue a Bystander.";
	public final static String DARK_TECHNOLOGY_FIGHT_MESSAGE = "You may recruit a Black or Blue Hero from the HQ for free";
	public final static String DESTROYER_ESCAPE_MESSAGE = "Each player KOs two of their Heroes";
	public final static String DOOMBOT_LEGION_FIGHT_MESSAGE = "Look at the top two cards of your deck. KO one of them and put the other back";
	public final static String DR_DOOM_MASTERSTRIKE_MESSAGE = "Each player with exactly 6 cards in hand reveals a Black Hero or puts 2 cards from their hand on top of their deck";
	public final static String ELECTROMAGNETIC_BUBBLE_FIGHT_MESSAGE = "Choose one of your XMen Heroes. When you draw a new hand of cards at the end of this turn, add that Hero to your hand as a seventh card";
	public final static String ENERGY_DRAIN_MESSAGE = "Red: You may KO a card from your hand or discard pile. If you do, you get +1 Buy Power";
	public final static String HEALING_FACTOR_MESSAGE = "You may KO a Wound from your hand or discard pile. If you do, draw a card";
	public final static String HERE_HOLD_THIS_FOR_A_SECOND_MESSAGE = "A Villain of your choice captures a Bystander";
	public final static String HEY_CAN_I_GET_A_DO_OVER_MESSAGE = "If this is the first Hero you played this turn, you may discard the rest of your hand and draw four cards";
	public final static String HYDRA_KIDNAPPERS_FIGHT_MESSAGE = "You may gain a SHIELD Officer";
	public final static String HYPNOTIC_CHARM_MESSAGE = "Reveal the top card of your deck. Discard it or put it back. Yellow: Do the same thing to each other player's deck";
	public final static String JUGGERNAUT_AMBUSH_MESSAGE = "Each player KOs two Heroes from their Discard Pile";
	public final static String JUGGERNAUT_ESCAPE_MESSAGE = "Each player KOs two Heroes from their hand";
	public final static String MAESTRO_FIGHT_MESSAGE = "For each of your Green Heroes, KO one of your Heroes";
	public final static String MAGNETO_MASTERSTRIKE_MESSAGE = "Each player reveals an X-Men Hero or discards down to four cards";
	public final static String MANIACAL_TYRANT_FIGHT_MESSAGE = "KO up to four cards from your discard pile";
	public final static String MELTER_FIGHT_MESSAGE = "Each player reveals the top card of their deck. For each card, you choose to KO it or put it back";
	public final static String MONARCHS_DECREE_FIGHT_MESSAGE = "Choose one: each other player draws a card or each other player discards a card";
	public final static String PAIBOK_THE_POWER_SKRULL_FIGHT_MESSAGE = "Choose a hero in the HQ for each player. Each player gains that hero";
	public final static String PURE_FURY_MESSAGE = "Defeat any Villain or Mastermind whose Hit Points is less than the number of SHIELD Heroes in the KO pile";
	public final static String RANDOM_ACTS_OF_UNKINDNESS_MESSAGE = "You may gain a Wound to your hand. Then each player passes a card from their hand to the player on their left";
	public final static String RED_SKULL_MASTERSTRIKE_MESSAGE = "Each player KOs a Hero from their hand";
	public final static String RUTHLESS_DICTATOR_FIGHT_MESSAGE = "Look at the top three cards of your deck. KO one, discard one and put one back on top of your deck";
	public final static String SENTINEL_FIGHT_MESSAGE = "KO one of your Heroes";
	public final static String SHADOWED_THOUGHTS_MESSAGE = "Red: You may play the top card of the Villain Deck. If you do, you get +2 Attack Power";
	public final static String SILENT_SNIPER_MESSAGE = "Defeat a Villain or Mastermind that has a Bystander";
	public final static String SPINNING_CYCLONE_MESSAGE = "You may move a Villain to a new city space. Rescue any Bystanders captured by that Villain. (If you move a Villain to a city space that already has a Villain, swap them)";
	public final static String STACK_THE_DECK_MESSAGE = "Draw two cards. Then put a card from your hand on top of your deck";
	public final static String SUPER_SKRULL_FIGHT_MESSAGE = "Each player KOs one of their Heroes";
	public final static String THE_AMAZING_SPIDERMAN_MESSAGE = "Reveal the top three cards of your deck. Put any that cost 2 or less into your hand. Put the rest back in any order";
	public final static String UNSTOPPABLE_HULK_MESSAGE = "You may KO a Wound from your hand or discard pile. If you do, you get +2 Attack Power";
	public final static String WHIRLWIND_FIGHT_MESSAGE = "If you fight Whirlwind on the Rooftops or Bridge, KO two of your Heroes";
	public final static String YMIR_FROST_GIANT_KING_FIGHT_MESSAGE = "Choose a player. That player KOs any number of Wounds from their hand and discard pile";
	public final static String LOST_MESSAGE = "The Mastermind won";
	public final static String WIN_MESSAGE = "Your team won!";
	
	
	//cardNameFixed
	public final static String BLACK_WIDOW_FIXED = "Black Widow";
	public final static String BROTHERHOOD_FIXED = "Brotherhood";
	public final static String CAPTAIN_AMERICA_FIXED = "Captain America";
	public final static String CYCLOPS_FIXED = "Cyclops";
	public final static String DR_DOOM_FIXED = "Dr Doom";
	public final static String DEADPOOL_FIXED = "Deadpool";
	public final static String DOOMBOT_LEGION_FIXED = "Doombot Legion";
	public final static String EMMA_FROST_FIXED = "Emma Frost";
	public final static String ENEMIES_OF_ASGARD_FIXED = "Enemies of Asgard";
	public final static String GAMBIT_FIXED = "Gambit";
	public final static String HAND_NINJAS_FIXED = "Hand Ninjas";
	public final static String HAWKEYE_FIXED = "Hawkeye";
	public final static String HENCHMAN_FIXED = " (Henchman)";
	public final static String HULK_FIXED = "Hulk";
	public final static String HYDRA_FIXED = "HYDRA";
	public final static String IRON_MAN_FIXED = "Iron Man";
	public final static String LOKI_FIXED = "Loki";
	public final static String MAGNETO_FIXED = "Magneto";
	public final static String MASTERS_OF_EVIL_FIXED = "Masters of Evil";
	public final static String MIDTOWN_BANK_ROBBERY_FIXED = "Midtown Bank Robbery";
	public final static String NEGATIVE_ZONE_PRISON_BREAKOUT_FIXED = "Negative Zone Prison Breakout";
	public final static String NICK_FURY_FIXED = "Nick Fury";
	public final static String PORTALS_TO_THE_DARK_DIMENSION_FIXED = "Portals to the Dark Dimension";
	public final static String RADIATION_FIXED = "Radiation";
	public final static String RED_SKULL_FIXED = "Red Skull";
	public final static String REPLACE_EARTHS_LEADERS_WITH_KILLBOTS_FIXED = "Replace Earths Leaders with Killbots";
	public final static String ROGUE_FIXED = "Rogue";
	public final static String SAVAGE_LAND_MUTATES_FIXED = "Savage Land Mutates";
	public final static String SECRET_INVASION_OF_THE_SKRULL_SHAPESHIFTERS_FIXED = "Secret Invasion of the Skrull Shapeshifters";
	public final static String SENTINEL_FIXED = "Sentinel";
	public final static String SKRULLS_FIXED = "Skrulls";
	public final static String SPIDER_FOES_FIXED = "Spider-foes";
	public final static String SPIDERMAN_FIXED = "Spiderman";
	public final static String STORM_FIXED = "Storm";
	public final static String SUPER_HERO_CIVIL_WAR_FIXED = "Super Hero Civil War";
	public final static String THE_LEGACY_VIRUS_FIXED = "The Legacy Virus";
	public final static String THOR_FIXED = "Thor";
	public final static String UNLEASH_THE_POWER_OF_THE_COSMIC_CUBE_FIXED = "Unleash the Power of the Cosmic Cube";
	public final static String VILLIAN_FIXED = " (Villian)";
	public final static String WOLVERINE_FIXED = "Wolverine";
}
