package client.definitions;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import client.main.ClientManager;

public class ClientCardFunctions implements ClientVariables{
	
	private final String CLASS = "client.definitions.ClientCardFunctions: ";
	
	protected int buyPower;
	protected int attackPower;
	protected int price;
	protected int points;
	protected int hitPoints;
	protected String path;
	protected String index;
	protected String name;
	protected String fixedName;
	protected String description;
	protected String ambush;
	protected String fight;
	protected String escape;
	protected List<String> colors;
	protected String category;
	protected String setup;
	protected String specialRules;
	protected String twist;
	protected String evilWins;
	protected String type = null;
	protected BufferedImage image = null;

	public ClientCardFunctions(String name, String path){
 		colors = new ArrayList<String>();
 		List<String> priority = new ArrayList<String>();
 		priority.add(BOARD);
 		priority.add(BYSTANDER);
 		priority.add(BACK);
 		priority.add(OFFICER);
 		priority.add(WOUND);
 		priority.add(CITY);
 		priority.add(HQ);

 		this.name = name;
		this.path = path;

		try{
			File input = new File(LOCATION_CARD_INFO);
			Scanner myScanner = new Scanner(input);
			while(myScanner.hasNextLine()){
				String line = myScanner.nextLine();
				Scanner lineScanner = new Scanner(line);
				lineScanner.useDelimiter(COMMA);
				String newName = lineScanner.next();
				if(name.equals(newName)){
					fixName();
					String priceString = lineScanner.next();
					if(isNumeric(priceString)){
						price = Integer.valueOf(priceString);
					}
					else{
						price = NEGATIVE_ONE;
					}
					String buyString = lineScanner.next();
					if(isNumeric(buyString)){
						buyPower = Integer.valueOf(buyString);
					}
					else{
						buyPower = NEGATIVE_ONE;
					}
					String attackString = lineScanner.next();
					if(isNumeric(attackString)){
						attackPower = Integer.valueOf(attackString);
					}
					else{
						attackPower = NEGATIVE_ONE;
					}
					description = lineScanner.next();
					for(int i = ZERO; i < FOUR; ++i){
						colors.add(lineScanner.next());
					}
					category = lineScanner.next();
					ambush = lineScanner.next();
					fight = lineScanner.next();
					escape = lineScanner.next();
					String pointsString = lineScanner.next();
					if(isNumeric(pointsString)){
						points = Integer.valueOf(pointsString);
					}
					else{
						points = NEGATIVE_ONE;
					}
					String hitPointsString = lineScanner.next();
					if(isNumeric(hitPointsString)){
						hitPoints = Integer.valueOf(hitPointsString);
					}
					else{
						hitPoints = NEGATIVE_ONE;
					}
					setup = lineScanner.next();
					specialRules = lineScanner.next();
					twist = lineScanner.next();
					evilWins = lineScanner.next();
					lineScanner.close();
					myScanner.close();
					if(priority.contains(name)){
//						image = javax.imageio.ImageIO.read(classLoader.getResourceAsStream(path));
						image = javax.imageio.ImageIO.read(new File(path));
					}
					return;
				}
				lineScanner.close();
			}
			myScanner.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
		System.out.println(CLASS + name + ERROR_NOT_FOUND);
		System.exit(SYSTEM_EXIT);
	}
	
	private void fixName(){
		StringBuilder str = new StringBuilder();
		for(int i = ZERO; i < name.length(); ++i){
			if(i == ZERO){
				str.append(Character.toUpperCase(name.charAt(i)));
				continue;
			}
			if(Character.isUpperCase(name.charAt(i))){
				str.append(SPACE);
			}
			str.append(name.charAt(i));
		}
		fixedName = str.toString();
		fixedName = fixedName.replaceAll(CARD_INFO_HYDRA, CSV_CATEGORY_HYDRA);
		fixedName = fixedName.replaceAll(CARD_INFO_XMEN, CSV_CATEGORY_XMEN);
	}
	
	private boolean isNumeric(String str){
		for(char a : str.toCharArray()){
			if(!Character.isDigit(a)){
				return false;
			}
		}
		return true;
	}

	public void setType(String type, String index){
		this.type = type;
		this.index = index;
	}
	
	public boolean allSame(){
		return colors.get(ZERO).equals(colors.get(ONE)) && colors.get(ZERO).equals(colors.get(TWO)) && colors.get(ZERO).equals(colors.get(THREE));
	}
	
	public void schemeSetup(){
		if(ClientManager.getInstance().getModel().getTurnTracker().getScheme().equals(MIDTOWN_BANK_ROBBERY)){
			if(hitPoints != NEGATIVE_ONE){
				List<String> captured = null;
				if(index.equals(Integer.toString(SEWERS_NUMBER))){
					captured = ClientManager.getInstance().getModel().getCity().getSewersCaptured();
				}
				else if(index.equals(Integer.toString(BANK_NUMBER))){
					captured = ClientManager.getInstance().getModel().getCity().getBankCaptured();
				}
				else if(index.equals(Integer.toString(ROOFTOPS_NUMBER))){
					captured = ClientManager.getInstance().getModel().getCity().getRooftopsCaptured();
				}
				else if(index.equals(Integer.toString(STREETS_NUMBER))){
					captured = ClientManager.getInstance().getModel().getCity().getStreetsCaptured();
				}
				else if(index.equals(Integer.toString(BRIDGE_NUMBER))){
					captured = ClientManager.getInstance().getModel().getCity().getBridgeCaptured();
				}
				else if(index.equals(Integer.toString(MASTERMIND_NUMBER))){
					captured = ClientManager.getInstance().getModel().getCity().getMastermindCaptured();
				}
				if(captured != null){
					int count = ZERO;
					for(String str : captured){
						if(str.equals(BYSTANDER)){
							++count;
						}
					}
					hitPoints += count;
				}
			}
		}
		if(ClientManager.getInstance().getModel().getTurnTracker().getScheme().equals(PORTALS_TO_THE_DARK_DIMENSION) &&
		   hitPoints != NEGATIVE_ONE){
			if(index.equals(Integer.toString(SEWERS_NUMBER)) &&
			   ClientManager.getInstance().getModel().getCity().getSewersPortal()){
				hitPoints++;
			}
			else if(index.equals(Integer.toString(BANK_NUMBER)) &&
					ClientManager.getInstance().getModel().getCity().getBankPortal()){
				hitPoints++;
			}
			else if(index.equals(Integer.toString(ROOFTOPS_NUMBER)) &&
					ClientManager.getInstance().getModel().getCity().getRooftopsPortal()){
				hitPoints++;
			}
			else if(index.equals(Integer.toString(STREETS_NUMBER)) &&
					ClientManager.getInstance().getModel().getCity().getStreetsPortal()){
				hitPoints++;
			}
			else if(index.equals(Integer.toString(BRIDGE_NUMBER)) &&
					ClientManager.getInstance().getModel().getCity().getBridgePortal()){
				hitPoints++;
			}
			else if(index.equals(Integer.toString(MASTERMIND_NUMBER)) &&
					ClientManager.getInstance().getModel().getCity().getMastermindPortal()){
				hitPoints++;
			}
		}
		if(ClientManager.getInstance().getModel().getTurnTracker().getScheme().equals(REPLACE_EARTHS_LEADERS_WITH_KILLBOTS) &&
		   name.equals(BYSTANDER)){
			hitPoints = ClientManager.getInstance().getModel().getOtherCards().getSchemeTwists();
		}
		if(ClientManager.getInstance().getModel().getTurnTracker().getScheme().equals(SECRET_INVASION_OF_THE_SKRULL_SHAPESHIFTERS) &&
		   price != NEGATIVE_ONE &&
		   type.equals(STATE_ATTACK)){
			hitPoints = price;
		}
	}
	
	public void schemeTearDown(){
		if(ClientManager.getInstance().getModel().getTurnTracker().getScheme().equals(MIDTOWN_BANK_ROBBERY)){
			if(hitPoints != NEGATIVE_ONE){
				List<String> captured = null;
				if(index.equals(Integer.toString(SEWERS_NUMBER))){
					captured = ClientManager.getInstance().getModel().getCity().getSewersCaptured();
				}
				else if(index.equals(Integer.toString(BANK_NUMBER))){
					captured = ClientManager.getInstance().getModel().getCity().getBankCaptured();
				}
				else if(index.equals(Integer.toString(ROOFTOPS_NUMBER))){
					captured = ClientManager.getInstance().getModel().getCity().getRooftopsCaptured();
				}
				else if(index.equals(Integer.toString(STREETS_NUMBER))){
					captured = ClientManager.getInstance().getModel().getCity().getStreetsCaptured();
				}
				else if(index.equals(Integer.toString(BRIDGE_NUMBER))){
					captured = ClientManager.getInstance().getModel().getCity().getBridgeCaptured();
				}
				else if(index.equals(Integer.toString(MASTERMIND_NUMBER))){
					captured = ClientManager.getInstance().getModel().getCity().getMastermindCaptured();
				}
				if(captured != null){
					int count = ZERO;
					for(String str : captured){
						if(str.equals(BYSTANDER)){
							++count;
						}
					}
					hitPoints -= count;
				}
			}
		}
		if(ClientManager.getInstance().getModel().getTurnTracker().getScheme().equals(PORTALS_TO_THE_DARK_DIMENSION) &&
		   hitPoints != NEGATIVE_ONE){
			if(index.equals(Integer.toString(SEWERS_NUMBER)) &&
			   ClientManager.getInstance().getModel().getCity().getSewersPortal()){
				hitPoints--;
			}
			else if(index.equals(Integer.toString(BANK_NUMBER)) &&
					ClientManager.getInstance().getModel().getCity().getBankPortal()){
				hitPoints--;
			}
			else if(index.equals(Integer.toString(ROOFTOPS_NUMBER)) &&
					ClientManager.getInstance().getModel().getCity().getRooftopsPortal()){
				hitPoints--;
			}
			else if(index.equals(Integer.toString(STREETS_NUMBER)) &&
					ClientManager.getInstance().getModel().getCity().getStreetsPortal()){
				hitPoints--;
			}
			else if(index.equals(Integer.toString(BRIDGE_NUMBER)) &&
					ClientManager.getInstance().getModel().getCity().getBridgePortal()){
				hitPoints--;
			}
			else if(index.equals(Integer.toString(MASTERMIND_NUMBER)) &&
					ClientManager.getInstance().getModel().getCity().getMastermindPortal()){
				hitPoints--;
			}
		}
		if(ClientManager.getInstance().getModel().getTurnTracker().getScheme().equals(REPLACE_EARTHS_LEADERS_WITH_KILLBOTS) &&
		   name.equals(BYSTANDER)){
			hitPoints = NEGATIVE_ONE;
		}
		if(ClientManager.getInstance().getModel().getTurnTracker().getScheme().equals(SECRET_INVASION_OF_THE_SKRULL_SHAPESHIFTERS) &&
		   points != NEGATIVE_ONE &&
		   type.equals(STATE_ATTACK)){
			hitPoints = NEGATIVE_ONE;
		}
	}
	
	public int getBuyPower() {
		return buyPower;
	}
	public void setBuyPower(int buyPower){
		this.buyPower = buyPower;
	}

	public int getAttackPower() {
		return attackPower;
	}
	public void setAttackPower(int attackPower){
		this.attackPower = attackPower;
	}

	public int getPoints() {
		return points;
	}

	public String getName() {
		return name;
	}
	public void setName(String name){
		this.name = name;
	}

	public String getFixedName(){
		return fixedName;
	}
	
	public String getDescription() {
		return description;
	}

	public String getAmbush() {
		return ambush;
	}

	public String getFight() {
		return fight;
	}

	public String getEscape() {
		return escape;
	}

	public List<String> getColors() {
		return colors;
	}
	public void setColor(int location, String color){
		colors.set(location, color);
	}

	public String getCategory() {
		return category;
	}

	public int getPrice(){
		return price;
	}
	
	public int getHitPoints(){
		return hitPoints;
	}
	public void setHitPoints(int hitPoints){
		this.hitPoints = hitPoints;
	}
	
	public BufferedImage getImage() {
		try{
			if(image == null){
				image = javax.imageio.ImageIO.read(new File(path));
			}
		}
		catch(IOException e){
			e.printStackTrace();
		}
		return image;
	}
}
