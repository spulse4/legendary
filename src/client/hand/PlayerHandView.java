package client.hand;

import java.awt.Color;
import java.awt.Dimension;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import client.base.OverlayView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;

public class PlayerHandView extends OverlayView implements ClientVariables{

	private static final long serialVersionUID = -7434683190288879071L;
		
	JPanel buttonPanel;
	JPanel draw;
	JButton discardButton;
	JButton victoryButton;
	JPanel legendary;
	
	public PlayerHandView(JPanel legendary){
		this.legendary = legendary;
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.setBackground(Color.WHITE);
		setCards(ZERO, null, null, null, null);
	}
	
	public PlayerHandController getPlayerHandController(){
		return (PlayerHandController)super.getController();
	}
	
	public void setCards(int draw, List<ClientCard> discard, List<ClientCard> victory, List<ClientCard> hand, List<ClientCard> played){
		JScrollPane scroll1 = new JScrollPane(new InHandPanel(hand, legendary, HAND_IN_HAND, STATE_USE));
		scroll1.setPreferredSize(new Dimension(ZERO, PLAYER_HAND_VIEW_WIDTH));
		scroll1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scroll1.getVerticalScrollBar().setUnitIncrement(SCROLL_SPEED);
		JScrollPane scroll2 = new JScrollPane(new InHandPanel(played, legendary, HAND_PLAYED, STATE_PLAYED));
		scroll2.setPreferredSize(new Dimension(ZERO, PLAYER_HAND_VIEW_WIDTH));
		scroll2.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scroll2.getVerticalScrollBar().setUnitIncrement(SCROLL_SPEED);
		this.removeAll();
		this.add(new ButtonPanel(draw, discard, victory, legendary));
		this.add(scroll1);
		this.add(scroll2);
		this.revalidate();
	}
}
