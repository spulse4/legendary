package client.hand;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JPanel;

import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;

public class PlayerCardPanel extends JPanel implements MouseListener, ClientVariables{

	private static final long serialVersionUID = -2329085647649662986L;

	private JPanel legendary;
	private ClientCard card;
	private BufferedImage background;
		
	public PlayerCardPanel(ClientCard card, JPanel legendary, String location, int index){
		this.legendary = legendary;
		this.card = card;
		if(index == NEGATIVE_ONE){
			this.card.setType(location, card.getName());
		}
		else{
			this.card.setType(location, Integer.toString(index));
		}
		this.setPreferredSize(new Dimension(CARD_PANEL_WIDTH, CARD_PANEL_HEIGHT));
		
		List<String> colors = card.getColors();

		StringBuilder str = new StringBuilder();
		
		for(String color : colors){
			if(color.equals(NA)){
				str.append(COLOR_GRAY);
			}
			else{
				str.append(color);
			}
		}
		background = ClientManager.getInstance().getBoard().getBackground(str.toString());
		this.addMouseListener(this);
	}
	
	public void setType(String type){
		this.card.type(type);
	}
	
	@Override
	public void mouseClicked(MouseEvent arg0){
		JDialog dialog = new JDialog();
		dialog.setTitle(card.getFixedName());
		JPanel select = card.getPanel(dialog);
		int wid = DIALOG_CLICKED_WIDTH;
		int hei = (int)select.getMinimumSize().getHeight();
		dialog.setSize(wid, hei);
		dialog.getContentPane().setBackground(Color.WHITE);
		if(legendary == null){
			dialog.setLocationRelativeTo(this);
		}
		else{
			dialog.setLocationRelativeTo(legendary);
		}
		dialog.add(select);
		dialog.getRootPane().setBorder(BorderFactory.createMatteBorder(DIALOG_BORDER_WIDTH, DIALOG_BORDER_WIDTH, DIALOG_BORDER_WIDTH, DIALOG_BORDER_WIDTH, Color.BLACK));
		dialog.setModal(true);
		dialog.setVisible(true);
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {}

	@Override
	public void mouseExited(MouseEvent arg0) {}

	@Override
	public void mousePressed(MouseEvent arg0) {}

	@Override
	public void mouseReleased(MouseEvent arg0) {}

	@Override
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.drawImage(background, ZERO, ZERO, null);
		g.setColor(Color.BLACK);
		g.setFont(FONT_PLAIN_20);
		FontMetrics fm = g.getFontMetrics();
		String name = card.getFixedName();
		String type = card.getCategory();
		StringBuilder str = new StringBuilder();
		str.append(Character.toUpperCase(type.charAt(ZERO)));
		str.append(type.substring(ONE));
		type = str.toString();
		int attackPower = card.getAttackPower();
		int buyPower = card.getBuyPower();
		if(attackPower == NEGATIVE_ONE){
			attackPower = ZERO;
		}
		if(buyPower == NEGATIVE_ONE){
			buyPower = ZERO;
		}
		String attack = CARD_PANEL_ATTACK + attackPower;
		String buy = CARD_PANEL_BUY + buyPower;
		g.drawString(name, CARD_PANEL_PLAYER_CARD_BUFFER, CARD_PANEL_PLAYER_CARD_ROW_1);
		g.drawString(type, CARD_PANEL_PLAYER_CARD_BUFFER, CARD_PANEL_PLAYER_CARD_ROW_2);
		g.drawString(attack, CARD_PANEL_PLAYER_CARD_BUFFER, CARD_PANEL_PLAYER_CARD_ROW_3);
		int width = fm.stringWidth(buy);
		g.drawString(buy, CARD_PANEL_WIDTH - CARD_PANEL_PLAYER_CARD_BUFFER - width, CARD_PANEL_PLAYER_CARD_ROW_3);
	}
}
