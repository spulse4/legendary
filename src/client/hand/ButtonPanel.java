package client.hand;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import client.definitions.ClientCard;
import client.definitions.ClientVariables;

public class ButtonPanel extends JPanel implements ActionListener, ClientVariables{
	
	private static final long serialVersionUID = 6935513696513136313L;

	private int drawSize;
	private JButton draw;
	private JButton discard;
	private JButton victory;
	private JPanel legendary;
	private List<ClientCard> discardPile;
	private List<ClientCard> victoryPile;
	
	public ButtonPanel(int drawSize, List<ClientCard> discardPile, List<ClientCard> victoryPile, JPanel legendary){
		this.drawSize = drawSize;
		this.legendary = legendary;
		this.discardPile = discardPile;
		this.victoryPile = victoryPile;
		this.setOpaque(false);
		this.setPreferredSize(new Dimension(ZERO, BUTTON_PANEL_HEIGHT));
		this.setLayout(new FlowLayout());

		int discardSize = ZERO;
		int victorySize = ZERO;
		if(discardPile != null){
			discardSize = discardPile.size();
		}
		if(victoryPile != null){
			victorySize = victoryPile.size();
		}
		draw = new JButton(HTML_BODY + HTML_CENTER + BUTTON_DRAW + HTML_BR + drawSize + HTML_CENTER_CLOSE + HTML_BODY_CLOSE);
		draw.setPreferredSize(new Dimension(BUTTON_PANEL_BUTTON_WIDTH, BUTTON_PANEL_BUTTON_HEIGHT));
		draw.addActionListener(this);
		discard = new JButton(HTML_BODY + HTML_CENTER + BUTTON_DISCARD + HTML_BR + discardSize + HTML_CENTER_CLOSE + HTML_BODY_CLOSE);
		discard.setPreferredSize(new Dimension(BUTTON_PANEL_BUTTON_WIDTH, BUTTON_PANEL_BUTTON_HEIGHT));
		discard.addActionListener(this);
		victory = new JButton(HTML_BODY + HTML_CENTER + BUTTON_VICTORY + HTML_BR + victorySize + HTML_CENTER_CLOSE + HTML_BODY_CLOSE);
		victory.setPreferredSize(new Dimension(BUTTON_PANEL_VICTORY_BUTTON_WIDTH, BUTTON_PANEL_BUTTON_HEIGHT));
		victory.addActionListener(this);
		this.add(draw);
		this.add(discard);
		this.add(victory);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		final JDialog dialog = new JDialog();
		dialog.setLayout(new BorderLayout());
		JButton button = new JButton(BUTTON_CANCEL);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				dialog.dispose();
			}
			
		});
		dialog.setSize(DIALOG_STANDARD_WIDTH, DIALOG_STANDARD_HEIGHT);
		dialog.getContentPane().setBackground(Color.WHITE);
		if(arg0.getSource() == draw){
			JPanel pan = new JPanel();
			pan.setLayout(new BoxLayout(pan, BoxLayout.Y_AXIS));
			dialog.setSize(DIALOG_NO_PEEKING_WIDTH, DIALOG_NO_PEEKING_HEIGHT);
			pan.add(new JLabel(HTML_BODY + HTML_H1 + TITLES_NO_PEEKING + HTML_H1_CLOSE + HTML_BODY_CLOSE));
			pan.add(new JLabel(HTML_BODY + HTML_H3 + drawSize + CARDS_IN_THE_DRAW_PILE + HTML_H3_CLOSE + HTML_BODY_CLOSE));
			dialog.setTitle(TITLES_DRAW_PILE);
			dialog.add(pan, BorderLayout.CENTER);
		}
		else if(arg0.getSource() == discard){
			JScrollPane scroll = new JScrollPane(new InHandPanel(discardPile, legendary, TITLES_DISCARD_PILE, NULL_STRING));
			scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			scroll.getVerticalScrollBar().setUnitIncrement(SCROLL_SPEED);
			dialog.setTitle(TITLES_DISCARD_PILE);
			dialog.add(scroll, BorderLayout.CENTER);
		}
		else if(arg0.getSource() == victory){
			JScrollPane scroll = new JScrollPane(new InHandPanel(victoryPile, legendary, TITLES_VICTORY_PILE, NULL_STRING));
			scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			scroll.getVerticalScrollBar().setUnitIncrement(SCROLL_SPEED);
			dialog.setTitle(TITLES_VICTORY_PILE);
			dialog.add(scroll, BorderLayout.CENTER);
		}
		dialog.add(button, BorderLayout.SOUTH);
		dialog.getRootPane().setBorder(BorderFactory.createMatteBorder(DIALOG_BORDER_WIDTH, DIALOG_BORDER_WIDTH, DIALOG_BORDER_WIDTH, DIALOG_BORDER_WIDTH, Color.BLACK));
		dialog.setLocationRelativeTo(legendary);
		dialog.setModal(true);
		dialog.setVisible(true);
	}
}
