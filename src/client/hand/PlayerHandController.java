package client.hand;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;

public class PlayerHandController extends Controller implements ClientVariables{
		
	private int drawCards;
	private List<ClientCard> discardCards;
	private List<ClientCard> victoryCards;
	private List<ClientCard> handCards;
	private List<ClientCard> playedCards;
	
	public PlayerHandController(IView view){
		super(view);
		ClientManager.getInstance().getModel().registerObserver(this);
		drawCards = ZERO;
		discardCards = new ArrayList<ClientCard>();
		victoryCards = new ArrayList<ClientCard>();
		handCards = new ArrayList<ClientCard>();
		playedCards = new ArrayList<ClientCard>();
	}
	
	public PlayerHandView getPlayerHandView(){
		return (PlayerHandView)super.getView();
	}
	
	@Override
	public void modelChanged(){
		discardCards.clear();
		victoryCards.clear();
		handCards.clear();
		playedCards.clear();
		
		drawCards = ClientManager.getInstance().getModel().getPlayerCards().getDrawCount();
		List<String> discard = ClientManager.getInstance().getModel().getPlayerCards().getDiscard();
		List<String> victory = ClientManager.getInstance().getModel().getPlayerCards().getVictoryPile();
		List<String> hand = ClientManager.getInstance().getModel().getPlayerCards().getHand();
		List<String> played = ClientManager.getInstance().getModel().getPlayerCards().getPlayed();

		for(String str : discard){
			discardCards.add(ClientManager.getInstance().getBoard().getCard(str));
		}
		for(String str : victory){
			victoryCards.add(ClientManager.getInstance().getBoard().getCard(str));
		}
		for(String str : hand){
			handCards.add(ClientManager.getInstance().getBoard().getCard(str));
		}
		for(String str : played){
			playedCards.add(ClientManager.getInstance().getBoard().getCard(str));
		}
		discardCards = sort(discardCards);
		victoryCards = sort(victoryCards);
		handCards = sort(handCards);
		
		((PlayerHandView)getView()).setCards(drawCards, discardCards, victoryCards, handCards, playedCards);
	}
	
	public List<ClientCard> sort(List<ClientCard> cards){
		List<ClientCard> sorted = new ArrayList<ClientCard>();
		List<String> colors = new ArrayList<String>();
		colors.add(NA);
		colors.add(COLOR_RED);
		colors.add(COLOR_YELLOW);
		colors.add(COLOR_GREEN);
		colors.add(COLOR_BLUE);
		colors.add(COLOR_BLACK);
		
		for(int i = ZERO; i < colors.size(); ++i){
			for(int j = ZERO; j < cards.size(); ++j){
				if(cards.get(j).getColors().contains(colors.get(i)) && cards.get(j).allSame()){
					sorted.add(cards.get(j));
					cards.remove(j);
					--j;
				}
			}
		}
		while(cards.size() != ZERO){
			sorted.add(cards.get(ZERO));
			cards.remove(ZERO);
		}
		

		for(int i = ZERO; i < sorted.size(); ++i){
			ClientCard c = sorted.get(i);
			for(int j = ZERO; j < i; ++j){
				ClientCard d = sorted.get(j);
				if(c.getColors().contains(d.getColors().get(ZERO)) && c.getFixedName().compareTo(d.getFixedName()) < ZERO && c.allSame()){
					sorted.remove(i);
					sorted.add(j, c);
					i = j;
				}
			}
		}
		
		return sorted;
	}
}
