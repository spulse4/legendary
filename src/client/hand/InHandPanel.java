package client.hand;

import java.awt.Dimension;
import java.awt.Font;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.main.ClientManager;

public class InHandPanel extends JPanel implements ClientVariables{

	private static final long serialVersionUID = -2922295064921812176L;

	public InHandPanel(List<ClientCard> cards, JPanel legendary, String message, String location){
		this.setOpaque(false);
		this.add(new JLabel(HTML_BODY + HTML_H1 + message + HTML_H1_CLOSE + HTML_BODY_CLOSE));
		if(cards == null){
			return;
		}
		if(message.equals(HAND_PLAYED)){
			JPanel pending = new JPanel();
			pending.setLayout(new BoxLayout(pending, BoxLayout.Y_AXIS));
			JLabel label1 = new JLabel(HAND_BUY_POINTS + ClientManager.getInstance().getModel().getPlayerCards().getPendingBuyPoints());
			JLabel label2 = new JLabel(HAND_ATTACK_POINTS + ClientManager.getInstance().getModel().getPlayerCards().getPendingAttackPoints());
			Font font = FONT_PLAIN_15;
			label1.setFont(font);
			label2.setFont(font);
			pending.add(label1);
			pending.add(label2);
			this.add(pending);
			this.setPreferredSize(new Dimension(ZERO, HAND_INITIAL_HEIGHT_PLAYED + (HAND_HEIGHT_PER_CARD * cards.size())));
		}
		else{
			this.setPreferredSize(new Dimension(ZERO, HAND_INITIAL_HEIGHT_NOT_PLAYED + (HAND_HEIGHT_PER_CARD * cards.size())));
		}
		
		for(ClientCard c : cards){
			PlayerCardPanel pan = new PlayerCardPanel(c, legendary, location, NEGATIVE_ONE);
			if(c.getName().equals(WOUND)){
				pan.setType(null);
			}
			this.add(pan);
		}
	}
}
