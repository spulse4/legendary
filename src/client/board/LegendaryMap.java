package client.board;

import java.util.HashMap;

import client.preloader.Preloader;

public class LegendaryMap<K, V> extends HashMap<K, V>{

	private static final long serialVersionUID = 7924497900497692831L;

	@SuppressWarnings("unchecked")
	public V put(Object key, Object value, boolean updatePreloader){
		V returned = super.put((K)key, (V)value);
		if(updatePreloader){
			Preloader.getInstance().incCurrent();
		}
		return returned;
	}
}
