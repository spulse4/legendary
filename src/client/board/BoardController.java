package client.board;

import java.util.ArrayList;
import java.util.List;

import client.base.Controller;
import client.base.IView;
import client.definitions.ClientVariables;
import client.legendary.MidPanel;
import client.main.ClientManager;
import shared.model.ModelFacade;
import shared.model.OtherCards;

public class BoardController extends Controller implements ClientVariables{

	List<String> changes;
	
	public BoardController(IView view) {
		super(view);
		changes = new ArrayList<String>();
		ClientManager.getInstance().getModel().registerObserver(this);
	}
	
	public BoardView getBoardView(){
		return (BoardView)super.getView();
	}
	
	private void setImages(BoardComponent board, ModelFacade model){
		String scheme = model.getTurnTracker().getScheme();
		
		String escaped = model.getEscaped().getMostRecent();
		
		String ko = model.getKoPile().getMostRecent();
		
		String mastermind = model.getTurnTracker().getMastermind();
		
		String bridge = model.getCity().getBridge();
		String streets = model.getCity().getStreets();
		String rooftops = model.getCity().getRooftops();
		String bank = model.getCity().getBank();
		String sewers = model.getCity().getSewers();
		boolean mastermindPortal = model.getCity().getMastermindPortal();
		boolean bridgePortal = model.getCity().getBridgePortal();
		boolean streetsPortal = model.getCity().getStreetsPortal();
		boolean rooftopsPortal = model.getCity().getRooftopsPortal();
		boolean bankPortal = model.getCity().getBankPortal();
		boolean sewersPortal = model.getCity().getSewersPortal();
		List<String> mastermindCaptured = model.getCity().getMastermindCaptured();
		List<String> bridgeCaptured = model.getCity().getBridgeCaptured();
		List<String> streetsCaptured = model.getCity().getStreetsCaptured();
		List<String> rooftopsCaptured = model.getCity().getRooftopsCaptured();
		List<String> bankCaptured = model.getCity().getBankCaptured();
		List<String> sewersCaptured = model.getCity().getSewersCaptured();
		
		String hq1 = model.getHeadQuarters().getHQ1();
		String hq2 = model.getHeadQuarters().getHQ2();
		String hq3 = model.getHeadQuarters().getHQ3();
		String hq4 = model.getHeadQuarters().getHQ4();
		String hq5 = model.getHeadQuarters().getHQ5();
		
		board.setScheme(scheme);
		board.setEscaped(escaped);
		board.setKO(ko);
		board.setMastermind(mastermind);
		board.setMastermindCapture(mastermindCaptured);
		board.setCity(sewers, bank, rooftops, streets, bridge);
		board.setPortals(mastermindPortal, bridgePortal, streetsPortal, rooftopsPortal, bankPortal, sewersPortal);
		board.setCityCapture(sewersCaptured, bankCaptured, rooftopsCaptured, streetsCaptured, bridgeCaptured);
		board.setHQ(hq1, hq2, hq3, hq4, hq5);
	}

	private void setCounts(BoardComponent board, ModelFacade model){
		OtherCards other = model.getOtherCards();
		
		int schemes = other.getSchemeTwists();
		int escaped = ZERO;
		escaped += model.getEscaped().getBrotherhood();
		escaped += model.getEscaped().getEnemiesOfAsgard();
		escaped += model.getEscaped().getHenchmen();
		escaped += model.getEscaped().getHydra();
		escaped += model.getEscaped().getMastersOfEvil();
		escaped += model.getEscaped().getRadiation();
		escaped += model.getEscaped().getSkrulls();
		escaped += model.getEscaped().getSpiderfoes();
		escaped += model.getEscaped().getHeroes();
		escaped += model.getEscaped().getBystanders();
		int kod = ZERO;
		kod += model.getKoPile().getAvengers();
		kod += model.getKoPile().getBystanders();
		kod += model.getKoPile().getDeadpool();
		kod += model.getKoPile().getShield();
		kod += model.getKoPile().getSpiderman();
		kod += model.getKoPile().getxMen();
		kod += model.getKoPile().getWounds();
		int wounds = other.getWounds();
		int bystanders = other.getBystanders();
		int mastermind = other.getMastermindsCount();
		int villians = other.getVilliansCount();
		int officers = other.getOfficers();
		int heros = other.getHerosCount();
		
		board.setSchemeCount(schemes);
		board.setEscapedCount(escaped);
		board.setKOCount(kod);
		board.setWoundCount(wounds);
		board.setBystanders(bystanders);
		board.setMastermindCount(mastermind);
		board.setVillians(villians);
		board.setOfficers(officers);
		board.setHeros(heros);
	}

	@Override
	public void modelChanged(){
		BoardComponent board = ((BoardView)getView()).getBoard();
		ModelFacade model = ClientManager.getInstance().getModel();

		setImages(board, model);
		
		setCounts(board, model);
		
		MidPanel.getPanel().repaint();
		board.repaint();
	}
}
