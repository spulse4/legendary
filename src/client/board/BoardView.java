package client.board;

import java.awt.BorderLayout;

import client.base.PanelView;

public class BoardView extends PanelView{

	private static final long serialVersionUID = 5973984789507161299L;

	private BoardComponent board = null;
	
	public BoardView(){
		this.setLayout(new BorderLayout());
		board = new BoardComponent();
		
		this.add(board, BorderLayout.CENTER);
	}
	
	public BoardController getBoardController(){
		return (BoardController)super.getController();
	}
	
	public BoardComponent getBoard(){
		return board;
	}
}
