package client.board;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import client.definitions.ClientCard;
import client.definitions.ClientVariables;
import client.hand.InHandPanel;
import client.main.ClientManager;
import client.preloader.Preloader;

public class BoardComponent extends JComponent implements ClientVariables{

	private final String NAME = "client.board.BoardComponent: ";

	private static final long serialVersionUID = -4455386353535913200L;
		
	private int width;
	private int height;
	private int cardWidth;
	private int widthOne;
	private int widthTwo;
	private int heightOne;	
	private int firstWidth;
	private int secondWidth;
	private int thirdWidth;
	private int fourthWidth;
	private int fifthWidth;
	private int firstHeight;
	private int secondHeight;
	private int thirdHeight;
	
	private int schemes = BOARD_COMPONENT_INITIAL_SCHEMES;
	private int escapedVillians = BOARD_COMPONENT_INITIAL_ESCAPED_VILLIANS;
	private int kos = BOARD_COMPONENT_INITIAL_KOS;
	private int wounds = BOARD_COMPONENT_INITIAL_WOUNDS;
	private int bystanders = BOARD_COMPONENT_INITIAL_BYSTANDERS;
	private int masterminds = BOARD_COMPONENT_INITIAL_MASTERMINDS;
	private int villians = BOARD_COMPONENT_INITIAL_VILLIANS;
	private int officers = BOARD_COMPONENT_INITIAL_OFFICERS;
	private int heros = BOARD_COMPONENT_INITIAL_HEROES;
	
	private String selectedString;
	
	private LegendaryMap<String, ClientCard> images;
	private Map<String, BufferedImage> backgrounds;
	
	private ClientCard HQ1 = null;
	private ClientCard HQ2 = null;
	private ClientCard HQ3 = null;
	private ClientCard HQ4 = null;
	private ClientCard HQ5 = null;
	private ClientCard sewers = null;
	private ClientCard bank = null;
	private ClientCard rooftops = null;
	private ClientCard streets = null;
	private ClientCard bridge = null;
	private boolean mastermindPortal = false;
	private boolean sewersPortal = false;
	private boolean bankPortal = false;
	private boolean rooftopsPortal = false;
	private boolean streetsPortal = false;
	private boolean bridgePortal = false;
	private List<ClientCard> sewersCaptured = null;
	private List<ClientCard> bankCaptured = null;
	private List<ClientCard> rooftopsCaptured = null;
	private List<ClientCard> streetsCaptured = null;
	private List<ClientCard> bridgeCaptured = null;
	private ClientCard mastermind = null;
	private List<ClientCard> mastermindCaptured = null;
	private ClientCard scheme = null;
	private ClientCard escaped = null;
	private ClientCard koPile = null;
	private ClientCard selected = null;
	private List<ClientCard> clicked = null;
		
	public BoardComponent(){
		ClientManager.getInstance().setBoard(this);
		setOpaque(true);
		selectedString = EMPTY;
		images = new LegendaryMap<String, ClientCard>();
		sewersCaptured = new ArrayList<ClientCard>();
		bankCaptured = new ArrayList<ClientCard>();
		rooftopsCaptured = new ArrayList<ClientCard>();
		streetsCaptured = new ArrayList<ClientCard>();
		bridgeCaptured = new ArrayList<ClientCard>();
		mastermindCaptured = new ArrayList<ClientCard>();
		backgrounds = new HashMap<String, BufferedImage>();
		if(!ClientManager.getInstance().resetting()){
			initializeImages();
		}
		this.addMouseListener(new MouseListener(){

			@Override
			public void mouseClicked(MouseEvent e) {
				clicked(e.getX(), e.getY());
			}

			@Override
			public void mouseEntered(MouseEvent e) {}
			@Override
			public void mouseExited(MouseEvent e) {}
			@Override
			public void mousePressed(MouseEvent e) {}
			@Override
			public void mouseReleased(MouseEvent e) {}
		});
	}
	
	private void initializeImages(){
		int count = NEGATIVE_ONE;
		File input = new File(LOCATION_CARD_INFO);
//		InputStream input = getClass().getClassLoader().getResourceAsStream(LOCATION_CARD_INFO);
		try{
			Scanner scanner = new Scanner(input);
			while(scanner.hasNextLine()){
				scanner.nextLine();
				++count;
			}
			scanner.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
		Preloader.getInstance().setTotalFiles(count);
		
		images.put(A_DAY_UNLIKE_ANY_OTHER, new ClientCard(A_DAY_UNLIKE_ANY_OTHER, A_DAY_UNLIKE_ANY_OTHER_PATH), true);
		images.put(ABOMINATION, new ClientCard(ABOMINATION, ABOMINATION_PATH), true);
		images.put(AGENT, new ClientCard(AGENT, AGENT_PATH), true);
		images.put(ARC_REACTOR, new ClientCard(ARC_REACTOR, ARC_REACTOR_PATH), true);
		images.put(ASTONISHING_STRENGTH, new ClientCard(ASTONISHING_STRENGTH, ASTONISHING_STRENGTH_PATH), true);
		images.put(AVENGERS_ASSEMBLE, new ClientCard(AVENGERS_ASSEMBLE, AVENGERS_ASSEMBLE_PATH), true);
		images.put(BACK, new ClientCard(BACK, BACK_PATH), true);
		images.put(BARON_ZEMO, new ClientCard(BARON_ZEMO, BARON_ZEMO_PATH), true);
		images.put(BATTLEFIELD_PROMOTION, new ClientCard(BATTLEFIELD_PROMOTION, BATTLEFIELD_PROMOTION_PATH), true);
		images.put(BERSERKER_RAGE, new ClientCard(BERSERKER_RAGE, BERSERKER_RAGE_PATH), true);
		images.put(BITTER_CAPTOR, new ClientCard(BITTER_CAPTOR, BITTER_CAPTOR_PATH), true);
		images.put(BLOB, new ClientCard(BLOB, BLOB_PATH), true);
		images.put(BOARD, new ClientCard(BOARD, BOARD_PATH), true);
		images.put(BORROWED_BRAWN, new ClientCard(BORROWED_BRAWN, BORROWED_BRAWN_PATH), true);
		images.put(BYSTANDER, new ClientCard(BYSTANDER, BYSTANDER_PATH), true);
		images.put(CALL_LIGHTNING, new ClientCard(CALL_LIGHTNING, CALL_LIGHTNING_PATH), true);
		images.put(CARD_SHARK, new ClientCard(CARD_SHARK, CARD_SHARK_PATH), true);
		images.put(CITY, new ClientCard(CITY, CITY_PATH), true);
		images.put(COPY_POWERS, new ClientCard(COPY_POWERS, COPY_POWERS_PATH), true);
		images.put(COVERING_FIRE, new ClientCard(COVERING_FIRE, COVERING_FIRE_PATH), true);
		images.put(COVERT_OPERATION, new ClientCard(COVERT_OPERATION, COVERT_OPERATION_PATH), true);
		images.put(CRAZED_RAMPAGE, new ClientCard(CRAZED_RAMPAGE, CRAZED_RAMPAGE_PATH), true);
		images.put(CRUEL_RULER, new ClientCard(CRUEL_RULER, CRUEL_RULER_PATH), true);
		images.put(CRUSHING_SHOCKWAVE, new ClientCard(CRUSHING_SHOCKWAVE, CRUSHING_SHOCKWAVE_PATH), true);
		images.put(DANGEROUS_RESCUE, new ClientCard(DANGEROUS_RESCUE, DANGEROUS_RESCUE_PATH), true);
		images.put(DARK_TECHNOLOGY, new ClientCard(DARK_TECHNOLOGY, DARK_TECHNOLOGY_PATH), true);
		images.put(DESTROYER, new ClientCard(DESTROYER, DESTROYER_PATH), true);
		images.put(DETERMINATION, new ClientCard(DETERMINATION, DETERMINATION_PATH), true);
		images.put(DIAMOND_FORM, new ClientCard(DIAMOND_FORM, DIAMOND_FORM_PATH), true);
		images.put(DIVING_BLOCK, new ClientCard(DIVING_BLOCK, DIVING_BLOCK_PATH), true);
		images.put(DOCTOR_OCTOPUS, new ClientCard(DOCTOR_OCTOPUS, DOCTOR_OCTOPUS_PATH), true);
		images.put(DOOMBOT_LEGION, new ClientCard(DOOMBOT_LEGION, DOOMBOT_LEGION_PATH), true);
		images.put(DR_DOOM, new ClientCard(DR_DOOM, DR_DOOM_PATH), true);
		images.put(ELECTROMAGNETIC_BUBBLE, new ClientCard(ELECTROMAGNETIC_BUBBLE, ELECTROMAGNETIC_BUBBLE_PATH), true);
		images.put(ENCHANTRESS, new ClientCard(ENCHANTRESS, ENCHANTRESS_PATH), true);
		images.put(ENDLESS_ARMIES_OF_HYDRA, new ClientCard(ENDLESS_ARMIES_OF_HYDRA, ENDLESS_ARMIES_OF_HYDRA_PATH), true);
		images.put(ENDLESS_INVENTION, new ClientCard(ENDLESS_INVENTION, ENDLESS_INVENTION_PATH), true);
		images.put(ENDLESS_RESOURCES, new ClientCard(ENDLESS_RESOURCES, ENDLESS_RESOURCES_PATH), true);
		images.put(ENERGY_DRAIN, new ClientCard(ENERGY_DRAIN, ENERGY_DRAIN_PATH), true);
		images.put(FRENZIED_SLASHING, new ClientCard(FRENZIED_SLASHING, FRENZIED_SLASHING_PATH), true);
		images.put(FROST_GIANT, new ClientCard(FROST_GIANT, FROST_GIANT_PATH), true);
		images.put(GATHERING_STORMCLOUDS, new ClientCard(GATHERING_STORMCLOUDS, GATHERING_STORMCLOUDS_PATH), true);
		images.put(GOD_OF_THUNDER, new ClientCard(GOD_OF_THUNDER, GOD_OF_THUNDER_PATH), true);
		images.put(GREAT_RESPONSIBILITY, new ClientCard(GREAT_RESPONSIBILITY, GREAT_RESPONSIBILITY_PATH), true);
		images.put(GREEN_GOBLIN, new ClientCard(GREEN_GOBLIN, GREEN_GOBLIN_PATH), true);
		images.put(GROWING_ANGER, new ClientCard(GROWING_ANGER, GROWING_ANGER_PATH), true);
		images.put(HAND_NINJAS, new ClientCard(HAND_NINJAS, HAND_NINJAS_PATH), true);
		images.put(HEALING_FACTOR, new ClientCard(HEALING_FACTOR, HEALING_FACTOR_PATH), true);
		images.put(HERE_HOLD_THIS_FOR_A_SECOND, new ClientCard(HERE_HOLD_THIS_FOR_A_SECOND, HERE_HOLD_THIS_FOR_A_SECOND_PATH), true);
		images.put(HEY_CAN_I_GET_A_DO_OVER, new ClientCard(HEY_CAN_I_GET_A_DO_OVER, HEY_CAN_I_GET_A_DO_OVER_PATH), true);
		images.put(HIGH_STAKES_JACKPOT, new ClientCard(HIGH_STAKES_JACKPOT, HIGH_STAKES_JACKPOT_PATH), true);
		images.put(HIGH_TECH_WEAPONRY, new ClientCard(HIGH_TECH_WEAPONRY, HIGH_TECH_WEAPONRY_PATH), true);
		images.put(HQ, new ClientCard(HQ, HQ_PATH), true);
		images.put(HULK_SMASH, new ClientCard(HULK_SMASH, HULK_SMASH_PATH), true);
		images.put(HYDRA_CONSPIRACY, new ClientCard(HYDRA_CONSPIRACY, HYDRA_CONSPIRACY_PATH), true);
		images.put(HYDRA_KIDNAPPERS, new ClientCard(HYDRA_KIDNAPPERS, HYDRA_KIDNAPPERS_PATH), true);
		images.put(HYPNOTIC_CHARM, new ClientCard(HYPNOTIC_CHARM, HYPNOTIC_CHARM_PATH), true);
		images.put(IMPOSSIBLE_TRICK_SHOT, new ClientCard(IMPOSSIBLE_TRICK_SHOT, IMPOSSIBLE_TRICK_SHOT_PATH), true);
		images.put(JUGGERNAUT, new ClientCard(JUGGERNAUT, JUGGERNAUT_PATH), true);
		images.put(KEEN_SENSES, new ClientCard(KEEN_SENSES, KEEN_SENSES_PATH), true);
		images.put(LEGENDARY_COMMANDER, new ClientCard(LEGENDARY_COMMANDER, LEGENDARY_COMMANDER_PATH), true);
		images.put(LIGHTNING_BOLT, new ClientCard(LIGHTNING_BOLT, LIGHTNING_BOLT_PATH), true);
		images.put(LOKI, new ClientCard(LOKI, LOKI_PATH), true);
		images.put(MAESTRO, new ClientCard(MAESTRO, MAESTRO_PATH), true);
		images.put(MAGNETO, new ClientCard(MAGNETO, MAGNETO_PATH), true);
		images.put(MANIACAL_TYRANT, new ClientCard(MANIACAL_TYRANT, MANIACAL_TYRANT_PATH), true);
		images.put(MASTERSTRIKE, new ClientCard(MASTERSTRIKE, MASTERSTRIKE_PATH), true);
		images.put(MELTER, new ClientCard(MELTER, MELTER_PATH), true);
		images.put(MENTAL_DISCIPLINE, new ClientCard(MENTAL_DISCIPLINE, MENTAL_DISCIPLINE_PATH), true);
		images.put(MIDTOWN_BANK_ROBBERY, new ClientCard(MIDTOWN_BANK_ROBBERY, MIDTOWN_BANK_ROBBERY_PATH), true);
		images.put(MISSION_ACCOMPLISHED, new ClientCard(MISSION_ACCOMPLISHED, MISSION_ACCOMPLISHED_PATH), true);
		images.put(MONARCHS_DECREE, new ClientCard(MONARCHS_DECREE, MONARCHS_DECREE_PATH), true);
		images.put(MYSTIQUE, new ClientCard(MYSTIQUE, MYSTIQUE_PATH), true);
		images.put(NEGABLAST_GRENADES, new ClientCard(NEGABLAST_GRENADES, NEGABLAST_GRENADES_PATH), true);
		images.put(NEGATIVE_ZONE_PRISON_BREAKOUT, new ClientCard(NEGATIVE_ZONE_PRISON_BREAKOUT, NEGATIVE_ZONE_PRISON_BREAKOUT_PATH), true);
		images.put(ODDBALL, new ClientCard(ODDBALL, ODDBALL_PATH), true);
		images.put(ODINSON, new ClientCard(ODINSON, ODINSON_PATH), true);
		images.put(OFFICER, new ClientCard(OFFICER, OFFICER_PATH), true);
		images.put(OPTIC_BLAST, new ClientCard(OPTIC_BLAST, OPTIC_BLAST_PATH), true);
		images.put(PAIBOK_THE_POWER_SKRULL, new ClientCard(PAIBOK_THE_POWER_SKRULL, PAIBOK_THE_POWER_SKRULL_PATH), true);
		images.put(PERFECT_TEAMWORK, new ClientCard(PERFECT_TEAMWORK, PERFECT_TEAMWORK_PATH), true);
		images.put(PORTALS_TO_THE_DARK_DIMENSION, new ClientCard(PORTALS_TO_THE_DARK_DIMENSION, PORTALS_TO_THE_DARK_DIMENSION_PATH), true);
		images.put(PSYCHIC_LINK, new ClientCard(PSYCHIC_LINK, PSYCHIC_LINK_PATH), true);
		images.put(PURE_FURY, new ClientCard(PURE_FURY, PURE_FURY_PATH), true);
		images.put(QUANTUM_BREAKTHROUGH, new ClientCard(QUANTUM_BREAKTHROUGH, QUANTUM_BREAKTHROUGH_PATH), true);
		images.put(QUICK_DRAW, new ClientCard(QUICK_DRAW, QUICK_DRAW_PATH), true);
		images.put(RANDOM_ACTS_OF_UNKINDNESS, new ClientCard(RANDOM_ACTS_OF_UNKINDNESS, RANDOM_ACTS_OF_UNKINDNESS_PATH), true);
		images.put(RED_SKULL, new ClientCard(RED_SKULL, RED_SKULL_PATH), true);
		images.put(REPLACE_EARTHS_LEADERS_WITH_KILLBOTS, new ClientCard(REPLACE_EARTHS_LEADERS_WITH_KILLBOTS, REPLACE_EARTHS_LEADERS_WITH_KILLBOTS_PATH), true);
		images.put(REPULSOR_RAYS, new ClientCard(REPULSOR_RAYS, REPULSOR_RAYS_PATH), true);
		images.put(RUTHLESS_DICTATOR, new ClientCard(RUTHLESS_DICTATOR, RUTHLESS_DICTATOR_PATH), true);
		images.put(SABRETOOTH, new ClientCard(SABRETOOTH, SABRETOOTH_PATH), true);
		images.put(SAVAGE_LAND_MUTATES, new ClientCard(SAVAGE_LAND_MUTATES, SAVAGE_LAND_MUTATES_PATH), true);
		images.put(SCHEME_TWIST, new ClientCard(SCHEME_TWIST, SCHEME_TWIST_PATH), true);
		images.put(SECRET_INVASION_OF_THE_SKRULL_SHAPESHIFTERS, new ClientCard(SECRET_INVASION_OF_THE_SKRULL_SHAPESHIFTERS, SECRET_INVASION_OF_THE_SKRULL_SHAPESHIFTERS_PATH), true);
		images.put(SECRETS_OF_TIME_TRAVEL, new ClientCard(SECRETS_OF_TIME_TRAVEL, SECRETS_OF_TIME_TRAVEL_PATH), true);
		images.put(SENTINEL, new ClientCard(SENTINEL, SENTINEL_PATH), true);
		images.put(SHADOWED_THOUGHTS, new ClientCard(SHADOWED_THOUGHTS, SHADOWED_THOUGHTS_PATH), true);
		images.put(SILENT_SNIPER, new ClientCard(SILENT_SNIPER, SILENT_SNIPER_PATH), true);
		images.put(SKRULL_QUEEN_VERANKE, new ClientCard(SKRULL_QUEEN_VERANKE, SKRULL_QUEEN_VERANKE_PATH), true);
		images.put(SKRULL_SHAPESHIFTERS, new ClientCard(SKRULL_SHAPESHIFTERS, SKRULL_SHAPESHIFTERS_PATH), true);
		images.put(SPINNING_CYCLONE, new ClientCard(SPINNING_CYCLONE, SPINNING_CYCLONE_PATH), true);
		images.put(STACK_THE_DECK, new ClientCard(STACK_THE_DECK, STACK_THE_DECK_PATH), true);
		images.put(STEAL_ABILITIES, new ClientCard(STEAL_ABILITIES, STEAL_ABILITIES_PATH), true);
		images.put(SUPER_HERO_CIVIL_WAR, new ClientCard(SUPER_HERO_CIVIL_WAR, SUPER_HERO_CIVIL_WAR_PATH), true);
		images.put(SUPER_SKRULL, new ClientCard(SUPER_SKRULL, SUPER_SKRULL_PATH), true);
		images.put(SUPREME_HYDRA, new ClientCard(SUPREME_HYDRA, SUPREME_HYDRA_PATH), true);
		images.put(SURGE_OF_POWER, new ClientCard(SURGE_OF_POWER, SURGE_OF_POWER_PATH), true);
		images.put(TEAM_PLAYER, new ClientCard(TEAM_PLAYER, TEAM_PLAYER_PATH), true);
		images.put(THE_AMAZING_SPIDERMAN, new ClientCard(THE_AMAZING_SPIDERMAN, THE_AMAZING_SPIDERMAN_PATH), true);
		images.put(THE_LEADER, new ClientCard(THE_LEADER, THE_LEADER_PATH), true);
		images.put(THE_LEGACY_VIRUS, new ClientCard(THE_LEGACY_VIRUS, THE_LEGACY_VIRUS_PATH), true);
		images.put(THE_LIZARD, new ClientCard(THE_LIZARD, THE_LIZARD_PATH), true);
		images.put(TIDAL_WAVE, new ClientCard(TIDAL_WAVE, TIDAL_WAVE_PATH), true);
		images.put(TREASURES_OF_LATVERIA, new ClientCard(TREASURES_OF_LATVERIA, TREASURES_OF_LATVERIA_PATH), true);
		images.put(TROOPER, new ClientCard(TROOPER, TROOPER_PATH), true);
		images.put(ULTRON, new ClientCard(ULTRON, ULTRON_PATH), true);
		images.put(UNENDING_ENERGY, new ClientCard(UNENDING_ENERGY, UNENDING_ENERGY_PATH), true);
		images.put(UNLEASH_THE_POWER_OF_THE_COSMIC_CUBE, new ClientCard(UNLEASH_THE_POWER_OF_THE_COSMIC_CUBE, UNLEASH_THE_POWER_OF_THE_COSMIC_CUBE_PATH), true);
		images.put(UNSTOPPABLE_HULK, new ClientCard(UNSTOPPABLE_HULK, UNSTOPPABLE_HULK_PATH), true);
		images.put(VANISHING_ILLUSIONS, new ClientCard(VANISHING_ILLUSIONS, VANISHING_ILLUSIONS_PATH), true);
		images.put(VENOM, new ClientCard(VENOM, VENOM_PATH), true);
		images.put(VIPER, new ClientCard(VIPER, VIPER_PATH), true);
		images.put(WEB_SHOOTERS, new ClientCard(WEB_SHOOTERS, WEB_SHOOTERS_PATH), true);
		images.put(WHIRLWIND, new ClientCard(WHIRLWIND, WHIRLWIND_PATH), true);
		images.put(WHISPERS_AND_LIES, new ClientCard(WHISPERS_AND_LIES, WHISPERS_AND_LIES_PATH), true);
		images.put(WOUND, new ClientCard(WOUND, WOUND_PATH), true);
		images.put(X_MEN_UNITED, new ClientCard(X_MEN_UNITED, X_MEN_UNITED_PATH), true);
		images.put(XAVIERS_NEMESIS, new ClientCard(XAVIERS_NEMESIS, XAVIERS_NEMESIS_PATH), true);
		images.put(YMIR_FROST_GIANT_KING, new ClientCard(YMIR_FROST_GIANT_KING, YMIR_FROST_GIANT_KING_PATH), true);
		images.put(ZZZAX, new ClientCard(ZZZAX, ZZZAX_PATH), true);
	}
	
	public LegendaryMap<String, ClientCard> getImages(){
		return images;
	};
	
	public void setImages(LegendaryMap<String, ClientCard> images){
		this.images = images;
	}
	
	public BufferedImage getImage(String image){
		return images.get(image).getImage();
	}
	
	private BufferedImage getShrunkenImage(String image){
		BufferedImage img = images.get(image).getImage();
		if(img == null){
			System.out.println(NAME + image + ERROR_NOT_FOUND);
			System.exit(SYSTEM_EXIT);
		}
		int extraY = BOARD_COMPONENT_SHRUNKEN_IMAGE_EXTRA_Y;
		int diffX = BOARD_COMPONENT_SHRUNKEN_IMAGE_DIFF_X;
		int diffY = BOARD_COMPONENT_SHRUNKEN_IMAGE_DIFF_Y;
		int width = img.getWidth() - diffX;
		int height = img.getHeight() - diffY;
		BufferedImage newImage = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);
				
		for(int i = ZERO; i < width; ++i){
			for(int j = ZERO; j < height; ++j){
				if(i < BOARD_COMPONENT_SHRUNKEN_IMAGE_BORDER_WIDTH_BOTTOM || i > width - BOARD_COMPONENT_SHRUNKEN_IMAGE_BORDER_WIDTH_TOP ||
				   j < BOARD_COMPONENT_SHRUNKEN_IMAGE_BORDER_WIDTH_BOTTOM || j > height - BOARD_COMPONENT_SHRUNKEN_IMAGE_BORDER_WIDTH_TOP){
					newImage.setRGB(i, j, Color.BLACK.getRGB());
					continue;
				}
				newImage.setRGB(i, j, img.getRGB(i + (diffX/TWO), j + extraY));
			}
		}
		return newImage;
	}

	public ClientCard getCard(String name){
		if(images.get(name) == null){
			if(name.contains(DELIMITER)){
				Scanner scanner = new Scanner(name);
				scanner.useDelimiter(DELIMITER);
				
				String nameCut = scanner.next();
				String copy = scanner.next();
				scanner.close();
				
				ClientCard original = getCard(nameCut).duplicate();
				ClientCard copyCard = getCard(copy).duplicate();
				original.setAttackPower(copyCard.getAttackPower());
				original.setBuyPower(copyCard.getBuyPower());
				
				List<String> allColors = new ArrayList<String>();
				for(String color : original.getColors()){
					if(!allColors.contains(color)){
						allColors.add(color);
					}
				}
				for(String color : copyCard.getColors()){
					if(!allColors.contains(color)){
						allColors.add(color);
					}
				}
				switch(allColors.size()){
				case 1:
					original.setColor(ZERO, allColors.get(ZERO));
					original.setColor(ONE, allColors.get(ZERO));
					original.setColor(TWO, allColors.get(ZERO));
					original.setColor(THREE, allColors.get(ZERO));
					break;
				case 2:
					original.setColor(ZERO, allColors.get(ZERO));
					original.setColor(ONE, allColors.get(ZERO));
					original.setColor(TWO, allColors.get(ONE));
					original.setColor(THREE, allColors.get(ONE));
					break;
				case 3:
					original.setColor(ZERO, allColors.get(ZERO));
					original.setColor(ONE, allColors.get(ZERO));
					original.setColor(TWO, allColors.get(ONE));
					original.setColor(THREE, allColors.get(TWO));
					break;
				case 4:
					original.setColor(ZERO, allColors.get(ZERO));
					original.setColor(ONE, allColors.get(ONE));
					original.setColor(TWO, allColors.get(TWO));
					original.setColor(THREE, allColors.get(THREE));
					break;
				}
				images.put(name, original, false);
			}
		}
		return images.get(name).duplicate();
	}
	public BufferedImage getBackground(String name){
		if(!backgrounds.containsKey(name)){
			try{
				backgrounds.put(name, javax.imageio.ImageIO.read(new File(FILE_COLOR + name + FILE_PNG)));
			}
			catch(IOException e){
				e.printStackTrace();
			}
		}
		return backgrounds.get(name);		
	}
	
	public void setScheme(String scheme){
		this.scheme = images.get(scheme);
	}
	public void setSchemeCount(int count){
		schemes = count;
	}	
	public void setEscaped(String escaped){
		if(escaped == null){
			this.escaped = null;
			return;
		}
		this.escaped = images.get(escaped);
	}	
	public void setEscapedCount(int count){
		escapedVillians = count;
	}	
	public void setKO(String name){
		if(name == null){
			this.koPile = null;
			return;
		}
		this.koPile = images.get(name);
	}	
	public void setKOCount(int count){
		kos = count;
	}
	public void setWoundCount(int count){
		wounds = count;
	}	
	public void setBystanders(int count){
		bystanders = count;
	}	
	public void setMastermind(String mastermind){
		this.mastermind = images.get(mastermind);
	}	
	public void setMastermindCount(int count){
		masterminds = count;
	}	
	public void setMastermindCapture(List<String> cards){
		mastermindCaptured = new ArrayList<ClientCard>();
		for(String str : cards){
			mastermindCaptured.add(images.get(str));
		}
	}
	public void setCity(String one, String two, String three, String four, String five){
		sewers = null;
		bank = null;
		rooftops = null;
		streets = null;
		bridge = null;
		if(one != null){
			sewers = getCard(one);
		}
		if(two != null){
			bank = getCard(two);
		}
		if(three != null){
			rooftops = getCard(three);
		}
		if(four != null){
			streets = getCard(four);
		}
		if(five != null){
			bridge = getCard(five);
		}
	}
	public void setPortals(boolean one, boolean two, boolean three, boolean four, boolean five, boolean six){
		mastermindPortal = one;
		bridgePortal = two;
		streetsPortal = three;
		rooftopsPortal = four;
		bankPortal = five;
		sewersPortal = six;
	}
	public void setCityCapture(List<String> one, List<String> two, List<String> three, List<String> four, List<String> five){
		sewersCaptured = new ArrayList<ClientCard>();;
		bankCaptured = new ArrayList<ClientCard>();;
		rooftopsCaptured = new ArrayList<ClientCard>();;
		streetsCaptured = new ArrayList<ClientCard>();;
		bridgeCaptured = new ArrayList<ClientCard>();;
		if(one != null){
			for(String str : one){
				sewersCaptured.add(images.get(str));
			}
		}
		if(two != null){
			for(String str : two){
				bankCaptured.add(images.get(str));
			}
		}
		if(three != null){
			for(String str : three){
				rooftopsCaptured.add(images.get(str));
			}
		}
		if(four != null){
			for(String str : four){
				streetsCaptured.add(images.get(str));
			}
		}
		if(five != null){
			for(String str : five){
				bridgeCaptured.add(images.get(str));
			}
		}
	}
	public void setVillians(int count){
		villians = count;
	}	
	public void setOfficers(int count){
		officers = count;
	}	
	public void setHQ(String one, String two, String three, String four, String five){
		HQ1 = null;
		HQ2 = null;
		HQ3 = null;
		HQ4 = null;
		HQ5 = null;
		if(one != null){
			HQ1 = images.get(one);
		}
		if(one != null){
			HQ2 = images.get(two);
		}
		if(one != null){
			HQ3 = images.get(three);
		}
		if(one != null){
			HQ4 = images.get(four);
		}
		if(one != null){
			HQ5 = images.get(five);
		}
	}	
	public void setHeros(int count){
		heros = count;
	}	
	public void setSelected(ClientCard c){
		this.selected = c;
	}
	
	public void clicked(int x, int y){
		setSelected(x, y);
		if(clicked != null){
			clickedCaptured();
		}
		else if(selectedString.equals(KO)){
			clickedKO();
		}
		else if(selectedString.equals(TITLES_ESCAPED)){
			clickedEscape();
		}
		else if(selected == null){
			return;
		}
		else{
			clicked();
		}
	}
	public void clicked(){
		JDialog dialog = new JDialog();
		dialog.setTitle(selected.getFixedName());
		if((selected.getName().equals(SKRULL_QUEEN_VERANKE) ||
			selected.getName().equals(SKRULL_SHAPESHIFTERS)) && 
		   selected == bridge){
			selected.setHitPoints(bridgeCaptured.get(ZERO).getPrice());
		}
		else if((selected.getName().equals(SKRULL_QUEEN_VERANKE) ||
				 selected.getName().equals(SKRULL_SHAPESHIFTERS)) && 
				selected == streets){
			selected.setHitPoints(streetsCaptured.get(ZERO).getPrice());
		}
		else if((selected.getName().equals(SKRULL_QUEEN_VERANKE) ||
				 selected.getName().equals(SKRULL_SHAPESHIFTERS)) && 
				selected == rooftops){
			selected.setHitPoints(rooftopsCaptured.get(ZERO).getPrice());
		}
		else if((selected.getName().equals(SKRULL_QUEEN_VERANKE) ||
				 selected.getName().equals(SKRULL_SHAPESHIFTERS)) && 
				selected == bank){
			selected.setHitPoints(bankCaptured.get(ZERO).getPrice());
		}
		else if((selected.getName().equals(SKRULL_QUEEN_VERANKE) ||
				 selected.getName().equals(SKRULL_SHAPESHIFTERS)) && 
				selected == sewers){
			selected.setHitPoints(sewersCaptured.get(ZERO).getPrice());
		}
		JPanel select = selected.getPanel(dialog);
		if(selected.getName().equals(SKRULL_QUEEN_VERANKE) ||
		   selected.getName().equals(SKRULL_SHAPESHIFTERS)){
			selected.setHitPoints(ZERO);
		}
		int wid = DIALOG_CLICKED_WIDTH;
		int hei = (int)select.getMinimumSize().getHeight();
		dialog.setSize(wid, hei);
		dialog.getContentPane().setBackground(Color.WHITE);
		dialog.add(select);
		dialog.getRootPane().setBorder(BorderFactory.createLineBorder(Color.BLACK, DIALOG_BORDER_WIDTH));
		dialog.setLocationRelativeTo(this);
		dialog.setModal(true);
		dialog.setVisible(true);
	}
	public void clickedKO(){
		JDialog dialog = new JDialog();
		dialog.setSize(DIALOG_KO_WIDTH, DIALOG_KO_HEIGHT);
		dialog.setTitle(TITLES_KO_PILE);
		dialog.getContentPane().setBackground(Color.WHITE);
		dialog.add(ClientManager.getInstance().getModel().getKoPile().getPanel(dialog));
		dialog.getRootPane().setBorder(BorderFactory.createLineBorder(Color.BLACK, DIALOG_BORDER_WIDTH));
		dialog.setLocationRelativeTo(this);
		dialog.setModal(true);
		dialog.setVisible(true);
	}
	public void clickedEscape(){
		JDialog dialog = new JDialog();
		dialog.setSize(DIALOG_ESCAPED_WIDTH, DIALOG_ESCAPED_HEIGHT);
		dialog.setTitle(TITLES_ESCAPED_PILE);
		dialog.getContentPane().setBackground(Color.WHITE);
		dialog.add(ClientManager.getInstance().getModel().getEscaped().getPanel(dialog));
		dialog.getRootPane().setBorder(BorderFactory.createLineBorder(Color.BLACK, DIALOG_BORDER_WIDTH));
		dialog.setLocationRelativeTo(this);
		dialog.setModal(true);
		dialog.setVisible(true);
	}
	public void clickedCaptured(){
		final JDialog dialog = new JDialog();
		dialog.setLayout(new BorderLayout());
		JButton button = new JButton(BUTTON_CANCEL);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				dialog.dispose();
			}
			
		});
		dialog.setSize(DIALOG_STANDARD_WIDTH, DIALOG_STANDARD_HEIGHT);
		dialog.getContentPane().setBackground(Color.WHITE);
		JScrollPane scroll = new JScrollPane(new InHandPanel(clicked, null, TITLES_CAPTURED_CARDS, NULL_STRING));
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scroll.getVerticalScrollBar().setUnitIncrement(SCROLL_SPEED);
		dialog.setTitle(TITLES_DISCARD_PILE);
		dialog.add(scroll, BorderLayout.CENTER);
		dialog.add(button, BorderLayout.SOUTH);
		dialog.getRootPane().setBorder(BorderFactory.createLineBorder(Color.BLACK, DIALOG_BORDER_WIDTH));
		dialog.setLocationRelativeTo(this);
		dialog.setModal(true);
		dialog.setVisible(true);
}
	
	private void setSelected(int x, int y){
		clicked = null;
		selectedString = EMPTY;
		if(x < firstWidth || y < firstHeight || x > fifthWidth + widthOne || y > thirdHeight + heightOne){
			selected = null;
		}
		else if(x < firstWidth + widthOne){
			int row = getrow(y);
			if(row == ZERO){
				selected = null;
			}
			else if(row == ONE){
				selected = scheme;
			}
			else if(row == TWO){
				if(mastermindCaptured.size() != ZERO){
					if(y < secondHeight + (heightOne - cardWidth)/TWO + BOARD_COMPONENT_CAPTURED_DIFF_Y){
						clicked = mastermindCaptured;
					}
					selected = mastermind;
					if(selected != null){
						selected.setType(CARD_TYPE_ATTACK, Integer.toString(MASTERMIND_NUMBER));
					}
				}
				else{
					selected = mastermind;
					if(selected != null){
						selected.setType(CARD_TYPE_ATTACK, Integer.toString(MASTERMIND_NUMBER));
					}
				}
			}
			else if(row == 3){
				selected = images.get(OFFICER);
				selected.setType(CARD_TYPE_BUY, Integer.toString(OFFICER_NUMBER));
			}
		}
		else if(x < secondWidth){
			selected = null;
		}
		else if(x < secondWidth + widthOne){
			int row = getrow(y);
			if(row == ZERO){
				selected = null;
			}
			else if(row == ONE){
				selected = escaped;
				selectedString = TITLES_ESCAPED;
			}
			else if(row == TWO){
				if(bridgeCaptured.size() != ZERO){
					if(y < secondHeight + (heightOne - cardWidth)/TWO + BOARD_COMPONENT_CAPTURED_DIFF_Y){
						clicked = bridgeCaptured;
					}
					selected = bridge;
					if(selected != null){
						selected.setType(CARD_TYPE_ATTACK, Integer.toString(BRIDGE_NUMBER));
					}
				}
				else{
					selected = bridge;
					if(selected != null){
						selected.setType(CARD_TYPE_ATTACK, Integer.toString(BRIDGE_NUMBER));
					}
				}
			}
			else if(row == 3){
				selected = HQ1;
				selected.setType(CARD_TYPE_BUY, Integer.toString(HQ_1_NUMBER));
			}
		}
		else if(x < thirdWidth){
			int row = getrow(y);
			if(row == ZERO || row == ONE){
				selected = null;
			}
			else if(row == TWO){
				if(streetsCaptured.size() != ZERO){
					if(y < secondHeight + (heightOne - cardWidth)/TWO + BOARD_COMPONENT_CAPTURED_DIFF_Y){
						clicked = streetsCaptured;
					}
					selected = streets;
					if(selected != null){
						selected.setType(CARD_TYPE_ATTACK, Integer.toString(STREETS_NUMBER));
					}
				}
				else{
					selected = streets;
					if(selected != null){
						selected.setType(CARD_TYPE_ATTACK, Integer.toString(STREETS_NUMBER));
					}
				}
			}
			else if(row == THREE){
				selected = HQ2;
				selected.setType(CARD_TYPE_BUY, Integer.toString(HQ_2_NUMBER));
			}
		}
		else if(x < thirdWidth + widthOne){
			int row = getrow(y);
			if(row == ZERO){
				selected = null;
			}
			else if(row == ONE){
				selected = koPile;
				selectedString = KO;
			}
			else if(row == TWO){
				if(rooftopsCaptured.size() != ZERO){
					if(y < secondHeight + (heightOne - cardWidth)/TWO + BOARD_COMPONENT_CAPTURED_DIFF_Y){
						clicked = rooftopsCaptured;
					}
					selected = rooftops;
					if(selected != null){
						selected.setType(CARD_TYPE_ATTACK, Integer.toString(ROOFTOPS_NUMBER));
					}
				}
				else{
					selected = rooftops;
					if(selected != null){
						selected.setType(CARD_TYPE_ATTACK, Integer.toString(ROOFTOPS_NUMBER));
					}
				}
			}
			else if(row == THREE){
				selected = HQ3;
				selected.setType(CARD_TYPE_BUY, Integer.toString(HQ_3_NUMBER));
			}
		}
		else if(x < fourthWidth){
			int row = getrow(y);
			if(row == ZERO || row == ONE){
				selected = null;
			}
			else if(row == TWO){
				if(bankCaptured.size() != ZERO){
					if(y < secondHeight + (heightOne - cardWidth)/TWO + BOARD_COMPONENT_CAPTURED_DIFF_Y){
						clicked = bankCaptured;
					}
					selected = bank;
					if(selected != null){
						selected.setType(CARD_TYPE_ATTACK, Integer.toString(BANK_NUMBER));
					}
				}
				else{
					selected = bank;
					if(selected != null){
						selected.setType(CARD_TYPE_ATTACK, Integer.toString(BANK_NUMBER));
					}
				}
			}
			else if(row == THREE){
				selected = HQ4;
				selected.setType(CARD_TYPE_BUY, Integer.toString(HQ_4_NUMBER));
			}
		}
		else if(x < fourthWidth + widthOne){
			int row = getrow(y);
			if(row == ZERO){
				selected = null;
			}
			else if(row == ONE){
				selected = images.get(WOUND);
			}
			else if(row == TWO){
				if(sewersCaptured.size() != ZERO){
					if(y < secondHeight + (heightOne - cardWidth)/TWO + BOARD_COMPONENT_CAPTURED_DIFF_Y){
						clicked = sewersCaptured;
					}
					selected = sewers;
					if(selected != null){
						selected.setType(CARD_TYPE_ATTACK, Integer.toString(SEWERS_NUMBER));
					}
				}
				else{
					selected = sewers;
					if(selected != null){
						selected.setType(CARD_TYPE_ATTACK, Integer.toString(SEWERS_NUMBER));
					}
				}
			}
			else if(row == THREE){
				selected = HQ5;
				selected.setType(CARD_TYPE_BUY, Integer.toString(HQ_5_NUMBER));
			}
		}
		else if(x < fifthWidth){
			selected = null;
		}
		else if(x < fifthWidth + widthOne){
			int row = getrow(y);
			if(row == ZERO || row == TWO || row == THREE){
				selected = null;
			}
			else if(row == ONE){
				selected = images.get(BYSTANDER);
			}
		}
		else{
			selected = null;
		}
	}
	private int getrow(int y){
		if(y < firstHeight + heightOne){
			return ONE;
		}
		else if(y < secondHeight){
			return ZERO;
		}
		else if(y < secondHeight + heightOne){
			return TWO;
		}
		else if(y < thirdHeight){
			return ZERO;
		}
		else if(y < thirdHeight + heightOne){
			return THREE;
		}
		return ZERO;
	}
		
	@Override
 	protected void paintComponent(Graphics g){	
		super.paintComponent(g);
		width = this.getWidth();
		height = this.getHeight();
		widthOne = (int) (width/8.5);
		widthTwo = (int) (width/1.7);
		heightOne = (int) (height/3.666);
		firstWidth = (int) (width/17);
		secondWidth = (int) (width/4.857);
		thirdWidth = (int) (width/2.26);
		fourthWidth = (int) (width/1.478);
		fifthWidth = (int) (width/1.214);
		firstHeight = (int) (height/11);
		secondHeight = (int) (height/2.444);		
		thirdHeight = (int) (height/1.375);
		if(widthOne < heightOne){
			cardWidth = widthOne - width/50;
		}
		else{
			cardWidth = heightOne - height/50;
		}
		if(cardWidth > heightOne - height/13){
			cardWidth = heightOne - height/13;
		}
		
		Graphics2D g2 = (Graphics2D)g;
				
		drawScheme(g2);
		drawEscaped(g2);
		drawKO(g2);
		drawWounds(g2);
		drawBystanders(g2);
		drawMastermind(g2);
		drawCity(g2);
		drawVillians(g2);
		drawOfficers(g2);
		drawHQ(g2);
		drawHeros(g2);
	}	
	private void drawScheme(Graphics2D g2){
		if(scheme == null){
			g2.drawImage(images.get(BACK).getImage(), firstWidth, firstHeight, widthOne, heightOne, null);
		}
		else{
			g2.drawImage(getShrunkenImage(scheme.getName()), firstWidth, firstHeight, widthOne, heightOne, null);
		}
		g2.setColor(Color.BLACK);
		int extra = 0;
		if(schemes > 9){
			extra = 14;
		}
		g2.fillRect(firstWidth + 3, firstHeight, 19 + extra, 27);
		g2.setColor(Color.WHITE);
		g2.setFont(FONT_BOLD_25);
		g2.drawString(Integer.toString(schemes), firstWidth + 5, firstHeight + 22);
	}	
	private void drawEscaped(Graphics2D g2){
		if(escaped == null){
			g2.drawImage(images.get(BACK).getImage(), secondWidth, firstHeight, widthOne, heightOne, null);
		}
		else{
			g2.drawImage(getShrunkenImage(escaped.getName()), secondWidth, firstHeight, widthOne, heightOne, null);
		}
		g2.setColor(Color.BLACK);
		g2.setColor(Color.BLACK);
		int extra = 0;
		if(escapedVillians > 9){
			extra = 14;
		}
		g2.fillRect(secondWidth + 3, firstHeight, 19 + extra, 27);
		g2.setColor(Color.WHITE);
		g2.setFont(FONT_BOLD_25);
		g2.drawString(Integer.toString(escapedVillians), secondWidth + 5, firstHeight + 22);
	}	
	private void drawKO(Graphics2D g2){
		if(koPile == null){
			g2.drawImage(images.get(BACK).getImage(), thirdWidth, firstHeight, widthOne, heightOne, null);
		}
		else{
			g2.drawImage(getShrunkenImage(koPile.getName()), thirdWidth, firstHeight, widthOne, heightOne, null);
		}
		g2.setColor(Color.BLACK);
		g2.setColor(Color.BLACK);
		int extra = 0;
		if(kos > 9){
			extra = 14;
		}
		g2.fillRect(thirdWidth + 3, firstHeight, 19 + extra, 27);
		g2.setColor(Color.WHITE);
		g2.setFont(FONT_BOLD_25);
		g2.drawString(Integer.toString(kos), thirdWidth + 5, firstHeight + 22);
	}	
	private void drawWounds(Graphics2D g2){
		g2.drawImage(getShrunkenImage(WOUND), fourthWidth, firstHeight, widthOne, heightOne, null);
		g2.setColor(Color.BLACK);
		g2.setColor(Color.BLACK);
		int extra = 0;
		if(wounds > 9){
			extra = 14;
		}
		g2.fillRect(fourthWidth + 3, firstHeight, 19 + extra, 27);
		g2.setColor(Color.WHITE);
		g2.setFont(FONT_BOLD_25);
		g2.drawString(Integer.toString(wounds), fourthWidth + 5, firstHeight + 22);
	}	
	private void drawBystanders(Graphics2D g2){
		g2.drawImage(getShrunkenImage(BYSTANDER), fifthWidth, firstHeight, widthOne, heightOne, null);
		g2.setColor(Color.BLACK);
		g2.setColor(Color.BLACK);
		int extra = 0;
		if(bystanders > 9){
			extra = 14;
		}
		g2.fillRect(fifthWidth + 3, firstHeight, 19 + extra, 27);
		g2.setColor(Color.WHITE);
		g2.setFont(FONT_BOLD_25);
		g2.drawString(Integer.toString(bystanders), fifthWidth + 5, firstHeight + 22);
	}	
	private void drawMastermind(Graphics2D g2){
		int diff = 0;
		if(mastermind == null){
			g2.drawImage(images.get(BACK).getImage(), firstWidth, secondHeight, widthOne, heightOne, null);
		}
		else{
			if(mastermindPortal == true){
				g2.setColor(Color.YELLOW);
				g2.fillRect(firstWidth - BOARD_COMPONENT_PORTAL_SIZE, secondHeight - BOARD_COMPONENT_PORTAL_SIZE, widthOne + (BOARD_COMPONENT_PORTAL_SIZE * 2), heightOne + (BOARD_COMPONENT_PORTAL_SIZE * 2));
			}
			if(mastermindCaptured.size() == 0){
				g2.drawImage(getShrunkenImage(mastermind.getName()), firstWidth, secondHeight, widthOne, heightOne, null);				
			}
			else{
				g2.drawImage(getShrunkenImage(mastermindCaptured.get(0).getName()), firstWidth, secondHeight, widthOne, widthOne, null);
				g2.drawImage(getShrunkenImage(mastermind.getName()), firstWidth, secondHeight + (heightOne - cardWidth)/2 + 10, widthOne, heightOne - ((heightOne - cardWidth)/2 + 10), null);
				diff = (heightOne - cardWidth)/2 + 10;
			}
		}
		g2.setColor(Color.BLACK);
		g2.fillRect(firstWidth + 3, secondHeight + diff, 19, 27);
		g2.setColor(Color.WHITE);
		g2.setFont(FONT_BOLD_25);
		g2.drawString(Integer.toString(masterminds), firstWidth + 5, secondHeight + 22 + diff);
	}
	private void drawCity(Graphics2D g2){
		int size;

		g2.drawImage(images.get(CITY).getImage(), secondWidth, secondHeight, widthTwo, heightOne, null);

		g2.setColor(Color.YELLOW);
		if(bridgePortal == true){
			g2.fillRect(secondWidth + (widthTwo*0/5) + (widthTwo/5 - cardWidth)/2 - BOARD_COMPONENT_PORTAL_SIZE, secondHeight + (heightOne - cardWidth)/2 + 10 - BOARD_COMPONENT_PORTAL_SIZE, cardWidth + (BOARD_COMPONENT_PORTAL_SIZE * 2), cardWidth + (BOARD_COMPONENT_PORTAL_SIZE * 2));
		}
		if(streetsPortal == true){
			g2.fillRect(secondWidth + (widthTwo*1/5) + (widthTwo/5 - cardWidth)/2 - BOARD_COMPONENT_PORTAL_SIZE, secondHeight + (heightOne - cardWidth)/2 + 10 - BOARD_COMPONENT_PORTAL_SIZE, cardWidth + (BOARD_COMPONENT_PORTAL_SIZE * 2), cardWidth + (BOARD_COMPONENT_PORTAL_SIZE * 2));
		}
		if(rooftopsPortal == true){
			g2.fillRect(secondWidth + (widthTwo*2/5) + (widthTwo/5 - cardWidth)/2 - BOARD_COMPONENT_PORTAL_SIZE, secondHeight + (heightOne - cardWidth)/2 + 10 - BOARD_COMPONENT_PORTAL_SIZE, cardWidth + (BOARD_COMPONENT_PORTAL_SIZE * 2), cardWidth + (BOARD_COMPONENT_PORTAL_SIZE * 2));
		}
		if(bankPortal == true){
			g2.fillRect(secondWidth + (widthTwo*3/5) + (widthTwo/5 - cardWidth)/2 - BOARD_COMPONENT_PORTAL_SIZE, secondHeight + (heightOne - cardWidth)/2 + 10 - BOARD_COMPONENT_PORTAL_SIZE, cardWidth + (BOARD_COMPONENT_PORTAL_SIZE * 2), cardWidth + (BOARD_COMPONENT_PORTAL_SIZE * 2));
		}
		if(sewersPortal == true){
			g2.fillRect(secondWidth + (widthTwo*4/5) + (widthTwo/5 - cardWidth)/2 - BOARD_COMPONENT_PORTAL_SIZE, secondHeight + (heightOne - cardWidth)/2 + 10 - BOARD_COMPONENT_PORTAL_SIZE, cardWidth + (BOARD_COMPONENT_PORTAL_SIZE * 2), cardWidth + (BOARD_COMPONENT_PORTAL_SIZE * 2));
		}
		
		g2.setColor(Color.BLACK);
		g2.setFont(FONT_BOLD_15);
		FontMetrics metric = g2.getFontMetrics();	

		size = metric.stringWidth(LOCATION_BRIDGE_FIXED);
		g2.drawString(LOCATION_BRIDGE_FIXED, secondWidth + (widthTwo*0/5) + ((widthTwo/5)/2) - (size/2), secondHeight + heightOne - 1);		
		size = metric.stringWidth(LOCATION_STREETS_FIXED);
		g2.drawString(LOCATION_STREETS_FIXED, secondWidth + (widthTwo*1/5) + ((widthTwo/5)/2) - (size/2), secondHeight + heightOne - 1);
		size = metric.stringWidth(LOCATION_ROOFTOPS_FIXED);
		g2.drawString(LOCATION_ROOFTOPS_FIXED, secondWidth + (widthTwo*2/5) + ((widthTwo/5)/2) - (size/2), secondHeight + heightOne - 1);
		size = metric.stringWidth(LOCATION_BANK_FIXED);
		g2.drawString(LOCATION_BANK_FIXED, secondWidth + (widthTwo*3/5) + ((widthTwo/5)/2) - (size/2), secondHeight + heightOne - 1);
		size = metric.stringWidth(LOCATION_SEWERS_FIXED);
		g2.drawString(LOCATION_SEWERS_FIXED, secondWidth + (widthTwo*4/5) + ((widthTwo/5)/2) - (size/2), secondHeight + heightOne - 1);
		
		if(bridgeCaptured.size() != 0){
			g2.drawImage(getShrunkenImage(bridgeCaptured.get(0).getName()), secondWidth + (widthTwo * 0/5) + (widthTwo/5 - cardWidth)/2, secondHeight, cardWidth, cardWidth, null);
		}
		if(streetsCaptured.size() != 0){
			g2.drawImage(getShrunkenImage(streetsCaptured.get(0).getName()), secondWidth + (widthTwo * 1/5) + (widthTwo/5 - cardWidth)/2, secondHeight, cardWidth, cardWidth, null);
		}
		if(rooftopsCaptured.size() != 0){
			g2.drawImage(getShrunkenImage(rooftopsCaptured.get(0).getName()), secondWidth + (widthTwo * 2/5) + (widthTwo/5 - cardWidth)/2, secondHeight, cardWidth, cardWidth, null);
		}
		if(bankCaptured.size() != 0){
			g2.drawImage(getShrunkenImage(bankCaptured.get(0).getName()), secondWidth + (widthTwo * 3/5) + (widthTwo/5 - cardWidth)/2, secondHeight, cardWidth, cardWidth, null);
		}
		if(sewersCaptured.size() != 0){
			g2.drawImage(getShrunkenImage(sewersCaptured.get(0).getName()), secondWidth + (widthTwo * 4/5) + (widthTwo/5 - cardWidth)/2, secondHeight, cardWidth, cardWidth, null);
		}
		
		if(bridge != null){
			g2.drawImage(getShrunkenImage(bridge.getName()), secondWidth + (widthTwo*0/5) + (widthTwo/5 - cardWidth)/2, secondHeight + (heightOne - cardWidth)/2 + 10, cardWidth, cardWidth, null);
		}
		if(streets != null){
			g2.drawImage(getShrunkenImage(streets.getName()), secondWidth + (widthTwo*1/5) + (widthTwo/5 - cardWidth)/2, secondHeight + (heightOne - cardWidth)/2 + 10, cardWidth, cardWidth, null);
		}
		if(rooftops != null){
			g2.drawImage(getShrunkenImage(rooftops.getName()), secondWidth + (widthTwo*2/5) + (widthTwo/5 - cardWidth)/2, secondHeight + (heightOne - cardWidth)/2 + 10, cardWidth, cardWidth, null);
		}
		if(bank != null){
			g2.drawImage(getShrunkenImage(bank.getName()), secondWidth + (widthTwo*3/5) + (widthTwo/5 - cardWidth)/2, secondHeight + (heightOne - cardWidth)/2 + 10, cardWidth, cardWidth, null);
		}
		if(sewers != null){
			g2.drawImage(getShrunkenImage(sewers.getName()), secondWidth + (widthTwo*4/5) + (widthTwo/5 - cardWidth)/2, secondHeight + (heightOne - cardWidth)/2 + 10, cardWidth, cardWidth, null);
		}
	}	
	private void drawVillians(Graphics2D g2){
		g2.drawImage(images.get(BACK).getImage(), fifthWidth, secondHeight, widthOne, heightOne, null);
		g2.setColor(Color.BLACK);
		g2.setColor(Color.BLACK);
		int extra = 0;
		if(villians > 9){
			extra = 14;
		}
		g2.fillRect(fifthWidth + 3, secondHeight, 19 + extra, 27);
		g2.setColor(Color.WHITE);
		g2.setFont(FONT_BOLD_25);
		g2.drawString(Integer.toString(villians), fifthWidth + 5, secondHeight + 22);
	}
	private void drawOfficers(Graphics2D g2){
		g2.drawImage(getShrunkenImage(OFFICER), firstWidth, thirdHeight, widthOne, heightOne, null);
		g2.setColor(Color.BLACK);
		g2.setColor(Color.BLACK);
		int extra = 0;
		if(officers > 9){
			extra = 14;
		}
		g2.fillRect(firstWidth + 3, thirdHeight, 19 + extra, 27);
		g2.setColor(Color.WHITE);
		g2.setFont(FONT_BOLD_25);
		g2.drawString(Integer.toString(officers), firstWidth + 5, thirdHeight + 22);
	}	
	private void drawHQ(Graphics2D g2){
		g2.drawImage(images.get(HQ).getImage(), secondWidth, thirdHeight, widthTwo, heightOne, null);
		g2.setColor(Color.WHITE);

		if(HQ1 != null){
			g2.drawImage(getShrunkenImage(HQ1.getName()), secondWidth + (widthTwo*0/5) + (widthTwo/5 - cardWidth)/2, thirdHeight + (heightOne - cardWidth)/2 + 10, cardWidth, cardWidth, null);
		}
		if(HQ2 != null){
			g2.drawImage(getShrunkenImage(HQ2.getName()), secondWidth + (widthTwo*1/5) + (widthTwo/5 - cardWidth)/2, thirdHeight + (heightOne - cardWidth)/2 + 10, cardWidth, cardWidth, null);
		}
		if(HQ3 != null){
			g2.drawImage(getShrunkenImage(HQ3.getName()), secondWidth + (widthTwo*2/5) + (widthTwo/5 - cardWidth)/2, thirdHeight + (heightOne - cardWidth)/2 + 10, cardWidth, cardWidth, null);
		}
		if(HQ4 != null){
			g2.drawImage(getShrunkenImage(HQ4.getName()), secondWidth + (widthTwo*3/5) + (widthTwo/5 - cardWidth)/2, thirdHeight + (heightOne - cardWidth)/2 + 10, cardWidth, cardWidth, null);
		}
		if(HQ5 != null){
			g2.drawImage(getShrunkenImage(HQ5.getName()), secondWidth + (widthTwo*4/5) + (widthTwo/5 - cardWidth)/2, thirdHeight + (heightOne - cardWidth)/2 + 10, cardWidth, cardWidth, null);
		}
	}	
	private void drawHeros(Graphics2D g2){
		g2.drawImage(images.get(BACK).getImage(), fifthWidth, thirdHeight, widthOne, heightOne, null);
		g2.setColor(Color.BLACK);
		g2.setColor(Color.BLACK);
		int extra = 0;
		if(heros > 9){
			extra = 14;
		}
		g2.fillRect(fifthWidth + 3, thirdHeight, 19 + extra, 27);
		g2.setColor(Color.WHITE);
		g2.setFont(FONT_BOLD_25);
		g2.drawString(Integer.toString(heros), fifthWidth + 5, thirdHeight + 22);
	}
}
