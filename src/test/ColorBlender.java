package test;

import java.awt.Color;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import client.definitions.ClientVariables;

public class ColorBlender implements ClientVariables{

	public static void main(String[] args){
		new ColorBlender().go();
	}

	private List<Color> colors;
	
	public ColorBlender(){
		colors = new ArrayList<Color>();
		colors.add(Color.BLUE);
		colors.add(Color.BLACK);
		colors.add(Color.GREEN);
		colors.add(Color.LIGHT_GRAY);
		colors.add(Color.RED);
		colors.add(Color.YELLOW);
	}
	
	public void go(){
		for(Color c1 : colors){
			for(Color c2 : colors){
				for(Color c3 : colors){
					for(Color c4 : colors){
						StringBuilder str = new StringBuilder();
						str.append(FILE_RESOURCES + FILE_COLOR);
						str.append(getColor(c1));
						str.append(getColor(c2));
						str.append(getColor(c3));
						str.append(getColor(c4));
						str.append(FILE_PNG);
						
						BufferedImage image = createImage(c1, c2, c3, c4);
						
						File output = new File(str.toString());
						try{
							ImageIO.write(image, FILE_EXTENSION_PNG, output);
						}
						catch(IOException e){
							e.printStackTrace();
						}
					}
				}
			}
		}		
	}
	
	private BufferedImage createImage(Color c1, Color c2, Color c3, Color c4){
		BufferedImage image = new BufferedImage(CARD_PANEL_WIDTH, CARD_PANEL_HEIGHT, ColorSpace.TYPE_RGB);

		for(int j = ZERO; j < CARD_PANEL_HEIGHT; ++j){
			int x1 = j * THREE;
			int x2 = (j * NEGATIVE_ONE * THREE) + CARD_PANEL_WIDTH - ONE;
			
			for(int i = ZERO; i < CARD_PANEL_WIDTH; ++i){
				if(x1 <= x2){
					if(i <= x1){
						image.setRGB(i, j, getRGB(c1));
					}
					else if(i > x2){
						image.setRGB(i, j, getRGB(c3));
					}
					else{
						image.setRGB(i, j, getRGB(c2));
					}
				}
				else{
					if(i <= x2){
						image.setRGB(i, j, getRGB(c1));
					}
					else if(i > x1){
						image.setRGB(i, j, getRGB(c3));
					}
					else{
						image.setRGB(i, j, getRGB(c4));
					}
				}
			}
		}
		
		return image;
	}
	
	private String getColor(Color color){
		if(color.equals(Color.BLUE)){
			return COLOR_BLUE;
		}
		else if(color.equals(Color.BLACK)){
			return COLOR_BLACK;
		}
		else if(color.equals(Color.GREEN)){
			return COLOR_GREEN;
		}
		else if(color.equals(Color.LIGHT_GRAY)){
			return COLOR_GRAY;
		}
		else if(color.equals(Color.RED)){
			return COLOR_RED;
		}
		else if(color.equals(Color.YELLOW)){
			return COLOR_YELLOW;
		}
		return NA;
	}
	
	private int getRGB(Color c){
		float[] hsb;
		hsb = Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), null);

		if(c.equals(Color.RED)){
			hsb[ONE] = COLOR_SATURATION_8;
		}
		else if(c.equals(Color.YELLOW)){
			hsb[ONE] = COLOR_SATURATION_8;
		}
		else if(c.equals(Color.GREEN)){
			hsb[ONE] = COLOR_SATURATION_8;
		}
		else if(c.equals(Color.BLUE)){
			hsb[ONE] = COLOR_SATURATION_5;
		}
		else if(c.equals(Color.BLACK)){
			hsb[TWO] = COLOR_SATURATION_5;
		}
		return Color.HSBtoRGB(hsb[ZERO], hsb[ONE], hsb[TWO]);
	}
}
