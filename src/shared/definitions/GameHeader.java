package shared.definitions;

import java.io.Serializable;
import java.util.List;

public class GameHeader implements Serializable{

	private static final long serialVersionUID = 4358006926741268284L;
	private int gameID;
	private boolean finished;
	private String name;
	private List<PlayerHeader> players;
	private boolean started;
	
	public GameHeader(int gameID, boolean finished, String name, List<PlayerHeader> players, boolean started){
		this.gameID = gameID;
		this.finished = finished;
		this.name = name;
		this.players = players;
		this.started = started;
	}

	public void setGameID(int gameID){
		this.gameID = gameID;
	}
	public int getGameID(){
		return gameID;
	}
	
	public void setFinished(boolean finished){
		this.finished = finished;
	}
	public boolean isFinished(){
		return finished;
	}
	
	public String getName(){
		return name;
	}
	
	public List<PlayerHeader> getPlayers(){
		return players;
	}
	
	public void setPlayers(List<PlayerHeader> players){
		this.players = players;
	}
	
	public boolean isStarted(){
		return started;
	}
	
	public void start(){
		started = true;
	}
}
