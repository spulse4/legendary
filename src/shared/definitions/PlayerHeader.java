package shared.definitions;

import java.io.Serializable;

public class PlayerHeader extends UserHeader implements Serializable{

	private static final long serialVersionUID = 4812164816529691386L;
	private int index;
	
	public PlayerHeader(int index, int playerID, String name){
		super(playerID, name);
		this.index = index;
	}
	
	public int getIndex(){
		return index;
	}
}
