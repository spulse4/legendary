package shared.definitions;

import java.io.Serializable;

public class UserHeader implements Serializable{

	private static final long serialVersionUID = -5263884678241382287L;
	private int id;
	private String name;
	
	public UserHeader(int id, String name){
		this.id = id;
		this.name = name;
	}
	
	public int getId(){
		return id;
	}
	
	public String getName(){
		return name;
	}
}
