package shared.definitions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public enum State implements SharedVariables{

	//pre-game
	NOT_STARTED(), 
	
	//playing
	PLAYING(), 
	
	//ability
	BATTLEFIELD_PROMOTION_ABILITY(),
	COPY_POWERS_ABILITY(),
	COVERING_FIRE_ABILITY(),
	COVERING_FIRE_DISCARD(),
	CYCLOPS_ABILITY(),
	DANGEROUS_RESCUE_ABILITY(),
	ENERGY_DRAIN_ABILITY(),
	HEALING_FACTOR_ABILITY(),
	HERE_HOLD_THIS_FOR_A_SECOND_ABILITY(),
	HEY_CAN_I_GET_A_DO_OVER_ABILITY(),
	HYPNOTIC_CHARM_ABILITY(),
	PURE_FURY_ABILITY(),
	RANDOM_ACTS_OF_UNKINDNESS_ABILITY(),
	RANDOM_ACTS_OF_UNKINDNESS_PASS(),
	SHADOWED_THOUGHTS_ABILITY(),
	SILENT_SNIPER_ABILITY(),
	SPINNING_CYCLONE_ABILITY(),
	STACK_THE_DECK_ABILITY(),
	THE_AMAZING_SPIDERMAN_ABILITY(),
	UNSTOPPABLE_HULK_ABILITY(),
		
	//ambush
	JUGGERNAUT_AMBUSH(), 

	//escape
	DESTROYER_ESCAPE(),
	JUGGERNAUT_ESCAPE(),
	
	//fight
	BITTER_CAPTOR_FIGHT(),
	CRUEL_RULER_FIGHT(),
	DARK_TECHNOLOGY_FIGHT(),
	DOOMBOT_LEGION_FIGHT(),
	ELECTROMAGNETIC_BUBBLE_FIGHT(),
	HYDRA_KIDNAPPERS_FIGHT(),
	MAESTRO_FIGHT(),
	MANIACAL_TYRANT_FIGHT(),
	MELTER_FIGHT(),
	MONARCHS_DECREE_FIGHT(),
	MONARCHS_DECREE_DISCARD(),
	PAIBOK_THE_POWER_SKRULL_FIGHT(),
	RUTHLESS_DICTATOR_KO(),
	RUTHLESS_DICTATOR_DISCARD(),
	SENTINEL_FIGHT(),
	SUPER_SKRULL_FIGHT(),
	VANISHING_ILLUSIONS_FIGHT(),
	WHIRLWIND_FIGHT(),
	YMIR_FROST_GIANT_KING_FIGHT(),
	YMIR_FROST_GIANT_KING_DISCARD(),

	//masterstrike
	DR_DOOM_MASTER_STRIKE(),
	MAGNETO_MASTER_STRIKE(),
	RED_SKULL_MASTER_STRIKE(),	
	
	//game status
	LOST(), WON(),
	
	//test
	TEST();
	
	private Map<Integer, List<Integer>> effected;
	
	private State(){
		effected = new HashMap<Integer, List<Integer>>();
	}

	public static State fromString(int gameID, String str){
		str = str.toLowerCase();
		Scanner scanner = new Scanner(str);
		scanner.useDelimiter(DELIMITER);
		
		String st = scanner.next();
		for(State state : State.values()){
			if(state.toString().toLowerCase().equals(st)){
				List<Integer> effected = new ArrayList<Integer>();
				while(scanner.hasNext()){
					effected.add(Integer.valueOf(scanner.next()));
				}
				scanner.close();
				state.setEffected(gameID, effected);
				return state;
			}
		}
		scanner.close();
		return null;
	}
	
	public List<Integer> getEffected(int gameID){
		return effected.get(gameID);
	}
	
	public void setEffected(int gameID, List<Integer> effected){
		this.effected.put(gameID, effected);
	}
	
	public String getString(int gameID){
		StringBuilder str = new StringBuilder();
		str.append(this.toString());
		if(effected.containsKey(gameID)){
			for(Integer i : effected.get(gameID)){
				str.append(DELIMITER + i);
			}
		}
		return str.toString();
	}
}
