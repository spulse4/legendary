package shared.model;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import client.main.ClientManager;
import server.definition.ServerLoader;
import server.definition.ServerVariables;
import server.exceptions.InsufficientWoundsException;
import shared.definitions.PlayerHeader;

public class PlayerCards implements Serializable, ServerVariables{

	private static final long serialVersionUID = 5233946304358129499L;
	private int id = ZERO;
	private int index = ZERO;
	private int pendingAttackPoints = ZERO;
	private int pendingBuyPoints = ZERO;
	private String name = EMPTY;
	private List<String> draw;
	private List<String> discard;
	private List<String> victoryPile;
	private List<String> hand;
	private List<String> played;
	
	public PlayerCards(){
		draw = new ArrayList<String>();
		discard = new ArrayList<String>();
		victoryPile = new ArrayList<String>();
		hand = new ArrayList<String>();
		played = new ArrayList<String>();
		
		List<String> deck = fillHand();
		for(int i = ZERO; i < SIX; ++i){
			hand.add(deck.get(i));
			draw.add(deck.get(i + SIX));
		}
	}
	
	@SuppressWarnings("unchecked")
	public void updateFromJson(JsonObject o, int index){
		this.index = index;
		Gson gson = new Gson();
		List<Object> temp = gson.fromJson(o.get(MODEL_PLAYERS), List.class);
		
		Object p = temp.get(index);
		JsonObject player = gson.toJsonTree(p).getAsJsonObject();
		draw.clear();
		discard.clear();
		victoryPile.clear();
		hand.clear();
		played.clear();
		
		id = player.get(MODEL_PLAYER_ID).getAsInt();
		name = player.get(MODEL_PLAYER_NAME).getAsString();
		pendingAttackPoints = player.get(MODEL_PLAYER_PENDING_ATTACK_POINTS).getAsInt();
		pendingBuyPoints = player.get(MODEL_PLAYER_PENDING_BUY_POINTS).getAsInt();
		
		temp = gson.fromJson(player.get(MODEL_PLAYER_HAND).getAsString(), List.class);
		for(Object obj : temp){
			hand.add((String)obj);
		}

		//for if you play a duplicate card that you don't atually get to keep
		for(int i = ZERO; i < hand.size(); ++i){
			if(hand.get(i).endsWith(PERIOD)){
				hand.set(i, hand.get(i).substring(ZERO, hand.get(i).length() - ONE));
			}
		}

		temp = gson.fromJson(player.get(MODEL_PLAYER_PLAYED).getAsString(), List.class);
		for(Object obj : temp){
			played.add((String)obj);
		}
		
		//for if you play a duplicate card that you don't atually get to keep
		for(int i = ZERO; i < played.size(); ++i){
			if(played.get(i).endsWith(PERIOD)){
				played.set(i, played.get(i).substring(ZERO, played.get(i).length() - ONE));
			}
		}
		
		temp = gson.fromJson(player.get(MODEL_PLAYER_DISCARD).getAsString(), List.class);
		for(Object obj : temp){
			discard.add((String)obj);
		}
		
		temp = gson.fromJson(player.get(MODEL_PLAYER_DRAW).getAsString(), List.class);
		for(Object obj : temp){
			draw.add((String)obj);
		}
		
		temp = gson.fromJson(player.get(MODEL_PLAYER_VICTORY).getAsString(), List.class);
		for(Object obj : temp){
			victoryPile.add((String)obj);
		}
	}

	public int getDrawCount() {
		return draw.size();
	}
	
	public List<String> getDiscard(){
		return discard;
	}
	public List<String> getDraw(){
		return draw;
	}
	public List<String> getHand(){
		return hand;
	}
	public List<String> getPlayed(){
		return played;
	}
	public List<String> getVictoryPile(){
		return victoryPile;
	}
	
	public int getPendingAttackPoints() {
		return pendingAttackPoints;
	}

	public int getPendingBuyPoints() {
		return pendingBuyPoints;
	}

	public void setPendingAttackPoints(int pendingAttackPoints) {
		this.pendingAttackPoints = pendingAttackPoints;
	}

	public void setPendingBuyPoints(int pendingBuyPoints) {
		this.pendingBuyPoints = pendingBuyPoints;
	}

	public int getId() {
		return id;
	}

	public int getIndex() {
		return index;
	}

	public String getName() {
		return name;
	}
	
	private List<String> fillHand(){
		ArrayList<String> returnList = new ArrayList<String>();
		for(int i = ZERO; i < FOUR; ++i){
			returnList.add(TROOPER);
		}
		for(int i = ZERO; i < EIGHT; ++i){
			returnList.add(AGENT);
		}
		
		return shuffle(returnList);
	}

	private List<String> shuffle(List<String> deck){
		ArrayList<String> newDeck = new ArrayList<String>();
		
		Random random = ClientManager.getInstance().getRandom();
		
		while(!deck.isEmpty()){
			int choice = random.nextInt(deck.size());
			newDeck.add(deck.get(choice));
			deck.remove(choice);
		}
		
		return newDeck;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setID(int id){
		this.id = id;
	}
	
	public void setIndex(int index){
		this.index = index;
	}
	
	public JsonObject getJson(){
		Gson gson = new Gson();
		JsonObject temp = new JsonObject();
		temp.addProperty(MODEL_PLAYER_NAME, name);
		temp.addProperty(MODEL_PLAYER_ID, id);
		temp.addProperty(MODEL_PLAYER_INDEX, index);
		temp.addProperty(MODEL_PLAYER_HAND, gson.toJson(hand));
		temp.addProperty(MODEL_PLAYER_PLAYED, gson.toJson(played));
		temp.addProperty(MODEL_PLAYER_PENDING_ATTACK_POINTS, pendingAttackPoints);
		temp.addProperty(MODEL_PLAYER_PENDING_BUY_POINTS, pendingBuyPoints);
		temp.addProperty(MODEL_PLAYER_DISCARD, gson.toJson(discard));
		temp.addProperty(MODEL_PLAYER_DRAW, gson.toJson(draw));
		temp.addProperty(MODEL_PLAYER_VICTORY, gson.toJson(victoryPile));
		return temp;
	}
	
	public void shuffleCards(){
		List<String> newDiscard = discard;
		newDiscard = shuffle(newDiscard);
		
		for(int i = ZERO; i < newDiscard.size(); ++i){
			draw.add(newDiscard.get(i));
		}
		discard = new ArrayList<String>();
	}
	public void replaceHand(int nextHand, String addToHand, ModelFacade model){
		for(int i = ZERO; i < hand.size(); ++i){
			if(hand.get(i).endsWith(PERIOD)){
				continue;
			}
			discard.add(hand.get(i));
		}
		for(int i = ZERO; i < played.size(); ++i){
			if(played.get(i).endsWith(PERIOD)){
				continue;
			}
			if(played.get(i).contains(DELIMITER)){
				discard.add(played.get(i).substring(ZERO, played.get(i).indexOf(COLON)));
			}
			else{
				discard.add(played.get(i));
			}
		}
		
		played = new ArrayList<String>();
		List<String> newHand = new ArrayList<String>();
		
		if(draw.size() < nextHand){
			shuffleCards();
		}
		
		for(int i = ZERO; i < nextHand; ++i){
			newHand.add(draw.get(ZERO));
			draw.remove(ZERO);
		}
		if(addToHand != null){
			if(!newHand.contains(addToHand)){
				newHand.add(addToHand);
				boolean success = false;
				for(int i = ZERO; i < discard.size(); ++i){
					if(discard.get(i).equals(addToHand)){
						discard.remove(i);
						success = true;
						break;
					}
				}
				if(success == false){
					for(int i = ZERO; i < draw.size(); ++i){
						if(draw.get(i).equals(addToHand)){
							draw.remove(i);
							break;
						}
					}
				}
			}
		}
		if(TESTING){
			for(String str : ServerLoader.loadHand()){
				newHand.add(str + PERIOD);
			}
		}
		
		hand = newHand;
		pendingAttackPoints = ZERO;
		pendingBuyPoints = ZERO;
	}
	
	public void addToDraw(int index, String card){
		draw.add(index, card);
	}
	public String getFromDraw(int i){
		if(i >= draw.size()){
			shuffleCards();
		}
		return draw.get(i);
	}
	public void removeFromDraw(int i){
		draw.remove(i);
		if(draw.size() == 0){
			shuffleCards();
		}
	}
	public void setInDraw(int i, String card){
		draw.set(i, card);
	}
	
	public void addWound(OtherCards cards, List<String> location) throws InsufficientWoundsException{
		if(hand.contains(DIVING_BLOCK)){
			return;
		}
		cards.removeWounds();
		location.add(WOUND);
	}
	
	public JPanel getPanel(boolean winner){
		JPanel outter = new JPanel();
		outter.add(Box.createRigidArea(new Dimension(FIVE, ZERO)));
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel indexLabel;
		if(winner){
			indexLabel = new JLabel(DIALOG_WINNER + DIALOG_PLAYER_TITLE + (index + ONE) + COLON);
		}
		else{
			indexLabel = new JLabel(DIALOG_PLAYER_TITLE + (index + ONE) + COLON);
		}
		indexLabel.setFont(FONT_PLAIN_20);
		int points = getPoints();
		JLabel pointsLabel = new JLabel(Integer.toString(points));
		pointsLabel.setFont(FONT_PLAIN_20);
		panel.add(indexLabel);
		panel.add(pointsLabel);
		if(index == ((PlayerHeader)ClientManager.getInstance().getUser()).getIndex()){
			panel.setBorder(BorderFactory.createLineBorder(Color.RED, ONE));
		}
		else{
			panel.setBorder(BorderFactory.createLineBorder(Color.BLACK, ONE));
		}
		if(winner){
			panel.setBackground(Color.YELLOW);
		}
		outter.add(panel);
		outter.add(Box.createRigidArea(new Dimension(FIVE, ZERO)));
		return outter;
	}
	
	public int getPoints(){
		int points = ZERO;
		for(String str : victoryPile){
			switch(str){
			case SUPREME_HYDRA:
				for(String string : victoryPile){
					if(string.equals(SUPREME_HYDRA)){
						continue;
					}
					if(ClientManager.getInstance().getBoard().getCard(string).getCategory().equals(CATEGORY_HYDRA)){
						points += THREE;
					}
				}
				break;
			case ULTRON:
				for(String string : draw){
					if(ClientManager.getInstance().getBoard().getCard(string).getColors().contains(COLOR_BLACK)){
						points += ONE;
					}
				}
				for(String string : discard){
					if(ClientManager.getInstance().getBoard().getCard(string).getColors().contains(COLOR_BLACK)){
						points += ONE;
					}
				}
				for(String string : hand){
					if(ClientManager.getInstance().getBoard().getCard(string).getColors().contains(COLOR_BLACK)){
						points += ONE;
					}
				}
				for(String string : played){
					if(ClientManager.getInstance().getBoard().getCard(string).getColors().contains(COLOR_BLACK)){
						points += ONE;
					}
				}
				break;
			}
			points += ClientManager.getInstance().getBoard().getCard(str).getPoints();
		}
		return points;
	}
}
