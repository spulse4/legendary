package shared.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import client.base.Controller;
import client.main.ClientManager;
import client.popup.ability.*;
import client.popup.ambush.*;
import client.popup.endGame.*;
import client.popup.escape.*;
import client.popup.fight.*;
import client.popup.masterstrike.*;
import client.test.*;
import shared.definitions.GameHeader;
import shared.definitions.PlayerHeader;
import shared.definitions.SharedVariables;

public class ModelFacade implements Serializable, SharedVariables{

	private static final long serialVersionUID = -3832428557114043641L;

	private static ModelFacade instance = null;
	
	private int gameID = NEGATIVE_ONE;
	private boolean started = false;
	private String messageString = null;
	private List<String> message;
	private List<Controller> observers = new ArrayList<Controller>();
	private GameHeader game;
	private Chat chat = null;
	private City city = null;
	private Escaped escaped = null;
	private HeadQuarters headQuarters = null;
	private KOPile koPile = null;
	private OtherCards otherCards = null;
	private PlayerCards playerCards = null;
	private TurnTracker turnTracker = null;
	private List<PlayerCards> playerCardList = null;
	private int version = ZERO;
	
	
	public static ModelFacade getInstance(){
		if(instance == null){
			instance = new ModelFacade();
		}
		return instance;
	}
	
	public ModelFacade(){
		message = new ArrayList<String>();
		chat = new Chat();
		city = new City();
		escaped = new Escaped();
		headQuarters = new HeadQuarters();
		koPile = new KOPile();
		otherCards = new OtherCards();
		playerCards = new PlayerCards();
		turnTracker = new TurnTracker();
		playerCardList = new ArrayList<PlayerCards>();
	}
	
	@SuppressWarnings("unchecked")
	public synchronized void updateFromJson(JsonObject model, int i){
		if(started == false){
			Gson gson = new Gson();
			gameID = model.get(MODEL_GAME_ID).getAsInt();
			List<Object> obj = gson.fromJson(model.get(MODEL_PLAYERS), List.class);
			List<PlayerHeader> players = new ArrayList<PlayerHeader>();
			for(Object o : obj){
				JsonObject j = gson.toJsonTree(o).getAsJsonObject();
				int index = j.get(MODEL_PLAYER_INDEX).getAsInt();
				int id = j.get(MODEL_PLAYER_ID).getAsInt();
				String name = j.get(MODEL_PLAYER_NAME).getAsString();
				players.add(new PlayerHeader(index, id, name));
			}
			
			ClientManager.getInstance().updatePlayers(players);
			turnTracker.updateFromJson(model);	
			for(Controller c : observers){
				c.playersChanged();
			}
			for(Controller c : observers){
				c.turnTrackerChanged();
			}
			if(model.get(MODEL_STARTED).getAsBoolean() == false){
				return;
			}
			for(Controller c : observers){
				c.gameStarted();
			}
			started = true;
			creatModel();
		}
		
		gameID = model.get(MODEL_GAME_ID).getAsInt();
		chat.updateFromJson(model.get(MODEL_CHAT).getAsString());
		city.updateFromJson(model.get(MODEL_DECK).getAsJsonObject());
		escaped.updateFromJson(model.get(MODEL_ESCAPED).getAsJsonObject());
		headQuarters.updateFromJson(model.get(MODEL_DECK).getAsJsonObject());
		koPile.updateFromJson(model.get(MODEL_KO_PILE).getAsJsonObject());
		otherCards.updateFromJson(model.get(MODEL_DECK).getAsJsonObject());
		List<Object> temp = new Gson().fromJson(model.get(MODEL_PLAYERS), List.class);
		playerCardList.clear();
		for(int j = ZERO; j < temp.size(); ++j){
			playerCards = new PlayerCards();
			playerCards.updateFromJson(model, j);
			playerCardList.add(playerCards);
		}
		playerCards = playerCardList.get(i);
		turnTracker.updateFromJson(model);	
		version = model.get(MODEL_VERSION).getAsInt();
		messageString = model.get(MODEL_MESSAGE).getAsString();
		Scanner scanner = new Scanner(messageString);
		scanner.useDelimiter(DELIMITER);
		while(scanner.hasNext()){
			message.add(scanner.next());
		}
		scanner.close();
		
		if(turnTracker.getCardsInEffect().contains(GOD_OF_THUNDER)){
			int pendingAttackPoints = playerCards.getPendingAttackPoints();
			int pendingBuyPoints = playerCards.getPendingBuyPoints();
			playerCards.setPendingAttackPoints(pendingAttackPoints + pendingBuyPoints);
		}
		for(Controller c : observers){
			c.modelChanged();
		}
	}
	
	public void registerObserver(Controller cont){
		observers.add(cont);
	}
		
	private void creatModel(){
		chat = new Chat();
		city = new City();
		escaped = new Escaped();
		headQuarters = new HeadQuarters();
		koPile = new KOPile();
		otherCards = new OtherCards();
		playerCards = new PlayerCards();
		setControllers();
	}
	
	public JsonObject getJson(){
		Gson gson = new Gson();
		JsonObject o = new JsonObject();
		
		o.addProperty(MODEL_GAME_ID, gameID);
		
		JsonObject temp = turnTracker.getJson(gameID);
		o.add(MODEL_TURN_TRACKER, temp);
		
		temp = new JsonObject();
		JsonObject cityO = city.getJson();
		JsonObject headO = headQuarters.getJson();
		JsonObject otherO = otherCards.getJson();
		for(Map.Entry<String, JsonElement> entry : cityO.entrySet()){
			temp.add(entry.getKey(), entry.getValue());
		}
		for(Map.Entry<String, JsonElement> entry : headO.entrySet()){
			temp.add(entry.getKey(), entry.getValue());
		}
		for(Map.Entry<String, JsonElement> entry : otherO.entrySet()){
			temp.add(entry.getKey(), entry.getValue());
		}
		o.add(MODEL_DECK, temp);
		
		temp = koPile.getJson();
		o.add(MODEL_KO_PILE, temp);
		
		temp = escaped.getJson();
		o.add(MODEL_ESCAPED, temp);
		
		List<JsonObject> listOfPlayers = new ArrayList<JsonObject>();
		for(PlayerCards pc : playerCardList){
			temp = pc.getJson();
			listOfPlayers.add(temp);
		}
		o.add(MODEL_PLAYERS, gson.toJsonTree(listOfPlayers));
		
		o.addProperty(MODEL_CHAT, chat.getJson());
		
		o.addProperty(MODEL_STARTED, started);
		
		o.addProperty(MODEL_VERSION, version);
		
		StringBuilder newMessage = new StringBuilder();
		for(String s : message){
			if(newMessage.length() != ZERO){
				newMessage.append(DELIMITER);
			}
			newMessage.append(s);
		}
		if(newMessage.length() == ONE){
			newMessage = new StringBuilder();
		}
		
		o.addProperty(MODEL_MESSAGE, newMessage.toString());
		
		return o;
	}

	public Chat getChat(){
		return chat;
	}
	public City getCity() {
		return city;
	}
	public void setCity(City city){
		this.city = city;
	}

	public Escaped getEscaped() {
		return escaped;
	}
	public void setEscaped(Escaped escaped){
		this.escaped = escaped;
	}

	public HeadQuarters getHeadQuarters() {
		return headQuarters;
	}
	public void setHeadQuarters(HeadQuarters headQuarters){
		this.headQuarters = headQuarters;
	}

	public KOPile getKoPile() {
		return koPile;
	}
	public void setKoPile(KOPile koPile){
		this.koPile = koPile;
	}

	public OtherCards getOtherCards() {
		return otherCards;
	}
	public void setOtherCards(OtherCards otherCards){
		this.otherCards = otherCards;
	}

	public PlayerCards getPlayerCards() {
		return playerCards;
	}

	public TurnTracker getTurnTracker() {
		return turnTracker;
	}
	public void setTurnTracker(TurnTracker turnTracker){
		this.turnTracker = turnTracker;
	}
	
	public List<PlayerCards> getPlayerCardList(){
		return playerCardList;
	}
	public void setPlayerCardList(List<PlayerCards> playerCardList){
		this.playerCardList = playerCardList;
	}
	
	public void setVersion(int version){
		this.version = version;
	}
	public int getVersion(){
		return version;
	}
	
	public void setStarted(boolean started){
		this.started = started;
	}
	
	public void setGameID(int gameID){
		this.gameID = gameID;
	}
	public int getGameID(){
		return gameID;
	}
	
	public void setGameHeader(GameHeader game){
		this.game = game;
	}
	public GameHeader getGameHeader(){
		return game;
	}
	
	public void setMessage(List<String> message){
		this.message = message;
	}
	public List<String> getMessage(){
		return message;
	}
	
	public String getMessageString(){
		return messageString;
	}
	
	public void setControllers(){
		setAbilityControllers();
		setAmbushControllers();
		setEndGameControllers();
		setEscapeControllers();
		setFightControllers();
		setMasterstrikeControllers();
		setTestControllers();
	}
	private void setAbilityControllers(){
		new AbilityBattlefieldPromotionController(new AbilityBattlefieldPromotionView());
		new AbilityCopyPowersController(new AbilityCopyPowersView());
		new AbilityCoveringFireController(new AbilityCoveringFireView());
		new AbilityCyclopsController(new AbilityCyclopsView());
		new AbilityDangerousRescueController(new AbilityDangerousRescueView());
		new AbilityEnergyDrainController(new AbilityEnergyDrainView());
		new AbilityHealingFactorController(new AbilityHealingFactorView());
		new AbilityHereHoldThisForASecondController(new AbilityHereHoldThisForASecondView());
		new AbilityHeyCanIGetADoOverController(new AbilityHeyCanIGetADoOverView());
		new AbilityHypnoticCharmController(new AbilityHypnoticCharmView());
		new AbilityPureFuryController(new AbilityPureFuryView());
		new AbilityRandomActsOfUnkindnessController(new AbilityRandomActsOfUnkindnessView());
		new AbilityShadowedThoughtsController(new AbilityShadowedThoughtsView());
		new AbilitySilentSniperController(new AbilitySilentSniperView());
		new AbilitySpinningCycloneController(new AbilitySpinningCycloneView());
		new AbilityStackTheDeckController(new AbilityStackTheDeckView());
		new AbilityTheAmazingSpidermanController(new AbilityTheAmazingSpidermanView());
		new AbilityUnstoppableHulkController(new AbilityUnstoppableHulkView());
		new DiscardCoveringFireController(new DiscardCoveringFireView());
		new PassRandomActsOfUnkindnessController(new PassRandomActsOfUnkindnessView());
	}
	private void setAmbushControllers(){
		new AmbushJuggernautController(new AmbushJuggernautView());
	}
	private void setEndGameControllers(){
		new EndGameLoseController(new EndGameLoseView());
		new EndGameWinController(new EndGameWinView());
	}
	private void setEscapeControllers(){
		new EscapeDestroyerController(new EscapeDestroyerView());
		new EscapeJuggernautController(new EscapeJuggernautView());
	}
	private void setFightControllers(){
		new DiscardMonarchsDecreeController(new DiscardMonarchsDecreeView());
		new DiscardRuthlessDictatorController(new DiscardRuthlessDictatorView());
		new DiscardYmirFrostGiantKingController(new DiscardYmirFrostGiantKingView());
		new FightBitterCaptorController(new FightBitterCaptorView());
		new FightCruelRulerController(new FightCruelRulerView());
		new FightDarkTechnologyController(new FightDarkTechnologyView());
		new FightDoombotLegionController(new FightDoombotLegionView());
		new FightElectromagneticBubbleController(new FightElectromagneticBubbleView());
		new FightHydraKidnappersController(new FightHydraKidnappersView());
		new FightMaestroController(new FightMaestroView());
		new FightManiacalTyrantController(new FightManiacalTyrantView());
		new FightMelterController(new FightMelterView());
		new FightMonarchsDecreeController(new FightMonarchsDecreeView());
		new FightPaibokThePowerSkrullController(new FightPaibokThePowerSkrullView());
		new FightSentinelController(new FightSentinelView());
		new FightSuperSkrullController(new FightSuperSkrullView());
		new FightVanishingIllusionsController(new FightVanishingIllusionsView());
		new FightWhirlwindController(new FightWhirlwindView());
		new FightYmirFrostGiantKingController(new FightYmirFrostGiantKingView());
		new KORuthlessDictatorController(new KORuthlessDictatorView());
	}
	private void setMasterstrikeControllers(){
		new MasterstrikeDrDoomController(new MasterstrikeDrDoomView());
		new MasterstrikeMagnetoController(new MasterstrikeMagnetoView());
		new MasterstrikeRedSkullController(new MasterstrikeRedSkullView());
	}
	private void setTestControllers(){
		new PopupController(new PopupView());
	}
}
