package shared.model;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.google.gson.JsonObject;

import shared.definitions.SharedVariables;

public class Escaped implements Serializable, SharedVariables{

	private static final long serialVersionUID = -2432028767132207880L;
	private int brotherhood = ZERO;
	private int mastersOfEvil = ZERO;
	private int enemiesOfAsgard = ZERO;
	private int spiderfoes = ZERO;
	private int hydra = ZERO;
	private int skrulls = ZERO;
	private int radiation = ZERO;
	private int henchmen = ZERO;
	private int bystanders = ZERO;
	private int heroes = ZERO;
	private String mostRecent = null;
	
	public Escaped(){}
	
	public void updateFromJson(JsonObject o){
		brotherhood = o.get(CATEGORY_BROTHERHOOD).getAsInt();
		mastersOfEvil = o.get(CATEGORY_MASTERS_OF_EVIL).getAsInt();
		enemiesOfAsgard = o.get(CATEGORY_ENEMIES_OF_ASGARD).getAsInt();
		spiderfoes = o.get(CATEGORY_SPIDER_FOES).getAsInt();
		hydra = o.get(CATEGORY_HYDRA).getAsInt();
		skrulls = o.get(CATEGORY_SKRULLS).getAsInt();
		radiation = o.get(CATEGORY_RADIATION).getAsInt();
		henchmen = o.get(CATEGORY_HENCHMEN).getAsInt();
		bystanders = o.get(CATEGORY_BYSTANDERS).getAsInt();
		heroes = o.get(CATEGORY_HEROES).getAsInt();
		mostRecent = o.get(CATEGORY_MOST_RECENT).getAsString();
		
		if(mostRecent.equals(NULL_STRING)){
			mostRecent = null;
		}
	}

	public int getBrotherhood() {
		return brotherhood;
	}

	public int getMastersOfEvil() {
		return mastersOfEvil;
	}

	public int getEnemiesOfAsgard() {
		return enemiesOfAsgard;
	}

	public int getSpiderfoes() {
		return spiderfoes;
	}

	public int getHydra() {
		return hydra;
	}

	public int getSkrulls() {
		return skrulls;
	}

	public int getRadiation() {
		return radiation;
	}

	public int getHenchmen() {
		return henchmen;
	}

	public int getBystanders() {
		return bystanders;
	}
	
	public int getHeroes(){
		return heroes;
	}

	public String getMostRecent() {
		return mostRecent;
	}
	
	public JPanel getPanel(final JDialog dialog){
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		JPanel namePanel = new JPanel();
		namePanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		JPanel brotherhoodPanel = new JPanel();
		brotherhoodPanel.setLayout(new BorderLayout());
		JPanel mastersOfEvilPanel = new JPanel();
		mastersOfEvilPanel.setLayout(new BorderLayout());
		JPanel enemiesOfAsgardPanel = new JPanel();
		enemiesOfAsgardPanel.setLayout(new BorderLayout());
		JPanel spiderfoesPanel = new JPanel();
		spiderfoesPanel.setLayout(new BorderLayout());
		JPanel hydraPanel = new JPanel();
		hydraPanel.setLayout(new BorderLayout());
		JPanel skrullsPanel = new JPanel();
		skrullsPanel.setLayout(new BorderLayout());
		JPanel radiationPanel = new JPanel();
		radiationPanel.setLayout(new BorderLayout());
		JPanel henchmenPanel = new JPanel();
		henchmenPanel.setLayout(new BorderLayout());
		JPanel bystandersPanel = new JPanel();
		bystandersPanel.setLayout(new BorderLayout());
		JPanel heroesPanel = new JPanel();
		heroesPanel.setLayout(new BorderLayout());
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		Font font = FONT_PLAIN_20;

		JLabel name = new JLabel(TITLES_ESCAPED_PILE);
		name.setFont(font);
		namePanel.add(name);
		
		JLabel brotherhood1 = new JLabel(ESCAPED_BROTHERHOOD);
		brotherhood1.setPreferredSize(new Dimension(ESCAPED_WIDTH, ZERO));
		brotherhood1.setFont(font);
		JLabel brotherhood2 = new JLabel(Integer.toString(this.brotherhood));
		brotherhood2.setFont(font);
		brotherhoodPanel.add(brotherhood1, BorderLayout.LINE_START);
		brotherhoodPanel.add(brotherhood2, BorderLayout.CENTER);

		JLabel mastersOfEvil1 = new JLabel(ESCAPED_MASTERS_OF_EVIL);
		mastersOfEvil1.setPreferredSize(new Dimension(ESCAPED_WIDTH, ZERO));
		mastersOfEvil1.setFont(font);
		JLabel mastersOfEvil2 = new JLabel(Integer.toString(this.mastersOfEvil));
		mastersOfEvil2.setFont(font);
		mastersOfEvilPanel.add(mastersOfEvil1, BorderLayout.LINE_START);
		mastersOfEvilPanel.add(mastersOfEvil2, BorderLayout.CENTER);

		JLabel spiderfoes1 = new JLabel(ESCAPED_SPIDER_FOES);
		spiderfoes1.setPreferredSize(new Dimension(ESCAPED_WIDTH, ZERO));
		spiderfoes1.setFont(font);
		JLabel spiderfoes2 = new JLabel(Integer.toString(this.spiderfoes));
		spiderfoes2.setFont(font);
		spiderfoesPanel.add(spiderfoes1, BorderLayout.LINE_START);
		spiderfoesPanel.add(spiderfoes2, BorderLayout.CENTER);

		JLabel hydra1 = new JLabel(ESCAPED_HYDRA);
		hydra1.setPreferredSize(new Dimension(ESCAPED_WIDTH, ZERO));
		hydra1.setFont(font);
		JLabel hydra2 = new JLabel(Integer.toString(this.hydra));
		hydra2.setFont(font);
		hydraPanel.add(hydra1, BorderLayout.LINE_START);
		hydraPanel.add(hydra2, BorderLayout.CENTER);

		JLabel skrulls1 = new JLabel(ESCAPED_SKRULLS);
		skrulls1.setPreferredSize(new Dimension(ESCAPED_WIDTH, ZERO));
		skrulls1.setFont(font);
		JLabel skrulls2= new JLabel(Integer.toString(this.skrulls));
		skrulls2.setFont(font);
		skrullsPanel.add(skrulls1, BorderLayout.LINE_START);
		skrullsPanel.add(skrulls2, BorderLayout.CENTER);

		JLabel radiation1 = new JLabel(ESCAPED_RADIATION);
		radiation1.setPreferredSize(new Dimension(ESCAPED_WIDTH, ZERO));
		radiation1.setFont(font);
		JLabel radiation2 = new JLabel(Integer.toString(this.radiation));
		radiation2.setFont(font);
		radiationPanel.add(radiation1, BorderLayout.LINE_START);
		radiationPanel.add(radiation2, BorderLayout.CENTER);

		JLabel henchmen1 = new JLabel(ESCAPED_HENCHMEN);
		henchmen1.setPreferredSize(new Dimension(ESCAPED_WIDTH, ZERO));
		henchmen1.setFont(font);
		JLabel henchmen2 = new JLabel(Integer.toString(this.henchmen));
		henchmen2.setFont(font);
		henchmenPanel.add(henchmen1, BorderLayout.LINE_START);
		henchmenPanel.add(henchmen2, BorderLayout.CENTER);

		JLabel bystanders1 = new JLabel(ESCAPED_BYSTANDERS);
		bystanders1.setPreferredSize(new Dimension(ESCAPED_WIDTH, ZERO));
		bystanders1.setFont(font);
		JLabel bystanders2 = new JLabel(Integer.toString(this.bystanders));
		bystanders2.setFont(font);
		bystandersPanel.add(bystanders1, BorderLayout.LINE_START);
		bystandersPanel.add(bystanders2, BorderLayout.CENTER);

		JLabel heroes1 = new JLabel(ESCAPED_HEROES);
		heroes1.setPreferredSize(new Dimension(ESCAPED_WIDTH, ZERO));
		heroes1.setFont(font);
		JLabel heroes2 = new JLabel(Integer.toString(this.heroes));
		heroes2.setFont(font);
		heroesPanel.add(heroes1, BorderLayout.LINE_START);
		heroesPanel.add(heroes2, BorderLayout.CENTER);

		JButton button = new JButton(BUTTON_CANCEL);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				dialog.dispose();
			}
			
		});
		buttonPanel.add(button);
		
		panel.add(namePanel);
		panel.add(brotherhoodPanel);
		panel.add(mastersOfEvilPanel);
		panel.add(enemiesOfAsgardPanel);
		panel.add(spiderfoesPanel);
		panel.add(hydraPanel);
		panel.add(skrullsPanel);
		panel.add(radiationPanel);
		panel.add(henchmenPanel);
		panel.add(bystandersPanel);
		panel.add(heroesPanel);
		panel.add(buttonPanel);
		return panel;
	}
	
	public String escaped(){
		StringBuilder str = new StringBuilder();
		str.append(brotherhood);
		str.append(DELIMITER);
		str.append(mastersOfEvil);
		str.append(DELIMITER);
		str.append(enemiesOfAsgard);
		str.append(DELIMITER);
		str.append(spiderfoes);
		str.append(DELIMITER);
		str.append(hydra);
		str.append(DELIMITER);
		str.append(skrulls);
		str.append(DELIMITER);
		str.append(radiation);
		str.append(DELIMITER);
		str.append(henchmen);
		str.append(DELIMITER);
		str.append(bystanders);
		str.append(DELIMITER);
		str.append(heroes);
		str.append(DELIMITER);
		str.append(mostRecent);
		return str.toString();
	}

	public void setBrotherhood(int brotherhood) {
		this.brotherhood = brotherhood;
	}

	public void setMastersOfEvil(int mastersOfEvil) {
		this.mastersOfEvil = mastersOfEvil;
	}

	public void setEnemiesOfAsgard(int enemiesOfAsgard) {
		this.enemiesOfAsgard = enemiesOfAsgard;
	}

	public void setSpiderfoes(int spiderfoes) {
		this.spiderfoes = spiderfoes;
	}

	public void setHydra(int hydra) {
		this.hydra = hydra;
	}

	public void setSkrulls(int skrulls) {
		this.skrulls = skrulls;
	}

	public void setRadiation(int radiation) {
		this.radiation = radiation;
	}

	public void setHenchmen(int henchmen) {
		this.henchmen = henchmen;
	}

	public void setBystanders(int bystanders) {
		this.bystanders = bystanders;
	}

	public void setHeroes(int heroes) {
		this.heroes = heroes;
	}

	public void setMostRecent(String mostRecent) {
		this.mostRecent = mostRecent;
	}
	
	public JsonObject getJson(){
		JsonObject temp = new JsonObject();
		String str = mostRecent;
		if(str == null){
			str = NULL_STRING;
		}
		temp.addProperty(CATEGORY_MOST_RECENT, str);
		temp.addProperty(CATEGORY_BROTHERHOOD, brotherhood);
		temp.addProperty(CATEGORY_MASTERS_OF_EVIL, mastersOfEvil);
		temp.addProperty(CATEGORY_ENEMIES_OF_ASGARD, enemiesOfAsgard);
		temp.addProperty(CATEGORY_SPIDER_FOES, spiderfoes);
		temp.addProperty(CATEGORY_HYDRA, hydra);
		temp.addProperty(CATEGORY_SKRULLS, skrulls);
		temp.addProperty(CATEGORY_RADIATION, radiation);
		temp.addProperty(CATEGORY_HENCHMEN, henchmen);
		temp.addProperty(CATEGORY_BYSTANDERS, bystanders);
		temp.addProperty(CATEGORY_HEROES, heroes);
		
		return temp;
	}
}
