package shared.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import server.exceptions.InsufficientBystandersException;
import server.exceptions.InsufficientOfficersException;
import server.exceptions.InsufficientWoundsException;
import shared.definitions.SharedVariables;

public class OtherCards implements Serializable, SharedVariables{

	private static final long serialVersionUID = -1910013810242970022L;
	private int wounds = ZERO;
	private int bystanders = ZERO;
	private int officers = ZERO;
	private int schemeTwists = ZERO;
	private List<String> heroes;
	private List<String> villians;
	private List<String> masterminds;
	
	public OtherCards(){
		heroes = new ArrayList<String>();
		villians = new ArrayList<String>();
		masterminds = new ArrayList<String>();
	}
	
	@SuppressWarnings("unchecked")
	public void updateFromJson(JsonObject o){
		
		wounds = o.get(MODEL_WOUNDS).getAsInt();
		bystanders = o.get(MODEL_BYSTANDERS).getAsInt();
		officers = o.get(MODEL_OFFICERS).getAsInt();
		schemeTwists = o.get(MODEL_SCHEME_TWISTS).getAsInt();
		
		Gson gson = new Gson();
		
		List<Object> obj = gson.fromJson(o.get(HEROES).getAsString(), List.class);
		heroes.clear();
		for(Object ob : obj){
			heroes.add((String)ob);
		}
		
		obj = gson.fromJson(o.get(MODEL_VILLIANS).getAsString(), List.class);
		villians.clear();
		for(Object ob : obj){
			villians.add((String)ob);
		}
		
		obj = gson.fromJson(o.get(MODEL_MASTERMINDS).getAsString(), List.class);
		masterminds.clear();
		for(Object ob : obj){
			masterminds.add((String)ob);
		}
	}

	public int getHerosCount() {
		return heroes.size();
	}

	public int getVilliansCount() {
		return villians.size();
	}
	
	public int getMastermindsCount(){
		return masterminds.size();
	}

	public int getWounds() {
		return wounds;
	}

	public int getBystanders() {
		return bystanders;
	}

	public int getOfficers() {
		return officers;
	}

	public int getSchemeTwists() {
		return schemeTwists;
	}
	
	public List<String> getHeroes(){
		return heroes;
	}
	
	public List<String> getVillians(){
		return villians;
	}
	
	public List<String> getMasterminds() {
		return masterminds;
	}

	public void setBystanders(int bystanders){
		this.bystanders = bystanders;
	}
	
	public void setWounds(int wounds) {
		this.wounds = wounds;
	}

	public void setOfficers(int officers) {
		this.officers = officers;
	}

	public void setSchemeTwists(int schemeTwists) {
		this.schemeTwists = schemeTwists;
	}

	public void setHeroes(List<String> heroes) {
		this.heroes = heroes;
	}

	public void setVillians(List<String> villians) {
		this.villians = villians;
	}

	public void setMasterminds(List<String> masterminds) {
		this.masterminds = masterminds;
	}
	
	public JsonObject getJson(){
		Gson gson = new Gson();
		JsonObject temp = new JsonObject();
		temp.addProperty(MODEL_HEROES, gson.toJson(heroes));
		temp.addProperty(MODEL_VILLIANS, gson.toJson(villians));
		temp.addProperty(MODEL_WOUNDS, wounds);
		temp.addProperty(MODEL_BYSTANDERS, bystanders);
		temp.addProperty(MODEL_OFFICERS, officers);
		temp.addProperty(MODEL_MASTERMINDS, gson.toJson(masterminds));
		temp.addProperty(MODEL_SCHEME_TWISTS, schemeTwists);
		
		return temp;
	}
	
	public void removeBystander(int count) throws InsufficientBystandersException{
		if(bystanders < count){
			throw new InsufficientBystandersException();
		}
		bystanders -= count;
	}
	public void removeOfficer() throws InsufficientOfficersException{
		if(officers == ZERO){
			throw new InsufficientOfficersException();
		}
		--officers;
	}
	public void removeWounds() throws InsufficientWoundsException{
		if(wounds == ZERO){
			throw new InsufficientWoundsException();
		}
		--wounds;
	}
}
