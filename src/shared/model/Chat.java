package shared.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import shared.definitions.SharedVariables;

public class Chat implements Serializable, SharedVariables{

	private static final long serialVersionUID = 385042643540576857L;

	private List<Message> messages;
	
	public Chat(){
		messages = new ArrayList<Message>();
	}
	
	public void updateFromJson(String modelMessages){
		messages.clear();
		Scanner scanner = new Scanner(modelMessages);
		scanner.useDelimiter(NEXT);
		
		while(scanner.hasNext()){
			String line = scanner.next();
			int index = Integer.valueOf(line.substring(ZERO, ONE));
			String message = line.substring(ONE);
			messages.add(new Message(index, message));
		}
		
		scanner.close();
	}
	
	public void addMessage(int index, String message){
		messages.add(new Message(index, message));
	}
	
	public List<Message> getMessages(){
		return messages;
	}
	
	public String getJson(){
		StringBuilder str = new StringBuilder();
		for(Message m : messages){
			str.append(m.getString());
		}
		return str.toString();
	}
}
