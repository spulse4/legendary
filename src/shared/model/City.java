package shared.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.google.gson.JsonObject;

import shared.definitions.SharedVariables;

public class City implements Serializable, SharedVariables{

	private static final long serialVersionUID = 4484339626422031773L;
	
	private String sewers = null;
	private String bank = null;
	private String rooftops = null;
	private String streets = null;
	private String bridge = null;
	private boolean mastermindPortal;
	private boolean sewersPortal;
	private boolean bankPortal;
	private boolean rooftopsPortal;
	private boolean streetsPortal;
	private boolean bridgePortal;
	private List<String> mastermindCaptured = null;
	private List<String> sewersCaptured = null;
	private List<String> bankCaptured = null;
	private List<String> rooftopsCaptured = null;
	private List<String> streetsCaptured = null;
	private List<String> bridgeCaptured = null;
	
	public City(){
		mastermindCaptured = new ArrayList<String>();
		sewersCaptured = new ArrayList<String>();
		bankCaptured = new ArrayList<String>();
		rooftopsCaptured = new ArrayList<String>();
		streetsCaptured = new ArrayList<String>();
		bridgeCaptured = new ArrayList<String>();		
	}
	
	public void updateFromJson(JsonObject o){
		String mastermind = o.get(MODEL_MASTERMIND_CAPTURED).getAsString();
		sewers = o.get(MODEL_SEWERS).getAsString();
		bank = o.get(MODEL_BANK).getAsString();
		rooftops = o.get(MODEL_ROOFTOPS).getAsString();
		streets = o.get(MODEL_STREETS).getAsString();
		bridge = o.get(MODEL_BRIDGE).getAsString();
		mastermindPortal = o.get(MODEL_MASTERMIND_PORTAL).getAsBoolean();
		sewersPortal = o.get(MODEL_SEWERS_PORTAL).getAsBoolean();
		bankPortal = o.get(MODEL_BANK_PORTAL).getAsBoolean();
		rooftopsPortal = o.get(MODEL_ROOFTOPS_PORTAL).getAsBoolean();
		streetsPortal = o.get(MODEL_STREETS_PORTAL).getAsBoolean();
		bridgePortal = o.get(MODEL_BRIDGE_PORTAL).getAsBoolean();

		if(mastermind.equals(NULL_STRING)){
			mastermind = null;
		}
		if(sewers.equals(NULL_STRING)){
			sewers = null;
		}
		if(bank.equals(NULL_STRING)){
			bank = null;
		}
		if(rooftops.equals(NULL_STRING)){
			rooftops = null;
		}
		if(streets.equals(NULL_STRING)){
			streets = null;
		}
		if(bridge.equals(NULL_STRING)){
			bridge = null;
		}
		
		mastermindCaptured = new ArrayList<String>();
		sewersCaptured = new ArrayList<String>();
		bankCaptured = new ArrayList<String>();
		rooftopsCaptured = new ArrayList<String>();
		streetsCaptured = new ArrayList<String>();
		bridgeCaptured = new ArrayList<String>();
		
		Scanner myScanner = new Scanner(mastermind);
		myScanner.useDelimiter(DELIMITER);
		while(myScanner.hasNext()){
			mastermindCaptured.add(myScanner.next());
		}
		myScanner.close();
		if(sewers != null){
			myScanner = new Scanner(sewers);
			myScanner.useDelimiter(DELIMITER);
			sewers = myScanner.next();
			while(myScanner.hasNext()){
				sewersCaptured.add(myScanner.next());
			}
			myScanner.close();
		}
		if(bank != null){
			myScanner = new Scanner(bank);
			myScanner.useDelimiter(DELIMITER);
			bank = myScanner.next();
			while(myScanner.hasNext()){
				bankCaptured.add(myScanner.next());
			}
			myScanner.close();
		}
		if(rooftops != null){
			myScanner = new Scanner(rooftops);
			myScanner.useDelimiter(DELIMITER);
			rooftops = myScanner.next();
			while(myScanner.hasNext()){
				rooftopsCaptured.add(myScanner.next());
			}
			myScanner.close();
		}
		if(streets != null){
			myScanner = new Scanner(streets);
			myScanner.useDelimiter(DELIMITER);
			streets = myScanner.next();
			while(myScanner.hasNext()){
				streetsCaptured.add(myScanner.next());
			}
			myScanner.close();
		}
		if(bridge != null){
			myScanner = new Scanner(bridge);
			myScanner.useDelimiter(DELIMITER);
			bridge = myScanner.next();
			while(myScanner.hasNext()){
				bridgeCaptured.add(myScanner.next());
			}
			myScanner.close();
		}
	}
	
	public String getSewers(){
		return sewers;
	}
	
	public String getBank(){
		return bank;
	}

	public String getRooftops(){
		return rooftops;
	}

	public String getStreets(){
		return streets;
	}

	public String getBridge(){
		return bridge;
	}
	
	public boolean getMastermindPortal(){
		return mastermindPortal;
	}

	public boolean getSewersPortal(){
		return sewersPortal;
	}

	public boolean getBankPortal(){
		return bankPortal;
	}

	public boolean getRooftopsPortal(){
		return rooftopsPortal;
	}

	public boolean getStreetsPortal(){
		return streetsPortal;
	}

	public boolean getBridgePortal(){
		return bridgePortal;
	}

	public List<String> getMastermindCaptured(){
		return mastermindCaptured;
	}

	public List<String> getSewersCaptured(){
		return sewersCaptured;
	}
	
	public List<String> getBankCaptured(){
		return bankCaptured;
	}

	public List<String> getRooftopsCaptured(){
		return rooftopsCaptured;
	}

	public List<String> getStreetsCaptured(){
		return streetsCaptured;
	}

	public List<String> getBridgeCaptured(){
		return bridgeCaptured;
	}
	
	public String mastermind(){
		StringBuilder str = new StringBuilder();
		for(String string : mastermindCaptured){
			if(str.length() != 0){
				str.append(DELIMITER);
			}
			str.append(string);
		}
		return str.toString();
	}
	
	public String sewers(){
		StringBuilder str = new StringBuilder();
		str.append(sewers);
		for(String string : sewersCaptured){
			str.append(DELIMITER + string);
		}
		return str.toString();
	}
	
	public String bank(){
		StringBuilder str = new StringBuilder();
		str.append(bank);
		for(String string : bankCaptured){
			str.append(DELIMITER + string);
		}
		return str.toString();
	}
	
	public String rooftops(){
		StringBuilder str = new StringBuilder();
		str.append(rooftops);
		for(String string : rooftopsCaptured){
			str.append(DELIMITER + string);
		}
		return str.toString();
	}
	
	public String streets(){
		StringBuilder str = new StringBuilder();
		str.append(streets);
		for(String string : streetsCaptured){
			str.append(DELIMITER + string);
		}
		return str.toString();
	}
	
	public String bridge(){
		StringBuilder str = new StringBuilder();
		str.append(bridge);
		for(String string : bridgeCaptured){
			str.append(DELIMITER + string);
		}
		return str.toString();
	}

	public void setSewers(String sewers) {
		this.sewers = sewers;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public void setRooftops(String rooftops) {
		this.rooftops = rooftops;
	}

	public void setStreets(String streets) {
		this.streets = streets;
	}

	public void setBridge(String bridge) {
		this.bridge = bridge;
	}

	public void setMastermindPortal(boolean mastermindPortal) {
		this.mastermindPortal = mastermindPortal;
	}

	public void setSewersPortal(boolean sewersPortal) {
		this.sewersPortal = sewersPortal;
	}

	public void setBankPortal(boolean bankPortal) {
		this.bankPortal = bankPortal;
	}

	public void setRooftopsPortal(boolean rooftopsPortal) {
		this.rooftopsPortal = rooftopsPortal;
	}

	public void setStreetsPortal(boolean streetsPortal) {
		this.streetsPortal = streetsPortal;
	}

	public void setBridgePortal(boolean bridgePortal) {
		this.bridgePortal = bridgePortal;
	}

	public void setMastermindCaptured(List<String> mastermindCaptured) {
		this.mastermindCaptured = mastermindCaptured;
	}

	public void setSewersCaptured(List<String> sewersCaptured) {
		this.sewersCaptured = sewersCaptured;
	}

	public void setBankCaptured(List<String> bankCaptured) {
		this.bankCaptured = bankCaptured;
	}

	public void setRooftopsCaptured(List<String> rooftopsCaptured) {
		this.rooftopsCaptured = rooftopsCaptured;
	}

	public void setStreetsCaptured(List<String> streetsCaptured) {
		this.streetsCaptured = streetsCaptured;
	}

	public void setBridgeCaptured(List<String> bridgeCaptured) {
		this.bridgeCaptured = bridgeCaptured;
	}
	
	public JsonObject getJson(){
		JsonObject temp = new JsonObject();
		String str = mastermind();

		temp.addProperty(MODEL_MASTERMIND_CAPTURED, str);
		temp.addProperty(MODEL_MASTERMIND_PORTAL, mastermindPortal);
		temp.addProperty(MODEL_SEWERS_PORTAL, sewersPortal);
		temp.addProperty(MODEL_BANK_PORTAL, bankPortal);
		temp.addProperty(MODEL_ROOFTOPS_PORTAL, rooftopsPortal);
		temp.addProperty(MODEL_STREETS_PORTAL, streetsPortal);
		temp.addProperty(MODEL_BRIDGE_PORTAL, bridgePortal);
		str = sewers();
		if(str == null){
			str = NULL_STRING;
		}
		temp.addProperty(MODEL_SEWERS, str);
		str = bank();
		if(str == null){
			str = NULL_STRING;
		}
		temp.addProperty(MODEL_BANK, str);
		str = rooftops();
		if(str == null){
			str = NULL_STRING;
		}
		temp.addProperty(MODEL_ROOFTOPS, str);
		str = streets();
		if(str == null){
			str = NULL_STRING;
		}
		temp.addProperty(MODEL_STREETS, str);
		str = bridge();
		if(str == null){
			str = NULL_STRING;
		}
		temp.addProperty(MODEL_BRIDGE, str);
		
		return temp;
	}
}
