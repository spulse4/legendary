package shared.model;

import java.io.Serializable;

import shared.definitions.SharedVariables;

public class Message implements Serializable, SharedVariables{

	private static final long serialVersionUID = -4347604143057073127L;
	
	private int index;
	private String message;
	
	public Message(int index, String message){
		this.index = index;
		this.message = message;
	}
	
	public int getIndex(){
		return index;
	}
	
	public String getMessage(){
		return message;
	}
	
	public String getString(){
		return index + message + NEXT;
	}
}
