package shared.model;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.google.gson.JsonObject;

import server.definition.ServerCard;
import shared.definitions.SharedVariables;

public class KOPile implements Serializable, SharedVariables{
	
	private static final long serialVersionUID = -3880300932404448047L;
	private int shield = ZERO;
	private int spiderman = ZERO;
	private int deadpool = ZERO;
	private int avengers = ZERO;
	private int xMen = ZERO;
	private int wounds = ZERO;
	private int bystanders = ZERO;
	private String mostRecent = null;
	
	public KOPile(){}
	
	public void updateFromJson(JsonObject o){
		shield = o.get(CATEGORY_SHIELD).getAsInt();
		spiderman = o.get(CATEGORY_SPIDERMAN).getAsInt();
		deadpool = o.get(CATEGORY_DEADPOOL).getAsInt();
		avengers = o.get(CATEGORY_AVENGERS).getAsInt();
		xMen = o.get(CATEGORY_X_MEN).getAsInt();
		wounds = o.get(CATEGORY_WOUNDS).getAsInt();
		bystanders = o.get(CATEGORY_BYSTANDERS).getAsInt();
		mostRecent = o.get(CATEGORY_MOST_RECENT).getAsString();
		
		if(mostRecent.equals(NULL_STRING)){
			mostRecent = null;
		}
	}

	public int getShield() {
		return shield;
	}

	public int getSpiderman() {
		return spiderman;
	}

	public int getDeadpool() {
		return deadpool;
	}

	public int getAvengers() {
		return avengers;
	}

	public int getxMen() {
		return xMen;
	}

	public int getWounds() {
		return wounds;
	}

	public int getBystanders() {
		return bystanders;
	}

	public String getMostRecent() {
		return mostRecent;
	}
	
	public JPanel getPanel(final JDialog dialog){
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		JPanel namePanel = new JPanel();
		namePanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		JPanel shield = new JPanel();
		shield.setLayout(new BorderLayout());
		JPanel spider = new JPanel();
		spider.setLayout(new BorderLayout());
		JPanel deadpool = new JPanel();
		deadpool.setLayout(new BorderLayout());
		JPanel avengers = new JPanel();
		avengers.setLayout(new BorderLayout());
		JPanel xmen = new JPanel();
		xmen.setLayout(new BorderLayout());
		JPanel wounds = new JPanel();
		wounds.setLayout(new BorderLayout());
		JPanel bystanders = new JPanel();
		bystanders.setLayout(new BorderLayout());
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		Font font = FONT_PLAIN_20;

		JLabel name = new JLabel(TITLES_KO_PILE);
		name.setFont(font);
		namePanel.add(name);
		
		JLabel shield1 = new JLabel(KO_SHIELD);
		shield1.setPreferredSize(new Dimension(130, ZERO));
		shield1.setFont(font);
		JLabel shield2 = new JLabel(Integer.toString(this.shield));
		shield2.setFont(font);
		shield.add(shield1, BorderLayout.LINE_START);
		shield.add(shield2, BorderLayout.CENTER);

		JLabel spider1 = new JLabel(KO_SPIDERMAN);
		spider1.setPreferredSize(new Dimension(130, ZERO));
		spider1.setFont(font);
		JLabel spider2 = new JLabel(Integer.toString(this.spiderman));
		spider2.setFont(font);
		spider.add(spider1, BorderLayout.LINE_START);
		spider.add(spider2, BorderLayout.CENTER);

		JLabel deadpool1 = new JLabel(KO_DEADPOOL);
		deadpool1.setPreferredSize(new Dimension(130, ZERO));
		deadpool1.setFont(font);
		JLabel deadpool2 = new JLabel(Integer.toString(this.deadpool));
		deadpool2.setFont(font);
		deadpool.add(deadpool1, BorderLayout.LINE_START);
		deadpool.add(deadpool2, BorderLayout.CENTER);

		JLabel avengers1 = new JLabel(KO_AVENGERS);
		avengers1.setPreferredSize(new Dimension(130, ZERO));
		avengers1.setFont(font);
		JLabel avengers2 = new JLabel(Integer.toString(this.avengers));
		avengers2.setFont(font);
		avengers.add(avengers1, BorderLayout.LINE_START);
		avengers.add(avengers2, BorderLayout.CENTER);

		JLabel xmen1 = new JLabel(KO_X_MEN);
		xmen1.setPreferredSize(new Dimension(130, ZERO));
		xmen1.setFont(font);
		JLabel xmen2= new JLabel(Integer.toString(this.xMen));
		xmen2.setFont(font);
		xmen.add(xmen1, BorderLayout.LINE_START);
		xmen.add(xmen2, BorderLayout.CENTER);

		JLabel wounds1 = new JLabel(KO_WOUNDS);
		wounds1.setPreferredSize(new Dimension(130, ZERO));
		wounds1.setFont(font);
		JLabel wounds2 = new JLabel(Integer.toString(this.wounds));
		wounds2.setFont(font);
		wounds.add(wounds1, BorderLayout.LINE_START);
		wounds.add(wounds2, BorderLayout.CENTER);

		JLabel bystanders1 = new JLabel(KO_BYSTANDERS);
		bystanders1.setPreferredSize(new Dimension(130, ZERO));
		bystanders1.setFont(font);
		JLabel bystanders2 = new JLabel(Integer.toString(this.bystanders));
		bystanders2.setFont(font);
		bystanders.add(bystanders1, BorderLayout.LINE_START);
		bystanders.add(bystanders2, BorderLayout.CENTER);

		JButton button = new JButton(BUTTON_CANCEL);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				dialog.dispose();
			}
			
		});
		buttonPanel.add(button);
		
		panel.add(namePanel);
		panel.add(shield);
		panel.add(spider);
		panel.add(deadpool);
		panel.add(avengers);
		panel.add(xmen);
		panel.add(wounds);
		panel.add(bystanders);
		panel.add(buttonPanel);
		return panel;
	}
	
	public String KO(){
		StringBuilder str = new StringBuilder();
		str.append(shield);
		str.append(DELIMITER);
		str.append(spiderman);
		str.append(DELIMITER);
		str.append(deadpool);
		str.append(DELIMITER);
		str.append(avengers);
		str.append(DELIMITER);
		str.append(xMen);
		str.append(DELIMITER);
		str.append(wounds);
		str.append(DELIMITER);
		str.append(bystanders);
		str.append(DELIMITER);
		str.append(mostRecent);
		return str.toString();
	}

	public void setShield(int shield) {
		this.shield = shield;
	}

	public void setSpiderman(int spiderman) {
		this.spiderman = spiderman;
	}

	public void setDeadpool(int deadpool) {
		this.deadpool = deadpool;
	}

	public void setAvengers(int avengers) {
		this.avengers = avengers;
	}

	public void setxMen(int xMen) {
		this.xMen = xMen;
	}

	public void setWounds(int wounds) {
		this.wounds = wounds;
	}

	public void setBystanders(int bystanders) {
		this.bystanders = bystanders;
	}

	public void setMostRecent(String mostRecent) {
		this.mostRecent = mostRecent;
	}
	
	public void addCard(ServerCard card){
		switch(card.getCategory()){
		case CATEGORY_AVENGERS:
			++avengers;
			break;
		case CATEGORY_DEADPOOL:
			++deadpool;
			break;
		case NA:
			if(card.getName().equals(BYSTANDER)){
				++bystanders;
			}
			else{
				++wounds;
			}
			break;
		case CATEGORY_SHIELD:
			++shield;
			break;
		case CATEGORY_SPIDERMAN:
			++spiderman;
			break;
		case CATEGORY_X_MEN:
			++xMen;
			break;
		}
		mostRecent = card.getName();
	}
	
	public JsonObject getJson(){
		JsonObject temp = new JsonObject();
		String str = mostRecent;
		if(str == null){
			str = NULL_STRING;
		}
		temp.addProperty(CATEGORY_MOST_RECENT, str);
		temp.addProperty(CATEGORY_SHIELD, shield);
		temp.addProperty(CATEGORY_SPIDERMAN, spiderman);
		temp.addProperty(CATEGORY_DEADPOOL, deadpool);
		temp.addProperty(CATEGORY_AVENGERS, avengers);
		temp.addProperty(CATEGORY_X_MEN, xMen);
		temp.addProperty(CATEGORY_WOUNDS, wounds);
		temp.addProperty(CATEGORY_BYSTANDERS, bystanders);
		
		return temp;
	}
}
