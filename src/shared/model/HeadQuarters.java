package shared.model;

import java.io.Serializable;

import com.google.gson.JsonObject;

import shared.definitions.SharedVariables;

public class HeadQuarters implements Serializable, SharedVariables{
	
	private static final long serialVersionUID = -6085025265897621905L;
	private String HQ1 = null;
	private String HQ2 = null;
	private String HQ3 = null;
	private String HQ4 = null;
	private String HQ5 = null;
	
	public HeadQuarters(){}
	
	public void updateFromJson(JsonObject o){
		HQ1 = o.get(MODEL_HQ1).getAsString();
		HQ2 = o.get(MODEL_HQ2).getAsString();
		HQ3 = o.get(MODEL_HQ3).getAsString();
		HQ4 = o.get(MODEL_HQ4).getAsString();
		HQ5 = o.get(MODEL_HQ5).getAsString();

		if(HQ1.equals(NULL_STRING)){
			HQ1 = null;
		}
		if(HQ2.equals(NULL_STRING)){
			HQ2 = null;
		}
		if(HQ3.equals(NULL_STRING)){
			HQ3 = null;
		}
		if(HQ4.equals(NULL_STRING)){
			HQ4 = null;
		}
		if(HQ5.equals(NULL_STRING)){
			HQ5 = null;
		}
	}
	
	public String getHQ1(){
		return HQ1;
	}

	public String getHQ2(){
		return HQ2;
	}

	public String getHQ3(){
		return HQ3;
	}

	public String getHQ4(){
		return HQ4;
	}

	public String getHQ5(){
		return HQ5;
	}

	public void setHQ1(String hQ1) {
		HQ1 = hQ1;
	}

	public void setHQ2(String hQ2) {
		HQ2 = hQ2;
	}

	public void setHQ3(String hQ3) {
		HQ3 = hQ3;
	}

	public void setHQ4(String hQ4) {
		HQ4 = hQ4;
	}

	public void setHQ5(String hQ5) {
		HQ5 = hQ5;
	}
	
	public JsonObject getJson(){
		JsonObject temp = new JsonObject();
		String str = HQ1;
		if(str == null){
			str = NULL_STRING;
		}
		temp.addProperty(MODEL_HQ1, str);
		str = HQ2;
		if(str == null){
			str = NULL_STRING;
		}
		temp.addProperty(MODEL_HQ2, str);
		str = HQ3;
		if(str == null){
			str = NULL_STRING;
		}
		temp.addProperty(MODEL_HQ3, str);
		str = HQ4;
		if(str == null){
			str = NULL_STRING;
		}
		temp.addProperty(MODEL_HQ4, str);
		str = HQ5;
		if(str == null){
			str = NULL_STRING;
		}
		temp.addProperty(MODEL_HQ5, str);

		return temp;
	}
}
