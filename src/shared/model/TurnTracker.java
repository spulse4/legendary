package shared.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import shared.definitions.SharedVariables;
import shared.definitions.State;

public class TurnTracker implements Serializable, SharedVariables{

	private static final long serialVersionUID = -6507981174374316586L;
	private int currentPlayer = ZERO;
	private int nextHandCount = SIX;
	private int nextPlayer = NEGATIVE_ONE;
	private String scheme = null;
	private String mastermind = null;
	private String addToHand = null;
	private boolean finished = false;
	private List<String> heroes = null;
	private List<String> villians = null;
	private List<String> henchmen = null;
	private List<String> pass = null;
	private List<String> cardsInEffect = null;
	private List<String> pendingCards = null;
	private State state= null;
	
	public TurnTracker(){
		heroes = new ArrayList<String>();
		villians = new ArrayList<String>();
		henchmen = new ArrayList<String>();
		cardsInEffect = new ArrayList<String>();
		pendingCards = new ArrayList<String>();
		state = State.PLAYING;
	}
	
	@SuppressWarnings("unchecked")
	public void updateFromJson(JsonObject model){
		JsonObject o = model.get(MODEL_TURN_TRACKER).getAsJsonObject();
		Gson gson = new Gson();
		currentPlayer = o.get(MODEL_CURRENT_PLAYER).getAsInt();
		
		scheme = o.get(MODEL_SCHEME).getAsString();
		mastermind = o.get(MODEL_MASTERMIND).getAsString();

		if(scheme.equals(NULL_STRING)){
			scheme = null;
		}
		if(mastermind.equals(NULL_STRING)){
			mastermind = null;
		}
		
		finished = o.get(MODEL_FINISHED).getAsBoolean();
		heroes.clear();
		villians.clear();
		henchmen.clear();
		
		List<Object> temp = gson.fromJson(o.get(MODEL_HEROES).getAsString(), List.class);
		for(Object obj : temp){
			heroes.add((String)obj);
		}
		temp = gson.fromJson(o.get(MODEL_VILLIANS).getAsString(), List.class);
		for(Object obj : temp){
			villians.add((String)obj);
		}
		temp = gson.fromJson(o.get(MODEL_HENCHMEN).getAsString(), List.class);
		for(Object obj : temp){
			henchmen.add((String)obj);
		}
		temp = gson.fromJson(o.get(MODEL_CARDS_IN_EFFECT).getAsString(), List.class);
		for(Object obj : temp){
			cardsInEffect.add((String)obj);
		}
		
		state = State.fromString(model.get(MODEL_GAME_ID).getAsInt(), o.get(MODEL_STATE).getAsString());
	}

	public int getCurrentPlayer() {
		return currentPlayer;
	}
	
	public void setCurrentPlayer(int currentPlayer){
		this.currentPlayer = currentPlayer;
	}

	public String getScheme() {
		return scheme;
	}
	
	public void setScheme(String scheme){
		this.scheme = scheme;
	}

	public String getMastermind() {
		return mastermind;
	}
	
	public void setMastermind(String mastermind){
		this.mastermind = mastermind;
	}

	public boolean isFinished() {
		return finished;
	}
	
	public void setFinished(boolean finished){
		this.finished = finished;
	}

	public List<String> getHeroes() {
		return heroes;
	}
	
	public void setHeroes(List<String> heroes){
		this.heroes = heroes;
	}

	public List<String> getVillians() {
		return villians;
	}
	
	public void setVillians(List<String> villians){
		this.villians = villians;
	}

	public List<String> getHenchmen() {
		return henchmen;
	}
	
	public void setHenchmen(List<String> henchmen){
		this.henchmen = henchmen;
	}

	public State getState() {
		return state;
	}
	
	public void setState(State state){
		if(this.state != State.LOST && this.state != State.WON){
			this.state = state;
		}
	}
	
	public int getNextHandCount(){
		return nextHandCount;
	}
	
	public void setNextHandCount(int nextHandCount){
		this.nextHandCount = nextHandCount;
	}
	
	public void setPass(String card, int index, int totalSize){
		if(pass == null){
			pass = new ArrayList<String>();
			for(int i = ZERO; i < totalSize; ++i){
				pass.add(EMPTY);
			}
		}
		pass.set(index, card);
		for(String str : pass){
			if(str.equals(EMPTY)){
				return;
			}
		}
		state = State.PLAYING;
	}
	
	public List<String> getPass(){
		return pass;
	}
	
	public void nullPass(){
		pass = null;
	}
	
	public void setAddToHand(String addToHand){
		this.addToHand = addToHand;
	}
	public String getAddToHand(){
		return addToHand;
	}
	
	public void setNextPlayer(int nextPlayer){
		this.nextPlayer = nextPlayer;
	}
	public int getNextPlayer(){
		return nextPlayer;
	}
	public List<String> getCardsInEffect(){
		return cardsInEffect;
	}
	public void resetCardsInEffect(){
		cardsInEffect = new ArrayList<String>();
	}
	public void addToCardsInEffect(String card){
		cardsInEffect.add(card);
	}
	
	public void setPendingCards(List<String> pendingCards){
		this.pendingCards = pendingCards;
	}
	public List<String> getPendingCards(){
		return pendingCards;
	}
	
	public JsonObject getJson(int gameID){
		Gson gson = new Gson();
		JsonObject temp = new JsonObject();
		temp.addProperty(MODEL_CURRENT_PLAYER, currentPlayer);
		temp.addProperty(MODEL_FINISHED, finished);
		temp.addProperty(MODEL_STATE, state.getString(gameID));
		String str = scheme;
		if(str == null){
			str = NULL_STRING;
		}
		temp.addProperty(MODEL_SCHEME, str);
		str = mastermind;
		if(str == null){
			str = NULL_STRING;
		}
		temp.addProperty(MODEL_MASTERMIND, str);
		temp.addProperty(MODEL_HEROES, gson.toJson(heroes));
		temp.addProperty(MODEL_VILLIANS, gson.toJson(villians));
		temp.addProperty(MODEL_HENCHMEN, gson.toJson(henchmen));
		temp.addProperty(MODEL_CARDS_IN_EFFECT, gson.toJson(cardsInEffect));

		return temp;
	}
}
