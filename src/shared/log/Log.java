package shared.log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;

import shared.definitions.SharedVariables;

public class Log implements SharedVariables{
	
	private Log(){}
	
	public static void logError(int gameID, String message){
		try{
			String fileName = LOCATION_LOG_INFO_1 + gameID + LOCATION_LOG_INFO_2;
			File file = new File(fileName);
			if(!file.exists()){
				Files.createFile(file.toPath());
			}
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileName, true));
			bw.write(message + DOUBLE_NEXT_LINE);
			bw.flush();
			bw.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public static void emptyAll(){
		try{
			File top = new File(LOCATION_LOG);
			if(!top.exists()){
				Files.createDirectories(top.toPath());
			}
			for(File inner : top.listFiles()){
				Files.delete(inner.toPath());
			}
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
}
