package shared.server;

import java.util.List;

import com.google.gson.JsonObject;

import shared.definitions.GameHeader;

public interface IServer{
	
	//ability
	public void battlefieldPromotionAbility(int gameID, String card);
	public void copyPowersAbility(int gameID, String card);
	public void coveringFireAbility(int gameID, boolean draw);
	public void coveringFireDiscard(int gameID, int playerIndex, String card);
	public void cyclopsAbility(int gameID, String card);
	public void dangerousRescueAbility(int gameID, String card);
	public void energyDrainAbility(int gameID, String card);
	public void healingFactorAbility(int gameID, boolean accept);
	public void hereHoldThisForASecondAbility(int gameID, int location);
	public void heyCanIGetADoOverAbility(int gameID, boolean accept);
	public void hypnoticCharmAbility(int gameID, int playerIndex, boolean discard);
	public void pureFuryAbility(int gameID, int cardIndex);
	public void randomActsOfUnkindnessAbility(int gameID, boolean accept);
	public void randomActsOfUnkindnessPass(int gameID, int playerIndex, String card);
	public void shadowedThoughtsAbility(int gameID, boolean accept);
	public void silentSniperAbility(int gameID, int location);
	public void spinningCycloneAbility(int gameID, int startingLocation, int endingLocation);
	public void stackTheDeckAbility(int gameID, String card);
	public void theAmazingSpidermanAbility(int gameID, String card);
	public void unstoppableHulkAbility(int gameID, boolean accept);
	//ambush
	public void juggernautAmbush(int gameID, int playerIndex, String card);
	//chat
	public void sendChat(int gameID, int playerIndex, String message);
	//escape
	public void destroyerEscape(int gameID, int playerIndex, String card);
	public void juggernautEscape(int gameID, int playerIndex, String card);
	//fight
	public void bitterCaptorFight(int gameID, int location);
	public void cruelRulerFight(int gameID, int location);
	public void darkTechnologyFight(int gameID, int location);
	public void doombotLegionFight(int gameID, String card);
	public void electromagneticBubbleFight(int gameID, String card);
	public void hydraKidnappersFight(int gameID, boolean accept);
	public void maestroFight(int gameID, String card);
	public void maniacalTyrantFight(int gameID, String card);
	public void melterFight(int gameID, int playerIndex, boolean ko);
	public void monarchsDecreeFight(int gameID, boolean draw);
	public void monarchsDecreeDiscard(int gameID, int playerIndex, String card);
	public void paibokThePowerSkrullFight(int gameID, int playerIndex, int location);
	public void ruthlessDictatorDiscard(int gameID, String card);
	public void ruthlessDictatorKO(int gameID, String card);
	public void sentinelFight(int gameID, String card);
	public void superSkrullFight(int gameID, int playerIndex, String card);
	public void vanishingIllusionsFight(int gameID, int playerIndex, String card);
	public void whirlwindFight(int gameID, String card);
	public void ymirFrostGiantKingFight(int gameID, int playerIndex);
	public void ymirFrostGiantKingDiscard(int gameID, int playerIndex, int number);
	//game
	public int createGame(String title);
	public List<GameHeader> getGames();
	public JsonObject getModel(int gameID, int versionID);
	public int joinGame(int gameID, int playerID, String playerName);
	public void setHeroes(int gameID, List<String> heroes);
	public void setMastermind(int gameID, String mastermind);
	public void setScheme(int gameID, String scheme);
	public void setVillians(int gameID, List<String> villians);
	public void startGame(int gameID);
	//masterstrike
	public void drDoomMasterstrike(int gameID, int playerIndex, String card);
	public void magnetoMasterstrike(int gameID, int playerIndex, String card);
	public void redSkullMasterstrike(int gameID, int playerIndex, String card);
	//moves
	public void attackCard(int gameID, int cardIndex);
	public void buyCard(int gameID, int playerIndex, int cardIndex);
	public void endTrun(int gameID, int playerIndex);
	public void playCard(int gameID, String name);
	//users
	public int login(String username, String password);
	public int register(String username, String password);
}
