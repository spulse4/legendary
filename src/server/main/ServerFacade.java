package server.main;

import java.util.List;

import com.google.gson.JsonObject;

import server.action.ability.*;
import server.action.ambush.*;
import server.action.chat.*;
import server.action.escape.*;
import server.action.fight.*;
import server.action.general.*;
import server.action.masterstrike.*;
import server.dao.DAOFacade;
import shared.definitions.GameHeader;
import shared.model.ModelFacade;
import shared.server.IServer;

public class ServerFacade implements IServer{

	private static ServerFacade instance = null;
	private DAOFacade facade;
	
	public static ServerFacade getInstance(){
		if(instance == null){
			instance = new ServerFacade();
		}
		return instance;
	}
	
	private ServerFacade(){
		facade = DAOFacade.getInstance();
	}
	//ability-------------------------------------------------------------------
	public void battlefieldPromotionAbility(int gameID, String card){
		ModelFacade model = facade.readModel(gameID);
		new BattlefieldPromotionAbility(facade).go(model, gameID, card);
		facade.updateModel(model);
	}
	public void copyPowersAbility(int gameID, String card){
		ModelFacade model = facade.readModel(gameID);
		new CopyPowersAbility(facade).go(model, gameID, card);
		facade.updateModel(model);
	}
	public void coveringFireAbility(int gameID, boolean draw){
		ModelFacade model = facade.readModel(gameID);
		new CoveringFireAbility().go(model, gameID, draw);
		facade.updateModel(model);
	}
	public void coveringFireDiscard(int gameID, int playerIndex, String card){
		ModelFacade model = facade.readModel(gameID);
		new CoveringFireDiscard().go(model, gameID, playerIndex, card);
		facade.updateModel(model);
	}
	public void cyclopsAbility(int gameID, String card){
		ModelFacade model = facade.readModel(gameID);
		new CyclopsAbility().go(model, gameID, card);
		facade.updateModel(model);
	}
	public void dangerousRescueAbility(int gameID, String card){
		ModelFacade model = facade.readModel(gameID);
		new DangerousRescueAbility().go(model, gameID, card);
		facade.updateModel(model);
	}
	public void energyDrainAbility(int gameID, String card){
		ModelFacade model = facade.readModel(gameID);
		new EnergyDrainAbility(facade).go(model, gameID, card);
		facade.updateModel(model);
	}
	public void healingFactorAbility(int gameID, boolean accept){
		ModelFacade model = facade.readModel(gameID);
		new HealingFactorAbility(facade).go(model, gameID, accept);
		facade.updateModel(model);
	}
	public void hereHoldThisForASecondAbility(int gameID, int location){
		ModelFacade model = facade.readModel(gameID);
		new HereHoldThisForASecondAbility().go(model, gameID, location);
		facade.updateModel(model);
	}
	public void heyCanIGetADoOverAbility(int gameID, boolean accept){
		ModelFacade model = facade.readModel(gameID);
		new HeyCanIGetADoOverAbility().go(model, gameID, accept);
		facade.updateModel(model);
	}
	public void hypnoticCharmAbility(int gameID, int playerIndex, boolean discard){
		ModelFacade model = facade.readModel(gameID);
		new HypnoticCharmAbility().go(model, gameID, playerIndex, discard);
		facade.updateModel(model);
	}
	public void pureFuryAbility(int gameID, int cardIndex){
		ModelFacade model = facade.readModel(gameID);
		new PureFuryAbility(facade).go(model, gameID, cardIndex);
		facade.updateModel(model);
	}
	public void randomActsOfUnkindnessAbility(int gameID, boolean accept){
		ModelFacade model = facade.readModel(gameID);
		new RandomActsOfUnkindnessAbility().go(model, gameID, accept);
		facade.updateModel(model);
	}
	public void randomActsOfUnkindnessPass(int gameID, int playerIndex, String card){
		ModelFacade model = facade.readModel(gameID);
		new RandomActsOfUnkindnessPass().go(model, gameID, playerIndex, card);
		facade.updateModel(model);
	}
	public void shadowedThoughtsAbility(int gameID, boolean accept){
		ModelFacade model = facade.readModel(gameID);
		new ShadowedThoughtsAbility(facade).go(model, gameID, accept);
		facade.updateModel(model);
	}
	public void silentSniperAbility(int gameID, int location){
		ModelFacade model = facade.readModel(gameID);
		new SilentSniperAbility(facade).go(model, gameID, location);
		facade.updateModel(model);
	}
	public void spinningCycloneAbility(int gameID, int startingLocation, int endingLocation){
		ModelFacade model = facade.readModel(gameID);
		new SpinningCycloneAbility().go(model, gameID, startingLocation, endingLocation);
		facade.updateModel(model);
	}
	public void stackTheDeckAbility(int gameID, String card){
		ModelFacade model = facade.readModel(gameID);
		new StackTheDeckAbility().go(model, gameID, card);
		facade.updateModel(model);
	}
	public void theAmazingSpidermanAbility(int gameID, String card){
		ModelFacade model = facade.readModel(gameID);
		new TheAmazingSpidermanAbility().go(model, gameID, card);
		facade.updateModel(model);
	}
	public void unstoppableHulkAbility(int gameID, boolean accept){
		ModelFacade model = facade.readModel(gameID);
		new UnstoppableHulkAbility(facade).go(model, gameID, accept);
		facade.updateModel(model);
	}
	//ambush--------------------------------------------------------------------
	public void juggernautAmbush(int gameID, int playerIndex, String card){
		ModelFacade model = facade.readModel(gameID);
		new JuggernautAmbush(facade).go(model, gameID, playerIndex, card);
		facade.updateModel(model);
	}
	//chat----------------------------------------------------------------------
	public void sendChat(int gameID, int playerIndex, String message){
		ModelFacade model = facade.readModel(gameID);
		new SendChat().go(model, playerIndex, message);
		facade.updateModel(model);
	}
	//escape--------------------------------------------------------------------
	public void destroyerEscape(int gameID, int playerIndex, String card){
		ModelFacade model = facade.readModel(gameID);
		new DestroyerEscape(facade).go(model, gameID, playerIndex, card);
		facade.updateModel(model);
	}
	public void juggernautEscape(int gameID, int playerIndex, String card){
		ModelFacade model = facade.readModel(gameID);
		new JuggernautEscape(facade).go(model, gameID, playerIndex, card);
		facade.updateModel(model);
	}
	//fight---------------------------------------------------------------------
	public void bitterCaptorFight(int gameID, int location){
		ModelFacade model = facade.readModel(gameID);
		new BitterCaptorFight(facade).go(model, gameID, location);
		facade.updateModel(model);
	}
	public void cruelRulerFight(int gameID, int location){
		ModelFacade model = facade.readModel(gameID);
		new CruelRulerFight(facade).go(model, gameID, location);
		facade.updateModel(model);
	}
	public void darkTechnologyFight(int gameID, int location){
		ModelFacade model = facade.readModel(gameID);
		new DarkTechnologyFight(facade).go(model, gameID, location);
		facade.updateModel(model);
	}
	public void doombotLegionFight(int gameID, String card){
		ModelFacade model = facade.readModel(gameID);
		new DoombotLegionFight(facade).go(model, gameID, card);
		facade.updateModel(model);
	}
	public void electromagneticBubbleFight(int gameID, String card){
		ModelFacade model = facade.readModel(gameID);
		new ElectromagneticBubbleFight().go(model, gameID, card);
		facade.updateModel(model);
	}
	public void hydraKidnappersFight(int gameID, boolean accept){
		ModelFacade model = facade.readModel(gameID);
		new HydraKidnappersFight().go(model, gameID, accept);
		facade.updateModel(model);
	}
	public void maestroFight(int gameID, String card){
		ModelFacade model = facade.readModel(gameID);
		new MaestroFight(facade).go(model, gameID, card);
		facade.updateModel(model);
	}
	public void maniacalTyrantFight(int gameID, String card){
		ModelFacade model = facade.readModel(gameID);
		new ManiacalTyrantFight(facade).go(model, gameID, card);
		facade.updateModel(model);
	}
	public void melterFight(int gameID, int playerIndex, boolean ko){
		ModelFacade model = facade.readModel(gameID);
		new MelterFight(facade).go(model, gameID, playerIndex, ko);
		facade.updateModel(model);
	}
	public void monarchsDecreeFight(int gameID, boolean draw){
		ModelFacade model = facade.readModel(gameID);
		new MonarchsDecreeFight().go(model, gameID, draw);
		facade.updateModel(model);
	}
	public void monarchsDecreeDiscard(int gameID, int playerIndex, String card){
		ModelFacade model = facade.readModel(gameID);
		new MonarchsDecreeDiscard().go(model, gameID, playerIndex, card);
		facade.updateModel(model);
	}
	public void paibokThePowerSkrullFight(int gameID, int playerIndex, int location){
		ModelFacade model = facade.readModel(gameID);
		new PaibokThePowerSkrullFight(facade).go(model, gameID, playerIndex, location);
		facade.updateModel(model);
	}
	public void ruthlessDictatorDiscard(int gameID, String card){
		ModelFacade model = facade.readModel(gameID);
		new RuthlessDictatorDiscard().go(model, gameID, card);
		facade.updateModel(model);
	}
	public void ruthlessDictatorKO(int gameID, String card){
		ModelFacade model = facade.readModel(gameID);
		new RuthlessDictatorKO(facade).go(model, gameID, card);
		facade.updateModel(model);
	}
	public void sentinelFight(int gameID, String card){
		ModelFacade model = facade.readModel(gameID);
		new SentinelFight(facade).go(model, gameID, card);
		facade.updateModel(model);
	}
	public void superSkrullFight(int gameID, int playerIndex, String card){
		ModelFacade model = facade.readModel(gameID);
		new SuperSkrullFight(facade).go(model, gameID, playerIndex, card);
		facade.updateModel(model);
	}
	public void vanishingIllusionsFight(int gameID, int playerIndex, String card){
		ModelFacade model = facade.readModel(gameID);
		new VanishingIllusionsFight(facade).go(model, gameID, playerIndex, card);
		facade.updateModel(model);
	}
	public void whirlwindFight(int gameID, String card){
		ModelFacade model = facade.readModel(gameID);
		new WhirlwindFight(facade).go(model, gameID, card);
		facade.updateModel(model);
	}
	public void ymirFrostGiantKingFight(int gameID, int playerIndex){
		ModelFacade model = facade.readModel(gameID);
		new YmirFrostGiantKingFight().go(model, gameID, playerIndex);
		facade.updateModel(model);
	}
	public void ymirFrostGiantKingDiscard(int gameID, int playerIndex, int number){
		ModelFacade model = facade.readModel(gameID);
		new YmirFrostGiantKingDiscard(facade).go(model, gameID, playerIndex, number);
		facade.updateModel(model);
	}
	//game----------------------------------------------------------------------
	@Override
	public int createGame(String title) {
		int id = new CreateGame(facade).go(title);
		return id;
	}
	@Override
	public List<GameHeader> getGames() {
		List<GameHeader> games = new GetGames(facade).go();
		return games;
	}
	@Override
	public JsonObject getModel(int gameID, int versionID) {
		JsonObject model = new GetModel(facade).go(gameID, versionID);
		return model;
	}
	@Override
	public int joinGame(int gameID, int playerID, String playerName) {
		int index = new JoinGame(facade).go(gameID, playerID, playerName);
		return index;
	}
	@Override
	public void setHeroes(int gameID, List<String> heroes) {
		ModelFacade model = facade.readModel(gameID);
		new SetHeroes().go(model, heroes);
		facade.updateModel(model);
	}
	@Override
	public void setMastermind(int gameID, String mastermind) {
		ModelFacade model = facade.readModel(gameID);
		new SetMastermind().go(model, mastermind);
		facade.updateModel(model);
	}
	@Override
	public void setScheme(int gameID, String scheme) {
		ModelFacade model = facade.readModel(gameID);
		new SetScheme().go(model, scheme);
		facade.updateModel(model);
	}
	@Override
	public void setVillians(int gameID, List<String> villians) {
		ModelFacade model = facade.readModel(gameID);
		new SetVillians().go(model, villians);
		facade.updateModel(model);
	}
	@Override
	public void startGame(int gameID) {
		ModelFacade model = facade.readModel(gameID);
		new StartGame(facade).go(model);
		facade.updateModel(model);
	}
	//masterstrike--------------------------------------------------------------
	public void drDoomMasterstrike(int gameID, int playerIndex, String card){
		ModelFacade model = facade.readModel(gameID);
		new DrDoomMasterstrike().go(model, gameID, playerIndex, card);
		facade.updateModel(model);
	}
	public void magnetoMasterstrike(int gameID, int playerIndex, String card){
		ModelFacade model = facade.readModel(gameID);
		new MagnetoMasterstrike().go(model, gameID, playerIndex, card);
		facade.updateModel(model);
	}
	public void redSkullMasterstrike(int gameID, int playerIndex, String card){
		ModelFacade model = facade.readModel(gameID);
		new RedSkullMasterstrike(facade).go(model, gameID, playerIndex, card);
		facade.updateModel(model);
	}
	//moves---------------------------------------------------------------------
	@Override
	public void attackCard(int gameID, int cardIndex) {
		ModelFacade model = facade.readModel(gameID);
		new AttackCard(facade).go(model, cardIndex);
		facade.updateModel(model);
	}
	@Override
	public void buyCard(int gameID, int playerIndex, int cardIndex) {
		ModelFacade model = facade.readModel(gameID);
		new BuyCard(facade).go(model, playerIndex, cardIndex, false);
		facade.updateModel(model);
	}
	@Override
	public void endTrun(int gameID, int playerIndex) {
		ModelFacade model = facade.readModel(gameID);
		new EndTurn(facade).go(model, playerIndex);
		facade.updateModel(model);
	}
	@Override
	public void playCard(int gameID, String name) {
		ModelFacade model = facade.readModel(gameID);
		new PlayCard(facade).go(model, name);
		facade.updateModel(model);
	}
	//users---------------------------------------------------------------------
	@Override
	public int login(String username, String password) {
		int id = new Login(facade).go(username, password);
		return id;
	}
	@Override
	public int register(String username, String password) {
		int id = new Register(facade).go(username, password);
		return id;
	}
}
