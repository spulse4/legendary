package server.main;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import server.dao.CardDAO;
import server.dao.DAOFacade;
import server.definition.ServerVariables;
import shared.definitions.GameHeader;
import shared.definitions.PlayerHeader;
import shared.definitions.UserHeader;
import shared.log.Log;
import shared.model.ModelFacade;
import shared.model.TurnTracker;
import server.handlers.ability.*;
import server.handlers.ambush.*;
import server.handlers.chat.*;
import server.handlers.escape.*;
import server.handlers.fight.*;
import server.handlers.game.*;
import server.handlers.masterstrike.*;
import server.handlers.moves.*;
import server.handlers.test.*;
import server.handlers.users.*;

public class Server implements ServerVariables{
	
	private final static String CLASS = "server.main.Server.java: ";
	
	private int SERVER_PORT_NUMBER;
	
	private HttpServer server;
	String persistenceType;
	
	//ability
	private HttpHandler battlefieldPromotionAbilityHandler = new BattlefieldPromotionAbilityHandler();
	private HttpHandler copyPowersAbilityHandler = new CopyPowersAbilityHandler();
	private HttpHandler coveringFireAbilityHandler = new CoveringFireAbilityHandler();
	private HttpHandler coveringFireDiscardHandler = new CoveringFireDiscardHandler();
	private HttpHandler cyclopsAbilityHandler = new CyclopsAbilityHandler();
	private HttpHandler dangerousRescueAbilityHandler = new DangerousRescueAbilityHandler();
	private HttpHandler energyDrainAbilityHandler = new EnergyDrainAbilityHandler();
	private HttpHandler healingFactorAbilityHandler = new HealingFactorAbilityHandler();
	private HttpHandler hereHoldThisForASecondAbilityHandler = new HereHoldThisForASecondAbilityHandler();
	private HttpHandler heyCanIGetADoOverAbilityHandler = new HeyCanIGetADoOverAbilityHandler();
	private HttpHandler hypnoticCharmAbilityHandler = new HypnoticCharmAbilityHandler();
	private HttpHandler pureFuryAbilityHandler = new PureFuryAbilityHandler();
	private HttpHandler randomActsOfUnkindnessAbilityHandler = new RandomActsOfUnkindnessAbilityHandler();
	private HttpHandler randomActsOfUnkindnessPassHandler = new RandomActsOfUnkindnessPassHandler();
	private HttpHandler shadowedThoughtsAbilityHandler = new ShadowedThoughtsAbilityHandler();
	private HttpHandler silentSniperAbilityHandler = new SilentSniperAbilityHandler();
	private HttpHandler spinningCycloneAbilityHandler = new SpinningCycloneAbilityHandler();
	private HttpHandler stackTheDeckAbilityHandler = new StackTheDeckAbilityHandler();
	private HttpHandler theAmazingSpidermanAbilityHandler = new TheAmazingSpidermanAbilityHandler();
	private HttpHandler unstoppableHulkAbilityHandler = new UnstoppableHulkAbilityHandler();
	
	//ambush
	private HttpHandler juggernautAmbushHandler = new JuggernautAmbushHandler();
	
	//chat
	private HttpHandler sendChatHandler = new SendChatHandler();
	
	//escape
	private HttpHandler destroyerEscapeHandler = new DestroyerEscapeHandler();
	private HttpHandler juggernautEscapeHandler = new JuggernautEscapeHandler();

	//fight
	private HttpHandler bitterCaptorFightHandler = new BitterCaptorFightHandler();
	private HttpHandler cruelRulerFightHandler = new CruelRulerFightHandler();
	private HttpHandler darkTechnologyFightHandler = new DarkTechnologyFightHandler();
	private HttpHandler doombotLegionFightHandler = new DoombotLegionFightHandler();
	private HttpHandler electromagneticBubbleFightHandler = new ElectromagneticBubbleFightHandler();
	private HttpHandler hydraKidnappersFightHandler = new HydraKidnappersFightHandler();
	private HttpHandler maestroFightHandler = new MaestroFightHandler();
	private HttpHandler maniacalTyrantFightHandler = new ManiacalTyrantFightHandler();
	private HttpHandler melterFightHandler = new MelterFightHandler();
	private HttpHandler monarchsDecreeFightHandler = new MonarchsDecreeFightHandler();
	private HttpHandler monarchsDecreeDiscardHandler = new MonarchsDecreeDiscardHandler();
	private HttpHandler paibokThePowerSkrullFightHandler = new PaibokThePowerSkrullFightHandler();
	private HttpHandler ruthlessDictatorDiscardHandler = new RuthlessDictatorDiscardHandler();
	private HttpHandler ruthlessDictatorKOHandler = new RuthlessDictatorKOHandler();
	private HttpHandler sentinelFightHandler = new SentinelFightHandler();
	private HttpHandler superSkrullFightHandler = new SuperSkrullFightHandler();
	private HttpHandler vanishingIllusionsFightHandler = new VanishingIllusionsFightHandler();
	private HttpHandler whirlwindFightHandler = new WhirlwindFightHandler();	
	private HttpHandler ymirFrostGiantKingFightHandler = new YmirFrostGiantKingFightHandler();
	private HttpHandler ymirFrostGiantKingDiscardHandler = new YmirFrostGiantKingDiscardHandler();

	//game
	private HttpHandler createGameHandler = new CreateGameHandler();
	private HttpHandler getGamesHandler = new GetGamesHandler();
	private HttpHandler getModelHandler = new GetModelHandler();
	private HttpHandler joinGameHandler = new JoinGameHandler();
	private HttpHandler setHeroesHandler = new SetHeroesHandler();
	private HttpHandler setMastermindHandler = new SetMastermindHandler();
	private HttpHandler setSchemeHandler = new SetSchemeHandler();
	private HttpHandler setVilliansHandler = new SetVilliansHandler();
	private HttpHandler startHandler = new StartHandler();
	
	//masterstrike
	private HttpHandler drDoomMasterstrikeHandler = new DrDoomMasterstrikeHandler();
	private HttpHandler magnetoMasterstrikeHandler = new MagnetoMasterstrikeHandler();
	private HttpHandler redSkullMasterstrikeHandler = new RedSkullMasterstrikeHandler();

	//moves
	private HttpHandler attackCardHandler = new AttackCardHandler();
	private HttpHandler buyCardHandler = new BuyCardHandler();
	private HttpHandler endTurnHandler = new EndTurnHandler();
	private HttpHandler playCardHandler = new PlayCardHandler();
	
	//test
	private HttpHandler getLogHandler = new GetLogHandler();
	private HttpHandler setHeroesTestHandler = new SetHeroesTestHandler();
	private HttpHandler setVillianTestHandler = new SetVillianTestHandler();
	private HttpHandler testHandler = new TestHandler();
	private HttpHandler test = new Test();
	
	//users
	private HttpHandler loginHandler = new LoginHandler();
	private HttpHandler registerHandler = new RegisterHandler();
	
	
	public static void main(String[] args) {
		Log.emptyAll();
				
		DAOFacade facade = DAOFacade.getInstance();	
		facade.deleteAll();
		new CardDAO().fill();
		initializeGame(facade);

		Server server = new Server();
		if (args.length == ONE) {
			server.initPortNum(Integer.parseInt(args[ZERO]));
		}
		else {
			server.initPortNum(PORT);
		}
		server.run();
		System.out.println(FINISHED);
	}

	private static void initializeGame(DAOFacade facade){
		UserHeader user = facade.addUser(EMPTY, EMPTY);
		
		int gameID = facade.createGame(EMPTY);
		
		GameHeader game = new GameHeader(gameID, false, null, new ArrayList<PlayerHeader>(), false);
		facade.addPlayer(game, user);
		
		ModelFacade model = facade.readModel(gameID);
		TurnTracker t = model.getTurnTracker();
		t.setMastermind(DR_DOOM);
		//MIDTOWN_BANK_ROBBERY
		//NEGATIVE_ZONE_PRISON_BREAKOUT
		//PORTALS_TO_THE_DARK_DIMENSION
		//REPLACE_EARTHS_LEADERS_WITH_KILLBOTS
		//SECRET_INVASION_OF_THE_SKRULL_SHAPESHIFTERS
		//SUPER_HERO_CIVIL_WAR
		//THE_LEGACY_VIRUS
		//UNLEASH_THE_POWER_OF_THE_COSMIC_CUBE
		
		t.setScheme(MIDTOWN_BANK_ROBBERY);

		List<String> cards = new ArrayList<String>();
		cards.add(HAND_NINJAS);
		t.setHenchmen(cards);
		
		cards = new ArrayList<String>();
		cards.add(CAPTAIN_AMERICA);
		cards.add(CYCLOPS);
		t.setHeroes(cards);
		
		cards = new ArrayList<String>();
		cards.add(CATEGORY_RADIATION);
		cards.add(CATEGORY_SKRULLS);
		cards.add(CATEGORY_BROTHERHOOD);
		cards.add(CATEGORY_ENEMIES_OF_ASGARD);
		t.setVillians(cards);
		
		facade.updateModel(model);
		
		ServerFacade.getInstance().startGame(gameID);
		System.out.println(CLASS + SERVER_DONE_SETTING_UP);
	}

	public Server() {}
	
	public void run() {
		try {
			server = HttpServer.create(new InetSocketAddress(SERVER_PORT_NUMBER),
					SERVER_MAX_WAITING_CONNECTIONS);
		} 
		catch (IOException e) {
			e.printStackTrace();
			return;
		}

		server.setExecutor(java.util.concurrent.Executors.newCachedThreadPool());
		
		ability();
		ambush();
		chat();
		escape();
		fight();
		game();
		masterstrike();
		moves();
		test();
		users();
		
		server.start();
	}
			
	private void initPortNum(int port) {
		SERVER_PORT_NUMBER = port;
	}
	
	private void ability(){
		server.createContext(ABILITY_BATTLEFIELD_PROMOTION_ABILITY, battlefieldPromotionAbilityHandler);
		server.createContext(ABILITY_COPY_POWERS_ABILITY, copyPowersAbilityHandler);
		server.createContext(ABILITY_COVERING_FIRE_ABILITY, coveringFireAbilityHandler);
		server.createContext(ABILITY_COVERING_FIRE_DISCARD, coveringFireDiscardHandler);
		server.createContext(ABILITY_CYCLOPS_ABILITY, cyclopsAbilityHandler);
		server.createContext(ABILITY_DANGEROUS_RESCUE_ABILITY, dangerousRescueAbilityHandler);
		server.createContext(ABILITY_ENERGY_DRAIN_ABILITY, energyDrainAbilityHandler);
		server.createContext(ABILITY_HEALING_FACTOR_ABILITY, healingFactorAbilityHandler);
		server.createContext(ABILITY_HERE_HOLD_THIS_FOR_A_SECOND_ABILITY, hereHoldThisForASecondAbilityHandler);
		server.createContext(ABILITY_HEY_CAN_I_GET_A_DO_OVER_ABILITY, heyCanIGetADoOverAbilityHandler);
		server.createContext(ABILITY_HYPNOTIC_CHARM_ABILITY, hypnoticCharmAbilityHandler);
		server.createContext(ABILITY_PURE_FURY_ABILITY, pureFuryAbilityHandler);
		server.createContext(ABILITY_RANDOM_ACTS_OF_UNKINDNESS_ABILITY, randomActsOfUnkindnessAbilityHandler);
		server.createContext(ABILITY_RANDOM_ACTS_OF_UNKINDNESS_PASS, randomActsOfUnkindnessPassHandler);
		server.createContext(ABILITY_SHADOWED_THOUGHTS_ABILITY, shadowedThoughtsAbilityHandler);
		server.createContext(ABILITY_SILENT_SNIPER_ABILITY, silentSniperAbilityHandler);
		server.createContext(ABILITY_SPINNING_CYCLONE_ABILITY, spinningCycloneAbilityHandler);
		server.createContext(ABILITY_STACK_THE_DECK_ABILITY, stackTheDeckAbilityHandler);
		server.createContext(ABILITY_THE_AMAZING_SPIDERMAN_ABILITY, theAmazingSpidermanAbilityHandler);
		server.createContext(ABILITY_UNSTOPPABLE_HULK_ABILITY, unstoppableHulkAbilityHandler);
	}
	private void ambush(){
		server.createContext(AMBUSH_JUGGERNAUT_AMBUSH, juggernautAmbushHandler);
	}
	private void chat(){
		server.createContext(CHAT_SEND_CHAT, sendChatHandler);
	}
	private void escape(){
		server.createContext(ESCAPE_DESTROYER_ESCAPE, destroyerEscapeHandler);
		server.createContext(ESCAPE_JUGGERNAUT_ESCAPE, juggernautEscapeHandler);
	}
	private void fight(){
		server.createContext(FIGHT_BITTER_CAPTOR_FIGHT, bitterCaptorFightHandler);
		server.createContext(FIGHT_CRUEL_RULER_FIGHT, cruelRulerFightHandler);
		server.createContext(FIGHT_DARK_TECHNOLOGY_FIGHT, darkTechnologyFightHandler);
		server.createContext(FIGHT_DOOMBOT_LEGION_FIGHT, doombotLegionFightHandler);
		server.createContext(FIGHT_ELECTROMAGNETIC_BUBBLE_FIGHT, electromagneticBubbleFightHandler);
		server.createContext(FIGHT_HYDRA_KIDNAPPERS_FIGHT, hydraKidnappersFightHandler);
		server.createContext(FIGHT_MAESTRO_FIGHT, maestroFightHandler);
		server.createContext(FIGHT_MANIACAL_TYRANT_FIGHT, maniacalTyrantFightHandler);
		server.createContext(FIGHT_MELTER_FIGHT, melterFightHandler);
		server.createContext(FIGHT_MONARCHS_DECREE_FIGHT, monarchsDecreeFightHandler);
		server.createContext(FIGHT_MONARCHS_DECREE_DISCARD, monarchsDecreeDiscardHandler);
		server.createContext(FIGHT_PAIBOK_THE_POWER_SKRULL_FIGHT, paibokThePowerSkrullFightHandler);
		server.createContext(FIGHT_RUTHLESS_DICTATOR_DISCARD, ruthlessDictatorDiscardHandler);
		server.createContext(FIGHT_RUTHLESS_DICTATOR_KO, ruthlessDictatorKOHandler);
		server.createContext(FIGHT_SENTINEL_FIGHT, sentinelFightHandler);
		server.createContext(FIGHT_SUPER_SKRULL_FIGHT, superSkrullFightHandler);
		server.createContext(FIGHT_VANISHING_ILLUSIONS_FIGHT, vanishingIllusionsFightHandler);
		server.createContext(FIGHT_WHIRLWIND_FIGHT, whirlwindFightHandler);
		server.createContext(FIGHT_YMIR_FROST_GIANT_KING_FIGHT, ymirFrostGiantKingFightHandler);
		server.createContext(FIGHT_YMIR_FROST_GIANT_KING_DISCARD, ymirFrostGiantKingDiscardHandler);
	}
	private void game(){
		server.createContext(GAME_CREATE, createGameHandler);
		server.createContext(GAME_GET, getGamesHandler);
		server.createContext(GAME_GET_MODEL, getModelHandler);
		server.createContext(GAME_JOIN, joinGameHandler);
		server.createContext(GAME_SET_HEROES, setHeroesHandler);
		server.createContext(GAME_SET_MASTERMIND, setMastermindHandler);
		server.createContext(GAME_SET_SCHEME, setSchemeHandler);
		server.createContext(GAME_SET_VILLIANS, setVilliansHandler);		
		server.createContext(GAME_START, startHandler);
	}
	private void masterstrike(){
		server.createContext(MASTERSTRIKE_DR_DOOM_MASTERSTRIKE, drDoomMasterstrikeHandler);
		server.createContext(MASTERSTRIKE_MAGNETO_MASTERSTRIKE, magnetoMasterstrikeHandler);
		server.createContext(MASTERSTRIKE_RED_SKULL_MASTERSTRIKE, redSkullMasterstrikeHandler);
	}
	private void moves(){
		server.createContext(MOVES_ATTACK_CARD, attackCardHandler);
		server.createContext(MOVES_BUY_CARD, buyCardHandler);
		server.createContext(MOVES_END_TURN, endTurnHandler);
		server.createContext(MOVES_PLAY_CARD, playCardHandler);
	}
	private void test(){
		server.createContext("/test/getLog", getLogHandler);
		server.createContext("/test/setHero", setHeroesTestHandler);
		server.createContext("/test/setVillian", setVillianTestHandler);
		server.createContext(TEST_TEST, testHandler);
		server.createContext("/t", test);
	}
	private void users(){
		server.createContext(USER_LOGIN, loginHandler);
		server.createContext(USER_REGISTER, registerHandler);
	}
}
