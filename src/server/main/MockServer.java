package server.main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import client.main.ClientManager;
import server.definition.ServerVariables;
import shared.definitions.GameHeader;
import shared.definitions.PlayerHeader;
import shared.definitions.State;
import shared.model.ModelFacade;
import shared.server.IServer;

public class MockServer implements IServer, ServerVariables{
		
	private int current = ZERO;
	private boolean second = false;
	private boolean moveForward = false;
	
	//ability-------------------------------------------------------------------
	public void battlefieldPromotionAbility(int gameID, String card){	advance();	}
	public void copyPowersAbility(int gameID, String card){	advance();	}
	public void coveringFireAbility(int gameID, boolean draw){	advance();	}
	public void coveringFireDiscard(int gameID, int playerIndex, String card){	advance();	}
	public void cyclopsAbility(int gameID, String card){	advance();	}
	public void dangerousRescueAbility(int gameID, String card){	advance();	}
	public void energyDrainAbility(int gameID, String card){	advance();	}
	public void healingFactorAbility(int gameID, boolean accept){	advance();	}
	public void hereHoldThisForASecondAbility(int gameID, int location){	advance();	}
	public void heyCanIGetADoOverAbility(int gameID, boolean accept){	advance();	}
	public void hypnoticCharmAbility(int gameID, int playerIndex, boolean discard){	advance();	}
	public void pureFuryAbility(int gameID, int cardIndex){	advance();	}
	public void randomActsOfUnkindnessAbility(int gameID, boolean accept){	advance();	}
	public void randomActsOfUnkindnessPass(int gameID, int playerIndex, String card){	advance();	}
	public void shadowedThoughtsAbility(int gameID, boolean accept){	advance();	}
	public void silentSniperAbility(int gameID, int location){	advance();	}
	public void spinningCycloneAbility(int gameID, int startingLocation, int endingLocation){	advance();	}
	public void stackTheDeckAbility(int gameID, String card){	advance();	}
	public void theAmazingSpidermanAbility(int gameID, String card){	advance();	}
	public void unstoppableHulkAbility(int gameID, boolean accept){	advance();	}
	//ambush--------------------------------------------------------------------
	public void juggernautAmbush(int gameID, int playerIndex, String card){	advance();	}
	//chat----------------------------------------------------------------------
	public void sendChat(int gameID, int playerIndex, String message){	advance();	}
	//escape--------------------------------------------------------------------
	public void destroyerEscape(int gameID, int playerIndex, String card){	advance();	}
	public void juggernautEscape(int gameID, int playerIndex, String card){	advance();	}
	//fight---------------------------------------------------------------------
	public void bitterCaptorFight(int gameID, int location){	advance();	}
	public void cruelRulerFight(int gameID, int location){	advance();	}
	public void darkTechnologyFight(int gameID, int location){	advance();	}
	public void doombotLegionFight(int gameID, String card){	advance();	}
	public void electromagneticBubbleFight(int gameID, String card){	advance();	}
	public void hydraKidnappersFight(int gameID, boolean accept){	advance();	}
	public void maestroFight(int gameID, String card){	advance();	}
	public void maniacalTyrantFight(int gameID, String card){	advance();	}
	public void melterFight(int gameID, int playerIndex, boolean ko){	advance();	}
	public void monarchsDecreeFight(int gameID, boolean draw){	advance();	}
	public void monarchsDecreeDiscard(int gameID, int playerIndex, String card){	advance();	}
	public void paibokThePowerSkrullFight(int gameID, int playerIndex, int location){	advance();	}
	public void ruthlessDictatorDiscard(int gameID, String card){	advance();	}
	public void ruthlessDictatorKO(int gameID, String card){	advance();	}
	public void sentinelFight(int gameID, String card){	advance();	}
	public void superSkrullFight(int gameID, int playerIndex, String card){	advance();	}
	public void vanishingIllusionsFight(int gameID, int playerIndex, String card){	advance();	}
	public void whirlwindFight(int gameID, String card){	advance();	}
	public void ymirFrostGiantKingFight(int gameID, int playerIndex){	advance();	}
	public void ymirFrostGiantKingDiscard(int gameID, int playerIndex, int number){	advance();	}
	//game----------------------------------------------------------------------
	@Override
	public int createGame(String title) {
		return ZERO;
	}
	@Override
	public List<GameHeader> getGames() {
		List<GameHeader> games = new ArrayList<GameHeader>();
		List<PlayerHeader> players = new ArrayList<PlayerHeader>();
		players.add(new PlayerHeader(ZERO, ZERO, EMPTY));
		games.add(new GameHeader(ZERO, false, EMPTY, players, true));
		return games;
	}
	@Override
	public JsonObject getModel(int gameID, int versionID) {
		String file= TEST_MODEL_INFO_1 + current + TEST_MODEL_INFO_2;
		ModelFacade model = readFromFile(file);
		if(model != null){
			if(!second){
				if(moveForward){
					model.getTurnTracker().setState(State.PLAYING);
				}
				second = true;
				return model.getJson();
			}
			return new JsonObject();
		}
		System.exit(SYSTEM_EXIT);
		return null;
	}
	@Override
	public int joinGame(int gameID, int playerID, String playerName) {
		return ZERO;
	}
	@Override
	public void setHeroes(int gameID, List<String> heroes) {	advance();	}
	@Override
	public void setMastermind(int gameID, String mastermind) {	advance();	}
	@Override
	public void setScheme(int gameID, String scheme) {	advance();	}
	@Override
	public void setVillians(int gameID, List<String> villians) {	advance();	}
	@Override
	public void startGame(int gameID) {	advance();	}
	//masterstrike--------------------------------------------------------------
	public void drDoomMasterstrike(int gameID, int playerIndex, String card){	advance();	}
	public void magnetoMasterstrike(int gameID, int playerIndex, String card){	advance();	}
	public void redSkullMasterstrike(int gameID, int playerIndex, String card){	advance();	}
	//moves---------------------------------------------------------------------
	@Override
	public void attackCard(int gameID, int cardIndex) {	advance();	}
	@Override
	public void buyCard(int gameID, int playerIndex, int cardIndex) {	advance();	}
	@Override
	public void endTrun(int gameID, int playerIndex) {
		second = false;
		moveForward = false;
		++current;
	}
	@Override
	public void playCard(int gameID, String name) {	advance();	}
//users---------------------------------------------------------------------
	@Override
	public int login(String username, String password) {
		return ZERO;
	}
	@Override
	public int register(String username, String password) {
		return ZERO;
	}
	
	private ModelFacade readFromFile(String file){
		try{
			File input = new File(file);
			if(!input.exists()){
				return null;
			}
			Scanner scanner = new Scanner(input);
			StringBuilder str = new StringBuilder();
			while(scanner.hasNextLine()){
				str.append(scanner.nextLine());
			}
			scanner.close();
			
			JsonParser parser = new JsonParser();
			JsonObject o = parser.parse(str.toString()).getAsJsonObject();
			ModelFacade model = new ModelFacade();
			ClientManager.getInstance().setGame(new GameHeader(ONE, false, EMPTY, null, false));
			model.updateFromJson(o, ZERO);
			return model;
		}
		catch(IOException e){
			e.printStackTrace();
		}
		return null;
	}
	
	private void advance(){
		moveForward = true;
		second = false;
	}
}
