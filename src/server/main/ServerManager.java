package server.main;

import server.definition.ServerVariables;
import shared.server.IServer;

public class ServerManager implements ServerVariables{

	private static ServerManager instance = null;
	private IServer server;
	
	public static ServerManager getInstance(){
		if(instance == null){
			instance = new ServerManager();
		}
		return instance;
	}
	
	private ServerManager(){
		if(MOCK_SERVER){
			server = new MockServer();
		}
		else{
			server = ServerFacade.getInstance();
		}
	}
	
	public IServer getServer(){
		return server;
	}
}
