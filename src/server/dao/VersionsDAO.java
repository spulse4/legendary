package server.dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import server.definition.ServerVariables;

public class VersionsDAO extends AbstractDAO implements ServerVariables{
	
	public VersionsDAO(){}
	
	public int read(Connection connection, int gameID){
		List<String> categories = new ArrayList<String>();
		categories.add(DATABASE_HEADER_VERSION);
		List<String> column = new ArrayList<String>();
		column.add(GAME_ID);
		List<String> condition = new ArrayList<String>();
		condition.add(Integer.toString(gameID));
		
		String selectStatement = super.convertSelectStatement(categories);
		String selectCondition = super.convertSelectCondition(column, condition);
		List<String> results = super.read(connection, selectStatement, DATABASE_TABLE_VERSIONS, selectCondition);		
		
		return Integer.valueOf(results.get(ZERO));
	}
	
	public void write(int gameID){
		List<String> columnsList = new ArrayList<String>();
		columnsList.add(DATABASE_HEADER_GAME_ID);
		columnsList.add(DATABASE_HEADER_VERSION);

		List<String> valuesList = new ArrayList<String>();
		valuesList.add(Integer.toString(gameID));
		valuesList.add(Integer.toString(ZERO));
		
		String columns = super.convertInsertColumn(columnsList);
		String values = super.convertInsertValues(valuesList);
		
		Connection connection = super.startTransaction();
		super.write(connection, DATABASE_TABLE_VERSIONS, columns, values);
		super.safeClose(connection);
	}
	
	public void update(int gameID){
		Connection connection = super.startTransaction();
		int version = read(connection, gameID);
		List<String> columnsAssign = new ArrayList<String>();
		columnsAssign.add(DATABASE_HEADER_VERSION);
		List<String> valuesAssign = new ArrayList<String>();
		valuesAssign.add(Integer.toString(version + 1));
		List<String> columnsCondition = new ArrayList<String>();
		columnsCondition.add(DATABASE_HEADER_GAME_ID);
		List<String> valuesCondition = new ArrayList<String>();
		valuesCondition.add(Integer.toString(gameID));
		
		String assignments = super.convertUpdateAssignments(columnsAssign, valuesAssign);
		String conditions = super.convertUpdateConditions(columnsCondition, valuesCondition);
		
		super.update(connection, DATABASE_TABLE_VERSIONS, assignments, conditions);
		super.safeClose(connection);
	}
}
