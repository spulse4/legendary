package server.dao;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import server.definition.ServerVariables;

public class DeleteDAO extends AbstractDAO implements ServerVariables{
	
	public DeleteDAO(){
		super();
	}
	
	public void deleteAll(){
		try{
			File input = new File(DATABASE_CLEAR_INFO);
			Scanner scanner = new Scanner(input);
			Connection connection = super.startTransaction();
			
			while(scanner.hasNextLine()){
				excecute(connection, scanner.nextLine());
			}
			super.safeClose(connection);
			scanner.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}

	private void excecute(Connection connection, String statement){
		try{
			Statement stmt = connection.createStatement();
			
			stmt.executeUpdate(statement);
			
			stmt.close();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}

}
