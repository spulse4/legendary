package server.dao;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import server.definition.ServerCard;

public class CardDAO extends AbstractDAO{
	
	public static void main(String[] args){
		new CardDAO().fill();
	}
	
	public void fill(){
		try{
			File input = new File(LOCATION_CARD_INFO);
			Scanner myScanner = new Scanner(input);
			myScanner.nextLine();
			while(myScanner.hasNextLine()){
				String line = myScanner.nextLine();
				Scanner lineScanner = new Scanner(line);
				lineScanner.useDelimiter(COMMA);
				List<String> columnsList = new ArrayList<String>();
				
				columnsList.add(DATABASE_HEADER_NAME);
				columnsList.add(DATABASE_HEADER_PRICE);
				columnsList.add(DATABASE_HEADER_BUY_POWER);
				columnsList.add(DATABASE_HEADER_ATTACK_POWER);
				columnsList.add(DATABASE_HEADER_DESCRIPTION);
				columnsList.add(DATABASE_HEADER_COLOR1);
				columnsList.add(DATABASE_HEADER_COLOR2);
				columnsList.add(DATABASE_HEADER_COLOR3);
				columnsList.add(DATABASE_HEADER_COLOR4);
				columnsList.add(DATABASE_HEADER_CATEGORY);
				columnsList.add(DATABASE_HEADER_AMBUSH);
				columnsList.add(DATABASE_HEADER_FIGHT);
				columnsList.add(DATABASE_HEADER_ESCAPE);
				columnsList.add(DATABASE_HEADER_POINTS);
				columnsList.add(DATABASE_HEADER_HITPOINTS);
				columnsList.add(DATABASE_HEADER_SETUP);
				columnsList.add(DATABASE_HEADER_SPECIAL_RULES);
				columnsList.add(DATABASE_HEADER_TWIST);
				columnsList.add(DATABASE_HEADER_EVIL_WINS);

				List<String> valuesList = new ArrayList<String>();
				while(lineScanner.hasNext()){
					valuesList.add(lineScanner.next());
				}

				String columns = this.convertInsertColumn(columnsList);
				String values = this.convertInsertValues(valuesList);
				Connection connection = this.startTransaction();
				this.write(connection, DATABASE_TABLE_CARDS, columns, values);
				this.safeClose(connection);
				
				lineScanner.close();
			}
			
			myScanner.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
		
	public ServerCard read(String name){
		List<String> categories = new ArrayList<String>();
		categories.add(DATABASE_HEADER_NAME);
		categories.add(DATABASE_HEADER_PRICE);
		categories.add(DATABASE_HEADER_BUY_POWER);
		categories.add(DATABASE_HEADER_ATTACK_POWER);
		categories.add(DATABASE_HEADER_DESCRIPTION);
		categories.add(DATABASE_HEADER_COLOR1);
		categories.add(DATABASE_HEADER_COLOR2);
		categories.add(DATABASE_HEADER_COLOR3);
		categories.add(DATABASE_HEADER_COLOR4);
		categories.add(DATABASE_HEADER_CATEGORY);
		categories.add(DATABASE_HEADER_AMBUSH);
		categories.add(DATABASE_HEADER_FIGHT);
		categories.add(DATABASE_HEADER_ESCAPE);
		categories.add(DATABASE_HEADER_POINTS);
		categories.add(DATABASE_HEADER_HITPOINTS);
		categories.add(DATABASE_HEADER_SETUP);
		categories.add(DATABASE_HEADER_SPECIAL_RULES);
		categories.add(DATABASE_HEADER_TWIST);
		categories.add(DATABASE_HEADER_EVIL_WINS);
		List<String> column = new ArrayList<String>();
		column.add(NAME);
		List<String> condition = new ArrayList<String>();
		condition.add(name);
		
		String selectStatement = super.convertSelectStatement(categories);
		String selectCondition = super.convertSelectCondition(column, condition);
		
		Connection connection = super.startTransaction();
		List<String> results = super.read(connection, selectStatement, DATABASE_TABLE_CARDS, selectCondition);		
		
		super.safeClose(connection);

		if(results.size() == ZERO){
			if(name.contains(DELIMITER)){
				Scanner scanner = new Scanner(name);
				scanner.useDelimiter(DELIMITER);
				
				String nameCut = scanner.next();
				String second = scanner.next();
				scanner.close();
				ServerCard original = read(nameCut);
				ServerCard secondCard = read(second);
				
				List<String> allColors = new ArrayList<String>();
				for(String str : original.getColors()){
					if(!allColors.contains(str)){
						allColors.add(str);
					}
				}
				for(String str : secondCard.getColors()){
					if(!allColors.contains(str)){
						allColors.add(str);
					}
				}
				switch(allColors.size()){
				case ONE:
					original.setColor(ZERO, allColors.get(ZERO));
					original.setColor(ONE, allColors.get(ZERO));
					original.setColor(TWO, allColors.get(ZERO));
					original.setColor(THREE, allColors.get(ZERO));
					break;
				case TWO:
					original.setColor(ZERO, allColors.get(ZERO));
					original.setColor(ONE, allColors.get(ZERO));
					original.setColor(TWO, allColors.get(ONE));
					original.setColor(THREE, allColors.get(ONE));
					break;
				case THREE:
					original.setColor(ZERO, allColors.get(ZERO));
					original.setColor(ONE, allColors.get(ZERO));
					original.setColor(TWO, allColors.get(ONE));
					original.setColor(THREE, allColors.get(TWO));
					break;
				case FOUR:
					original.setColor(ZERO, allColors.get(ZERO));
					original.setColor(ONE, allColors.get(ONE));
					original.setColor(TWO, allColors.get(TWO));
					original.setColor(THREE, allColors.get(THREE));
					break;
				}
				write(original);
				return original;
			}
		}
		
		return new ServerCard(results);
	}
	
	private void write(ServerCard card){
		List<String> columnsList = new ArrayList<String>();
		
		columnsList.add(DATABASE_HEADER_NAME);
		columnsList.add(DATABASE_HEADER_PRICE);
		columnsList.add(DATABASE_HEADER_BUY_POWER);
		columnsList.add(DATABASE_HEADER_ATTACK_POWER);
		columnsList.add(DATABASE_HEADER_DESCRIPTION);
		columnsList.add(DATABASE_HEADER_COLOR1);
		columnsList.add(DATABASE_HEADER_COLOR2);
		columnsList.add(DATABASE_HEADER_COLOR3);
		columnsList.add(DATABASE_HEADER_COLOR4);
		columnsList.add(DATABASE_HEADER_CATEGORY);
		columnsList.add(DATABASE_HEADER_AMBUSH);
		columnsList.add(DATABASE_HEADER_FIGHT);
		columnsList.add(DATABASE_HEADER_ESCAPE);
		columnsList.add(DATABASE_HEADER_POINTS);
		columnsList.add(DATABASE_HEADER_HITPOINTS);
		columnsList.add(DATABASE_HEADER_SETUP);
		columnsList.add(DATABASE_HEADER_SPECIAL_RULES);
		columnsList.add(DATABASE_HEADER_TWIST);
		columnsList.add(DATABASE_HEADER_EVIL_WINS);
		List<String> valuesList = new ArrayList<String>();
		valuesList.add(card.getName());
		valuesList.add(card.getPrice());
		valuesList.add(card.getBuyPower());
		valuesList.add(card.getAttackPower());
		valuesList.add(card.getDescription());
		valuesList.add(card.getColors().get(ZERO));
		valuesList.add(card.getColors().get(ONE));
		valuesList.add(card.getColors().get(TWO));
		valuesList.add(card.getColors().get(THREE));
		valuesList.add(card.getCategory());
		valuesList.add(card.getAmbush());
		valuesList.add(card.getFight());
		valuesList.add(card.getEscape());
		valuesList.add(card.getPoints());
		valuesList.add(card.getHitPoints());
		valuesList.add(card.getSetup());
		valuesList.add(card.getSpecialRules());
		valuesList.add(card.getTwist());
		valuesList.add(card.getEvilWins());
		
		String columns = super.convertInsertColumn(columnsList);
		String values = super.convertInsertValues(valuesList);
		
		Connection connection = super.startTransaction();
		super.write(connection, DATABASE_TABLE_CARDS, columns, values);
		super.safeClose(connection);
	}
}
