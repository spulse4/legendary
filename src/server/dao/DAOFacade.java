package server.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import server.definition.ServerCard;
import server.definition.ServerVariables;
import shared.definitions.GameHeader;
import shared.definitions.PlayerHeader;
import shared.definitions.UserHeader;
import shared.model.ModelFacade;
import shared.model.PlayerCards;

public class DAOFacade implements ServerVariables{
	
	private static DAOFacade instance = null;
	
	private Map<String, ServerCard> cache;
	
	private CardDAO card;
	private ModelDAO model;
	private UserDAO user;
	private VersionsDAO version;
	
	public static DAOFacade getInstance(){
		if(instance == null){
			instance = new DAOFacade();
		}
		return instance;
	}
		
	private DAOFacade(){	
		cache = new HashMap<String, ServerCard>();
		card = new CardDAO();
		model = new ModelDAO();
		user = new UserDAO();
		version = new VersionsDAO();
	}

	public UserHeader addUser(String username, String password){
		UserHeader userHeader = user.write(username, password);
		return userHeader;
	}

	public UserHeader checkUser(String username, String password){
		UserHeader userHeader = user.read(username, password);
		return userHeader;
	}
	
	public int createGame(String name){
		GameHeader header = new GameHeader(NEGATIVE_ONE, false, name, new ArrayList<PlayerHeader>(), false);
		int gameID = model.addGame(header);
		version.write(gameID);
		return gameID;
	}
	
	public int addPlayer(GameHeader game, UserHeader user){
		for(PlayerHeader player : game.getPlayers()){
			if(player.getId() == user.getId()){
				return player.getIndex();
			}
		}

		int index = game.getPlayers().size();

		List<PlayerHeader> players = game.getPlayers();
		players.add(new PlayerHeader(index, user.getId(), user.getName()));
		game.setPlayers(players);
		
		ModelFacade model = readModel(game.getGameID());
		model.setGameHeader(game);
		
		List<PlayerCards> cards = model.getPlayerCardList();
		cards.add(new PlayerCards());
		
		this.model.updateGame(model);
		version.update(game.getGameID());

		return index;
	}
		
	public void startGame(ModelFacade model){
		model.getGameHeader().start();
		model.setStarted(true);
		this.model.updateGame(model);
		version.update(model.getGameID());
	}
		
	public List<GameHeader> getGames(){
		return this.model.readHeaders();
	}
	
	public ModelFacade readModel(int gameID){
		return this.model.readModel(gameID);
	}
	
	public ServerCard getCard(String name){
		if(name.endsWith(PERIOD)){
			name = name.substring(ZERO, name.length() - ONE);
		}
		if(!cache.containsKey(name)){
			cache.put(name, card.read(name));
		}
		return cache.get(name);
	}

	public void updateModel(ModelFacade model){
		this.model.updateGame(model);
		version.update(model.getGameID());
	}

	public void deleteAll(){
		new DeleteDAO().deleteAll();
	}
}
