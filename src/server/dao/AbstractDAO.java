package server.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.google.gson.JsonObject;

import server.definition.ServerVariables;
import shared.definitions.PlayerHeader;

public class AbstractDAO implements ServerVariables{
	
	public AbstractDAO(){
		try {
			Class.forName(DATABASE_DRIVER);
		}
		catch(ClassNotFoundException e) {
			e.printStackTrace(); 
		}
	}
	
	public Connection startTransaction(){
		try {
			Connection connection = DriverManager.getConnection(DATABASE_URL);
			connection.setAutoCommit(false);
			return connection;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void safeClose(Connection connection){
		if(connection == null){
			try{
				throw new Exception(DATABASE_ERROR);
			}
			catch(Exception e){
				e.printStackTrace();
			}
			return;
		}
		try {
			connection.commit();
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		finally {
			try{
				connection.close();
				connection = null;
			}
			catch(SQLException e){
				e.printStackTrace();
			}
		}
	}
		
	public List<String> read(Connection connection, String select, String db, String where){
		List<String> results = new ArrayList<String>();

		try{
			StringBuilder str = new StringBuilder();
			str.append(DATABASE_SELECT_1);
			str.append(select);
			str.append(DATABASE_SELECT_2);
			str.append(db);
			str.append(DATABASE_SELECT_3);
			if(where == null){
				str.append(DATABASE_SELECT_4);
			}
			else{
				str.append(where);
			}
			String selectStatement = str.toString();
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(selectStatement);
			int columns = resultSet.getMetaData().getColumnCount();
			while(resultSet.next()){
				for(int i = ZERO; i < columns; ++i){
					results.add(resultSet.getString(i + ONE));
				}
			}
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		
		return results;
	}
	
	public int write(Connection connection, String db, String columns, String values){
		try{
			StringBuilder str = new StringBuilder();
			str.append(DATABASE_INSERT_1);
			str.append(db);
			str.append(DATABASE_INSERT_2);
			str.append(columns);
			str.append(DATABASE_INSERT_3);
			str.append(values);
			str.append(DATABASE_INSERT_4);
			String insertStatement = str.toString();
			PreparedStatement statement = connection.prepareStatement(insertStatement, Statement.RETURN_GENERATED_KEYS);
			statement.executeUpdate();
			try{
				ResultSet generatedKeys = statement.getGeneratedKeys();
				generatedKeys.next();
				return (int)generatedKeys.getLong(ONE);
			}
			catch(SQLException e){
				e.printStackTrace();
			}
			
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return NEGATIVE_ONE;
	}
	
	public void update(Connection connection, String db, String assignments, String conditions){
		try{
			StringBuilder str = new StringBuilder();
			str.append(DATABASE_UPDATE_1);
			str.append(db);
			str.append(DATABASE_UPDATE_2);
			str.append(assignments);
			str.append(DATABASE_UPDATE_3);
			str.append(conditions);
			String updateStatement = str.toString();
			Statement statement = connection.createStatement();
			statement.executeUpdate(updateStatement);
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public String convertSelectStatement(List<String> categories){
		StringBuilder str = new StringBuilder();
		
		for(String string : categories){
			if(str.length() != ZERO){
				str.append(COMMA + SPACE);
			}
			str.append(string);
		}
		return str.toString();		
	}
	
	public String convertSelectCondition(List<String> column, List<String> condition){
		StringBuilder str = new StringBuilder();
		
		for(int i = ZERO; i < column.size(); ++i){
			if(str.length() != ZERO){
				str.append(DATABASE_AND);
			}
			str.append(column.get(i));
			str.append(DATABASE_EQUAL);
			str.append(condition.get(i));
			str.append(DATABASE_QUOTE);
		}
		
		return str.toString();		
	}
	
	public String convertInsertColumn(List<String> columns){
		return convertSelectStatement(columns);		
	}
	
	public String convertInsertValues(List<String> values){
		StringBuilder str = new StringBuilder();
		for(String string : values){
			if(str.length() != ZERO){
				str.append(COMMA + SPACE);
			}
			str.append(DATABASE_QUOTE + string + DATABASE_QUOTE);
		}
		return str.toString();		
	}
	
	public String convertUpdateAssignments(List<String> column, List<String> value){		
		StringBuilder str = new StringBuilder();
		
		for(int i = ZERO; i < column.size(); ++i){
			if(str.length() != ZERO){
				str.append(COMMA);
			}
			str.append(column.get(i));
			str.append(DATABASE_EQUAL);
			str.append(value.get(i));
			str.append(DATABASE_QUOTE);
		}
		
		return str.toString();	
	}
	
	public String convertUpdateConditions(List<String> column, List<String> condition){
		return convertSelectCondition(column, condition);
	}

	public JsonObject convertToEscapedJSON(String str){
		Scanner myScanner = new Scanner(str);
		myScanner.useDelimiter(DELIMITER);
		
		int brotherhood = Integer.valueOf(myScanner.next());
		int mastersOfEvil = Integer.valueOf(myScanner.next());
		int enemiesOfAsgard = Integer.valueOf(myScanner.next());
		int spiderfoes = Integer.valueOf(myScanner.next());
		int hydra = Integer.valueOf(myScanner.next());
		int skrulls = Integer.valueOf(myScanner.next());
		int radiation = Integer.valueOf(myScanner.next());
		int henchmen = Integer.valueOf(myScanner.next());
		int bystanders = Integer.valueOf(myScanner.next());
		int heroes = Integer.valueOf(myScanner.next());
		String mostRecent = myScanner.next();
		
		myScanner.close();
		
		JsonObject o = new JsonObject();
		o.addProperty(CATEGORY_BROTHERHOOD, brotherhood);
		o.addProperty(CATEGORY_MASTERS_OF_EVIL, mastersOfEvil);
		o.addProperty(CATEGORY_ENEMIES_OF_ASGARD, enemiesOfAsgard);
		o.addProperty(CATEGORY_SPIDER_FOES, spiderfoes);
		o.addProperty(CATEGORY_HYDRA, hydra);
		o.addProperty(CATEGORY_SKRULLS, skrulls);
		o.addProperty(CATEGORY_RADIATION, radiation);
		o.addProperty(CATEGORY_HENCHMEN, henchmen);
		o.addProperty(CATEGORY_BYSTANDERS, bystanders);
		o.addProperty(CATEGORY_HEROES, heroes);
		o.addProperty(CATEGORY_MOST_RECENT, mostRecent);
	
		return o;
	}

	public String convertPlayerHeaderListToString(List<PlayerHeader> players){
		StringBuilder str = new StringBuilder();
		for(PlayerHeader player : players){
			if(str.length() != ONE){
				str.append(EXCLAMATION_POINT);
			}
			str.append(player.getIndex());
			str.append(COMMA);
			str.append(player.getId());
			str.append(COMMA);
			str.append(player.getName());
		}
		return str.toString();
	}

	public JsonObject convertToKOJSON(String str){
		Scanner myScanner = new Scanner(str);
		myScanner.useDelimiter(DELIMITER);
		
		int shield = Integer.valueOf(myScanner.next());
		int spiderman = Integer.valueOf(myScanner.next());
		int deadpool = Integer.valueOf(myScanner.next());
		int avengers = Integer.valueOf(myScanner.next());
		int xMen = Integer.valueOf(myScanner.next());
		int wounds = Integer.valueOf(myScanner.next());
		int bystanders = Integer.valueOf(myScanner.next());
		String mostRecent = myScanner.next();
		
		myScanner.close();
		
		JsonObject o = new JsonObject();
		o.addProperty(CATEGORY_SHIELD, shield);
		o.addProperty(CATEGORY_SPIDERMAN, spiderman);
		o.addProperty(CATEGORY_DEADPOOL, deadpool);
		o.addProperty(CATEGORY_AVENGERS, avengers);
		o.addProperty(CATEGORY_X_MEN, xMen);
		o.addProperty(CATEGORY_WOUNDS, wounds);
		o.addProperty(CATEGORY_BYSTANDERS, bystanders);
		o.addProperty(CATEGORY_MOST_RECENT, mostRecent);
	
		return o;
	}
	
	public String convertToString(List<String> strings){
		StringBuilder str = new StringBuilder();
		str.append(BRACKET_OPEN);
		
		for(String string : strings){
			str.append(string);
			str.append(COMMA);
		}
		if(str.length() > ONE){
			str.setLength(str.length() - ONE);
		}
		
		str.append(BRACKET_CLOSE);
		
		return str.toString();
	}
}
