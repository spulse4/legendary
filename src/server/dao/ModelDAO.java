package server.dao;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import server.definition.ServerVariables;
import shared.definitions.GameHeader;
import shared.model.ModelFacade;

public class ModelDAO extends AbstractDAO implements ServerVariables{

	public ModelDAO(){
		super();
	}
	
	public List<GameHeader> readHeaders(){
		List<GameHeader> headers = new ArrayList<>();
		Connection connection = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;		
		
		try {
			String query = DATABASE_MODEL_READ_HEADER;
			connection = super.startTransaction();
			stmt = connection.prepareStatement(query);
			rs = stmt.executeQuery();
			
			while (rs.next()) {
				InputStream blobStream = new ByteArrayInputStream(rs.getBytes(1));
				ObjectInputStream objStream = new ObjectInputStream(blobStream);
				headers.add((GameHeader) objStream.readObject());
			}
		}
		catch (SQLException | ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		finally {
			try{
				rs.close();
				stmt.close();
				super.safeClose(connection);
			}
			catch(SQLException e){
				e.printStackTrace();
			}
		}
		return headers;
	}
	
	public ModelFacade readModel(int gameID){
		ModelFacade model = null;
		Connection connection = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try{
			String query = DATABASE_MODEL_READ_MODEL;
			connection = super.startTransaction();
			stmt = connection.prepareStatement(query);
			stmt.setString(ONE, Integer.toString(gameID));
			rs = stmt.executeQuery();
			
			while(rs.next()){
				InputStream blobStream = new ByteArrayInputStream(rs.getBytes(ONE));
				ObjectInputStream objStream = new ObjectInputStream(blobStream);
				model = (ModelFacade) objStream.readObject();
			}
		}
		catch(SQLException | ClassNotFoundException | IOException e){
			e.printStackTrace();
		}
		finally{
			try{
				rs.close();
				stmt.close();
				super.safeClose(connection);
			}
			catch(SQLException e){
				e.printStackTrace();
			}
		}
		return model;
	}
	
	public int addGame(GameHeader game){
		int id = NEGATIVE_ONE;
		ModelFacade model = new ModelFacade();
		Connection connection = null;
		PreparedStatement statement = null;
		
		try{
			String insertStatement = DATABASE_MODEL_INSERT;
			connection = super.startTransaction();
			statement = connection.prepareStatement(insertStatement, Statement.RETURN_GENERATED_KEYS);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(model);
			byte[] bytes = baos.toByteArray();
			ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
			
			statement.setBinaryStream(ONE, bais, bytes.length);
			
			ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
			ObjectOutputStream oos2 = new ObjectOutputStream(baos2);
			oos2.writeObject(game);
			byte[] bytes2 = baos2.toByteArray();
			ByteArrayInputStream bais2 = new ByteArrayInputStream(bytes2);
			
			statement.setBinaryStream(TWO, bais2, bytes2.length);

			statement.executeUpdate();
			try{
				ResultSet generatedKeys = statement.getGeneratedKeys();
				generatedKeys.next();
				id = (int)generatedKeys.getLong(1);
			}
			catch(SQLException e){
				e.printStackTrace();
			}
			game.setGameID(id);
			model.setGameID(id);
			model.setGameHeader(game);
			statement.close();
			statement = null;
			super.safeClose(connection);
			connection = null;
			updateGame(model);
		}
		catch(SQLException | IOException e){
			e.printStackTrace();
		}
		finally{
			try{
				if(statement != null){
					statement.close();
					super.safeClose(connection);
				}
			}
			catch(SQLException e){
				e.printStackTrace();
			}
		}
		return id;
	}
	
	public void updateGame(ModelFacade game){
		game.setVersion(game.getVersion() + ONE);
		Connection connection = null;
		PreparedStatement statement = null;
		try{
			String remove = DATABASE_MODEL_DELETE;
			connection = super.startTransaction();
			statement = connection.prepareStatement(remove);
			
			statement.setInt(ONE, game.getGameHeader().getGameID());
			statement.executeUpdate();
			statement.close();
			
			String insertStatement = DATABASE_MODEL_INSERT_WITH_ID;
			statement = connection.prepareStatement(insertStatement);
			
			statement.setInt(ONE, game.getGameHeader().getGameID());

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(game);
			byte[] bytes = baos.toByteArray();
			ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
			statement.setBinaryStream(TWO, bais, bytes.length);

			ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
			ObjectOutputStream oos2 = new ObjectOutputStream(baos2);
			oos2.writeObject(game.getGameHeader());
			byte[] bytes2 = baos2.toByteArray();
			ByteArrayInputStream bais2 = new ByteArrayInputStream(bytes2);
			statement.setBinaryStream(THREE, bais2, bytes2.length);

			statement.executeUpdate();
		}
		catch(SQLException | IOException e){
			e.printStackTrace();
		}
		finally{
			try{
				statement.close();
				super.safeClose(connection);
			}
			catch(SQLException e){
				e.printStackTrace();
			}
		}
	}
}
