package server.dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import server.definition.ServerVariables;
import shared.definitions.UserHeader;

public class UserDAO extends AbstractDAO implements ServerVariables{

	public UserDAO(){}
	
	public UserHeader read(String username, String password){
		List<String> categories = new ArrayList<String>();
		categories.add(DATABASE_HEADER_ID);
		List<String> column = new ArrayList<String>();
		column.add(DATABASE_HEADER_USERNAME);
		column.add(DATABASE_HEADER_PASSWORD);
		List<String> condition = new ArrayList<String>();
		condition.add(username);
		condition.add(password);
		
		String selectStatement = super.convertSelectStatement(categories);
		String selectCondition = super.convertSelectCondition(column, condition);
		
		Connection connection = super.startTransaction();
		List<String> results = super.read(connection, selectStatement, DATABASE_TABLE_USERS, selectCondition);		
		super.safeClose(connection);
		
		if(results.isEmpty()){
			return null;
		}		
		return new UserHeader(Integer.valueOf(results.get(ZERO)), username);
	}
	
	public UserHeader write(String username, String password){
		List<String> columnsList = new ArrayList<String>();
		columnsList.add(DATABASE_HEADER_USERNAME);
		columnsList.add(DATABASE_HEADER_PASSWORD);
		List<String> valuesList = new ArrayList<String>();
		valuesList.add(username);
		valuesList.add(password);
		
		String columns = super.convertInsertColumn(columnsList);
		String values = super.convertInsertValues(valuesList);
		
		Connection connection = super.startTransaction();
		int id = super.write(connection, DATABASE_TABLE_USERS, columns, values);
		super.safeClose(connection);
		
		return new UserHeader(id, username);
	}
}
