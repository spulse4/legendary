package server.action.fight;

import java.util.ArrayList;
import java.util.List;

import server.dao.DAOFacade;
import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class RuthlessDictatorKO implements ServerVariables{
	//Look at the top three cards of your deck. KO one, discard one and put one back on top of your deck

	private DAOFacade facade;
	
	public RuthlessDictatorKO(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int gameID, String card){
		List<Integer> effected = State.RUTHLESS_DICTATOR_KO.getEffected(gameID);
		effected.remove(ZERO);
		State.RUTHLESS_DICTATOR_KO.setEffected(gameID, new ArrayList<Integer>());
		State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
		effected = new ArrayList<Integer>();
		effected.add(model.getTurnTracker().getCurrentPlayer());
		State.RUTHLESS_DICTATOR_DISCARD.setEffected(gameID, effected);
		model.getTurnTracker().setState(State.RUTHLESS_DICTATOR_DISCARD);
		int index = model.getTurnTracker().getCurrentPlayer();
		if(model.getPlayerCardList().get(index).getFromDraw(ZERO).equals(card)){
			model.getKoPile().addCard(facade.getCard(card));
			model.getPlayerCardList().get(index).removeFromDraw(ZERO);
		}
		else if(model.getPlayerCardList().get(index).getFromDraw(ONE).equals(card)){
			model.getKoPile().addCard(facade.getCard(card));
			model.getPlayerCardList().get(index).removeFromDraw(ONE);
		}
		else{
			model.getKoPile().addCard(facade.getCard(card));
			model.getPlayerCardList().get(index).removeFromDraw(TWO);
		}
	}
}