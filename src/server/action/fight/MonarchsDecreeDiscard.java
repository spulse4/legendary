package server.action.fight;

import java.util.ArrayList;
import java.util.List;

import server.action.general.DiscardCard;
import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class MonarchsDecreeDiscard implements ServerVariables{
	//Choose one: each other player draws a card or each other player discards a card

	public MonarchsDecreeDiscard(){}
	
	public void go(ModelFacade model, int gameID, int playerIndex, String card){
		List<Integer> effected = State.MONARCHS_DECREE_DISCARD.getEffected(gameID);
		for(int i = ZERO; i < effected.size(); ++i){
			if(effected.get(i) == playerIndex){
				effected.remove(i);
				break;
			}
		}
		if(effected.size() == ZERO){
			State.MONARCHS_DECREE_DISCARD.setEffected(gameID, new ArrayList<Integer>());
			State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
			model.getTurnTracker().setState(State.PLAYING);
		}
		new DiscardCard().go(model, playerIndex, card);
	}
}