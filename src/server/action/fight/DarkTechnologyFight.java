package server.action.fight;

import java.util.ArrayList;

import server.action.general.ReceiveCard;
import server.dao.DAOFacade;
import shared.definitions.State;
import shared.model.ModelFacade;

public class DarkTechnologyFight {
	//You may recruit a Black or Blue Hero from the HQ for free

	private DAOFacade facade;
	
	public DarkTechnologyFight(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int gameID, int location){
		State.DARK_TECHNOLOGY_FIGHT.setEffected(gameID, new ArrayList<Integer>());
		State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
		model.getTurnTracker().setState(State.PLAYING);
		int index = model.getTurnTracker().getCurrentPlayer();
		new ReceiveCard(facade).go(model, index, location);
	}
}