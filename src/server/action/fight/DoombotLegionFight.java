package server.action.fight;

import java.util.ArrayList;

import server.action.general.KOCard;
import server.dao.DAOFacade;
import shared.definitions.State;
import shared.model.ModelFacade;

public class DoombotLegionFight {
	//Look at the top two cards of your deck. KO one of them and put the other back

	private DAOFacade facade;
	
	public DoombotLegionFight(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int gameID, String card){
		State.DOOMBOT_LEGION_FIGHT.setEffected(gameID, new ArrayList<Integer>());
		State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
		model.getTurnTracker().setState(State.PLAYING);
		int index = model.getTurnTracker().getCurrentPlayer();
		new KOCard(facade).KODraw(model, index, card);
	}
}