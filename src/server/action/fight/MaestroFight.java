package server.action.fight;

import java.util.ArrayList;
import java.util.List;

import server.action.general.KOCard;
import server.dao.DAOFacade;
import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class MaestroFight implements ServerVariables{
	//For each of your Green Heroes, KO one of your Heroes

	private DAOFacade facade;
	
	public MaestroFight(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int gameID, String card){
		List<Integer> effected = State.MAESTRO_FIGHT.getEffected(gameID);
		effected.remove(ZERO);
		if(effected.size() == ZERO){
			State.MAESTRO_FIGHT.setEffected(gameID, new ArrayList<Integer>());
			State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
			model.getTurnTracker().setState(State.PLAYING);
		}
		int index = model.getTurnTracker().getCurrentPlayer();
		new KOCard(facade).go(model, index, card);
	}
}