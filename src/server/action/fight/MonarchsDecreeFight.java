package server.action.fight;

import java.util.ArrayList;
import java.util.List;

import server.action.general.DrawCard;
import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class MonarchsDecreeFight implements ServerVariables{
	//Choose one: each other player draws a card or each other player discards a card

	public MonarchsDecreeFight(){}
	
	public void go(ModelFacade model, int gameID, boolean draw){
		State.MONARCHS_DECREE_FIGHT.setEffected(gameID, new ArrayList<Integer>());

		if(draw){
			State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
			model.getTurnTracker().setState(State.PLAYING);
			int index = model.getTurnTracker().getCurrentPlayer();
			for(int i = ZERO; i < model.getPlayerCardList().size(); ++i){
				if(i == index){
					continue;
				}
				new DrawCard().go(model, i);
			}
		}
		else{
			List<Integer> effected = new ArrayList<Integer>();
			int index = model.getTurnTracker().getCurrentPlayer();
			for(int i = ZERO; i < model.getPlayerCardList().size(); ++i){
				if(i == index){
					continue;
				}
				effected.add(i);
			}
			State.MONARCHS_DECREE_DISCARD.setEffected(gameID, effected);
			model.getTurnTracker().setState(State.MONARCHS_DECREE_DISCARD);
		}
	}
}