package server.action.fight;

import java.util.ArrayList;

import server.action.general.AttackCard;
import server.dao.DAOFacade;
import shared.definitions.State;
import shared.model.ModelFacade;

public class CruelRulerFight {
	//Defeat a Villain in the City for free

	private DAOFacade facade;
	
	public CruelRulerFight(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int gameID, int location){
		State.CRUEL_RULER_FIGHT.setEffected(gameID, new ArrayList<Integer>());
		State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
		model.getTurnTracker().setState(State.PLAYING);
		new AttackCard(facade).attack(model, location);
	}
}