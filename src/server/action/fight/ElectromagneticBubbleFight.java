package server.action.fight;

import java.util.ArrayList;

import shared.definitions.State;
import shared.model.ModelFacade;

public class ElectromagneticBubbleFight {
	//Choose one of your XMen Heroes. When you draw a new hand of cards at the end of this turn, add that Hero to your hand as a seventh card

	public ElectromagneticBubbleFight(){}
	
	public void go(ModelFacade model, int gameID, String card){
		State.ELECTROMAGNETIC_BUBBLE_FIGHT.setEffected(gameID, new ArrayList<Integer>());
		State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
		model.getTurnTracker().setState(State.PLAYING);
		model.getTurnTracker().setAddToHand(card);
	}
}