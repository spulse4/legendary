package server.action.fight;

import java.util.ArrayList;

import server.action.general.KOCard;
import server.dao.DAOFacade;
import shared.definitions.State;
import shared.model.ModelFacade;

public class SentinelFight {
	//KO one of your Heroes

	private DAOFacade facade;
	
	public SentinelFight(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int gameID, String card){
		State.SENTINEL_FIGHT.setEffected(gameID, new ArrayList<Integer>());
		State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
		model.getTurnTracker().setState(State.PLAYING);
		int index = model.getTurnTracker().getCurrentPlayer();
		new KOCard(facade).go(model, index, card);
	}
}