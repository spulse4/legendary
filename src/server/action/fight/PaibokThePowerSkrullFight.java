package server.action.fight;

import java.util.ArrayList;
import java.util.List;

import server.action.general.ReceiveCard;
import server.dao.DAOFacade;
import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class PaibokThePowerSkrullFight implements ServerVariables{
	//Choose a hero in the HQ for each player. Each player gains that hero

	private DAOFacade facade;
	
	public PaibokThePowerSkrullFight(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int gameID, int playerIndex, int location){
		List<Integer> effected = State.PAIBOK_THE_POWER_SKRULL_FIGHT.getEffected(gameID);
		for(int i = ZERO; i < effected.size(); ++i){
			if(effected.get(i) == playerIndex){
				effected.remove(i);
				break;
			}
		}
		if(effected.size() == ZERO){
			State.PAIBOK_THE_POWER_SKRULL_FIGHT.setEffected(gameID, new ArrayList<Integer>());
			State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
			model.getTurnTracker().setState(State.PLAYING);
		}
		new ReceiveCard(facade).go(model, playerIndex, location);
	}
}