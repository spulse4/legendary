package server.action.fight;

import java.util.ArrayList;
import java.util.List;

import server.action.general.KOCard;
import server.dao.DAOFacade;
import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class ManiacalTyrantFight implements ServerVariables{
	//KO up to four cards from your discard pile

	private DAOFacade facade;
	
	public ManiacalTyrantFight(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int gameID, String card){
		List<Integer> effected = State.MANIACAL_TYRANT_FIGHT.getEffected(gameID);
		effected.remove(ZERO);
		if(card.equals(EMPTY)){
			State.MANIACAL_TYRANT_FIGHT.setEffected(gameID, new ArrayList<Integer>());
			State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
			model.getTurnTracker().setState(State.PLAYING);
			return;
		}
		if(effected.size() == ZERO){
			State.MANIACAL_TYRANT_FIGHT.setEffected(gameID, new ArrayList<Integer>());
			State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
			model.getTurnTracker().setState(State.PLAYING);
		}
		int index = model.getTurnTracker().getCurrentPlayer();
		new KOCard(facade).KODiscard(model, index, card);
	}
}