package server.action.fight;

import java.util.ArrayList;

import server.action.general.KOCard;
import server.dao.DAOFacade;
import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class YmirFrostGiantKingDiscard implements ServerVariables{
	//Choose a player. That player KOs any number of Wounds from their hand and discard pile

	private DAOFacade facade;
	
	public YmirFrostGiantKingDiscard(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int gameID, int playerIndex, int number){
		State.YMIR_FROST_GIANT_KING_DISCARD.setEffected(gameID, new ArrayList<Integer>());
		State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
		model.getTurnTracker().setState(State.PLAYING);
		for(int i = ZERO; i < number; ++i){
			boolean success = new KOCard(facade).KODiscard(model, playerIndex, WOUND);
			if(!success){
				new KOCard(facade).KOHand(model, playerIndex, WOUND);
			}
		}
	}
}