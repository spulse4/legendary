package server.action.fight;

import java.util.ArrayList;

import server.definition.ServerVariables;
import server.exceptions.InsufficientOfficersException;
import shared.definitions.State;
import shared.model.ModelFacade;

public class HydraKidnappersFight implements ServerVariables{
	//You may gain a SHIELD Officer

	public HydraKidnappersFight(){}
	
	public void go(ModelFacade model, int gameID, boolean accept){
		State.HYDRA_KIDNAPPERS_FIGHT.setEffected(gameID, new ArrayList<Integer>());
		State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
		model.getTurnTracker().setState(State.PLAYING);
		if(!accept){
			return;
		}
		int index = model.getTurnTracker().getCurrentPlayer();
		try{
			model.getOtherCards().removeOfficer();
			model.getPlayerCardList().get(index).getDiscard().add(OFFICER);
		}
		catch(InsufficientOfficersException e){}
	}
}