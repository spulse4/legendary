package server.action.fight;

import java.util.ArrayList;
import java.util.List;

import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class RuthlessDictatorDiscard implements ServerVariables{
	//Look at the top three cards of your deck. KO one, discard one and put one back on top of your deck

	public RuthlessDictatorDiscard(){}
	
	public void go(ModelFacade model, int gameID, String card){
		List<Integer> effected = State.RUTHLESS_DICTATOR_DISCARD.getEffected(gameID);
		effected.remove(ZERO);
		State.RUTHLESS_DICTATOR_DISCARD.setEffected(gameID, new ArrayList<Integer>());
		State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
		model.getTurnTracker().setState(State.PLAYING);
		int index = model.getTurnTracker().getCurrentPlayer();
		if(model.getPlayerCardList().get(index).getFromDraw(ZERO).equals(card)){
			model.getPlayerCardList().get(index).getDiscard().add(card);
			model.getPlayerCardList().get(index).removeFromDraw(ZERO);
		}
		else{
			model.getPlayerCardList().get(index).getDiscard().add(card);
			model.getPlayerCardList().get(index).removeFromDraw(ONE);
		}
	}
}