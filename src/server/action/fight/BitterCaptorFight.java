package server.action.fight;

import java.util.ArrayList;

import server.action.general.BuyCard;
import server.dao.DAOFacade;
import shared.definitions.State;
import shared.model.ModelFacade;

public class BitterCaptorFight {
	//Recruit an XMen Hero from the HQ for free

	private DAOFacade facade;
	
	public BitterCaptorFight(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int gameID, int location){
		State.BITTER_CAPTOR_FIGHT.setEffected(gameID, new ArrayList<Integer>());
		State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
		model.getTurnTracker().setState(State.PLAYING);
		int index = model.getTurnTracker().getCurrentPlayer();
		new BuyCard(facade).go(model, index, location, true);
	}
}