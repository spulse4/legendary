package server.action.fight;

import java.util.ArrayList;
import java.util.List;

import server.action.general.KOCard;
import server.dao.DAOFacade;
import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class MelterFight implements ServerVariables{
	//Each player reveals the top card of their deck. For each card, you choose to KO it or put it back

	private DAOFacade facade;
	
	public MelterFight(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int gameID, int playerIndex, boolean ko){
		List<Integer> effected = State.MELTER_FIGHT.getEffected(gameID);
		for(int i = ZERO; i < effected.size(); ++i){
			if(effected.get(i) == playerIndex){
				effected.remove(i);
				break;
			}
		}
		if(effected.size() == ZERO){
			State.MELTER_FIGHT.setEffected(gameID, new ArrayList<Integer>());
			State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
			model.getTurnTracker().setState(State.PLAYING);
		}
		if(ko){
			String card = model.getPlayerCardList().get(playerIndex).getFromDraw(ZERO);
			new KOCard(facade).KODraw(model, playerIndex, card);
		}
	}
}