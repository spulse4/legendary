package server.action.fight;

import java.util.ArrayList;
import java.util.List;

import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class YmirFrostGiantKingFight implements ServerVariables{
	//Choose a player. That player KOs any number of Wounds from their hand and discard pile

	public YmirFrostGiantKingFight(){}
	
	public void go(ModelFacade model, int gameID, int playerIndex){
		State.YMIR_FROST_GIANT_KING_FIGHT.setEffected(gameID, new ArrayList<Integer>());
		List<String> hand = model.getPlayerCardList().get(playerIndex).getHand();
		List<String> discard = model.getPlayerCardList().get(playerIndex).getDiscard();
		boolean success = false;
		for(String str : hand){
			if(str.equals(WOUND)){
				success = true;
				break;
			}
		}
		if(success == false){
			for(String str : discard){
				if(str.equals(WOUND)){
					success = true;
					break;
				}
			}
		}
		if(success){
			List<Integer> effected = new ArrayList<Integer>();
			effected.add(playerIndex);
			State.YMIR_FROST_GIANT_KING_DISCARD.setEffected(gameID, effected);
			model.getTurnTracker().setState(State.YMIR_FROST_GIANT_KING_DISCARD);
		}
		else{
			State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
			model.getTurnTracker().setState(State.PLAYING);
		}
	}
}