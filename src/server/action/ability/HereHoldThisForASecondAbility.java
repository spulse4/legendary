package server.action.ability;

import java.util.ArrayList;

import server.definition.ServerVariables;
import server.exceptions.InsufficientBystandersException;
import shared.definitions.State;
import shared.model.ModelFacade;

public class HereHoldThisForASecondAbility implements ServerVariables{
	//A Villain of your choice captures a Bystander

	public HereHoldThisForASecondAbility(){}
	
	public void go(ModelFacade model, int gameID, int location){
		State.HERE_HOLD_THIS_FOR_A_SECOND_ABILITY.setEffected(gameID, new ArrayList<Integer>());
		State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
		model.getTurnTracker().setState(State.PLAYING);
		try{
			model.getOtherCards().removeBystander(ONE);
			switch(location){
			case 1:
				model.getCity().getSewersCaptured().add(BYSTANDER);
				break;
			case 2:
				model.getCity().getBankCaptured().add(BYSTANDER);
				break;
			case 3:
				model.getCity().getRooftopsCaptured().add(BYSTANDER);
				break;
			case 4:
				model.getCity().getStreetsCaptured().add(BYSTANDER);
				break;
			case 5:
				model.getCity().getBridgeCaptured().add(BYSTANDER);
				break;
			case 6:
				model.getCity().getMastermindCaptured().add(BYSTANDER);
				break;
			}
		}
		catch(InsufficientBystandersException e){}
	}
}