package server.action.ability;

import java.util.ArrayList;
import java.util.List;

import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class SpinningCycloneAbility implements ServerVariables{
	//You may move a Villain to a new city space. Rescue any Bystanders captured by that Villain.
	//(If you move a Villain to a city space that already has a Villain, swap them)

	public SpinningCycloneAbility(){}
	
	public void go(ModelFacade model, int gameID, int startingLocation, int endingLocation){
		State.SPINNING_CYCLONE_ABILITY.setEffected(gameID, new ArrayList<Integer>());
		State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
		model.getTurnTracker().setState(State.PLAYING);
		if(startingLocation == endingLocation){
			return;
		}
		List<String> capturedStart = null;
		String villianStart = null;
		List<String> capturedEnd = null;
		String villianEnd = null;
		switch(startingLocation){
		case 1:
			villianStart = model.getCity().getSewers();
			capturedStart = rescueBystanders(model, model.getCity().getSewersCaptured());
			model.getCity().setSewers(null);
			model.getCity().setSewersCaptured(new ArrayList<String>());
			break;
		case 2:
			villianStart = model.getCity().getBank();
			capturedStart = rescueBystanders(model, model.getCity().getBankCaptured());
			model.getCity().setBank(null);
			model.getCity().setBankCaptured(new ArrayList<String>());
			break;
		case 3:
			villianStart = model.getCity().getRooftops();
			capturedStart = rescueBystanders(model, model.getCity().getRooftopsCaptured());
			model.getCity().setRooftops(null);
			model.getCity().setRooftopsCaptured(new ArrayList<String>());
			break;
		case 4:
			villianStart = model.getCity().getStreets();
			capturedStart = rescueBystanders(model, model.getCity().getStreetsCaptured());
			model.getCity().setRooftops(null);
			model.getCity().setRooftopsCaptured(new ArrayList<String>());
			break;
		case 5:
			villianStart = model.getCity().getBridge();
			capturedStart = rescueBystanders(model, model.getCity().getBridgeCaptured());
			model.getCity().setBridge(null);
			model.getCity().setBridgeCaptured(new ArrayList<String>());
			break;
		}
		switch(endingLocation){
		case 1:
			villianEnd = model.getCity().getSewers();
			capturedEnd = rescueBystanders(model, model.getCity().getSewersCaptured());
			model.getCity().setSewers(villianStart);
			model.getCity().setSewersCaptured(capturedStart);
			break;
		case 2:
			villianEnd = model.getCity().getBank();
			capturedEnd = rescueBystanders(model, model.getCity().getBankCaptured());
			model.getCity().setBank(villianStart);
			model.getCity().setBankCaptured(capturedStart);
			break;
		case 3:
			villianEnd = model.getCity().getRooftops();
			capturedEnd = rescueBystanders(model, model.getCity().getRooftopsCaptured());
			model.getCity().setRooftops(villianStart);
			model.getCity().setRooftopsCaptured(capturedStart);
			break;
		case 4:
			villianEnd = model.getCity().getStreets();
			capturedEnd = rescueBystanders(model, model.getCity().getStreetsCaptured());
			model.getCity().setStreets(villianStart);
			model.getCity().setStreetsCaptured(capturedStart);
			break;
		case 5:
			villianEnd = model.getCity().getBridge();
			capturedEnd = rescueBystanders(model, model.getCity().getBridgeCaptured());
			model.getCity().setBridge(villianStart);
			model.getCity().setBridgeCaptured(capturedStart);
			break;
		}	
		
		switch(startingLocation){
		case 1:
			model.getCity().setSewers(villianEnd);
			model.getCity().setSewersCaptured(capturedEnd);
			break;
		case 2:
			model.getCity().setBank(villianEnd);
			model.getCity().setBankCaptured(capturedEnd);
			break;
		case 3:
			model.getCity().setRooftops(villianEnd);
			model.getCity().setRooftopsCaptured(capturedEnd);
			break;
		case 4:
			model.getCity().setStreets(villianEnd);
			model.getCity().setStreetsCaptured(capturedEnd);
			break;
		case 5:
			model.getCity().setBridge(villianEnd);
			model.getCity().setBridgeCaptured(capturedEnd);
			break;
		}	
	}
	
	private List<String> rescueBystanders(ModelFacade model, List<String> captured){
		int index = model.getTurnTracker().getCurrentPlayer();
		List<String> returned = new ArrayList<String>();
		for(String str : captured){
			if(str.equals(BYSTANDER)){
				model.getPlayerCardList().get(index).getVictoryPile().add(BYSTANDER);
			}
			else{
				returned.add(str);
			}
		}
		return returned;
	}
}