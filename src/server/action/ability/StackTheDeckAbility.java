package server.action.ability;

import java.util.ArrayList;

import server.action.general.PlaceCardOnDeck;
import shared.definitions.State;
import shared.model.ModelFacade;

public class StackTheDeckAbility {
	//Draw two cards. Then put a card from your hand on top of your deck

	public StackTheDeckAbility(){}
	
	public void go(ModelFacade model, int gameID, String card){
		State.STACK_THE_DECK_ABILITY.setEffected(gameID, new ArrayList<Integer>());
		State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
		model.getTurnTracker().setState(State.PLAYING);
		int index = model.getTurnTracker().getCurrentPlayer();
		new PlaceCardOnDeck().go(model, index, card);
	}
}