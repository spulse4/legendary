package server.action.ability;

import java.util.ArrayList;
import java.util.List;

import server.action.general.CardAbility;
import server.action.general.PlayCard;
import server.dao.DAOFacade;
import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class CopyPowersAbility implements ServerVariables{
	//Play this card as a copy of another Hero you played this turn. This card is both Red and the color you copy

	private DAOFacade facade;
	
	public CopyPowersAbility(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int gameID, String card){
		State.COPY_POWERS_ABILITY.setEffected(gameID, new ArrayList<Integer>());
		State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
		model.getTurnTracker().setState(State.PLAYING);
		int index = model.getTurnTracker().getCurrentPlayer();
		
		List<String> played = model.getPlayerCardList().get(index).getPlayed();
		for(int i = ZERO; i < played.size(); ++i){
			if(played.get(i).equals(COPY_POWERS)){
				played.set(i, played.get(i) + DELIMITER + card);
				new PlayCard(facade).increasePendingPoints(model, card);
				new CardAbility(facade).go(model, card);
				break;
			}
		}
	}
}