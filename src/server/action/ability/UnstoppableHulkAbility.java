package server.action.ability;

import java.util.ArrayList;

import server.action.general.KOCard;
import server.dao.DAOFacade;
import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class UnstoppableHulkAbility implements ServerVariables{
	//You may KO a Wound from your hand or discard pile. If you do, you get +2 Attack Power

	private DAOFacade facade;
	
	public UnstoppableHulkAbility(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int gameID, boolean accept){
		State.UNSTOPPABLE_HULK_ABILITY.setEffected(gameID, new ArrayList<Integer>());		
		State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
		model.getTurnTracker().setState(State.PLAYING);
		if(!accept){
			return;
		}
		int index = model.getTurnTracker().getCurrentPlayer();
		boolean success = new KOCard(facade).KODiscard(model, index, WOUND);
		if(!success){
			new KOCard(facade).KOHand(model, index, WOUND);
		}
		int pendingAttack = model.getPlayerCardList().get(index).getPendingAttackPoints();
		model.getPlayerCardList().get(index).setPendingAttackPoints(pendingAttack + TWO);
	}
}