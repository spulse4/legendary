package server.action.ability;

import java.util.ArrayList;
import java.util.List;

import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class HypnoticCharmAbility implements ServerVariables{
	//Reveal the top card of your deck. Discard it or put it back.
	//Yellow: Do the same thing to each other player's deck

	public HypnoticCharmAbility(){}
	
	public void go(ModelFacade model, int gameID, int playerIndex, boolean discard){
		List<Integer> effected = State.HYPNOTIC_CHARM_ABILITY.getEffected(gameID);
		for(int i = ZERO; i < effected.size(); ++i){
			if(effected.get(i) == playerIndex){
				effected.remove(i);
				break;
			}
		}
		if(effected.size() == ZERO){
			State.HYPNOTIC_CHARM_ABILITY.setEffected(gameID, new ArrayList<Integer>());
			State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
			model.getTurnTracker().setState(State.PLAYING);
		}
		if(discard){
			String card = model.getPlayerCardList().get(playerIndex).getFromDraw(ZERO);
			model.getPlayerCardList().get(playerIndex).removeFromDraw(ZERO);
			model.getPlayerCardList().get(playerIndex).getDiscard().add(card);
		}
	}
}