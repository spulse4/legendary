package server.action.ability;

import java.util.ArrayList;

import server.action.general.KOCard;
import server.dao.DAOFacade;
import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class EnergyDrainAbility implements ServerVariables{
	//Red: You may KO a card from your hand or discard pile. If you do, you get +1 Buy Power

	private DAOFacade facade;
	
	public EnergyDrainAbility(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int gameID, String card){
		State.ENERGY_DRAIN_ABILITY.setEffected(gameID, new ArrayList<Integer>());
		State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
		model.getTurnTracker().setState(State.PLAYING);
		if(card.equals(EMPTY)){
			return;
		}
		int index = model.getTurnTracker().getCurrentPlayer();
		boolean success = new KOCard(facade).KODiscard(model, index, card);
		if(!success){
			new KOCard(facade).KOHand(model, index, card);
		}
		int pendingBuy = model.getPlayerCardList().get(index).getPendingBuyPoints();
		model.getPlayerCardList().get(index).setPendingBuyPoints(pendingBuy + ONE);
	}
}