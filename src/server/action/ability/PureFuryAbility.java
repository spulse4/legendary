package server.action.ability;

import java.util.ArrayList;

import server.action.general.AttackCard;
import server.dao.DAOFacade;
import shared.definitions.State;
import shared.model.ModelFacade;

public class PureFuryAbility {
	//Defeat any Villain or Mastermind whose Hit Points is less than the number of SHIELD Heroes in the KO pile

	private DAOFacade facade;
	
	public PureFuryAbility(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int gameID, int cardIndex){
		State.PURE_FURY_ABILITY.setEffected(gameID, new ArrayList<Integer>());
		State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
		model.getTurnTracker().setState(State.PLAYING);
		new AttackCard(facade).attack(model, cardIndex);
	}
}