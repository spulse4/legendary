package server.action.ability;

import java.util.ArrayList;

import server.action.general.DrawCard;
import server.action.general.KOCard;
import server.dao.DAOFacade;
import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class HealingFactorAbility implements ServerVariables{
	//You may KO a Wound from your hand or discard pile. If you do, draw a card

	private DAOFacade facade;
	
	public HealingFactorAbility(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int gameID, boolean accept){
		State.HEALING_FACTOR_ABILITY.setEffected(gameID, new ArrayList<Integer>());
		State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
		model.getTurnTracker().setState(State.PLAYING);
		if(!accept){
			return;
		}
		int index = model.getTurnTracker().getCurrentPlayer();
		boolean success = new KOCard(facade).KODiscard(model, index, WOUND);
		if(!success){
			new KOCard(facade).KOHand(model, index, WOUND);
		}
		new DrawCard().go(model, index);
	}
}