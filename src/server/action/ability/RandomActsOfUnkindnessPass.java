package server.action.ability;

import java.util.ArrayList;
import java.util.List;

import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class RandomActsOfUnkindnessPass implements ServerVariables{
	//You may gain a Wound to your hand. Then each player passes a card from their hand to the player on their left

	public RandomActsOfUnkindnessPass(){}
	
	public void go(ModelFacade model, int gameID, int playerIndex, String card){
		List<Integer> effected = State.RANDOM_ACTS_OF_UNKINDNESS_PASS.getEffected(gameID);
		for(int i = ZERO; i < effected.size(); ++i){
			if(effected.get(i) == playerIndex){
				effected.remove(i);
				break;
			}
		}
		for(int i = ZERO; i < model.getPlayerCardList().get(playerIndex).getHand().size(); ++i){
			if(model.getPlayerCardList().get(playerIndex).getHand().get(i).equals(card)){
				model.getPlayerCardList().get(playerIndex).getHand().remove(i);
			}
		}
		List<String> pendingCards = model.getTurnTracker().getPendingCards();
		int index = playerIndex;
		if(playerIndex > model.getTurnTracker().getCurrentPlayer()){
			--index;
		}
		pendingCards.set(index, card);
		if(effected.size() == ZERO){
			State.RANDOM_ACTS_OF_UNKINDNESS_PASS.setEffected(gameID, new ArrayList<Integer>());
			State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
			model.getTurnTracker().setState(State.PLAYING);
			int current = model.getTurnTracker().getCurrentPlayer();
			for(int i = ZERO; i < model.getPlayerCardList().size(); ++i){
				if(i == current){
					continue;
				}
				int takeFrom = i - ONE;
				if(i > current){
					--takeFrom;
				}
				if(takeFrom < ZERO){
					takeFrom = pendingCards.size() - ONE;
				}
				model.getPlayerCardList().get(i).getHand().add(pendingCards.get(takeFrom));
			}
			model.getTurnTracker().setPendingCards(new ArrayList<String>());
		}
	}
}