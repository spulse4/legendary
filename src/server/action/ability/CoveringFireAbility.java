package server.action.ability;

import java.util.ArrayList;
import java.util.List;

import server.action.general.DrawCard;
import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class CoveringFireAbility implements ServerVariables{
	//Black: Choose one: each other player draws a card or each other player discards a card

	public CoveringFireAbility(){}
	
	public void go(ModelFacade model, int gameID, boolean draw){
		State.COVERING_FIRE_ABILITY.setEffected(gameID, new ArrayList<Integer>());
		int index = model.getTurnTracker().getCurrentPlayer();

		if(draw){
			for(int i = ZERO; i < model.getPlayerCardList().size(); ++i){
				if(i == index){
					continue;
				}
				new DrawCard().go(model, i);
			}
			State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
			model.getTurnTracker().setState(State.PLAYING);
		}
		else{
			List<Integer> effected = new ArrayList<Integer>();
			for(int i = ZERO; i < model.getPlayerCardList().size(); ++i){
				if(i == index){
					continue;
				}
				effected.add(i);
			}
			if(effected.size() != ZERO){
				State.COVERING_FIRE_DISCARD.setEffected(gameID, effected);
				model.getTurnTracker().setState(State.COVERING_FIRE_DISCARD);
			}
			else{
				State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
				model.getTurnTracker().setState(State.PLAYING);
			}
		}
	}
}