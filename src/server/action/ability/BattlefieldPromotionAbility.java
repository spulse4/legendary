package server.action.ability;

import java.util.ArrayList;

import server.action.general.KOCard;
import server.dao.DAOFacade;
import server.definition.ServerVariables;
import server.exceptions.InsufficientOfficersException;
import shared.definitions.State;
import shared.model.ModelFacade;

public class BattlefieldPromotionAbility implements ServerVariables{
	//You may KO a SHIELD Hero from your hand or discard pile. If you do, you may gain a SHIELD Officer to your hand.

	private DAOFacade facade;
	
	public BattlefieldPromotionAbility(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int gameID, String card){
		State.BATTLEFIELD_PROMOTION_ABILITY.setEffected(gameID, new ArrayList<Integer>());
		State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
		model.getTurnTracker().setState(State.PLAYING);
		if(card.equals(EMPTY)){
			return;
		}
		int index = model.getTurnTracker().getCurrentPlayer();
		boolean found = new KOCard(facade).KODiscard(model, index, card);
		if(!found){
			new KOCard(facade).KOHand(model, index, card);
		}
		
		try{
			model.getOtherCards().removeOfficer();
			model.getPlayerCardList().get(index).getHand().add(OFFICER);
		}
		catch(InsufficientOfficersException e){}
	}
}