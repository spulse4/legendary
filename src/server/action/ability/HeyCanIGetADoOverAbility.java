package server.action.ability;

import java.util.ArrayList;

import server.action.general.DiscardCard;
import server.action.general.DrawCard;
import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class HeyCanIGetADoOverAbility implements ServerVariables{
	//If this is the first Hero you played this turn, you may discard the rest of your hand and draw four cards

	public HeyCanIGetADoOverAbility(){}
	
	public void go(ModelFacade model, int gameID, boolean accept){
		State.HEY_CAN_I_GET_A_DO_OVER_ABILITY.setEffected(gameID, new ArrayList<Integer>());
		State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
		model.getTurnTracker().setState(State.PLAYING);
		if(!accept){
			return;
		}
		int index = model.getTurnTracker().getCurrentPlayer();
		while(model.getPlayerCardList().get(index).getHand().size() != ZERO){
			new DiscardCard().go(model, index, model.getPlayerCardList().get(index).getHand().get(ZERO));
		}
		new DrawCard().go(model, index);
		new DrawCard().go(model, index);
		new DrawCard().go(model, index);
		new DrawCard().go(model, index);
	}
}