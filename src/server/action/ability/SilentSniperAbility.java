package server.action.ability;

import java.util.ArrayList;

import server.action.general.AttackCard;
import server.dao.DAOFacade;
import shared.definitions.State;
import shared.model.ModelFacade;

public class SilentSniperAbility {
	//Defeat a Villain or Mastermind that has a Bystander
	
	private DAOFacade facade;

	public SilentSniperAbility(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int gameID, int location){
		State.SILENT_SNIPER_ABILITY.setEffected(gameID, new ArrayList<Integer>());
		State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
		model.getTurnTracker().setState(State.PLAYING);
		new AttackCard(facade).attack(model, location);
	}
}