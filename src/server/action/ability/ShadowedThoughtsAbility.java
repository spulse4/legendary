package server.action.ability;

import java.util.ArrayList;

import server.action.general.PlayVillianCard;
import server.dao.DAOFacade;
import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class ShadowedThoughtsAbility implements ServerVariables{
	//Red: You may play the top card of the Villain Deck. If you do, you get +2 Attack Power

	private DAOFacade facade;
	
	public ShadowedThoughtsAbility(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int gameID, boolean accept){
		State.SHADOWED_THOUGHTS_ABILITY.setEffected(gameID, new ArrayList<Integer>());
		State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
		model.getTurnTracker().setState(State.PLAYING);
		if(!accept){
			return;
		}
		new PlayVillianCard(facade).go(model);
		int index = model.getTurnTracker().getCurrentPlayer();
		int pendingAttack = model.getPlayerCardList().get(index).getPendingAttackPoints();
		model.getPlayerCardList().get(index).setPendingAttackPoints(pendingAttack + TWO);
	}
}