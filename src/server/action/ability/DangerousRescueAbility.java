package server.action.ability;

import java.util.ArrayList;

import server.action.general.DiscardCard;
import server.definition.ServerVariables;
import server.exceptions.InsufficientBystandersException;
import shared.definitions.State;
import shared.model.ModelFacade;

public class DangerousRescueAbility implements ServerVariables{
	//Red: You may KO a card from your hand or discard pile. If you do, rescue a Bystander.

	public DangerousRescueAbility(){}
	
	public void go(ModelFacade model, int gameID, String card){
		State.DANGEROUS_RESCUE_ABILITY.setEffected(gameID, new ArrayList<Integer>());
		State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
		model.getTurnTracker().setState(State.PLAYING);
		if(card.equals(EMPTY)){
			return;
		}
		int index = model.getTurnTracker().getCurrentPlayer();
		new DiscardCard().go(model, index, card);
		try{
			model.getOtherCards().removeBystander(ONE);
			model.getPlayerCardList().get(index).getVictoryPile().add(BYSTANDER);
		}
		catch(InsufficientBystandersException e){}
	}
}