package server.action.ability;

import java.util.ArrayList;
import java.util.List;

import server.action.general.DiscardCard;
import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class CoveringFireDiscard implements ServerVariables{
	//Black: Choose one: each other player draws a card or each other player discards a card

	public CoveringFireDiscard(){}
	
	public void go(ModelFacade model, int gameID, int playerIndex, String card){
		List<Integer> effected = State.COVERING_FIRE_DISCARD.getEffected(gameID);
		for(int i = ZERO; i < effected.size(); ++i){
			if(effected.get(i) == playerIndex){
				effected.remove(i);
				new DiscardCard().go(model, playerIndex, card);
				break;
			}
		}
		if(effected.size() == ZERO){
			State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
			model.getTurnTracker().setState(State.PLAYING);
		}
	}
}