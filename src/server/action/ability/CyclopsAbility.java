package server.action.ability;

import java.util.ArrayList;

import server.action.general.DiscardCard;
import shared.definitions.State;
import shared.model.ModelFacade;

public class CyclopsAbility {
	//To play this card, you must discard a card from your hand

	public CyclopsAbility(){}
	
	public void go(ModelFacade model, int gameID, String card){
		State.CYCLOPS_ABILITY.setEffected(gameID, new ArrayList<Integer>());
		State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
		model.getTurnTracker().setState(State.PLAYING);
		int index = model.getTurnTracker().getCurrentPlayer();
		new DiscardCard().go(model, index, card);
	}
}