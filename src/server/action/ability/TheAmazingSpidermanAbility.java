package server.action.ability;

import java.util.ArrayList;
import java.util.List;

import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class TheAmazingSpidermanAbility implements ServerVariables{
	//Reveal the top three cards of your deck. Put any that cost 2 or less into your hand. Put the rest back in any order

	public TheAmazingSpidermanAbility(){}
	
	public void go(ModelFacade model, int gameID, String card){
		List<Integer> effected = State.THE_AMAZING_SPIDERMAN_ABILITY.getEffected(gameID);
		effected.remove(effected.size() - ONE);
		if(effected.size() == ZERO){
			State.THE_AMAZING_SPIDERMAN_ABILITY.setEffected(gameID, new ArrayList<Integer>());
			State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
			model.getTurnTracker().setState(State.PLAYING);
		}
		int index = model.getTurnTracker().getCurrentPlayer();
		int location = ZERO;
		for(int i = ZERO; i < (effected.size() + ONE); ++i){
			if(model.getPlayerCardList().get(index).getFromDraw(i).equals(card)){
				location = i;
				break;
			}
		}
		String last = model.getPlayerCardList().get(index).getFromDraw(effected.size());
		model.getPlayerCardList().get(index).setInDraw(effected.size(), card);
		model.getPlayerCardList().get(index).setInDraw(location, last);
	}
}