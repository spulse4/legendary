package server.action.ability;

import java.util.ArrayList;
import java.util.List;

import server.definition.ServerVariables;
import server.exceptions.InsufficientWoundsException;
import shared.definitions.State;
import shared.model.ModelFacade;
import shared.model.OtherCards;

public class RandomActsOfUnkindnessAbility implements ServerVariables{
	//You may gain a Wound to your hand. Then each player passes a card from their hand to the player on their left

	public RandomActsOfUnkindnessAbility(){}
	
	public void go(ModelFacade model, int gameID, boolean accept){
		State.RANDOM_ACTS_OF_UNKINDNESS_ABILITY.setEffected(gameID, new ArrayList<Integer>());
		State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
		model.getTurnTracker().setState(State.PLAYING);
		
		if(!accept){
			return;
		}
		int index = model.getTurnTracker().getCurrentPlayer();
		try{
			OtherCards cards = model.getOtherCards();
			List<String> hand = model.getPlayerCardList().get(index).getHand();
			model.getPlayerCardList().get(index).addWound(cards, hand);
		}
		catch(InsufficientWoundsException e){}
		List<Integer> effected = new ArrayList<Integer>();
		for(int i = ZERO; i < model.getPlayerCardList().size(); ++i){
			if(i == index){
				continue;
			}
			effected.add(i);
		}
		List<String> pendingCards = new ArrayList<String>();
		for(int i = ZERO; i < model.getPlayerCardList().size(); ++i){
			if(i == index){
				continue;
			}
			pendingCards.add(EMPTY);
		}
		model.getTurnTracker().setPendingCards(new ArrayList<String>());
		if(effected.size() != 0){
			State.RANDOM_ACTS_OF_UNKINDNESS_PASS.setEffected(gameID, effected);
			model.getTurnTracker().setState(State.RANDOM_ACTS_OF_UNKINDNESS_PASS);
		}
	}
}