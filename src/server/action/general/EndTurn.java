package server.action.general;

import server.dao.DAOFacade;
import server.definition.ServerVariables;
import shared.log.Log;
import shared.model.ModelFacade;

public class EndTurn implements ServerVariables{
	
	private final String CLASS = "server.action.EndTurn.java: ";

	private DAOFacade facade;
	
	public EndTurn(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int playerIndex){		
		Log.logError(model.getGameID(), CLASS + playerIndex + SPACE + LOG_END_TURN);
		int numberOfPlayers = model.getPlayerCardList().size();

		if(model.getTurnTracker().getCurrentPlayer() != playerIndex){
			return;
		}
		replaceHand(model);
		
		++playerIndex;
		if(playerIndex >= numberOfPlayers){
			playerIndex = ZERO;
		}
		if(model.getTurnTracker().getNextPlayer() != NEGATIVE_ONE){
			playerIndex = model.getTurnTracker().getNextPlayer();
			model.getTurnTracker().setNextPlayer(NEGATIVE_ONE);
		}
		model.getTurnTracker().resetCardsInEffect();
		model.getTurnTracker().setCurrentPlayer(playerIndex);
		
		new PlayVillianCard(facade).go(model);
	}
	
	private void replaceHand(ModelFacade model){
		int index = model.getTurnTracker().getCurrentPlayer();
		int nextHand = model.getTurnTracker().getNextHandCount();
		String addToHand = model.getTurnTracker().getAddToHand();
		model.getTurnTracker().setAddToHand(null);
		model.getTurnTracker().setNextHandCount(SIX);
		
		if(model.getPlayerCardList().get(index).getPlayed().size() == ZERO){
			while(new KOCard(facade).KOHand(model, index, WOUND)){}
		}
		
		model.getPlayerCardList().get(index).replaceHand(nextHand, addToHand, model);
	}
}
