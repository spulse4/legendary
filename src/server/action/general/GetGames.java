package server.action.general;

import java.util.List;

import server.dao.DAOFacade;
import shared.definitions.GameHeader;

public class GetGames {

	private DAOFacade facade;
	
	public GetGames(DAOFacade facade){
		this.facade = facade;
	}
	
	public List<GameHeader> go(){
		return facade.getGames();
	}
}
