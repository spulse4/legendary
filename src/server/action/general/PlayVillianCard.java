package server.action.general;

import java.util.ArrayList;
import java.util.List;

import server.dao.DAOFacade;
import server.definition.ServerCard;
import server.definition.ServerLoader;
import server.definition.ServerVariables;
import shared.log.Log;
import shared.model.ModelFacade;

public class PlayVillianCard implements ServerVariables{
	
	private final String CLASS = "server.action.PlayVillianCard.java: ";

	private DAOFacade facade;
	
	public PlayVillianCard(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model){
		List<String> villians = model.getOtherCards().getVillians();
		if(villians.size() == ZERO){
			new FinishGame().go(model, false);
			return;
		}
		String villian = villians.get(ZERO);
		if(TESTING){
			villian = ServerLoader.loadVillian();
		}
		else{
			villians.remove(ZERO);
		}
		Log.logError(model.getGameID(), CLASS + LOG_VILLIAN_PLAYED + villian);
		moveCards(model, villian);
	}
	
	public void moveCards(ModelFacade model, String villian){
		String ambush = villian;
		
		List<String> cap1 = new ArrayList<String>();
		String villian2 = model.getCity().getSewers();
		List<String> cap2 = model.getCity().getSewersCaptured();
		if(villian.equals(SCHEME_TWIST)){
			new SchemeTwist(facade).go(model);
			return;
		}
		else if(villian.equals(MASTERSTRIKE)){
			new MasterStrike(facade).go(model);
			return;
		}
		else if(villian.equals(BYSTANDER) && !model.getTurnTracker().getScheme().equals(REPLACE_EARTHS_LEADERS_WITH_KILLBOTS)){
			if(model.getCity().getSewers() != null){
				model.getCity().getSewersCaptured().add(villian);
			}
			else if(model.getCity().getBank() != null){
				model.getCity().getBankCaptured().add(villian);
			}
			else if(model.getCity().getRooftops() != null){
				model.getCity().getRooftopsCaptured().add(villian);
			}
			else if(model.getCity().getStreets() != null){
				model.getCity().getStreetsCaptured().add(villian);
			}
			else if(model.getCity().getBridge() != null){
				model.getCity().getBridgeCaptured().add(villian);
			}
			else{
				model.getCity().getMastermindCaptured().add(villian);
			}
			return;
		}
		model.getCity().setSewers(villian);
		model.getCity().setSewersCaptured(cap1);
		cap1 = new ArrayList<String>();
		villian = villian2;
		for(String str : cap2){
			cap1.add(str);
		}
		villian2 = model.getCity().getBank();
		cap2 = model.getCity().getBankCaptured();
		if(villian != null){
			model.getCity().setBank(villian);
			model.getCity().setBankCaptured(cap1);
			villian = villian2;
			cap1 = new ArrayList<String>();
			for(String str : cap2){
				cap1.add(str);
			}
			villian2 = model.getCity().getRooftops();
			cap2 = model.getCity().getRooftopsCaptured();
			if(villian != null){
				model.getCity().setRooftops(villian);
				model.getCity().setRooftopsCaptured(cap1);
				villian = villian2;
				cap1 = new ArrayList<String>();
				for(String str : cap2){
					cap1.add(str);
				}
				villian2 = model.getCity().getStreets();
				cap2 = model.getCity().getStreetsCaptured();
				if(villian != null){
					model.getCity().setStreets(villian);
					model.getCity().setStreetsCaptured(cap1);
					villian = villian2;
					cap1 = new ArrayList<String>();
					for(String str : cap2){
						cap1.add(str);
					}
					villian2 = model.getCity().getBridge();
					cap2 = model.getCity().getBridgeCaptured();
					if(villian != null){
						model.getCity().setBridge(villian);
						model.getCity().setBridgeCaptured(cap1);
						villian = villian2;
						cap1 = new ArrayList<String>();
						for(String str : cap2){
							cap1.add(str);
						}
						if(villian != null){
							for(String str : cap1){
								updateEscaped(model, facade.getCard(str));
							}
							updateEscaped(model, facade.getCard(villian));
							new Escape(facade).go(model, villian);
						}
					}
				}
			}
		}
		
		new Ambush(facade).go(model, ambush);
	}
	
	private void updateEscaped(ModelFacade model, ServerCard card){
		switch(card.getCategory()){
		case CATEGORY_BROTHERHOOD:
			model.getEscaped().setBrotherhood(model.getEscaped().getBrotherhood() + ONE);
			break;
		case CATEGORY_ENEMIES_OF_ASGARD:
			model.getEscaped().setEnemiesOfAsgard(model.getEscaped().getEnemiesOfAsgard() + ONE);
			break;
		case CATEGORY_HYDRA:
			model.getEscaped().setHydra(model.getEscaped().getHydra() + ONE);
			break;
		case CATEGORY_MASTERS_OF_EVIL:
			model.getEscaped().setMastersOfEvil(model.getEscaped().getMastersOfEvil() + ONE);
			break;
		case CATEGORY_RADIATION:
			model.getEscaped().setRadiation(model.getEscaped().getRadiation() + ONE);
			break;
		case CATEGORY_SKRULLS:
			model.getEscaped().setSkrulls(model.getEscaped().getSkrulls() + ONE);
			break;
		case CATEGORY_SPIDER_FOES:
			model.getEscaped().setSpiderfoes(model.getEscaped().getSpiderfoes() + ONE);
			break;
		case CATEGORY_HENCHMEN:
			model.getEscaped().setHenchmen(model.getEscaped().getHenchmen() + ONE);
			break;
		case NA:
			if(model.getTurnTracker().getScheme().equals(MIDTOWN_BANK_ROBBERY)){
				if(model.getEscaped().getBystanders() + ONE >= EIGHT){
					new FinishGame().go(model, false);
				}
			}
			model.getEscaped().setBystanders(model.getEscaped().getBystanders() + ONE);
			break;
		default:
			model.getEscaped().setHeroes(model.getEscaped().getHeroes() + ONE);
			break;
		}
		model.getEscaped().setMostRecent(card.getName());
		
		if(model.getTurnTracker().getScheme().equals(NEGATIVE_ZONE_PRISON_BREAKOUT)){
			int count = ZERO;
			count += model.getEscaped().getBrotherhood();
			count += model.getEscaped().getEnemiesOfAsgard();
			count += model.getEscaped().getHenchmen();
			count += model.getEscaped().getHydra();
			count += model.getEscaped().getMastersOfEvil();
			count += model.getEscaped().getRadiation();
			count += model.getEscaped().getSkrulls();
			count += model.getEscaped().getSpiderfoes();
			if(count >= TWELVE){
				new FinishGame().go(model, false);
			}
		}
		if(model.getTurnTracker().getScheme().equals(REPLACE_EARTHS_LEADERS_WITH_KILLBOTS)){
			if(model.getEscaped().getBystanders() >= FIVE){
				new FinishGame().go(model, false);
			}
		}
		if(model.getTurnTracker().getScheme().equals(SECRET_INVASION_OF_THE_SKRULL_SHAPESHIFTERS)){
			if(model.getEscaped().getHeroes() >= SIX){
				new FinishGame().go(model, false);
			}
		}
	}
}
