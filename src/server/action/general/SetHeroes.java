package server.action.general;

import java.util.List;

import shared.model.ModelFacade;

public class SetHeroes {

	public SetHeroes(){}
	
	public void go(ModelFacade model, List<String> heroes){
		model.getTurnTracker().setHeroes(heroes);
	}
}
