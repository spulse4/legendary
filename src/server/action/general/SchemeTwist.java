package server.action.general;

import java.util.List;

import server.dao.DAOFacade;
import server.definition.ServerCard;
import server.definition.ServerVariables;
import server.exceptions.InsufficientBystandersException;
import server.exceptions.InsufficientWoundsException;
import shared.log.Log;
import shared.model.ModelFacade;
import shared.model.OtherCards;
import shared.model.PlayerCards;

public class SchemeTwist implements ServerVariables{

	private final String CLASS = "server.action.SchemeTwist: ";
	private DAOFacade facade;
	
	public SchemeTwist(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model){
		model.getOtherCards().setSchemeTwists(model.getOtherCards().getSchemeTwists() + ONE);
		
		String scheme = model.getTurnTracker().getScheme();
		switch(scheme){
		case MIDTOWN_BANK_ROBBERY:
			midtownBankRobbery(model);
			break;
		case NEGATIVE_ZONE_PRISON_BREAKOUT:
			negativeZonePrisonBreakout(model);
			break;
		case PORTALS_TO_THE_DARK_DIMENSION:
			portalsToTheDarkDimension(model);
			break;
		case REPLACE_EARTHS_LEADERS_WITH_KILLBOTS:
			replaceEarthsLeadersWithKillbots(model);
			break;
		case SECRET_INVASION_OF_THE_SKRULL_SHAPESHIFTERS:
			secretInvasionOfTheSkrullShapeshifters(model);
			break;
		case SUPER_HERO_CIVIL_WAR:
			superHeroCivilWar(model);
			break;
		case THE_LEGACY_VIRUS:
			theLegacyVirus(model);
			break;
		case UNLEASH_THE_POWER_OF_THE_COSMIC_CUBE:
			unleashThePowerOfTheCosmicCube(model);
			break;
		default:
			Log.logError(ZERO, CLASS + scheme + ERROR_NOT_FOUND);
		}
	}
	
	private void midtownBankRobbery(ModelFacade model){
		//Twist: Any Villain in the Bank captures 2 Bystanders. Then play the top card of the Villain Deck
		//Evil Wins: When 8 Bystanders are carried away by escaping Villains
		String bank = model.getCity().getBank();
		if(bank != null){
			try{
				model.getOtherCards().removeBystander(ONE);
				model.getCity().getBankCaptured().add(BYSTANDER);
				model.getOtherCards().removeBystander(ONE);
				model.getCity().getBankCaptured().add(BYSTANDER);
			}
			catch(InsufficientBystandersException e){}
		}
		new PlayVillianCard(facade).go(model);
	}
	private void negativeZonePrisonBreakout(ModelFacade model){
		//Twist: Play the top 2 cards of the Villain Deck
		//Evil Wins: If 12 Villains Escape
		new PlayVillianCard(facade).go(model);
		new PlayVillianCard(facade).go(model);
	}
	private void portalsToTheDarkDimension(ModelFacade model){
		//Twist 1: Put the Dark Portal above the Mastermind. The Mastermind gets +1 HP
		//Twist 2-6: Put the Dark Portal in the leftmost city space that doesn't yet have a Dark Portal. Villains in that city space get +1 HP
		//Twist 7: Evil Wins
		
		int number = model.getOtherCards().getSchemeTwists();
		switch(number){
		case ONE:
			model.getCity().setMastermindPortal(true);
			break;
		case TWO:
			model.getCity().setBridgePortal(true);
			break;
		case THREE:
			model.getCity().setStreetsPortal(true);
			break;
		case FOUR:
			model.getCity().setRooftopsPortal(true);
			break;
		case FIVE:
			model.getCity().setBankPortal(true);
			break;
		case SIX:
			model.getCity().setSewersPortal(true);
			break;
		case SEVEN:
			new FinishGame().go(model, false);
			break;
		}
	}
	private void replaceEarthsLeadersWithKillbots(ModelFacade model){
		//Twist: Put the Twist next to this Scheme
		//Evil Wins: If 5 "Killbots" escape
	}
	private void secretInvasionOfTheSkrullShapeshifters(ModelFacade model){
		//Twist: The highest-cost Hero from the HQ moves into the Sewers as a Skrull Villain, as above
		//Evil Wins: If 6 Heroes get into the Escaped Villains Pile
		
		ServerCard hq1 = facade.getCard(model.getHeadQuarters().getHQ1());
		ServerCard hq2 = facade.getCard(model.getHeadQuarters().getHQ2());
		ServerCard hq3 = facade.getCard(model.getHeadQuarters().getHQ3());
		ServerCard hq4 = facade.getCard(model.getHeadQuarters().getHQ4());
		ServerCard hq5 = facade.getCard(model.getHeadQuarters().getHQ5());
		int highest = Integer.valueOf(hq1.getPrice());
		if(Integer.valueOf(hq2.getPrice()) > highest){
			highest = Integer.valueOf(hq2.getPrice());
		}
		if(Integer.valueOf(hq3.getPrice()) > highest){
			highest = Integer.valueOf(hq3.getPrice());
		}
		if(Integer.valueOf(hq4.getPrice()) > highest){
			highest = Integer.valueOf(hq4.getPrice());
		}
		if(Integer.valueOf(hq5.getPrice()) > highest){
			highest = Integer.valueOf(hq5.getPrice());
		}
		
		String newCard = model.getOtherCards().getHeroes().get(0);
		model.getOtherCards().getHeroes().remove(0);

		String villian = EMPTY;
		if(Integer.valueOf(hq1.getPrice()) == highest){
			villian = hq1.getName();
			model.getHeadQuarters().setHQ1(newCard);
		}
		else if(Integer.valueOf(hq2.getPrice()) == highest){
			villian = hq2.getName();
			model.getHeadQuarters().setHQ2(newCard);
		}
		else if(Integer.valueOf(hq3.getPrice()) == highest){
			villian = hq3.getName();
			model.getHeadQuarters().setHQ3(newCard);
		}
		else if(Integer.valueOf(hq4.getPrice()) == highest){
			villian = hq4.getName();
			model.getHeadQuarters().setHQ4(newCard);
		}
		else if(Integer.valueOf(hq5.getPrice()) == highest){
			villian = hq5.getName();
			model.getHeadQuarters().setHQ5(newCard);
		}		
		
		new PlayVillianCard(facade).moveCards(model, villian);
	}
	private void superHeroCivilWar(ModelFacade model){
		//Twist: KO all the Heroes in the HQ
		//Evil Wins: If the Hero Deck runs out
		
		if(model.getOtherCards().getHeroes().size() < SIX){
			new FinishGame().go(model, false);
			return;
		}
		model.getHeadQuarters().setHQ1(model.getOtherCards().getHeroes().get(ZERO));
		model.getHeadQuarters().setHQ2(model.getOtherCards().getHeroes().get(ONE));
		model.getHeadQuarters().setHQ3(model.getOtherCards().getHeroes().get(TWO));
		model.getHeadQuarters().setHQ4(model.getOtherCards().getHeroes().get(THREE));
		model.getHeadQuarters().setHQ5(model.getOtherCards().getHeroes().get(FOUR));
		for(int i = ZERO; i < FIVE; ++i){
			ServerCard card = facade.getCard(model.getOtherCards().getHeroes().get(ZERO));
			model.getKoPile().addCard(card);
			model.getOtherCards().getHeroes().remove(ZERO);
		}
	}
	private void theLegacyVirus(ModelFacade model){
		//Twist: Each player reveals a Black Hero or gains a Wound
		//Evil Wins: If the Wound Stack runs out
		
		for(PlayerCards cards : model.getPlayerCardList()){
			boolean black = false;
			for(String str : cards.getHand()){
				if(facade.getCard(str).getColors().contains(COLOR_BLACK)){
					black = true;
				}
			}
			if(black == false){
				try{
					OtherCards c = model.getOtherCards();
					List<String> discard = cards.getDiscard();
					cards.addWound(c, discard);
				}
				catch(InsufficientWoundsException e){}
				if(model.getOtherCards().getWounds() == 0){
					new FinishGame().go(model, false);
					break;
				}
			}
		}
	}
	private void unleashThePowerOfTheCosmicCube(ModelFacade model){
		//Twist: Put the Twist next to this Scheme
		//Twists 5-6: Each player gains a Wound
		//Twist 7: Each player gains 3 Wounds
		//Twist 8: Evil Wins
		
		int count = model.getOtherCards().getSchemeTwists();
		switch(count){
		case FIVE:
		case SIX:
			for(PlayerCards cards : model.getPlayerCardList()){
				try{
					OtherCards c = model.getOtherCards();
					List<String> discard = cards.getDiscard();
					cards.addWound(c, discard);
				}
				catch(InsufficientWoundsException e){}
			}
			break;
		case SEVEN:
			for(PlayerCards cards : model.getPlayerCardList()){
				try{
					OtherCards c = model.getOtherCards();
					List<String> discard = cards.getHand();
					cards.addWound(c, discard);
					cards.addWound(c, discard);
					cards.addWound(c, discard);
				}
				catch(InsufficientWoundsException e){}
			}
			break;
		case EIGHT:
			new FinishGame().go(model, false);
			break;
		default:		
		}
	}
}
