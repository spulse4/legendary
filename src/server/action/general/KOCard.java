package server.action.general;

import java.util.List;

import server.dao.DAOFacade;
import server.definition.ServerCard;
import server.definition.ServerVariables;
import shared.log.Log;
import shared.model.ModelFacade;
import shared.model.PlayerCards;

public class KOCard implements ServerVariables{

	private final String CLASS = "server.action.KOCard.java: ";

	private DAOFacade facade;
	
	public KOCard(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int playerIndex, String name){
		Log.logError(model.getGameID(), CLASS + playerIndex + SPACE + LOG_KO + name);
		boolean done = KOVictory(model, playerIndex, name);
		
		if(done == false){
			done = KODiscard(model, playerIndex, name);
		}
		if(done == false){
			done = KODraw(model, playerIndex, name);
		}
		if(done == false){
			done = KOHand(model, playerIndex, name);
		}
	}
	
	public boolean KODiscard(ModelFacade model, int playerIndex, String name){
		List<String> discard = model.getPlayerCardList().get(playerIndex).getDiscard();
		for(int i = ZERO; i < discard.size(); ++i){
			if(discard.get(i).equals(name)){
				discard.remove(i);
				discard(model, name);
				return true;
			}
		}
		return false;
	}
	public boolean KOHand(ModelFacade model, int playerIndex, String name){
		List<String> cardsInHand = model.getPlayerCardList().get(playerIndex).getHand();
		for(int i = ZERO; i < cardsInHand.size(); ++i){
			if(cardsInHand.get(i).equals(name)){
				cardsInHand.remove(i);
				discard(model, name);
				return true;
			}
		}
		return false;
	}
	public boolean KODraw(ModelFacade model, int playerIndex, String name){
		PlayerCards cards = model.getPlayerCardList().get(playerIndex);
		List<String> draw = cards.getDraw();
		for(int i = ZERO; i < draw.size(); ++i){
			if(cards.getFromDraw(i).equals(name)){
				cards.removeFromDraw(i);
				discard(model, name);
				return true;
			}
		}
		return false;
	}
	public boolean KOVictory(ModelFacade model, int playerIndex, String name){
		List<String> victory = model.getPlayerCardList().get(playerIndex).getVictoryPile();
		for(int i = ZERO; i < victory.size(); ++i){
			if(victory.get(i).equals(name)){
				victory.remove(i);
				discard(model, name);
				return true;
			}
		}
		return false;
	}
	
	private void discard(ModelFacade model, String name){
		ServerCard card = facade.getCard(name);
		model.getKoPile().addCard(card);
	}
}
