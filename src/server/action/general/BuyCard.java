package server.action.general;

import java.util.List;

import server.dao.DAOFacade;
import server.definition.ServerCard;
import server.definition.ServerVariables;
import server.exceptions.InsufficientOfficersException;
import shared.log.Log;
import shared.model.ModelFacade;

public class BuyCard implements ServerVariables{

	private final String CLASS = "server.action.BuyCard.java: ";

	private DAOFacade facade;
	
	public BuyCard(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int playerIndex, int cardIndex, boolean free){
		List<String> heroes = model.getOtherCards().getHeroes();
		String hero = EMPTY;
		switch(cardIndex){
		case 1:
			hero = model.getHeadQuarters().getHQ5();
			model.getHeadQuarters().setHQ5(heroes.get(ZERO));
			heroes.remove(ZERO);
			break;
		case 2:
			hero = model.getHeadQuarters().getHQ4();
			model.getHeadQuarters().setHQ4(heroes.get(ZERO));
			heroes.remove(ZERO);
			break;
		case 3:
			hero = model.getHeadQuarters().getHQ3();
			model.getHeadQuarters().setHQ3(heroes.get(ZERO));
			heroes.remove(ZERO);
			break;
		case 4:
			hero = model.getHeadQuarters().getHQ2();
			model.getHeadQuarters().setHQ2(heroes.get(ZERO));
			heroes.remove(ZERO);
			break;
		case 5:
			hero = model.getHeadQuarters().getHQ1();
			model.getHeadQuarters().setHQ1(heroes.get(ZERO));
			heroes.remove(ZERO);
			break;
		case 6:
			try{
				model.getOtherCards().removeOfficer();
				hero = OFFICER;
			}
			catch(InsufficientOfficersException e){
				return;
			}
		}
		Log.logError(model.getGameID(), CLASS + playerIndex + SPACE + LOG_ATTACK_CARD + hero);
		
		ServerCard card = facade.getCard(hero);
		int cardCost = Integer.valueOf(card.getPrice());
		if(free){
			cardCost = ZERO;
		}
		int currentBuy = model.getPlayerCardList().get(playerIndex).getPendingBuyPoints();
		model.getPlayerCardList().get(playerIndex).setPendingBuyPoints(currentBuy - cardCost);
		model.getPlayerCardList().get(playerIndex).getDiscard().add(hero);
	}
}
