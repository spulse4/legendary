package server.action.general;

import java.util.List;

import server.dao.DAOFacade;
import server.definition.ServerCard;
import server.definition.ServerVariables;
import shared.model.ModelFacade;

public class PlayCard implements ServerVariables{

	private DAOFacade facade;
	
	public PlayCard(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, String name){	
		increasePendingPoints(model, name);
		removeCardFromHand(model, name);
	}
	
	public void increasePendingPoints(ModelFacade model, String name){
		int currentPlayer = model.getTurnTracker().getCurrentPlayer();
		ServerCard card = facade.getCard(name);
		if(!card.getBuyPower().equals(NA)){
			int buyPower = model.getPlayerCardList().get(currentPlayer).getPendingBuyPoints();
			int addition = Integer.valueOf(card.getBuyPower());
			model.getPlayerCardList().get(currentPlayer).setPendingBuyPoints(buyPower + addition);
		}
		if(!card.getAttackPower().equals(NA)){
			int attackPower = model.getPlayerCardList().get(currentPlayer).getPendingAttackPoints();
			int addition = Integer.valueOf(card.getAttackPower());
			model.getPlayerCardList().get(currentPlayer).setPendingAttackPoints(attackPower + addition);
		}
	}
	
	public void removeCardFromHand(ModelFacade model, String name){
		int currentPlayer = model.getTurnTracker().getCurrentPlayer();
		List<String> hand = model.getPlayerCardList().get(currentPlayer).getHand();
		for(int i = ZERO; i < hand.size(); ++i){
			if(hand.get(i).equals(name)){
				hand.remove(i);
				new CardAbility(facade).go(model, name);
				model.getPlayerCardList().get(currentPlayer).getPlayed().add(name);
				break;
			}
			if(hand.get(i).equals(name + PERIOD)){
				hand.remove(i);
				new CardAbility(facade).go(model, name);
				model.getPlayerCardList().get(currentPlayer).getPlayed().add(name + PERIOD);
				break;
			}
		}
	}
}
