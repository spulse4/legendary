package server.action.general;

import server.dao.DAOFacade;
import shared.model.ModelFacade;

public class ReceiveCard {

	private DAOFacade facade;
	
	public ReceiveCard(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int playerIndex, int cardIndex){
		new BuyCard(facade).go(model, playerIndex, cardIndex, true);
	}
}
