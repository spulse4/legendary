package server.action.general;

import com.google.gson.JsonObject;

import server.dao.DAOFacade;
import shared.model.ModelFacade;

public class GetModel {

	private DAOFacade facade;
	
	public GetModel(DAOFacade facade){
		this.facade = facade;
	}
	
	public JsonObject go(int gameID, int versionID){
		ModelFacade model = facade.readModel(gameID);
		if(model != null && versionID != model.getVersion()){
			return model.getJson();
		}
		return new JsonObject();
	}
}
