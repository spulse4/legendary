package server.action.general;

import java.util.List;

import server.dao.DAOFacade;
import server.definition.ServerVariables;
import shared.definitions.GameHeader;
import shared.definitions.UserHeader;

public class JoinGame implements ServerVariables{

	private DAOFacade facade;
	
	public JoinGame(DAOFacade facade){
		this.facade = facade;
	}
	
	public int go(int gameID, int playerID, String playerName){
		int index = NEGATIVE_ONE;
		
		List<GameHeader> games = facade.getGames();
		
		for(GameHeader game : games){
			if(game.getGameID() == gameID){
				UserHeader user = new UserHeader(playerID, playerName);
				index = facade.addPlayer(game, user);
				break;
			}
		}
		
		return index;
	}
}
