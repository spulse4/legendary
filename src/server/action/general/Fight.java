package server.action.general;

import java.util.ArrayList;
import java.util.List;

import server.dao.DAOFacade;
import server.definition.ServerVariables;
import server.exceptions.InsufficientBystandersException;
import server.exceptions.InsufficientWoundsException;
import shared.definitions.State;
import shared.model.ModelFacade;
import shared.model.OtherCards;
import shared.model.PlayerCards;

public class Fight implements ServerVariables{

	private DAOFacade facade;
	
	public Fight(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, String villian, String location){	
		switch(villian){
		case ABOMINATION:
			abomination(model, location);
			break;
		case BARON_ZEMO:
			baronZemo(model);
			break;
		case BITTER_CAPTOR:
			bitterCaptor(model);
			break;
		case CRUEL_RULER:
			cruelRuler(model);
			break;
		case CRUSHING_SHOCKWAVE:
			crushingShockwave(model);
			break;
		case DARK_TECHNOLOGY:
			darkTechnology(model);
			break;
		case DESTROYER:
			destroyer(model);
			break;
		case DOCTOR_OCTOPUS:
			doctorOctopus(model);
			break;
		case DOOMBOT_LEGION:
			doombotLegion(model);
			break;
		case ELECTROMAGNETIC_BUBBLE:
			electromagneticBubble(model);
			break;
		case ENCHANTRESS:
			enchantress(model);
			break;
		case ENDLESS_ARMIES_OF_HYDRA:
			endlessArmiesOfHydra(model);
			break;
		case ENDLESS_RESOURCES:
			endlessResources(model);
			break;
		case FROST_GIANT:
			frostGiant(model);
			break;
		case HAND_NINJAS:
			handNinjas(model);
			break;
		case HYDRA_CONSPIRACY:
			hydraConspiracy(model);
			break;
		case HYDRA_KIDNAPPERS:
			hydraKidnappers(model);
			break;
		case MAESTRO:
			maestro(model);
			break;
		case MANIACAL_TYRANT:
			maniacalTyrant(model);
			break;
		case MELTER:
			melter(model);
			break;
		case MONARCHS_DECREE:
			monarchsDecree(model);
			break;
		case NEGABLAST_GRENADES:
			negablastGrenades(model);
			break;
		case PAIBOK_THE_POWER_SKRULL:
			paibokThePowerSkrull(model);
			break;
		case RUTHLESS_DICTATOR:
			ruthlessDictator(model);
			break;
		case SABRETOOTH:
			sabretooth(model);
			break;
		case SAVAGE_LAND_MUTATES:
			savageLandMutates(model);
			break;
		case SECRETS_OF_TIME_TRAVEL:
			secretsOfTimeTravel(model);
			break;
		case SENTINEL:
			sentinel(model);
			break;
		case SUPER_SKRULL:
			superSkrull(model);
			break;
		case THE_LIZARD:
			theLizard(model, location);
			break;
		case TREASURES_OF_LATVERIA:
			treasuresOfLatveria(model);
			break;
		case VANISHING_ILLUSIONS:
			vanishingIllusions(model);
			break;
		case VIPER:
			viper(model);
			break;
		case WHIRLWIND:
			whirlwind(model, location);
			break;
		case WHISPERS_AND_LIES:
			whispersAndLies(model);
			break;
		case XAVIERS_NEMESIS:
			xaviersNemesis(model);
			break;
		case YMIR_FROST_GIANT_KING:
			ymirFrostGiantKing(model);
			break;
		case ZZZAX:
			zzzax(model);
			break;
		}
	}
	
	private void abomination(ModelFacade model, String location){
		//If you fight Abomination on the Streets or Bridge, rescue three Bystanders
		if(location.equals(STREETS) || location.equals(BRIDGE)){
			int index = model.getTurnTracker().getCurrentPlayer();
			try{
				model.getOtherCards().removeBystander(THREE);
				model.getPlayerCardList().get(index).getVictoryPile().add(BYSTANDER);
				model.getPlayerCardList().get(index).getVictoryPile().add(BYSTANDER);
				model.getPlayerCardList().get(index).getVictoryPile().add(BYSTANDER);
			}
			catch(InsufficientBystandersException e){}
		}
	}
	private void baronZemo(ModelFacade model){
		//For each of your Avenger Heroes, rescue a Bystander
		for(PlayerCards cards : model.getPlayerCardList()){
			for(String str : cards.getHand()){
				if(facade.getCard(str).getCategory().equals(CATEGORY_AVENGERS)){
					try{
						model.getOtherCards().removeBystander(ONE);
						cards.getVictoryPile().add(BYSTANDER);
					}
					catch(InsufficientBystandersException e){}
				}
			}
		}
	}
	private void bitterCaptor(ModelFacade model){
		//Recruit an XMen Hero from the HQ for free
		int index = model.getTurnTracker().getCurrentPlayer();
		List<Integer> effected = new ArrayList<Integer>();
		effected.add(index);
		State.BITTER_CAPTOR_FIGHT.setEffected(model.getGameID(), effected);
		model.getTurnTracker().setState(State.BITTER_CAPTOR_FIGHT);
	}
	private void cruelRuler(ModelFacade model){
		//Defeat a Villain in the City for free
		int index = model.getTurnTracker().getCurrentPlayer();
		List<Integer> effected = new ArrayList<Integer>();
		effected.add(index);
		State.CRUEL_RULER_FIGHT.setEffected(model.getGameID(), effected);
		model.getTurnTracker().setState(State.CRUEL_RULER_FIGHT);
	}
	private void crushingShockwave(ModelFacade model){
		//Each other player reveals an XMen Hero or gains two Wounds
		int index = model.getTurnTracker().getCurrentPlayer();
		for(int i = ZERO; i < model.getPlayerCardList().size(); ++i){
			if(i == index){
				continue;
			}
			boolean xMen = false;
			
			for(String str : model.getPlayerCardList().get(i).getHand()){
				if(facade.getCard(str).getCategory().equals(CATEGORY_X_MEN)){
					xMen = true;
					break;
				}
			}
			if(xMen == false){
				try{
					OtherCards cards = model.getOtherCards();
					List<String> discardCards = model.getPlayerCardList().get(index).getDiscard();
					model.getPlayerCardList().get(index).addWound(cards, discardCards);
					model.getPlayerCardList().get(index).addWound(cards, discardCards);
				}
				catch(InsufficientWoundsException e){}
			}
		}
	}
	private void darkTechnology(ModelFacade model){
		//You may recruit a Black or Blue Hero from the HQ for free
		boolean success = false;
		if(facade.getCard(model.getHeadQuarters().getHQ1()).getColors().contains(COLOR_BLACK)){
			success = true;
		}
		if(facade.getCard(model.getHeadQuarters().getHQ1()).getColors().contains(COLOR_BLUE)){
			success = true;
		}
		if(facade.getCard(model.getHeadQuarters().getHQ2()).getColors().contains(COLOR_BLACK)){
			success = true;
		}
		if(facade.getCard(model.getHeadQuarters().getHQ2()).getColors().contains(COLOR_BLUE)){
			success = true;
		}
		if(facade.getCard(model.getHeadQuarters().getHQ3()).getColors().contains(COLOR_BLACK)){
			success = true;
		}
		if(facade.getCard(model.getHeadQuarters().getHQ3()).getColors().contains(COLOR_BLUE)){
			success = true;
		}
		if(facade.getCard(model.getHeadQuarters().getHQ4()).getColors().contains(COLOR_BLACK)){
			success = true;
		}
		if(facade.getCard(model.getHeadQuarters().getHQ4()).getColors().contains(COLOR_BLUE)){
			success = true;
		}
		if(facade.getCard(model.getHeadQuarters().getHQ5()).getColors().contains(COLOR_BLACK)){
			success = true;
		}
		if(facade.getCard(model.getHeadQuarters().getHQ5()).getColors().contains(COLOR_BLUE)){
			success = true;
		}
		
		if(success){
			int index = model.getTurnTracker().getCurrentPlayer();
			List<Integer> effected = new ArrayList<Integer>();
			effected.add(index);
			State.DARK_TECHNOLOGY_FIGHT.setEffected(model.getGameID(), effected);
			model.getTurnTracker().setState(State.DARK_TECHNOLOGY_FIGHT);
		}
	}
	private void destroyer(ModelFacade model){
		//KO all your SHIELD Heroes
		int index = model.getTurnTracker().getCurrentPlayer();
		for(int i = ZERO; i < model.getPlayerCardList().get(index).getHand().size(); ++i){
			String str = model.getPlayerCardList().get(index).getHand().get(i);
			if(facade.getCard(str).getCategory().equals(CATEGORY_SHIELD)){
				new KOCard(facade).KOHand(model, index, str);
				--i;
			}
		}
	}
	private void doctorOctopus(ModelFacade model){
		//When you draw a new hand of cards at the end of this turn, draw eight cards instead of six
		model.getTurnTracker().setNextHandCount(EIGHT);
	}
	private void doombotLegion(ModelFacade model){
		//Look at the top two cards of your deck. KO one of them and put the other back
		int index = model.getTurnTracker().getCurrentPlayer();
		List<Integer> effected = new ArrayList<Integer>();
		effected.add(index);
		State.DOOMBOT_LEGION_FIGHT.setEffected(model.getGameID(), effected);
		model.getTurnTracker().setState(State.DOOMBOT_LEGION_FIGHT);
	}
	private void electromagneticBubble(ModelFacade model){
		//Choose one of your XMen Heroes. When you draw a new hand of cards at the end of this turn, add that Hero to your hand as a seventh card
		boolean xMen = false;
		int index = model.getTurnTracker().getCurrentPlayer();
		for(String str : model.getPlayerCardList().get(index).getDraw()){
			if(facade.getCard(str).getCategory().equals(CATEGORY_X_MEN)){
				xMen = true;
				break;
			}
		}
		if(xMen == false){
			for(String str : model.getPlayerCardList().get(index).getHand()){
				if(facade.getCard(str).getCategory().equals(CATEGORY_X_MEN)){
					xMen = true;
					break;
				}
			}
		}
		if(xMen == false){
			for(String str : model.getPlayerCardList().get(index).getDiscard()){
				if(facade.getCard(str).getCategory().equals(CATEGORY_X_MEN)){
					xMen = true;
					break;
				}
			}
		}
		if(xMen){
			List<Integer> effected = new ArrayList<Integer>();
			effected.add(index);
			State.ELECTROMAGNETIC_BUBBLE_FIGHT.setEffected(model.getGameID(), effected);
			model.getTurnTracker().setState(State.ELECTROMAGNETIC_BUBBLE_FIGHT);
		}
	}
	private void enchantress(ModelFacade model){
		//Draw three cards
		int index = model.getTurnTracker().getCurrentPlayer();
		new DrawCard().go(model, index);
		new DrawCard().go(model, index);
		new DrawCard().go(model, index);
	}
	private void endlessArmiesOfHydra(ModelFacade model){
		//Play the top two cards of the Villain Deck
		new PlayVillianCard(facade).go(model);
		new PlayVillianCard(facade).go(model);
	}
	private void endlessResources(ModelFacade model){
		//You get +4 Buy Power
		int index = model.getTurnTracker().getCurrentPlayer();
		model.getPlayerCardList().get(index).setPendingBuyPoints(model.getPlayerCardList().get(index).getPendingBuyPoints() + FOUR);
	}
	private void frostGiant(ModelFacade model){
		//Each player reveals a Blue Hero or gains a Wound
		for(PlayerCards cards : model.getPlayerCardList()){
			boolean blue = false;
			for(String str : cards.getHand()){
				if(facade.getCard(str).getColors().contains(COLOR_BLUE)){
					blue = true;
					break;
				}
			}
			if(blue == false){
				try{
					OtherCards c = model.getOtherCards();
					List<String> discard = cards.getDiscard();
					cards.addWound(c, discard);
				}
				catch(InsufficientWoundsException e){}
			}
		}
	}
	private void handNinjas(ModelFacade model){
		//You get +1 Buy Power
		int index = model.getTurnTracker().getCurrentPlayer();
		model.getPlayerCardList().get(index).setPendingBuyPoints(model.getPlayerCardList().get(index).getPendingBuyPoints() + ONE);
	}
	private void hydraConspiracy(ModelFacade model){
		//Draw two cards. Then draw another card for each HYDRA Villain in your Victory Pile
		int index = model.getTurnTracker().getCurrentPlayer();
		new DrawCard().go(model, index);
		new DrawCard().go(model, index);
		int draw = ZERO;
		
		for(String str : model.getPlayerCardList().get(index).getVictoryPile()){
			if(facade.getCard(str).getCategory().equals(CATEGORY_HYDRA)){
				++draw;
			}
		}
		
		for(int i = ZERO; i < draw; ++i){
			new DrawCard().go(model, index);
		}
	}
	private void hydraKidnappers(ModelFacade model){
		//You may gain a SHIELD Officer
		int index = model.getTurnTracker().getCurrentPlayer();
		List<Integer> effected = new ArrayList<Integer>();
		effected.add(index);
		State.HYDRA_KIDNAPPERS_FIGHT.setEffected(model.getGameID(), effected);
		model.getTurnTracker().setState(State.HYDRA_KIDNAPPERS_FIGHT);
	}
	private void maestro(ModelFacade model){
		//For each of your Green Heroes, KO one of your Heroes
		int index = model.getTurnTracker().getCurrentPlayer();
		int count = ZERO;
		
		for(String str : model.getPlayerCardList().get(index).getHand()){
			if(facade.getCard(str).getColors().contains(COLOR_GREEN)){
				++count;
			}
		}
		
		List<Integer> effected = new ArrayList<Integer>();
		for(int i = ZERO; i < count; ++i){
			effected.add(index);
		}
		if(effected.size() > ZERO){
			State.MAESTRO_FIGHT.setEffected(model.getGameID(), effected);
			model.getTurnTracker().setState(State.MAESTRO_FIGHT);
		}
	}
	private void maniacalTyrant(ModelFacade model){
		//KO up to four cards from your discard pile
		int index = model.getTurnTracker().getCurrentPlayer();
		List<Integer> effected = new ArrayList<Integer>();
		effected.add(index);
		effected.add(index);
		effected.add(index);
		effected.add(index);
		State.MANIACAL_TYRANT_FIGHT.setEffected(model.getGameID(), effected);
		model.getTurnTracker().setState(State.MANIACAL_TYRANT_FIGHT);
	}
	private void melter(ModelFacade model){
		//Each player reveals the top card of their deck. For each card, you choose to KO it or put it back
		List<Integer> effected = new ArrayList<Integer>();
		for(int i = ZERO; i < model.getPlayerCardList().size(); ++i){
			effected.add(i);
			if(model.getPlayerCardList().get(i).getDraw().size() == ZERO){
				model.getPlayerCardList().get(i).shuffleCards();
			}
		}
		State.MELTER_FIGHT.setEffected(model.getGameID(), effected);
		model.getTurnTracker().setState(State.MELTER_FIGHT);
	}
	private void monarchsDecree(ModelFacade model){
		//Choose one: each other player draws a card or each other player discards a card
		if(model.getPlayerCardList().size() != ONE){
			int index = model.getTurnTracker().getCurrentPlayer();
			List<Integer> effected = new ArrayList<Integer>();
			effected.add(index);
			State.MONARCHS_DECREE_FIGHT.setEffected(model.getGameID(), effected);
			model.getTurnTracker().setState(State.MONARCHS_DECREE_FIGHT);
		}
	}
	private void negablastGrenades(ModelFacade model){
		//You get +3 Attack Power
		int index = model.getTurnTracker().getCurrentPlayer();
		model.getPlayerCardList().get(index).setPendingAttackPoints(model.getPlayerCardList().get(index).getPendingAttackPoints() + THREE);
	}
	private void paibokThePowerSkrull(ModelFacade model){
		//Choose a hero in the HQ for each player. Each player gains that hero
		int index = model.getTurnTracker().getCurrentPlayer();
		List<Integer> effected = new ArrayList<Integer>();
		effected.add(index);
		State.PAIBOK_THE_POWER_SKRULL_FIGHT.setEffected(model.getGameID(), effected);
		model.getTurnTracker().setState(State.PAIBOK_THE_POWER_SKRULL_FIGHT);
	}
	private void ruthlessDictator(ModelFacade model){
		//Look at the top three cards of your deck. KO one, discard one and put one back on top of your deck
		int index = model.getTurnTracker().getCurrentPlayer();
		List<Integer> effected = new ArrayList<Integer>();
		effected.add(index);
		if(model.getPlayerCardList().get(index).getDraw().size() < THREE){
			model.getPlayerCardList().get(index).shuffleCards();
		}
		State.RUTHLESS_DICTATOR_KO.setEffected(model.getGameID(), effected);
		model.getTurnTracker().setState(State.RUTHLESS_DICTATOR_KO);
	}
	private void sabretooth(ModelFacade model){
		//Each player reveals an XMen Hero or gains a Wound
		for(PlayerCards cards : model.getPlayerCardList()){
			boolean xMen = false;
			for(String str : cards.getHand()){
				if(facade.getCard(str).getCategory().equals(CATEGORY_X_MEN)){
					xMen = true;
					break;
				}
			}
			if(xMen == false){
				try{
					OtherCards c = model.getOtherCards();
					List<String> discard = cards.getDiscard();
					cards.addWound(c, discard);
				}
				catch(InsufficientWoundsException e){}
			}
		}

	}
	private void savageLandMutates(ModelFacade model){
		//When you draw a new hand of cards at the end of this turn, draw an extra card
		model.getTurnTracker().setNextHandCount(model.getTurnTracker().getNextHandCount() + ONE);
	}
	private void secretsOfTimeTravel(ModelFacade model){
		//Take another turn after this one
		int index = model.getTurnTracker().getCurrentPlayer();
		model.getTurnTracker().setNextPlayer(index);
	}
	private void sentinel(ModelFacade model){
		//KO one of your Heroes
		int index = model.getTurnTracker().getCurrentPlayer();
		List<Integer> effected = new ArrayList<Integer>();
		effected.add(index);
		State.SENTINEL_FIGHT.setEffected(model.getGameID(), effected);
		model.getTurnTracker().setState(State.SENTINEL_FIGHT);
	}
	private void superSkrull(ModelFacade model){
		//Each player KOs one of their Heroes
		List<Integer> effected = new ArrayList<Integer>();
		for(int i = ZERO; i < model.getPlayerCardList().size(); ++i){
			effected.add(i);
		}
		State.SUPER_SKRULL_FIGHT.setEffected(model.getGameID(), effected);
		model.getTurnTracker().setState(State.SUPER_SKRULL_FIGHT);
	}
	private void theLizard(ModelFacade model, String location){
		//If you fight the Lizard in the Sewers, each other player gains a Wound
		int index = model.getTurnTracker().getCurrentPlayer();
		if(location.equals(SEWERS)){
			for(int i = ZERO; i < model.getPlayerCardList().size(); ++i){
				if(i == index){
					continue;
				}
				try{
					OtherCards c = model.getOtherCards();
					List<String> discard = model.getPlayerCardList().get(i).getDiscard();
					model.getPlayerCardList().get(i).addWound(c, discard);
				}
				catch(InsufficientWoundsException e){}
			}
		}
	}
	private void treasuresOfLatveria(ModelFacade model){
		//When you draw a new hand of cards at the end of this turn, draw three extra cards
		model.getTurnTracker().setNextHandCount(model.getTurnTracker().getNextHandCount() + THREE);
	}
	private void vanishingIllusions(ModelFacade model){
		//Each other player KOs a Villain from their Victory Pile
		int index = model.getTurnTracker().getCurrentPlayer();
		List<Integer> effected = new ArrayList<Integer>();
		for(int i = ZERO; i < model.getPlayerCardList().size(); ++i){
			if(i == index){
				continue;
			}
			effected.add(i);
		}
		if(effected.size() != ZERO){
			State.VANISHING_ILLUSIONS_FIGHT.setEffected(model.getGameID(), effected);
			model.getTurnTracker().setState(State.VANISHING_ILLUSIONS_FIGHT);
		}
	}
	private void viper(ModelFacade model){
		//Each player without another HYDRA Villain in their Victory Pile gains a Wound
		for(PlayerCards cards : model.getPlayerCardList()){
			boolean hydra = false;
			for(String str : cards.getVictoryPile()){
				if(facade.getCard(str).getCategory().equals(CATEGORY_HYDRA)){
					hydra = true;
					break;
				}
			}
			if(hydra == false){
				try{
					OtherCards c = model.getOtherCards();
					List<String> discard = cards.getDiscard();
					cards.addWound(c, discard);
				}
				catch(InsufficientWoundsException e){}
			}
		}
	}
	private void whirlwind(ModelFacade model, String location){
		//If you fight Whirlwind on the Rooftops or Bridge, KO two of your Heroes
		if(location.equals(ROOFTOPS) || location.equals(BRIDGE)){
			int index = model.getTurnTracker().getCurrentPlayer();
			List<Integer> effected = new ArrayList<Integer>();
			effected.add(index);
			effected.add(index);
			State.WHIRLWIND_FIGHT.setEffected(model.getGameID(), effected);
			model.getTurnTracker().setState(State.WHIRLWIND_FIGHT);
		}
	}
	private void whispersAndLies(ModelFacade model){
		//Each other player KOs two Bystanders from their Victory Pile
		int index = model.getTurnTracker().getCurrentPlayer();
		for(int i = ZERO; i < model.getPlayerCardList().size(); ++i){
			if(i == index){
				continue;
			}
			new KOCard(facade).KOVictory(model, i, BYSTANDER);
			new KOCard(facade).KOVictory(model, i, BYSTANDER);
		}
	}
	private void xaviersNemesis(ModelFacade model){
		//For each of your XMen Heroes, rescue a Bystander
		int index = model.getTurnTracker().getCurrentPlayer();
		for(String str : model.getPlayerCardList().get(index).getHand()){
			if(facade.getCard(str).getCategory().equals(CATEGORY_X_MEN)){
				try{
					model.getOtherCards().removeBystander(ONE);
					model.getPlayerCardList().get(index).getVictoryPile().add(BYSTANDER);
				}
				catch(InsufficientBystandersException e){}
			}
		}
	}
	private void ymirFrostGiantKing(ModelFacade model){
		//Choose a player. That player KOs any number of Wounds from their hand and discard pile
		int index = model.getTurnTracker().getCurrentPlayer();
		List<Integer> effected = new ArrayList<Integer>();
		effected.add(index);
		State.YMIR_FROST_GIANT_KING_FIGHT.setEffected(model.getGameID(), effected);
		model.getTurnTracker().setState(State.YMIR_FROST_GIANT_KING_FIGHT);
	}
	private void zzzax(ModelFacade model){
		//Each player reveals a Green Hero or gains a Wound
		for(PlayerCards cards : model.getPlayerCardList()){
			boolean green = false;
			for(String str : cards.getHand()){
				if(facade.getCard(str).getColors().contains(COLOR_GREEN)){
					green = true;
					break;
				}
			}
			if(green == false){
				try{
					OtherCards c = model.getOtherCards();
					List<String> discard = cards.getDiscard();
					cards.addWound(c, discard);
				}
				catch(InsufficientWoundsException e){}
			}
		}
	}
}
