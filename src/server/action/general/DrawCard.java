package server.action.general;

import java.util.List;

import server.definition.ServerVariables;
import shared.log.Log;
import shared.model.ModelFacade;

public class DrawCard implements ServerVariables{

	private final String CLASS = "server.action.DrawCard.java: ";

	public DrawCard(){}
	
	public void go(ModelFacade model, int playerIndex){
		Log.logError(model.getGameID(), CLASS + playerIndex + SPACE + LOG_DRAW_CARD);
		List<String> hand = model.getPlayerCardList().get(playerIndex).getHand();
		
		hand.add(model.getPlayerCardList().get(playerIndex).getFromDraw(ZERO));
		model.getPlayerCardList().get(playerIndex).removeFromDraw(ZERO);
	}
}
