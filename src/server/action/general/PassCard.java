package server.action.general;

import java.util.List;

import server.definition.ServerVariables;
import shared.definitions.State;
import shared.log.Log;
import shared.model.ModelFacade;

public class PassCard implements ServerVariables{

	private final String CLASS = "server.action.PassCard.java: ";

	public PassCard(){}
	
	public void go(ModelFacade model, int playerIndex, String card){
		Log.logError(model.getGameID(), CLASS + playerIndex + SPACE + LOG_PASS + card);
		int players = model.getPlayerCardList().size();
		model.getTurnTracker().setPass(card, playerIndex, players);
		if(model.getTurnTracker().getState() == State.PLAYING){
			trade(model);
		}
	}
	
	private void trade(ModelFacade model){
		List<String> tradeList = model.getTurnTracker().getPass();
		for(int i = ZERO; i < tradeList.size(); ++i){
			List<String> hand = model.getPlayerCardList().get(i).getHand();
			String card = null;
			if(i == ZERO){
				card = tradeList.get(tradeList.size() - ONE);
			}
			else{
				card = tradeList.get(i - ONE);
			}
			for(int j = ZERO; j < hand.size(); ++j){
				if(hand.get(j).equals(tradeList.get(i))){
					hand.set(j, card);
				}
			}
		}
	}
}
