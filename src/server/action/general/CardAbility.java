package server.action.general;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import server.dao.DAOFacade;
import server.definition.ServerVariables;
import server.exceptions.InsufficientBystandersException;
import server.exceptions.InsufficientWoundsException;
import shared.definitions.State;
import shared.model.ModelFacade;
import shared.model.OtherCards;
import shared.model.PlayerCards;

public class CardAbility implements ServerVariables{

	private DAOFacade facade;
	
	public CardAbility(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, String name){
		switch(name){
		case A_DAY_UNLIKE_ANY_OTHER:
			aDayUnlikeAnyOther(model);
			break;
		case ARC_REACTOR:
			arcReactor(model);
			break;
		case ASTONISHING_STRENGTH:
			astonishingStrength(model);
			break;
		case AVENGERS_ASSEMBLE:
			avengersAssemble(model);
			break;
		case BATTLEFIELD_PROMOTION:
			battlefieldPromotion(model);
			break;
		case BERSERKER_RAGE:
			berserkerRage(model);
			break;
		case BORROWED_BRAWN:
			borrowedBrawn(model);
			break;
		case CALL_LIGHTNING:
			callLightning(model);
			break;
		case CARD_SHARK:
			cardShark(model);
			break;
		case COPY_POWERS:
			copyPowers(model);
			break;
		case COVERING_FIRE:
			coveringFire(model);
			break;
		case COVERT_OPERATION:
			covertOperation(model);
			break;
		case CRAZED_RAMPAGE:
			crazedRampage(model);
			break;
		case DANGEROUS_RESCUE:
			dangerousRescue(model);
			break;
		case DETERMINATION:
			determination(model);
			break;
		case DIAMOND_FORM:
			diamondForm(model);
			break;
		case ENDLESS_INVENTION:
			endlessInvention(model);
			break;
		case ENERGY_DRAIN:
			energyDrain(model);
			break;
		case FRENZIED_SLASHING:
			frenziedSlashing(model);
			break;
		case GATHERING_STORMCLOUDS:
			gatheringStormclouds(model);
			break;
		case GOD_OF_THUNDER:
			godOfThunder(model);
			break;
		case GREAT_RESPONSIBILITY:
			greatResponsibility(model);
			break;
		case GROWING_ANGER:
			growingAnger(model);
			break;
		case HEALING_FACTOR:
			healingFactor(model);
			break;
		case HERE_HOLD_THIS_FOR_A_SECOND:
			hereHoldThisForASecond(model);
			break;
		case HEY_CAN_I_GET_A_DO_OVER:
			heyCanIGetADoOver(model);
			break;
		case HIGH_STAKES_JACKPOT:
			highStakesJackpot(model);
			break;
		case HIGH_TECH_WEAPONRY:
			highTechWeaponry(model);
			break;
		case HULK_SMASH:
			hulkSmash(model);
			break;
		case HYPNOTIC_CHARM:
			hypnoticCharm(model);
			break;
		case IMPOSSIBLE_TRICK_SHOT:
			impossibleTrickShot(model);
			break;
		case KEEN_SENSES:
			keenSenses(model);
			break;
		case LEGENDARY_COMMANDER:
			legendaryCommander(model);
			break;
		case LIGHTNING_BOLT:
			lightningBolt(model);
			break;
		case MENTAL_DISCIPLINE:
			mentalDiscipline(model);
			break;
		case MISSION_ACCOMPLISHED:
			missionAccomplished(model);
			break;
		case ODDBALL:
			oddball(model);
			break;
		case ODINSON:
			odinson(model);
			break;
		case OPTIC_BLAST:
			opticBlast(model);
			break;
		case PERFECT_TEAMWORK:
			perfectTeamwork(model);
			break;
		case PSYCHIC_LINK:
			psychicLink(model);
			break;
		case PURE_FURY:
			pureFury(model);
			break;
		case QUANTUM_BREAKTHROUGH:
			quantumBreakthrough(model);
			break;
		case QUICK_DRAW:
			quickDraw(model);
			break;
		case RANDOM_ACTS_OF_UNKINDNESS:
			randomActsOfUnkindness(model);
			break;
		case REPULSOR_RAYS:
			repulsorRays(model);
			break;
		case SHADOWED_THOUGHTS:
			shadowedThoughts(model);
			break;
		case SILENT_SNIPER:
			silentSniper(model);
			break;
		case SPINNING_CYCLONE:
			spinningCyclone(model);
			break;
		case STACK_THE_DECK:
			stackTheDeck(model);
			break;
		case STEAL_ABILITIES:
			stealAbilities(model);
			break;
		case SURGE_OF_POWER:
			surgeOfPower(model);
			break;
		case TEAM_PLAYER:
			teamPlayer(model);
			break;
		case THE_AMAZING_SPIDERMAN:
			theAmazingSpiderman(model);
			break;
		case TIDAL_WAVE:
			tidalWave(model);
			break;
		case UNSTOPPABLE_HULK:
			unstoppableHulk(model);
			break;
		case WEB_SHOOTERS:
			webShooters(model);
			break;
		case X_MEN_UNITED:
			xMenUnited(model);
			break;
		}
	}
	
	private void aDayUnlikeAnyOther(ModelFacade model){
		//Avenger: You get +3 Attack Power for each other Avenger Hero you played this turn
		int index = model.getTurnTracker().getCurrentPlayer();
		int count = ZERO;
		for(String str : model.getPlayerCardList().get(index).getPlayed()){
			if(facade.getCard(str).getCategory().equals(CATEGORY_AVENGERS)){
				++count;
			}
		}
		int pendingAttack = model.getPlayerCardList().get(index).getPendingAttackPoints();
		model.getPlayerCardList().get(index).setPendingAttackPoints(pendingAttack + (THREE * count));
	}
	private void arcReactor(ModelFacade model){
		//Black: You get +1 Attack Power for each other Black Hero you played this turn
		int index = model.getTurnTracker().getCurrentPlayer();
		int count = ZERO;
		for(String str : model.getPlayerCardList().get(index).getPlayed()){
			if(facade.getCard(str).getColors().contains(COLOR_BLACK)){
				++count;
			}
		}
		int pendingAttack = model.getPlayerCardList().get(index).getPendingAttackPoints();
		model.getPlayerCardList().get(index).setPendingAttackPoints(pendingAttack + (ONE * count));
	}
	private void astonishingStrength(ModelFacade model){
		//Reveal the top card of your deck. If that card costs 2 or less, draw it
		int index = model.getTurnTracker().getCurrentPlayer();
		String str = model.getPlayerCardList().get(index).getFromDraw(ZERO);
		if(Integer.valueOf(facade.getCard(str).getPrice()) <= TWO){
			model.getPlayerCardList().get(index).getHand().add(str);
			model.getPlayerCardList().get(index).removeFromDraw(ZERO);
		}
	}
	private void avengersAssemble(ModelFacade model){
		//You get +1 Buy Power for each color of Hero you have
		int index = model.getTurnTracker().getCurrentPlayer();
		Set<String> colors = new TreeSet<String>();
		colors.add(COLOR_YELLOW);
		for(String str : model.getPlayerCardList().get(index).getHand()){
			List<String> colorList = facade.getCard(str).getColors();
			colors.add(colorList.get(ZERO));
			colors.add(colorList.get(ONE));
			colors.add(colorList.get(TWO));
			colors.add(colorList.get(THREE));			
		}
		if(colors.contains(NA)){
			colors.remove(NA);
		}
		int pendingBuy = model.getPlayerCardList().get(index).getPendingBuyPoints();
		model.getPlayerCardList().get(index).setPendingBuyPoints(pendingBuy + (ONE * colors.size()));
	}
	private void battlefieldPromotion(ModelFacade model){
		//You may KO a SHIELD Hero from your hand or discard pile. If you do, you may gain a SHIELD Officer to your hand.
		int index = model.getTurnTracker().getCurrentPlayer();
		List<Integer> effected = new ArrayList<Integer>();
		effected.add(index);
		State.BATTLEFIELD_PROMOTION_ABILITY.setEffected(model.getGameID(), effected);
		model.getTurnTracker().setState(State.BATTLEFIELD_PROMOTION_ABILITY);
	}
	private void berserkerRage(ModelFacade model){
		//Draw three cards.
		//Yellow: You get +1 Attack Power for each extra card you've drawn this turn
		int index = model.getTurnTracker().getCurrentPlayer();
		new DrawCard().go(model, index);
		new DrawCard().go(model, index);
		new DrawCard().go(model, index);
		int count = model.getPlayerCardList().get(index).getHand().size();
		count += model.getPlayerCardList().get(index).getPlayed().size();
		count -= SIX;
		int pendingAttackPoints = model.getPlayerCardList().get(index).getPendingAttackPoints();
		model.getPlayerCardList().get(index).setPendingAttackPoints(pendingAttackPoints + (ONE * count));
	}
	private void borrowedBrawn(ModelFacade model){
		//Green: You get +3 Attack Power	
		int index = model.getTurnTracker().getCurrentPlayer();
		boolean green = false;
		for(String str : model.getPlayerCardList().get(index).getPlayed()){
			if(facade.getCard(str).getColors().contains(COLOR_GREEN)){
				green = true;
				break;
			}
		}
		if(green){
			int pendingAttack = model.getPlayerCardList().get(index).getPendingAttackPoints();
			model.getPlayerCardList().get(index).setPendingAttackPoints(pendingAttack + THREE);
		}
	}
	private void callLightning(ModelFacade model){
		//Blue: You get +3 Attack Power
		int index = model.getTurnTracker().getCurrentPlayer();
		boolean blue = false;
		for(String str : model.getPlayerCardList().get(index).getPlayed()){
			if(facade.getCard(str).getColors().contains(COLOR_BLUE)){
				blue = true;
				break;
			}
		}
		if(blue){
			int pendingAttack = model.getPlayerCardList().get(index).getPendingAttackPoints();
			model.getPlayerCardList().get(index).setPendingAttackPoints(pendingAttack + THREE);
		}
	}
	private void cardShark(ModelFacade model){
		//Reveal the top card of your deck. If it's an XMen Hero, draw it
		int index = model.getTurnTracker().getCurrentPlayer();
		String str = model.getPlayerCardList().get(index).getFromDraw(ZERO);
		if(facade.getCard(str).getCategory().equals(CATEGORY_X_MEN)){
			model.getPlayerCardList().get(index).getHand().add(str);
			model.getPlayerCardList().get(index).removeFromDraw(ZERO);
		}
	}
	private void copyPowers(ModelFacade model){
		//Play this card as a copy of another Hero you played this turn. This card is both Red and the color you copy
		int index = model.getTurnTracker().getCurrentPlayer();
		List<Integer> effected = new ArrayList<Integer>();
		effected.add(index);
		State.COPY_POWERS_ABILITY.setEffected(model.getGameID(), effected);
		model.getTurnTracker().setState(State.COPY_POWERS_ABILITY);
	}
	private void coveringFire(ModelFacade model){
		//Black: Choose one: each other player draws a card or each other player discards a card
		int index = model.getTurnTracker().getCurrentPlayer();
		boolean black = false;
		for(String str : model.getPlayerCardList().get(index).getPlayed()){
			if(facade.getCard(str).getColors().contains(COLOR_BLACK)){
				black = true;
				break;
			}
		}
		if(black){
			List<Integer> effected = new ArrayList<Integer>();
			effected.add(index);
			State.COVERING_FIRE_ABILITY.setEffected(model.getGameID(), effected);
			model.getTurnTracker().setState(State.COVERING_FIRE_ABILITY);
		}
	}
	private void covertOperation(ModelFacade model){
		//You get +1 Attack Power for each Bystander in your Victory Pile
		int index = model.getTurnTracker().getCurrentPlayer();
		int count = ZERO;
		for(String str : model.getPlayerCardList().get(index).getVictoryPile()){
			if(str.equals(BYSTANDER)){
				++count;
			}
		}
		int pendingAttack = model.getPlayerCardList().get(index).getPendingAttackPoints();
		model.getPlayerCardList().get(index).setPendingAttackPoints(pendingAttack + (ONE * count));
	}
	private void crazedRampage(ModelFacade model){
		//Each player gains a Wound
		for(PlayerCards cards : model.getPlayerCardList()){
			try{
				OtherCards c = model.getOtherCards();
				List<String> discard = cards.getDiscard();
				cards.addWound(c, discard);
			}
			catch(InsufficientWoundsException e){}
		}
	}
	private void dangerousRescue(ModelFacade model){
		//Red: You may KO a card from your hand or discard pile. If you do, rescue a Bystander.
		int index = model.getTurnTracker().getCurrentPlayer();
		boolean red = false;
		for(String str : model.getPlayerCardList().get(index).getPlayed()){
			if(facade.getCard(str).getColors().contains(COLOR_RED)){
				red = true;
				break;
			}
		}
		if(red){
			List<Integer> effected = new ArrayList<Integer>();
			effected.add(index);
			State.DANGEROUS_RESCUE_ABILITY.setEffected(model.getGameID(), effected);
			model.getTurnTracker().setState(State.DANGEROUS_RESCUE_ABILITY);
		}
	}
	private void determination(ModelFacade model){
		//To play this card, you must discard a card from your hand
		int index = model.getTurnTracker().getCurrentPlayer();
		List<Integer> effected = new ArrayList<Integer>();
		effected.add(index);
		State.CYCLOPS_ABILITY.setEffected(model.getGameID(), effected);
		model.getTurnTracker().setState(State.CYCLOPS_ABILITY);
	}
	private void diamondForm(ModelFacade model){
		//Whenever you defeat a Villain or Mastermind this turn, you get +3 Buy Power
		model.getTurnTracker().addToCardsInEffect(DIAMOND_FORM);
	}
	private void endlessInvention(ModelFacade model){
		//Draw a card.
		//Black: Draw another card
		int index = model.getTurnTracker().getCurrentPlayer();
		new DrawCard().go(model, index);
		boolean black = false;
		for(String str : model.getPlayerCardList().get(index).getPlayed()){
			if(facade.getCard(str).getColors().contains(COLOR_BLACK)){
				black = true;
				break;
			}
		}
		if(black){
			new DrawCard().go(model, index);
		}
	}
	private void energyDrain(ModelFacade model){
		//Red: You may KO a card from your hand or discard pile. If you do, you get +1 Buy Power
		int index = model.getTurnTracker().getCurrentPlayer();
		boolean red = false;
		for(String str : model.getPlayerCardList().get(index).getPlayed()){
			if(facade.getCard(str).getColors().contains(COLOR_RED)){
				red = true;
				break;
			}
		}
		if(red){
			List<Integer> effected = new ArrayList<Integer>();
			effected.add(index);
			State.ENERGY_DRAIN_ABILITY.setEffected(model.getGameID(), effected);
			model.getTurnTracker().setState(State.ENERGY_DRAIN_ABILITY);
		}
	}
	private void frenziedSlashing(ModelFacade model){
		//Yellow: Draw two cards
		int index = model.getTurnTracker().getCurrentPlayer();
		boolean yellow = false;
		for(String str : model.getPlayerCardList().get(index).getPlayed()){
			if(facade.getCard(str).getColors().contains(COLOR_YELLOW)){
				yellow = true;
				break;
			}
		}
		if(yellow){
			new DrawCard().go(model, index);
			new DrawCard().go(model, index);
		}
	}
	private void gatheringStormclouds(ModelFacade model){
		//Blue: Draw a card
		int index = model.getTurnTracker().getCurrentPlayer();
		boolean blue = false;
		for(String str : model.getPlayerCardList().get(index).getPlayed()){
			if(facade.getCard(str).getColors().contains(COLOR_BLUE)){
				blue = true;
				break;
			}
		}
		if(blue){
			new DrawCard().go(model, index);
		}
	}
	private void godOfThunder(ModelFacade model){
		//You can use Buy Power as Attack Power this turn
		model.getTurnTracker().addToCardsInEffect(GOD_OF_THUNDER);
	}
	private void greatResponsibility(ModelFacade model){
		//Reveal the top card of your deck If that card costs 2 or less, draw it
		int index = model.getTurnTracker().getCurrentPlayer();
		String str = model.getPlayerCardList().get(index).getFromDraw(ZERO);
		if(Integer.valueOf(facade.getCard(str).getPrice()) <= TWO){
			model.getPlayerCardList().get(index).getHand().add(str);
			model.getPlayerCardList().get(index).removeFromDraw(ZERO);
		}
	}
	private void growingAnger(ModelFacade model){
		//Green: You get +1 Attack Power
		int index = model.getTurnTracker().getCurrentPlayer();
		boolean green = false;
		for(String str : model.getPlayerCardList().get(index).getPlayed()){
			if(facade.getCard(str).getColors().contains(COLOR_GREEN)){
				green = true;
				break;
			}
		}
		if(green){
			int pendingAttack = model.getPlayerCardList().get(index).getPendingAttackPoints();
			model.getPlayerCardList().get(index).setPendingAttackPoints(pendingAttack + ONE);
		}
	}
	private void healingFactor(ModelFacade model){
		//You may KO a Wound from your hand or discard pile. If you do, draw a card
		int index = model.getTurnTracker().getCurrentPlayer();
		boolean wound = false;
		for(String str : model.getPlayerCardList().get(index).getHand()){
			if(str.equals(WOUND)){
				wound = true;
				break;
			}
		}
		if(wound == false){
			for(String str : model.getPlayerCardList().get(index).getDiscard()){
				if(str.equals(WOUND)){
					wound = true;
					break;
				}
			}
		}
		if(wound){
			List<Integer> effected = new ArrayList<Integer>();
			effected.add(index);
			State.HEALING_FACTOR_ABILITY.setEffected(model.getGameID(), effected);
			model.getTurnTracker().setState(State.HEALING_FACTOR_ABILITY);
		}
	}
	private void hereHoldThisForASecond(ModelFacade model){
		//A Villain of your choice captures a Bystander
		int index = model.getTurnTracker().getCurrentPlayer();
		List<Integer> effected = new ArrayList<Integer>();
		effected.add(index);
		State.HERE_HOLD_THIS_FOR_A_SECOND_ABILITY.setEffected(model.getGameID(), effected);
		model.getTurnTracker().setState(State.HERE_HOLD_THIS_FOR_A_SECOND_ABILITY);
	}
	private void heyCanIGetADoOver(ModelFacade model){
		//If this is the first Hero you played this turn, you may discard the rest of your hand and draw four cards
		int index = model.getTurnTracker().getCurrentPlayer();
		if(model.getPlayerCardList().get(index).getPlayed().size() != ZERO){
			return;
		}
		List<Integer> effected = new ArrayList<Integer>();
		effected.add(index);
		State.HEY_CAN_I_GET_A_DO_OVER_ABILITY.setEffected(model.getGameID(), effected);
		model.getTurnTracker().setState(State.HEY_CAN_I_GET_A_DO_OVER_ABILITY);
	}
	private void highStakesJackpot(ModelFacade model){
		//Reveal the top card of your deck. You get + Attack Power equal to that card's cost
		int index = model.getTurnTracker().getCurrentPlayer();
		String str = model.getPlayerCardList().get(index).getFromDraw(ZERO);
		int additional = Integer.valueOf(facade.getCard(str).getPrice());
		int pendingAttack = model.getPlayerCardList().get(index).getPendingAttackPoints();
		model.getPlayerCardList().get(index).setPendingAttackPoints(additional + pendingAttack);
	}
	private void highTechWeaponry(ModelFacade model){
		//Black: You get +1 Attack
		int index = model.getTurnTracker().getCurrentPlayer();
		boolean black = false;
		for(String str : model.getPlayerCardList().get(index).getPlayed()){
			if(facade.getCard(str).getColors().contains(COLOR_BLACK)){
				black = true;
				break;
			}
		}
		if(black){
			int pendingAttack = model.getPlayerCardList().get(index).getPendingAttackPoints();
			model.getPlayerCardList().get(index).setPendingAttackPoints(pendingAttack + ONE);
		}
	}
	private void hulkSmash(ModelFacade model){
		//Green: You get +5 Attack Power
		int index = model.getTurnTracker().getCurrentPlayer();
		boolean green = false;
		for(String str : model.getPlayerCardList().get(index).getPlayed()){
			if(facade.getCard(str).getColors().contains(COLOR_GREEN)){
				green = true;
				break;
			}
		}
		if(green){
			int pendingAttack = model.getPlayerCardList().get(index).getPendingAttackPoints();
			model.getPlayerCardList().get(index).setPendingAttackPoints(pendingAttack + FIVE);
		}
	}
	private void hypnoticCharm(ModelFacade model){
		//Reveal the top card of your deck. Discard it or put it back.
		//Yellow: Do the same thing to each other player's deck
		int index = model.getTurnTracker().getCurrentPlayer();
		List<Integer> effected = new ArrayList<Integer>();
		effected.add(index);
		boolean yellow = false;
		for(String str : model.getPlayerCardList().get(index).getPlayed()){
			if(facade.getCard(str).getColors().contains(COLOR_YELLOW)){
				yellow = true;
				break;
			}
		}
		if(yellow){
			for(int i = ZERO; i < model.getPlayerCardList().size(); ++i){
				if(i == index){
					continue;
				}
				if(model.getPlayerCardList().get(i).getDraw().size() == ZERO){
					model.getPlayerCardList().get(i).shuffleCards();
				}
				effected.add(i);
			}
		}
		State.HYPNOTIC_CHARM_ABILITY.setEffected(model.getGameID(), effected);
		model.getTurnTracker().setState(State.HYPNOTIC_CHARM_ABILITY);
		if(model.getPlayerCardList().get(index).getDraw().size() == ZERO){
			model.getPlayerCardList().get(index).shuffleCards();
		}
	}
	private void impossibleTrickShot(ModelFacade model){
		//Whenever you defeat a Villain or Mastermind this turn, rescue three Bystanders
		model.getTurnTracker().addToCardsInEffect(IMPOSSIBLE_TRICK_SHOT);
	}
	private void keenSenses(ModelFacade model){
		//Yellow: Draw a card
		int index = model.getTurnTracker().getCurrentPlayer();
		boolean yellow = false;
		for(String str : model.getPlayerCardList().get(index).getPlayed()){
			if(facade.getCard(str).getColors().contains(COLOR_YELLOW)){
				yellow = true;
				break;
			}
		}
		if(yellow){
			new DrawCard().go(model, index);
		}
	}
	private void legendaryCommander(ModelFacade model){
		//You get +1 Attack Power for each other SHIELD Hero you played this turn
		int index = model.getTurnTracker().getCurrentPlayer();
		int count = ZERO;
		for(String str : model.getPlayerCardList().get(index).getPlayed()){
			if(facade.getCard(str).getCategory().equals(CATEGORY_SHIELD)){
				++count;
			}
		}
		int pendingAttack = model.getPlayerCardList().get(index).getPendingAttackPoints();
		model.getPlayerCardList().get(index).setPendingAttackPoints(pendingAttack + (ONE * count));
	}
	private void lightningBolt(ModelFacade model){
		//Any Villain you fight on the Rooftops this turn gets -2 Hit Points
		model.getTurnTracker().addToCardsInEffect(LIGHTNING_BOLT);
	}
	private void mentalDiscipline(ModelFacade model){
		//Draw a card
		int index = model.getTurnTracker().getCurrentPlayer();
		new DrawCard().go(model, index);
	}
	private void missionAccomplished(ModelFacade model){
		//Draw a card
		//Black: Rescue a Bystander
		int index = model.getTurnTracker().getCurrentPlayer();
		new DrawCard().go(model, index);
		boolean black = false;
		for(String str : model.getPlayerCardList().get(index).getPlayed()){
			if(facade.getCard(str).getColors().contains(COLOR_BLACK)){
				black = true;
				break;
			}
		}
		if(black){
			try{
				model.getOtherCards().removeBystander(ONE);
				model.getPlayerCardList().get(index).getVictoryPile().add(BYSTANDER);
			}
			catch(InsufficientBystandersException e){}
		}
	}
	private void oddball(ModelFacade model){
		//You get +1 Attack Power for each other Hero with an odd-numberd Price you played this turn
		int index = model.getTurnTracker().getCurrentPlayer();
		int count = ZERO;
		for(String str : model.getPlayerCardList().get(index).getPlayed()){
			if(Integer.valueOf(facade.getCard(str).getPrice()) % TWO == ONE){
				++count;
			}
		}
		int pendingAttack = model.getPlayerCardList().get(index).getPendingAttackPoints();
		model.getPlayerCardList().get(index).setPendingAttackPoints(pendingAttack + (ONE * count));
	}
	private void odinson(ModelFacade model){
		//Green: You get +2 Buy Power
		int index = model.getTurnTracker().getCurrentPlayer();
		boolean green = false;
		for(String str : model.getPlayerCardList().get(index).getPlayed()){
			if(facade.getCard(str).getColors().contains(COLOR_GREEN)){
				green = true;
				break;
			}
		}
		if(green){
			int pendingBuy = model.getPlayerCardList().get(index).getPendingBuyPoints();
			model.getPlayerCardList().get(index).setPendingBuyPoints(pendingBuy + TWO);
		}
	}
	private void opticBlast(ModelFacade model){
		//To play this card, you must discard a card from your hand
		int index = model.getTurnTracker().getCurrentPlayer();
		List<Integer> effected = new ArrayList<Integer>();
		effected.add(index);
		State.CYCLOPS_ABILITY.setEffected(model.getGameID(), effected);
		model.getTurnTracker().setState(State.CYCLOPS_ABILITY);
	}
	private void perfectTeamwork(ModelFacade model){
		//You get +1 Attack Power for each color of Hero you have
		int index = model.getTurnTracker().getCurrentPlayer();
		Set<String> colors = new TreeSet<String>();
		for(String str : model.getPlayerCardList().get(index).getHand()){
			List<String> colorList = facade.getCard(str).getColors();
			colors.add(colorList.get(ZERO));
			colors.add(colorList.get(ONE));
			colors.add(colorList.get(TWO));
			colors.add(colorList.get(THREE));			
		}
		if(colors.contains(NA)){
			colors.remove(NA);
		}
		int pendingAttack = model.getPlayerCardList().get(index).getPendingAttackPoints();
		model.getPlayerCardList().get(index).setPendingAttackPoints(pendingAttack + (ONE * colors.size()));
	}
	private void psychicLink(ModelFacade model){
		//Each player may reveal another XMen Hero. Each player who does draws a card
		for(PlayerCards cards : model.getPlayerCardList()){
			boolean xMen = false;
			for(String str : cards.getHand()){
				if(facade.getCard(str).getCategory().equals(CATEGORY_X_MEN)){
					xMen = true;
					break;
				}
			}
			if(xMen){
				String str = cards.getFromDraw(ZERO);
				cards.removeFromDraw(ZERO);
				cards.getHand().add(str);
			}
		}
	}
	private void pureFury(ModelFacade model){
		//Defeat any Villain or Mastermind whose Hit Points is less than the number of SHIELD Heroes in the KO pile
		int shield = model.getKoPile().getShield();
		boolean success = false;
		String sewers = model.getCity().getSewers();
		String bank = model.getCity().getBank();
		String rooftops = model.getCity().getRooftops();
		String streets = model.getCity().getStreets();
		String bridge = model.getCity().getBridge();
		if(sewers != null && Integer.valueOf(facade.getCard(sewers).getHitPoints()) < shield){
			success = true;
		}
		else if(bank != null && Integer.valueOf(facade.getCard(bank).getHitPoints()) < shield){
			success = true;
		}
		else if(rooftops != null && Integer.valueOf(facade.getCard(rooftops).getHitPoints()) < shield){
			success = true;
		}
		else if(streets != null && Integer.valueOf(facade.getCard(streets).getHitPoints()) < shield){
			success = true;
		}
		else if(bridge != null && Integer.valueOf(facade.getCard(bridge).getHitPoints()) < shield){
			success = true;
		}
		else if(Integer.valueOf(facade.getCard(model.getTurnTracker().getMastermind()).getHitPoints()) < shield){
			success = true;
		}
		if(!success){
			return;
		}
		int index = model.getTurnTracker().getCurrentPlayer();
		List<Integer> effected = new ArrayList<Integer>();
		effected.add(index);
		State.PURE_FURY_ABILITY.setEffected(model.getGameID(), effected);
		model.getTurnTracker().setState(State.PURE_FURY_ABILITY);
	}
	private void quantumBreakthrough(ModelFacade model){
		//Draw two cards.
		//Black: Draw two more cards
		int index = model.getTurnTracker().getCurrentPlayer();
		new DrawCard().go(model, index);
		new DrawCard().go(model, index);
		boolean black = false;
		for(String str : model.getPlayerCardList().get(index).getPlayed()){
			if(facade.getCard(str).getColors().contains(COLOR_BLACK)){
				black = true;
				break;
			}
		}
		if(black){
			new DrawCard().go(model, index);
			new DrawCard().go(model, index);
		}
	}
	private void quickDraw(ModelFacade model){
		//Draw a card
		int index = model.getTurnTracker().getCurrentPlayer();
		new DrawCard().go(model, index);
	}
	private void randomActsOfUnkindness(ModelFacade model){
		//You may gain a Wound to your hand. Then each player passes a card from their hand to the player on their left
		int index = model.getTurnTracker().getCurrentPlayer();
		List<Integer> effected = new ArrayList<Integer>();
		effected.add(index);
		State.RANDOM_ACTS_OF_UNKINDNESS_ABILITY.setEffected(model.getGameID(), effected);
		model.getTurnTracker().setState(State.RANDOM_ACTS_OF_UNKINDNESS_ABILITY);
	}
	private void repulsorRays(ModelFacade model){
		//Blue: You get +1 Attack Power
		int index = model.getTurnTracker().getCurrentPlayer();
		boolean blue = false;
		for(String str : model.getPlayerCardList().get(index).getPlayed()){
			if(facade.getCard(str).getColors().contains(COLOR_BLUE)){
				blue = true;
				break;
			}
		}
		if(blue){
			int pendingAttack = model.getPlayerCardList().get(index).getPendingAttackPoints();
			model.getPlayerCardList().get(index).setPendingAttackPoints(pendingAttack + ONE);
		}
	}
	private void shadowedThoughts(ModelFacade model){
		//Red: You may play the top card of the Villain Deck. If you do, you get +2 Attack Power
		int index = model.getTurnTracker().getCurrentPlayer();
		boolean red = false;
		for(String str : model.getPlayerCardList().get(index).getPlayed()){
			if(facade.getCard(str).getColors().contains(COLOR_RED)){
				red = true;
				break;
			}
		}
		if(red){
			List<Integer> effected = new ArrayList<Integer>();
			effected.add(index);
			State.SHADOWED_THOUGHTS_ABILITY.setEffected(model.getGameID(), effected);
			model.getTurnTracker().setState(State.SHADOWED_THOUGHTS_ABILITY);
		}
	}
	private void silentSniper(ModelFacade model){
		//Defeat a Villain or Mastermind that has a Bystander
		boolean bystander = false;
		for(String str : model.getCity().getSewersCaptured()){
			if(str.equals(BYSTANDER)){
				bystander = true;
				break;
			}
		}
		if(bystander == false){
			for(String str : model.getCity().getBankCaptured()){
				if(str.equals(BYSTANDER)){
					bystander = true;
					break;
				}
			}
		}
		if(bystander == false){
			for(String str : model.getCity().getRooftopsCaptured()){
				if(str.equals(BYSTANDER)){
					bystander = true;
					break;
				}
			}
		}
		if(bystander == false){
			for(String str : model.getCity().getStreetsCaptured()){
				if(str.equals(BYSTANDER)){
					bystander = true;
					break;
				}
			}
		}
		if(bystander == false){
			for(String str : model.getCity().getBridgeCaptured()){
				if(str.equals(BYSTANDER)){
					bystander = true;
					break;
				}
			}
		}
		if(bystander == false){
			for(String str : model.getCity().getMastermindCaptured()){
				if(str.equals(BYSTANDER)){
					bystander = true;
					break;
				}
			}
		}
		if(bystander){
			int index = model.getTurnTracker().getCurrentPlayer();
			List<Integer> effected = new ArrayList<Integer>();
			effected.add(index);
			State.SILENT_SNIPER_ABILITY.setEffected(model.getGameID(), effected);
			model.getTurnTracker().setState(State.SILENT_SNIPER_ABILITY);
		}
	}
	private void spinningCyclone(ModelFacade model){
		//You may move a Villain to a new city space. Rescue any Bystanders captured by that Villain.
		//(If you move a Villain to a city space that already has a Villain, swap them)
		boolean success = false;
		if(model.getCity().getSewers() != null){
			success = true;
		}
		if(model.getCity().getBank() != null){
			success = true;
		}
		if(model.getCity().getRooftops() != null){
			success = true;
		}
		if(model.getCity().getStreets() != null){
			success = true;
		}
		if(model.getCity().getBridge() != null){
			success = true;
		}
		
		if(success){
			int index = model.getTurnTracker().getCurrentPlayer();
			List<Integer> effected = new ArrayList<Integer>();
			effected.add(index);
			State.SPINNING_CYCLONE_ABILITY.setEffected(model.getGameID(), effected);
			model.getTurnTracker().setState(State.SPINNING_CYCLONE_ABILITY);
		}
	}
	private void stackTheDeck(ModelFacade model){
		//Draw two cards. Then put a card from your hand on top of your deck
		int index = model.getTurnTracker().getCurrentPlayer();
		new DrawCard().go(model, index);
		new DrawCard().go(model, index);
		List<Integer> effected = new ArrayList<Integer>();
		effected.add(index);
		State.STACK_THE_DECK_ABILITY.setEffected(model.getGameID(), effected);
		model.getTurnTracker().setState(State.STACK_THE_DECK_ABILITY);
	}
	private void stealAbilities(ModelFacade model){
		//Each player discards the top card of their deck. Play a copy of each of those cards
		int index = model.getTurnTracker().getCurrentPlayer();
		for(PlayerCards cards : model.getPlayerCardList()){
			String str = cards.getFromDraw(ZERO);
			cards.removeFromDraw(ZERO);
			cards.getDiscard().add(str);
			new PlayCard(facade).increasePendingPoints(model, str);
			new CardAbility(facade).go(model, str);
			model.getPlayerCardList().get(index).getPlayed().add(str + PERIOD);
		}
	}
	private void surgeOfPower(ModelFacade model){
		//If you made 8 or more Buy Power this turn, you get +3 Attack Power
		int index = model.getTurnTracker().getCurrentPlayer();
		int pendingBuyPoints = model.getPlayerCardList().get(index).getPendingBuyPoints();
		if(pendingBuyPoints >= EIGHT){
			int pendingAttackPoints = model.getPlayerCardList().get(index).getPendingAttackPoints();
			model.getPlayerCardList().get(index).setPendingAttackPoints(THREE + pendingAttackPoints);
		}
	}
	private void teamPlayer(ModelFacade model){
		//Avenger: You get +1 Attack Power
		int index = model.getTurnTracker().getCurrentPlayer();
		boolean avenger = false;
		for(String str : model.getPlayerCardList().get(index).getPlayed()){
			if(facade.getCard(str).getCategory().equals(CATEGORY_AVENGERS)){
				avenger = true;
				break;
			}
		}
		if(avenger){
			int pendingAttack = model.getPlayerCardList().get(index).getPendingAttackPoints();
			model.getPlayerCardList().get(index).setPendingAttackPoints(pendingAttack + ONE);
		}
	}
	private void theAmazingSpiderman(ModelFacade model){
		//Reveal the top three cards of your deck. Put any that cost 2 or less into your hand. Put the rest back in any order
		int index = model.getTurnTracker().getCurrentPlayer();
		List<String> above = new ArrayList<String>();
		String one = model.getPlayerCardList().get(index).getFromDraw(ZERO);
		model.getPlayerCardList().get(index).removeFromDraw(ZERO);
		String two = model.getPlayerCardList().get(index).getFromDraw(ZERO);
		model.getPlayerCardList().get(index).removeFromDraw(ZERO);
		String three = model.getPlayerCardList().get(index).getFromDraw(ZERO);
		model.getPlayerCardList().get(index).removeFromDraw(ZERO);
		int onePrice = Integer.valueOf(facade.getCard(one).getPrice());
		int twoPrice = Integer.valueOf(facade.getCard(two).getPrice());
		int threePrice = Integer.valueOf(facade.getCard(three).getPrice());
		if(threePrice <= TWO){
			model.getPlayerCardList().get(index).getHand().add(three);
		}
		else{
			above.add(three);
			model.getPlayerCardList().get(index).addToDraw(ZERO, three);
		}
		if(twoPrice <= TWO){
			model.getPlayerCardList().get(index).getHand().add(two);
		}
		else{
			above.add(two);
			model.getPlayerCardList().get(index).addToDraw(ZERO, two);
		}
		if(onePrice <= TWO){
			model.getPlayerCardList().get(index).getHand().add(one);
		}
		else{
			above.add(one);
			model.getPlayerCardList().get(index).addToDraw(ZERO, one);
		}
		if(above.size() > ZERO){
			List<Integer> effected = new ArrayList<Integer>();
			for(int i = ZERO; i < above.size(); ++i){
				effected.add(index);
			}
			State.THE_AMAZING_SPIDERMAN_ABILITY.setEffected(model.getGameID(), effected);
			model.getTurnTracker().setState(State.THE_AMAZING_SPIDERMAN_ABILITY);
		}
		
	}
	private void tidalWave(ModelFacade model){
		//Any Villain you fight on the Bridge this turn gets -2 Hit Points.
		//Blue: The Mastermind gets -2 Hit Points this turn
		int index = model.getTurnTracker().getCurrentPlayer();
		boolean blue = false;
		for(String str : model.getPlayerCardList().get(index).getPlayed()){
			if(facade.getCard(str).getColors().contains(COLOR_BLUE)){
				blue = true;
				break;
			}
		}
		if(blue){
			model.getTurnTracker().addToCardsInEffect(TIDAL_WAVE_MASTERMIND);
		}
		model.getTurnTracker().addToCardsInEffect(TIDAL_WAVE);
	}
	private void unstoppableHulk(ModelFacade model){
		//You may KO a Wound from your hand or discard pile. If you do, you get +2 Attack Power
		int index = model.getTurnTracker().getCurrentPlayer();
		boolean wound = false;
		for(String str : model.getPlayerCardList().get(index).getHand()){
			if(str.equals(WOUND)){
				wound = true;
				break;
			}
		}
		if(wound == false){
			for(String str : model.getPlayerCardList().get(index).getDiscard()){
				if(str.equals(WOUND)){
					wound = true;
					break;
				}
			}
		}
		if(wound){
			List<Integer> effected = new ArrayList<Integer>();
			effected.add(index);
			State.UNSTOPPABLE_HULK_ABILITY.setEffected(model.getGameID(), effected);
			model.getTurnTracker().setState(State.UNSTOPPABLE_HULK_ABILITY);
		}
	}
	private void webShooters(ModelFacade model){
		//Rescue a Bystander.
		//Reveal the top card of your deck. If that card costs 2 or less, draw it
		int index = model.getTurnTracker().getCurrentPlayer();
		try{
			model.getOtherCards().removeBystander(ONE);
			model.getPlayerCardList().get(index).getVictoryPile().add(BYSTANDER);
		}
		catch(InsufficientBystandersException e){}
		String str = model.getPlayerCardList().get(index).getFromDraw(ZERO);
		if(Integer.valueOf(facade.getCard(str).getPrice()) <= TWO){
			model.getPlayerCardList().get(index).getHand().add(str);
			model.getPlayerCardList().get(index).removeFromDraw(ZERO);
		}
	}
	private void xMenUnited(ModelFacade model){
		//XMen: You get +2 Attack Power for each other XMen Hero you played this turn
		int index = model.getTurnTracker().getCurrentPlayer();
		int count = ZERO;
		for(String str : model.getPlayerCardList().get(index).getPlayed()){
			if(facade.getCard(str).getCategory().equals(CATEGORY_X_MEN)){
				++count;
			}
		}
		int pendingAttack = model.getPlayerCardList().get(index).getPendingAttackPoints();
		model.getPlayerCardList().get(index).setPendingAttackPoints(pendingAttack + (TWO * count));
	}
}
