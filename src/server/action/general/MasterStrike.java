package server.action.general;

import java.util.ArrayList;
import java.util.List;

import server.dao.DAOFacade;
import server.definition.ServerVariables;
import server.exceptions.InsufficientWoundsException;
import shared.definitions.State;
import shared.log.Log;
import shared.model.ModelFacade;
import shared.model.OtherCards;
import shared.model.PlayerCards;

public class MasterStrike implements ServerVariables{
	
	private String className = "server.action.MasterStrike: ";
	private DAOFacade facade;

	public MasterStrike(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model){
		String mastermind = model.getTurnTracker().getMastermind();
		
		switch(mastermind){
		case DR_DOOM:
			drDoom(model);
			break;
		case LOKI:
			loki(model);
			break;
		case MAGNETO:
			magneto(model);
			break;
		case RED_SKULL:
			redSkull(model);
			break;
		default:
			Log.logError(ZERO, className + mastermind + ERROR_NOT_FOUND);
		}
	}
	
	private void drDoom(ModelFacade model){
		//Each player with exactly 6 cards in hand reveals a Black Hero or puts 2 cards from their hand on top of their deck
		List<PlayerCards> players = model.getPlayerCardList();
		List<Integer> effected = new ArrayList<Integer>();
		for(int i = ZERO; i < players.size(); ++i){
			if(players.get(i).getHand().size() == SIX){
				boolean black = false;
				for(String str : players.get(i).getHand()){
					if(facade.getCard(str).getColors().contains(COLOR_BLACK)){
						black = true;
						break;
					}
				}
				if(black == false){
					effected.add(i);
					effected.add(i);
				}
			}
		}
		if(effected.size() != ZERO){
			State.DR_DOOM_MASTER_STRIKE.setEffected(model.getGameID(), effected);
			model.getTurnTracker().setState(State.DR_DOOM_MASTER_STRIKE);
		}
	}
	private void loki(ModelFacade model){
		//Each player reveals a Green Hero or gains a Wound
		List<PlayerCards> players = model.getPlayerCardList();
		for(int i = ZERO; i < players.size(); ++i){
			boolean green = false;
			for(String str : players.get(i).getHand()){
				if(facade.getCard(str).getColors().contains(COLOR_GREEN)){
					green = true;
					break;
				}
			}
			if(green == false){
				try{
					OtherCards c = model.getOtherCards();
					List<String> discard = model.getPlayerCardList().get(i).getDiscard();
					model.getPlayerCardList().get(i).addWound(c, discard);
				}
				catch(InsufficientWoundsException e){}
			}
		}		
	}
	private void magneto(ModelFacade model){
		//Each player reveals an X-Men Hero or discards down to four cards
		List<PlayerCards> players = model.getPlayerCardList();
		List<Integer> effected = new ArrayList<Integer>();
		for(int i = ZERO; i < players.size(); ++i){
			boolean xMen = false;
			for(String str : players.get(i).getHand()){
				if(facade.getCard(str).getCategory().equals(CATEGORY_X_MEN)){
					xMen = true;
					break;
				}
			}
			if(xMen == false){
				int size = players.get(i).getHand().size();
				for(int j = FOUR; j < size; ++j){
					effected.add(i);
				}
			}
		}
		
		if(effected.size() != ZERO){
			State.MAGNETO_MASTER_STRIKE.setEffected(model.getGameID(), effected);
			model.getTurnTracker().setState(State.MAGNETO_MASTER_STRIKE);
		}
	}
	private void redSkull(ModelFacade model){
		//Each player KOs a Hero from their hand
		List<Integer> effected = new ArrayList<Integer>();
		for(int i = ZERO; i < model.getPlayerCardList().size(); ++i){
			effected.add(i);
		}

		State.RED_SKULL_MASTER_STRIKE.setEffected(model.getGameID(), effected);
		model.getTurnTracker().setState(State.RED_SKULL_MASTER_STRIKE);
	}
}
