package server.action.general;

import server.dao.DAOFacade;
import server.definition.ServerVariables;
import shared.definitions.UserHeader;

public class Login implements ServerVariables{

	private DAOFacade facade;
	
	public Login(DAOFacade facade){
		this.facade = facade;
	}
	
	public int go(String username, String password){
		UserHeader user = facade.checkUser(username, password);
		if(user == null){
			return NEGATIVE_ONE;
		}
		return user.getId();
	}
}
