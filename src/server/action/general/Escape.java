package server.action.general;

import java.util.ArrayList;
import java.util.List;

import server.dao.DAOFacade;
import server.definition.ServerVariables;
import server.exceptions.InsufficientWoundsException;
import shared.definitions.State;
import shared.log.Log;
import shared.model.ModelFacade;
import shared.model.OtherCards;
import shared.model.PlayerCards;

public class Escape implements ServerVariables{

	private final String CLASS = "server.action.Escape.java: ";

	private DAOFacade facade;
	
	public Escape(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, String name){
		Log.logError(model.getGameID(), CLASS + LOG_ESCAPE + name);
		switch(name){
		case DESTROYER:
			destroyer(model);
			break;
		case FROST_GIANT:
			frostGiant(model);
			break;
		case JUGGERNAUT:
			juggernaut(model);
			break;
		case MYSTIQUE:
			mystique(model);
			break;
		case SABRETOOTH:
			sabretooth(model);
			break;
		case ULTRON:
			ultron(model);
			break;
		case VENOM:
			venom(model);
			break;
		case VIPER:
			viper(model);
			break;
		case ZZZAX:
			zzzax(model);
			break;
		}
	}

	private void destroyer(ModelFacade model){
		//Each player KOs two of their Heroes
		List<Integer> effected = new ArrayList<Integer>();
		for(int i = ZERO; i < model.getPlayerCardList().size(); ++i){
			effected.add(i);
			effected.add(i);
		}
		State.DESTROYER_ESCAPE.setEffected(model.getGameID(), effected);
		model.getTurnTracker().setState(State.DESTROYER_ESCAPE);
	}
	private void frostGiant(ModelFacade model){
		//Same effect
		new Fight(facade).go(model, FROST_GIANT, LOCATION_ESCAPED);
	}
	private void juggernaut(ModelFacade model){
		//Each player KOs two Heroes from their hand
		List<Integer> effected = new ArrayList<Integer>();
		for(int i = ZERO; i < model.getPlayerCardList().size(); ++i){
			effected.add(i);
			effected.add(i);
		}
		State.JUGGERNAUT_ESCAPE.setEffected(model.getGameID(), effected);
		model.getTurnTracker().setState(State.JUGGERNAUT_ESCAPE);
	}
	private void mystique(ModelFacade model){
		//Mystique becomes a Scheme Twist that takes effect immediately
		new SchemeTwist(facade).go(model);
	}
	private void sabretooth(ModelFacade model){
		//Same effect
		new Fight(facade).go(model, SABRETOOTH, LOCATION_ESCAPED);
	}
	private void ultron(ModelFacade model){
		//Each player reveals a Black Hero or gains a Wound
		for(PlayerCards cards : model.getPlayerCardList()){
			boolean black = false;
			for(String str : cards.getHand()){
				if(facade.getCard(str).getColors().contains(COLOR_BLACK)){
					black = true;
					break;
				}
			}
			if(black == false){
				try{
					OtherCards c = model.getOtherCards();
					List<String> discard = cards.getDiscard();
					cards.addWound(c, discard);
				}
				catch(InsufficientWoundsException e){}
			}
		}
	}
	private void venom(ModelFacade model){
		//Each player gains a Wound
		for(PlayerCards cards : model.getPlayerCardList()){
			try{
				OtherCards c = model.getOtherCards();
				List<String> discard = cards.getDiscard();
				cards.addWound(c, discard);
			}
			catch(InsufficientWoundsException e){}
		}
	}
	private void viper(ModelFacade model){
		//Same effect
		new Fight(facade).go(model, VIPER, LOCATION_ESCAPED);
	}
	private void zzzax(ModelFacade model){
		//Same effect
		new Fight(facade).go(model, ZZZAX, LOCATION_ESCAPED);
	}
}
