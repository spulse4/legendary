package server.action.general;

import shared.model.ModelFacade;

public class SetScheme {

	public SetScheme(){}
	
	public void go(ModelFacade model, String scheme){
		model.getTurnTracker().setScheme(scheme);
	}
}
