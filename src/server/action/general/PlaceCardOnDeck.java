package server.action.general;

import java.util.List;

import server.definition.ServerVariables;
import shared.model.ModelFacade;

public class PlaceCardOnDeck implements ServerVariables{

	public PlaceCardOnDeck(){}
	
	public void go(ModelFacade model, int playerIndex, String name){
		List<String> hand = model.getPlayerCardList().get(playerIndex).getHand();
		
		for(int i = ZERO; i < hand.size(); ++i){
			if(hand.get(i).equals(name)){
				hand.remove(i);
				break;
			}
		}
		
		model.getPlayerCardList().get(playerIndex).addToDraw(ZERO, name);
	}
}
