package server.action.general;

import java.util.ArrayList;
import java.util.List;

import server.dao.DAOFacade;
import server.definition.ServerCard;
import server.definition.ServerVariables;
import server.exceptions.InsufficientBystandersException;
import server.exceptions.InsufficientWoundsException;
import shared.definitions.State;
import shared.model.ModelFacade;
import shared.model.OtherCards;
import shared.model.PlayerCards;

public class Ambush implements ServerVariables{

	private DAOFacade facade;
	
	public Ambush(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, String name){
		switch(name){
		case GREEN_GOBLIN:
			greenGoblin(model);
			break;
		case JUGGERNAUT:
			juggernaut(model);
			break;
		case SKRULL_QUEEN_VERANKE:
			skrullQueenVeranke(model);
			break;
		case SKRULL_SHAPESHIFTERS:
			skrullShapeshifters(model);
			break;
		case THE_LEADER:
			theLeader(model);
			return;
		case YMIR_FROST_GIANT_KING:
			ymirFrostGiantKing(model);
			break;
		}
	}

	private void greenGoblin(ModelFacade model) {
		//Green Goblin captures a Bystander
		try{
			model.getOtherCards().removeBystander(ONE);
			model.getCity().getSewersCaptured().add(BYSTANDER);
		}
		catch(InsufficientBystandersException e){}
	}
	private void juggernaut(ModelFacade model){
		//Each player KOs two Heroes from their discard pile
		List<Integer> effected = new ArrayList<Integer>();
		for(int i = ZERO; i < model.getPlayerCardList().size(); ++i){
			List<String> discard = model.getPlayerCardList().get(i).getDiscard();
			List<String> newDiscard = new ArrayList<String>();
			for(String str : discard){
				if(!str.equals(WOUND)){
					newDiscard.add(str);
				}
			}
			if(newDiscard.size() == ZERO){
				continue;
			}
			if(newDiscard.size() != ONE){
				effected.add(i);
			}
			effected.add(i);
		}
		State.JUGGERNAUT_AMBUSH.setEffected(model.getGameID(), effected);
		model.getTurnTracker().setState(State.JUGGERNAUT_AMBUSH);
	}
	private void skrullQueenVeranke(ModelFacade model){
		//Put the hightest-cost Hero from the HQ under this Villain. This Villain's Hit Points is equal to that Hero's Price
		ServerCard hq1 = facade.getCard(model.getHeadQuarters().getHQ1());
		ServerCard hq2 = facade.getCard(model.getHeadQuarters().getHQ2());
		ServerCard hq3 = facade.getCard(model.getHeadQuarters().getHQ3());
		ServerCard hq4 = facade.getCard(model.getHeadQuarters().getHQ4());
		ServerCard hq5 = facade.getCard(model.getHeadQuarters().getHQ5());
		int highestCost = Integer.valueOf(hq1.getPrice());
		if(Integer.valueOf(hq2.getPrice()) > highestCost){
			highestCost = Integer.valueOf(hq2.getPrice());
		}
		if(Integer.valueOf(hq3.getPrice()) > highestCost){
			highestCost = Integer.valueOf(hq3.getPrice());
		}
		if(Integer.valueOf(hq4.getPrice()) > highestCost){
			highestCost = Integer.valueOf(hq4.getPrice());
		}
		if(Integer.valueOf(hq5.getPrice()) > highestCost){
			highestCost = Integer.valueOf(hq5.getPrice());
		}
		
		List<String> heroes = model.getOtherCards().getHeroes();
		if(Integer.valueOf(hq1.getPrice()) == highestCost){
			model.getCity().getSewersCaptured().add(hq1.getName());
			model.getHeadQuarters().setHQ1(heroes.get(ZERO));
		}
		else if(Integer.valueOf(hq2.getPrice()) == highestCost){
			model.getCity().getSewersCaptured().add(hq2.getName());
			model.getHeadQuarters().setHQ2(heroes.get(ZERO));
		}
		else if(Integer.valueOf(hq3.getPrice()) == highestCost){
			model.getCity().getSewersCaptured().add(hq3.getName());
			model.getHeadQuarters().setHQ3(heroes.get(ZERO));
		}
		else if(Integer.valueOf(hq4.getPrice()) == highestCost){
			model.getCity().getSewersCaptured().add(hq4.getName());
			model.getHeadQuarters().setHQ4(heroes.get(ZERO));
		}
		else if(Integer.valueOf(hq5.getPrice()) == highestCost){
			model.getCity().getSewersCaptured().add(hq5.getName());
			model.getHeadQuarters().setHQ5(heroes.get(ZERO));
		}
		heroes.remove(ZERO);
	}
	private void skrullShapeshifters(ModelFacade model){
		//Put the rightmost Hero from the HQ under this Villain. This Villain's Hit Points is equal to that Hero's Price
		model.getCity().getSewersCaptured().add(model.getHeadQuarters().getHQ5());
		List<String> heroList = model.getOtherCards().getHeroes();
		model.getHeadQuarters().setHQ5(heroList.get(ZERO));
		heroList.remove(ZERO);
	}
	private void theLeader(ModelFacade model){
		//Play the top card of the Villain Deck
		new PlayVillianCard(facade).go(model);		
	}
	private void ymirFrostGiantKing(ModelFacade model){
		//Each player reveals a Blue Hero or gains a Wound.
		List<PlayerCards> playerCards = model.getPlayerCardList();
		for(int i = ZERO; i < playerCards.size(); ++i){
			List<String> playerHand = playerCards.get(i).getHand();
			boolean blue = false;
			for(int j = ZERO; j < playerHand.size(); ++j){
				ServerCard c = facade.getCard(playerHand.get(j));
				if(c.getColors().contains(COLOR_BLUE)){
					blue = true;
					break;
				}
			}
			if(blue == false){
				try{
					OtherCards cards = model.getOtherCards();
					List<String> discard = model.getPlayerCardList().get(i).getDiscard();
					model.getPlayerCardList().get(i).addWound(cards, discard);
				}
				catch(InsufficientWoundsException e){}
			}
		}
	}
}
