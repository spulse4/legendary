package server.action.general;

import server.dao.DAOFacade;

public class Register {

	private DAOFacade facade;
	
	public Register(DAOFacade facade){
		this.facade = facade;
	}
	
	public int go(String username, String password){
		return facade.addUser(username, password).getId();
	}
}
