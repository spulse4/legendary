package server.action.general;

import shared.definitions.State;
import shared.model.ModelFacade;

public class StopTest {

	public StopTest(){}
	
	public void go(ModelFacade model){
		model.getTurnTracker().setState(State.PLAYING);
	}
}
