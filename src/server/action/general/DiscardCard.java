package server.action.general;

import java.util.List;

import server.definition.ServerVariables;
import shared.log.Log;
import shared.model.ModelFacade;

public class DiscardCard implements ServerVariables{

	private final String CLASS = "server.action.DiscardCard.java: ";

	public DiscardCard(){}
	
	public void go(ModelFacade model, int playerIndex, String name){
		Log.logError(model.getGameID(), CLASS + playerIndex + SPACE + LOG_DISCARD_CARD + name);
		if(name.equals(UNENDING_ENERGY)){
			return;
		}
		List<String> hand = model.getPlayerCardList().get(playerIndex).getHand();
		List<String> discard = model.getPlayerCardList().get(playerIndex).getDiscard();
		
		for(int i = ZERO; i < hand.size(); ++i){
			if(hand.get(i).equals(name)){
				hand.remove(i);
				break;
			}
		}
		discard.add(name);
	}
}
