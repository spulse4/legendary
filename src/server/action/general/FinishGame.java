package server.action.general;

import shared.definitions.State;
import shared.model.ModelFacade;

public class FinishGame {

	public FinishGame(){}
	
	public void go(ModelFacade model, boolean victory){		
		model.getTurnTracker().setFinished(true);
		if(victory){
			model.getTurnTracker().setState(State.WON);
		}
		else{
			model.getTurnTracker().setState(State.LOST);			
		}
		model.getGameHeader().setFinished(true);
	}
}
