package server.action.general;

import server.dao.DAOFacade;

public class CreateGame {

	private DAOFacade facade;
	
	public CreateGame(DAOFacade facade){
		this.facade = facade;
	}
	
	public int go(String title){
		return facade.createGame(title);
	}
}
