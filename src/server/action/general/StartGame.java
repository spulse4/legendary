package server.action.general;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import client.main.ClientManager;
import server.dao.DAOFacade;
import server.definition.ServerVariables;
import shared.definitions.GameHeader;
import shared.model.ModelFacade;

public class StartGame implements ServerVariables{

	private int bystanderCount = INITIAL_BYSTANDER_COUNT;
	private int woundCount = INITIAL_WOUND_COUNT;
	
	private DAOFacade facade;
	
	public StartGame(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model){
		List<GameHeader> games = facade.getGames();
		for(GameHeader game : games){
			if(game.getGameID() == model.getGameID()){
				facade.startGame(model);
				break;
			}
		}
		
		String mastermind = model.getTurnTracker().getMastermind();
		String scheme = model.getTurnTracker().getScheme();
		List<String> heroes = model.getTurnTracker().getHeroes();
		List<String> villians = model.getTurnTracker().getVillians();
		List<String> henchmen = model.getTurnTracker().getHenchmen();
		List<String> masterminds = model.getOtherCards().getMasterminds();
		List<String> heroCards = model.getOtherCards().getHeroes();
		List<String> villianCards = model.getOtherCards().getVillians();
		
		int masterstrikes = FIVE;
		if(model.getPlayerCardList().size() == ONE){
			masterstrikes = ONE;
		}
		for(int i = ZERO; i < masterstrikes; ++i){
			villianCards.add(MASTERSTRIKE);
		}
		

		fillMastermind(mastermind, masterminds);
		fillHeroes(heroes, heroCards);
		fillVillians(villians, villianCards);
		fillHenchmen(henchmen, villianCards, model.getGameHeader().getPlayers().size());
		fillScheme(scheme, model, heroCards, villianCards, model.getPlayerCardList().size());
		
		masterminds = shuffle(masterminds, true);
		heroCards = shuffle(heroCards, false);
		villianCards = shuffle(villianCards, false);
		
		model.getOtherCards().setBystanders(bystanderCount);
		model.getOtherCards().setOfficers(INITIAL_OFFICER_COUNT);
		model.getOtherCards().setWounds(woundCount);
		model.getOtherCards().setMasterminds(masterminds);
		model.getOtherCards().setVillians(villianCards);
		model.getHeadQuarters().setHQ1(heroCards.get(ZERO));
		model.getHeadQuarters().setHQ2(heroCards.get(ONE));
		model.getHeadQuarters().setHQ3(heroCards.get(TWO));
		model.getHeadQuarters().setHQ4(heroCards.get(THREE));
		model.getHeadQuarters().setHQ5(heroCards.get(FOUR));
		heroCards.remove(ZERO);
		heroCards.remove(ZERO);
		heroCards.remove(ZERO);
		heroCards.remove(ZERO);
		heroCards.remove(ZERO);
		model.getOtherCards().setHeroes(heroCards);

		new PlayVillianCard(facade).go(model);
	}
	
	private void fillMastermind(String mastermind, List<String> masterminds){
		switch(mastermind){
		case DR_DOOM:
			masterminds.add(DR_DOOM);
			masterminds.add(DARK_TECHNOLOGY);
			masterminds.add(MONARCHS_DECREE);
			masterminds.add(SECRETS_OF_TIME_TRAVEL);
			masterminds.add(TREASURES_OF_LATVERIA);
			break;
		case LOKI:
			masterminds.add(LOKI);
			masterminds.add(CRUEL_RULER);
			masterminds.add(MANIACAL_TYRANT);
			masterminds.add(VANISHING_ILLUSIONS);
			masterminds.add(WHISPERS_AND_LIES);
			break;
		case MAGNETO:
			masterminds.add(MAGNETO);
			masterminds.add(BITTER_CAPTOR);
			masterminds.add(CRUSHING_SHOCKWAVE);
			masterminds.add(ELECTROMAGNETIC_BUBBLE);
			masterminds.add(XAVIERS_NEMESIS);
			break;
		case RED_SKULL:
			masterminds.add(RED_SKULL);
			masterminds.add(ENDLESS_RESOURCES);
			masterminds.add(HYDRA_CONSPIRACY);
			masterminds.add(NEGABLAST_GRENADES);
			masterminds.add(RUTHLESS_DICTATOR);
			break;
		}
	}
	private void fillHeroes(List<String> heroes, List<String> heroCards){
		if(heroes.contains(BLACK_WIDOW)){
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(MISSION_ACCOMPLISHED);
			}
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(DANGEROUS_RESCUE);
			}
			for(int i = ZERO; i < THREE; ++i){
				heroCards.add(COVERT_OPERATION);
			}
			heroCards.add(SILENT_SNIPER);
		}
		if(heroes.contains(CAPTAIN_AMERICA)){
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(PERFECT_TEAMWORK);
			}
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(AVENGERS_ASSEMBLE);
			}
			for(int i = ZERO; i < THREE; ++i){
				heroCards.add(DIVING_BLOCK);
			}
			heroCards.add(A_DAY_UNLIKE_ANY_OTHER);
		}
		if(heroes.contains(CYCLOPS)){
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(DETERMINATION);
			}
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(OPTIC_BLAST);
			}
			for(int i = ZERO; i < THREE; ++i){
				heroCards.add(UNENDING_ENERGY);
			}
			heroCards.add(X_MEN_UNITED);
		}
		if(heroes.contains(DEADPOOL)){
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(HERE_HOLD_THIS_FOR_A_SECOND);
			}
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(ODDBALL);
			}
			for(int i = ZERO; i < THREE; ++i){
				heroCards.add(HEY_CAN_I_GET_A_DO_OVER);
			}
			heroCards.add(RANDOM_ACTS_OF_UNKINDNESS);
		}
		if(heroes.contains(EMMA_FROST)){
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(SHADOWED_THOUGHTS);
			}
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(MENTAL_DISCIPLINE);
			}
			for(int i = ZERO; i < THREE; ++i){
				heroCards.add(PSYCHIC_LINK);
			}
			heroCards.add(DIAMOND_FORM);
		}
		if(heroes.contains(GAMBIT)){
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(STACK_THE_DECK);
			}
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(CARD_SHARK);
			}
			for(int i = ZERO; i < THREE; ++i){
				heroCards.add(HYPNOTIC_CHARM);
			}
			heroCards.add(HIGH_STAKES_JACKPOT);
		}
		if(heroes.contains(HAWKEYE)){
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(TEAM_PLAYER);
			}
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(QUICK_DRAW);
			}
			for(int i = ZERO; i < THREE; ++i){
				heroCards.add(COVERING_FIRE);
			}
			heroCards.add(IMPOSSIBLE_TRICK_SHOT);
		}
		if(heroes.contains(HULK)){
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(GROWING_ANGER);
			}
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(UNSTOPPABLE_HULK);
			}
			for(int i = ZERO; i < THREE; ++i){
				heroCards.add(CRAZED_RAMPAGE);
			}
			heroCards.add(HULK_SMASH);
		}
		if(heroes.contains(IRON_MAN)){
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(ENDLESS_INVENTION);
			}
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(REPULSOR_RAYS);
			}
			for(int i = ZERO; i < THREE; ++i){
				heroCards.add(ARC_REACTOR);
			}
			heroCards.add(QUANTUM_BREAKTHROUGH);
		}
		if(heroes.contains(NICK_FURY)){
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(HIGH_TECH_WEAPONRY);
			}
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(BATTLEFIELD_PROMOTION);
			}
			for(int i = ZERO; i < THREE; ++i){
				heroCards.add(LEGENDARY_COMMANDER);
			}
			heroCards.add(PURE_FURY);
		}
		if(heroes.contains(ROGUE)){
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(ENERGY_DRAIN);
			}
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(BORROWED_BRAWN);
			}
			for(int i = ZERO; i < THREE; ++i){
				heroCards.add(COPY_POWERS);
			}
			heroCards.add(STEAL_ABILITIES);
		}
		if(heroes.contains(SPIDERMAN)){
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(ASTONISHING_STRENGTH);
			}
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(GREAT_RESPONSIBILITY);
			}
			for(int i = ZERO; i < THREE; ++i){
				heroCards.add(WEB_SHOOTERS);
			}
			heroCards.add(THE_AMAZING_SPIDERMAN);
		}
		if(heroes.contains(STORM)){
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(GATHERING_STORMCLOUDS);
			}
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(LIGHTNING_BOLT);
			}
			for(int i = ZERO; i < THREE; ++i){
				heroCards.add(SPINNING_CYCLONE);
			}
			heroCards.add(TIDAL_WAVE);
		}
		if(heroes.contains(THOR)){
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(ODINSON);
			}
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(SURGE_OF_POWER);
			}
			for(int i = ZERO; i < THREE; ++i){
				heroCards.add(CALL_LIGHTNING);
			}
			heroCards.add(GOD_OF_THUNDER);
		}
		if(heroes.contains(WOLVERINE)){
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(KEEN_SENSES);
			}
			for(int i = ZERO; i < FIVE; ++i){
				heroCards.add(HEALING_FACTOR);
			}
			for(int i = ZERO; i < THREE; ++i){
				heroCards.add(FRENZIED_SLASHING);
			}
			heroCards.add(BERSERKER_RAGE);
		}
	}
	private void fillVillians(List<String> villians, List<String> villianCards){
		if(villians.contains(CATEGORY_BROTHERHOOD)){
			villianCards.add(BLOB);
			villianCards.add(BLOB);
			villianCards.add(JUGGERNAUT);
			villianCards.add(JUGGERNAUT);
			villianCards.add(MYSTIQUE);
			villianCards.add(MYSTIQUE);
			villianCards.add(SABRETOOTH);
			villianCards.add(SABRETOOTH);
		}
		if(villians.contains(CATEGORY_ENEMIES_OF_ASGARD)){
			villianCards.add(YMIR_FROST_GIANT_KING);
			villianCards.add(YMIR_FROST_GIANT_KING);
			villianCards.add(FROST_GIANT);
			villianCards.add(FROST_GIANT);
			villianCards.add(FROST_GIANT);
			villianCards.add(ENCHANTRESS);
			villianCards.add(ENCHANTRESS);
			villianCards.add(DESTROYER);
		}
		if(villians.contains(CATEGORY_HYDRA)){
			villianCards.add(ENDLESS_ARMIES_OF_HYDRA);
			villianCards.add(ENDLESS_ARMIES_OF_HYDRA);
			villianCards.add(ENDLESS_ARMIES_OF_HYDRA);
			villianCards.add(HYDRA_KIDNAPPERS);
			villianCards.add(HYDRA_KIDNAPPERS);
			villianCards.add(HYDRA_KIDNAPPERS);
			villianCards.add(SUPREME_HYDRA);
			villianCards.add(VIPER);
		}
		if(villians.contains(CATEGORY_MASTERS_OF_EVIL)){
			villianCards.add(BARON_ZEMO);
			villianCards.add(BARON_ZEMO);
			villianCards.add(MELTER);
			villianCards.add(MELTER);
			villianCards.add(ULTRON);
			villianCards.add(ULTRON);
			villianCards.add(WHIRLWIND);
			villianCards.add(WHIRLWIND);
		}
		if(villians.contains(CATEGORY_RADIATION)){
			villianCards.add(ABOMINATION);
			villianCards.add(ABOMINATION);
			villianCards.add(THE_LEADER);
			villianCards.add(THE_LEADER);
			villianCards.add(MAESTRO);
			villianCards.add(MAESTRO);
			villianCards.add(ZZZAX);
			villianCards.add(ZZZAX);
		}
		if(villians.contains(CATEGORY_SKRULLS)){
			villianCards.add(PAIBOK_THE_POWER_SKRULL);
			villianCards.add(SKRULL_QUEEN_VERANKE);
			villianCards.add(SKRULL_SHAPESHIFTERS);
			villianCards.add(SKRULL_SHAPESHIFTERS);
			villianCards.add(SKRULL_SHAPESHIFTERS);
			villianCards.add(SUPER_SKRULL);
			villianCards.add(SUPER_SKRULL);
			villianCards.add(SUPER_SKRULL);
		}
		if(villians.contains(CATEGORY_SPIDER_FOES)){
			villianCards.add(DOCTOR_OCTOPUS);
			villianCards.add(DOCTOR_OCTOPUS);
			villianCards.add(GREEN_GOBLIN);
			villianCards.add(GREEN_GOBLIN);
			villianCards.add(THE_LIZARD);
			villianCards.add(THE_LIZARD);
			villianCards.add(VENOM);
			villianCards.add(VENOM);
		}
	}
	private void fillHenchmen(List<String> henchmen, List<String> villianCards, int players){
		int count = TEN;
		if(players == ONE){
			count = THREE;
		}
		if(henchmen.contains(DOOMBOT_LEGION)){
			for(int i = ZERO; i < count; ++i){
				villianCards.add(DOOMBOT_LEGION);
			}
		}
		if(henchmen.contains(HAND_NINJAS)){
			for(int i = ZERO; i < count; ++i){
				villianCards.add(HAND_NINJAS);
			}
		}
		if(henchmen.contains(SAVAGE_LAND_MUTATES)){
			for(int i = ZERO; i < count; ++i){
				villianCards.add(SAVAGE_LAND_MUTATES);
			}
		}
		if(henchmen.contains(SENTINEL)){
			for(int i = ZERO; i < count; ++i){
				villianCards.add(SENTINEL);
			}
		}
	}
	private void fillScheme(String scheme, ModelFacade model, List<String> heroCards, List<String> villianCards, int playerCount){
		int schemeCount = EIGHT;
		int bystandersCount = TWO;
		if(model.getPlayerCardList().size() == ONE){
			bystandersCount = ONE;
		}
		switch(scheme){
		case MIDTOWN_BANK_ROBBERY:
			bystandersCount = TWELVE;
			break;
		case NEGATIVE_ZONE_PRISON_BREAKOUT:
			break;
		case PORTALS_TO_THE_DARK_DIMENSION:
			schemeCount = SEVEN;
			break;
		case REPLACE_EARTHS_LEADERS_WITH_KILLBOTS:
			schemeCount = FIVE;
			bystandersCount = EIGHTEEN;
			model.getOtherCards().setSchemeTwists(THREE);
			break;
		case SECRET_INVASION_OF_THE_SKRULL_SHAPESHIFTERS:
			Random random = ClientManager.getInstance().getRandom();
			for(int i = ZERO; i < TWELVE; ++i){
				int choice = random.nextInt(heroCards.size());
				villianCards.add(heroCards.get(choice));
				heroCards.remove(choice);
			}
			break;
		case SUPER_HERO_CIVIL_WAR:
			switch(playerCount){
			case ONE:
				schemeCount = TEN;
				break;
			case TWO:
			case THREE:
				break;
			case FOUR:
			case FIVE:
				schemeCount = FIVE;
				break;
			}
			break;
		case THE_LEGACY_VIRUS:
			woundCount = playerCount * SIX;
			break;
		case UNLEASH_THE_POWER_OF_THE_COSMIC_CUBE:
			break;
		}
		for(int i = ZERO; i < schemeCount; ++i){
			villianCards.add(SCHEME_TWIST);
		}
		for(int i = ZERO; i < bystandersCount; ++i){
			villianCards.add(BYSTANDER);
		}
		bystanderCount -= bystandersCount;
	}
	private List<String> shuffle(List<String> deck, boolean leaveFirst){
		List<String> returnString = new ArrayList<String>();
		if(leaveFirst){
			if(deck.size() > ZERO){
				returnString.add(deck.get(ZERO));
				deck.remove(ZERO);
			}
		}
		
		Random random = ClientManager.getInstance().getRandom();
		while(deck.size() > ZERO){
			int choice = random.nextInt(deck.size());
			returnString.add(deck.get(choice));
			deck.remove(choice);
		}
		
		return returnString;
	}
}
