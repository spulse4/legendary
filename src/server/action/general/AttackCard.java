package server.action.general;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import client.main.ClientManager;
import server.dao.DAOFacade;
import server.definition.ServerCard;
import server.definition.ServerVariables;
import server.exceptions.InsufficientBystandersException;
import shared.log.Log;
import shared.model.ModelFacade;
import shared.model.PlayerCards;

public class AttackCard implements ServerVariables{
	
	private final String CLASS = "server.action.AttackCard.java: ";

	private DAOFacade facade;
	
	public AttackCard(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int cardIndex){
		Log.logError(model.getGameID(), CLASS + model.getTurnTracker().getCurrentPlayer() + SPACE + LOG_ATTACK_CARD + cardIndex);
		String villian = null;
		List<String> captured;
		switch(cardIndex){
		case ONE:
			villian = model.getCity().getSewers();
			break;
		case TWO:
			villian = model.getCity().getBank();
			break;
		case THREE:
			villian = model.getCity().getRooftops();
			break;
		case FOUR:
			villian = model.getCity().getStreets();
			break;
		case FIVE:
			villian = model.getCity().getBridge();
			break;
		case SIX:
			captured = attackMastermind(model, MASTERMIND);
			List<String> masterminds = model.getOtherCards().getMasterminds();
			villian = masterminds.get(ZERO);
			changeAttackPoints(model, villian, cardIndex, captured);
			return;
		}
		captured = attack(model, cardIndex);
		changeAttackPoints(model, villian, cardIndex, captured);
	}
	
	public List<String> attack(ModelFacade model, int cardIndex){
		String villian = null;
		String location = null;
		List<String> captured = null;
		switch(cardIndex){
		case SEWERS_NUMBER:
			villian = model.getCity().getSewers();
			model.getCity().setSewers(null);
			captured = model.getCity().getSewersCaptured();
			model.getCity().setSewersCaptured(new ArrayList<String>());
			location = SEWERS;
			break;
		case BANK_NUMBER:
			villian = model.getCity().getBank();
			model.getCity().setBank(null);
			captured = model.getCity().getBankCaptured();
			model.getCity().setBankCaptured(new ArrayList<String>());
			location = BANK;
			break;
		case ROOFTOPS_NUMBER:
			villian = model.getCity().getRooftops();
			model.getCity().setRooftops(null);
			captured = model.getCity().getRooftopsCaptured();
			model.getCity().setRooftopsCaptured(new ArrayList<String>());
			location = ROOFTOPS;
			break;
		case STREETS_NUMBER:
			villian = model.getCity().getStreets();
			model.getCity().setStreets(null);
			captured = model.getCity().getStreetsCaptured();
			model.getCity().setStreetsCaptured(new ArrayList<String>());
			location = STREETS;
			break;
		case BRIDGE_NUMBER:
			villian = model.getCity().getBridge();
			model.getCity().setBridge(null);
			captured = model.getCity().getBridgeCaptured();
			model.getCity().setBridgeCaptured(new ArrayList<String>());
			location = BRIDGE;
			break;
		case MASTERMIND_NUMBER:
			captured = attackMastermind(model, MASTERMIND);
			return captured;
		}
		
		attackVillian(model, villian, captured, location);
		if(model.getTurnTracker().getCardsInEffect().contains(IMPOSSIBLE_TRICK_SHOT)){
			try{
				int index = model.getTurnTracker().getCurrentPlayer();
				model.getOtherCards().removeBystander(ONE);
				model.getPlayerCardList().get(index).getVictoryPile().add(BYSTANDER);
				model.getOtherCards().removeBystander(ONE);
				model.getPlayerCardList().get(index).getVictoryPile().add(BYSTANDER);
				model.getOtherCards().removeBystander(ONE);
				model.getPlayerCardList().get(index).getVictoryPile().add(BYSTANDER);
			}
			catch(InsufficientBystandersException e){}
		}
		if(model.getTurnTracker().getCardsInEffect().contains(DIAMOND_FORM)){
			int index = model.getTurnTracker().getCurrentPlayer();
			int pendingBuyPoints = model.getPlayerCardList().get(index).getPendingBuyPoints();
			model.getPlayerCardList().get(index).setPendingBuyPoints(pendingBuyPoints + 3);
		}
		return captured;
	}
	
	private List<String> attackMastermind(ModelFacade model, String location){
		List<String> masterminds = model.getOtherCards().getMasterminds();
		if(masterminds.size() == ONE){
			endGame(model, location);
			return new ArrayList<String>();
		}
		Random rand = ClientManager.getInstance().getRandom();
		int defeated = rand.nextInt(masterminds.size() - ONE);
		String villian = masterminds.get(defeated + ONE);
		masterminds.remove(defeated + ONE);
		model.getOtherCards().setMasterminds(masterminds);
		List<String> captured = model.getCity().getMastermindCaptured();
		model.getCity().setMastermindCaptured(new ArrayList<String>());
		
		attackVillian(model, villian, captured, location);
		return captured;
	}
	
	private void endGame(ModelFacade model, String location){
		String villian = model.getOtherCards().getMasterminds().get(ZERO);
		List<String> captured = model.getCity().getMastermindCaptured();
		
		attackVillian(model, villian, captured, location);

		new FinishGame().go(model, true);
	}
	
	private void attackVillian(ModelFacade model, String villian, List<String> captured, String location){
		int currentPlayer = model.getTurnTracker().getCurrentPlayer();
		PlayerCards playerCards = model.getPlayerCardList().get(currentPlayer);
		List<String> playersVictoryPile = playerCards.getVictoryPile();
		List<String> playerDiscardPile = playerCards.getDiscard();
		
		ServerCard villianCard = facade.getCard(villian);

		if(!villianCard.getFight().equals(NA)){
			new Fight(facade).go(model, villianCard.getName(), location);	
		}

		playersVictoryPile.add(villian);
				
		for(String str : captured){
			if(str.equals(BYSTANDER)){
				playersVictoryPile.add(BYSTANDER);
			}
			else{
				playerDiscardPile.add(str);
			}
		}				
	}
	
	private void changeAttackPoints(ModelFacade model, String villian, int cardIndex, List<String> captured){
		int currentPlayer = model.getTurnTracker().getCurrentPlayer();
		PlayerCards playerCards = model.getPlayerCardList().get(currentPlayer);
		ServerCard villianCard = facade.getCard(villian);
		int currentAttackPoints = playerCards.getPendingAttackPoints();
		int villianHP;
		if(villianCard.getHitPoints().equals("N/A")){
			villianHP = ZERO;
		}
		else{
			villianHP = Integer.valueOf(villianCard.getHitPoints());
		}
		
		if(villianCard.getName().equals(SKRULL_SHAPESHIFTERS) || villianCard.getName().equals(SKRULL_QUEEN_VERANKE)){
			villianHP = Integer.valueOf(facade.getCard(captured.get(ZERO)).getPrice());
		}
		if(model.getTurnTracker().getScheme().equals(MIDTOWN_BANK_ROBBERY)){
			for(String str : captured){
				if(str.equals(BYSTANDER)){
					++villianHP;
				}
			}
		}
		if(model.getTurnTracker().getScheme().equals(PORTALS_TO_THE_DARK_DIMENSION) &&
		   model.getCity().getMastermindPortal() &&
		   cardIndex == MASTERMIND_NUMBER){
			++villianHP;
		}
		if(model.getTurnTracker().getScheme().equals(PORTALS_TO_THE_DARK_DIMENSION) &&
		   model.getCity().getBridgePortal() &&
		   cardIndex == BRIDGE_NUMBER){
			++villianHP;
		}
		if(model.getTurnTracker().getScheme().equals(PORTALS_TO_THE_DARK_DIMENSION) &&
		   model.getCity().getStreetsPortal() &&
		   cardIndex == STREETS_NUMBER){
			++villianHP;
		}
		if(model.getTurnTracker().getScheme().equals(PORTALS_TO_THE_DARK_DIMENSION) &&
		   model.getCity().getRooftopsPortal() &&
		   cardIndex == ROOFTOPS_NUMBER){
			++villianHP;
		}
		if(model.getTurnTracker().getScheme().equals(PORTALS_TO_THE_DARK_DIMENSION) &&
		   model.getCity().getBankPortal() &&
		   cardIndex == BANK_NUMBER){
			++villianHP;
		}
		if(model.getTurnTracker().getScheme().equals(PORTALS_TO_THE_DARK_DIMENSION) &&
		   model.getCity().getSewersPortal() &&
		   cardIndex == SEWERS_NUMBER){
			++villianHP;
		}
		if(model.getTurnTracker().getScheme().equals(REPLACE_EARTHS_LEADERS_WITH_KILLBOTS) &&
		   villian.equals(BYSTANDER)){
			villianHP = model.getOtherCards().getSchemeTwists();
		}
		if(model.getTurnTracker().getScheme().equals(SECRET_INVASION_OF_THE_SKRULL_SHAPESHIFTERS) &&
		   !villianCard.getPrice().equals(NA)){
			villianHP = Integer.valueOf(villianCard.getPrice());
		}
		
		if(model.getTurnTracker().getCardsInEffect().contains(LIGHTNING_BOLT) &&
		   cardIndex == ROOFTOPS_NUMBER){
			villianHP -= TWO;
		}
		if(model.getTurnTracker().getCardsInEffect().contains(TIDAL_WAVE) &&
		   cardIndex == BRIDGE_NUMBER){
			villianHP -= TWO;
		}
		if(model.getTurnTracker().getCardsInEffect().contains(TIDAL_WAVE_MASTERMIND) &&
			    cardIndex == MASTERMIND_NUMBER){
			villianHP -= TWO;
		}
		
		if(model.getTurnTracker().getCardsInEffect().contains(GOD_OF_THUNDER) &&
				currentAttackPoints < villianHP){
			int difference = villianHP - currentAttackPoints;
			int pendingBuyPoints = playerCards.getPendingBuyPoints();
			playerCards.setPendingBuyPoints(pendingBuyPoints - difference);
			currentAttackPoints = villianHP;
		}
		
		currentAttackPoints -= villianHP;
		playerCards.setPendingAttackPoints(currentAttackPoints);
	}
}
