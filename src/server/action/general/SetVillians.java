package server.action.general;

import java.util.ArrayList;
import java.util.List;

import server.definition.ServerVariables;
import shared.model.ModelFacade;

public class SetVillians implements ServerVariables{

	public SetVillians(){}
	
	public void go(ModelFacade model, List<String> villians){
		List<String> henchmen = new ArrayList<String>();
		if(villians.contains(DOOMBOT_LEGION)){
			henchmen.add(DOOMBOT_LEGION);
			villians.remove(DOOMBOT_LEGION);
		}
		if(villians.contains(HAND_NINJAS)){
			henchmen.add(HAND_NINJAS);
			villians.remove(HAND_NINJAS);
		}
		if(villians.contains(SAVAGE_LAND_MUTATES)){
			henchmen.add(SAVAGE_LAND_MUTATES);
			villians.remove(SAVAGE_LAND_MUTATES);
		}
		if(villians.contains(SENTINEL)){
			henchmen.add(SENTINEL);
			villians.remove(SENTINEL);
		}
		model.getTurnTracker().setVillians(villians);
		model.getTurnTracker().setHenchmen(henchmen);
	}
}
