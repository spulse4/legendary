package server.action.general;

import shared.model.ModelFacade;

public class SetMastermind {

	public SetMastermind(){}
	
	public void go(ModelFacade model, String mastermind){
		model.getTurnTracker().setMastermind(mastermind);
	}
}
