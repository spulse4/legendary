package server.action.chat;

import shared.model.ModelFacade;

public class SendChat {

	public SendChat(){}
	
	public void go(ModelFacade model, int playerIndex, String message){
		model.getChat().addMessage(playerIndex, message);
	}
}
