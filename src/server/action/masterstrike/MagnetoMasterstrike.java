package server.action.masterstrike;

import java.util.ArrayList;
import java.util.List;

import server.action.general.DiscardCard;
import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class MagnetoMasterstrike implements ServerVariables{
	//Each player reveals an X-Men Hero or discards down to four cards

	public MagnetoMasterstrike(){}
	
	public void go(ModelFacade model, int gameID, int playerIndex, String card){
		List<Integer> effected = State.MAGNETO_MASTER_STRIKE.getEffected(gameID);
		for(int i = ZERO; i < effected.size(); ++i){
			if(effected.get(i) == playerIndex){
				effected.remove(i);
				break;
			}
		}
		if(effected.size() == ZERO){
			State.MAGNETO_MASTER_STRIKE.setEffected(gameID, new ArrayList<Integer>());
			State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
			model.getTurnTracker().setState(State.PLAYING);
		}
		new DiscardCard().go(model, playerIndex, card);
	}
}