package server.action.masterstrike;

import java.util.ArrayList;
import java.util.List;

import server.action.general.KOCard;
import server.dao.DAOFacade;
import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class RedSkullMasterstrike implements ServerVariables{
	//Each player KOs a Hero from their hand

	private DAOFacade facade;
	
	public RedSkullMasterstrike(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int gameID, int playerIndex, String card){
		List<Integer> effected = State.RED_SKULL_MASTER_STRIKE.getEffected(gameID);
		for(int i = ZERO; i < effected.size(); ++i){
			if(effected.get(i) == playerIndex){
				effected.remove(i);
				break;
			}
		}
		if(effected.size() == ZERO){
			State.RED_SKULL_MASTER_STRIKE.setEffected(gameID, new ArrayList<Integer>());
			State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
			model.getTurnTracker().setState(State.PLAYING);
		}
		new KOCard(facade).KOHand(model, playerIndex, card);
	}
}