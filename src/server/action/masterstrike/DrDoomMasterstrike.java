package server.action.masterstrike;

import java.util.ArrayList;
import java.util.List;

import server.action.general.PlaceCardOnDeck;
import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class DrDoomMasterstrike implements ServerVariables{
	//Each player with exactly 6 cards in hand reveals a Black Hero or puts 2 cards from their hand on top of their deck

	public DrDoomMasterstrike(){}
	
	public void go(ModelFacade model, int gameID, int playerIndex, String card){
		List<Integer> effected = State.DR_DOOM_MASTER_STRIKE.getEffected(gameID);
		for(int i = ZERO; i < effected.size(); ++i){
			if(effected.get(i) == playerIndex){
				effected.remove(i);
				break;
			}
		}
		if(effected.size() == ZERO){
			State.DR_DOOM_MASTER_STRIKE.setEffected(gameID, new ArrayList<Integer>());
			State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
			model.getTurnTracker().setState(State.PLAYING);
		}
		new PlaceCardOnDeck().go(model, playerIndex, card);
	}
}