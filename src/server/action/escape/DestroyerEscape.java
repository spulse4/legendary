package server.action.escape;

import java.util.ArrayList;
import java.util.List;

import server.action.general.KOCard;
import server.dao.DAOFacade;
import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class DestroyerEscape implements ServerVariables{
	//Each player KOs two of their Heroes

	private DAOFacade facade;

	public DestroyerEscape(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int gameID, int playerIndex, String card){
		List<Integer> effected = State.DESTROYER_ESCAPE.getEffected(gameID);
		for(int i = ZERO; i < effected.size(); ++i){
			if(effected.get(i) == playerIndex){
				effected.remove(i);
				break;
			}
		}
		if(effected.size() == ZERO){
			State.DESTROYER_ESCAPE.setEffected(gameID, new ArrayList<Integer>());
			State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
			model.getTurnTracker().setState(State.PLAYING);
		}
		new KOCard(facade).go(model, playerIndex, card);
	}
}