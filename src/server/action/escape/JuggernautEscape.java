package server.action.escape;

import java.util.ArrayList;
import java.util.List;

import server.action.general.KOCard;
import server.dao.DAOFacade;
import server.definition.ServerVariables;
import shared.definitions.State;
import shared.model.ModelFacade;

public class JuggernautEscape implements ServerVariables{
	//Each player KOs two Heroes from their hand

	private DAOFacade facade;
	
	public JuggernautEscape(DAOFacade facade){
		this.facade = facade;
	}
	
	public void go(ModelFacade model, int gameID, int playerIndex, String card){
		List<Integer> effected = State.JUGGERNAUT_ESCAPE.getEffected(gameID);
		for(int i = ZERO; i < effected.size(); ++i){
			if(effected.get(i) == playerIndex){
				effected.remove(i);
				break;
			}
		}
		if(effected.size() == ZERO){
			State.JUGGERNAUT_ESCAPE.setEffected(gameID, new ArrayList<Integer>());
			State.PLAYING.setEffected(gameID, new ArrayList<Integer>());
			model.getTurnTracker().setState(State.PLAYING);
		}
		new KOCard(facade).KOHand(model, playerIndex, card);
	}
}