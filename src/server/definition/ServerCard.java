package server.definition;

import java.util.ArrayList;
import java.util.List;

public class ServerCard implements ServerVariables{

	private String name;
	private String price;
	private String buyPower;
	private String attackPower;
	private String description;
	private List<String> colors;
	private String category;
	private String ambush;
	private String fight;
	private String escape;
	private String points;
	private String hitPoints;
	private String setup;
	private String specialRules;
	private String twist;
	private String evilWins;
	
	public ServerCard(List<String> values){
		colors = new ArrayList<String>();
		name = values.get(ZERO);
		price = values.get(ONE);
		buyPower = values.get(TWO);
		attackPower = values.get(THREE);
		description = values.get(FOUR);
		colors.add(values.get(FIVE));
		colors.add(values.get(SIX));
		colors.add(values.get(SEVEN));
		colors.add(values.get(EIGHT));
		category = values.get(NINE);
		ambush = values.get(TEN);
		fight = values.get(ELEVEN);
		escape = values.get(TWELVE);
		points = values.get(THIRTEEN);
		hitPoints = values.get(FOURTEEN);
		setup = values.get(FIFTEEN);
		specialRules = values.get(SIXTEEN);
		twist = values.get(SEVENTEEN);
		evilWins = values.get(EIGHTEEN);
	}

	public String getName() {
		return name;
	}

	public String getPrice() {
		return price;
	}

	public String getBuyPower() {
		return buyPower;
	}

	public String getAttackPower() {
		return attackPower;
	}

	public String getDescription() {
		return description;
	}

	public List<String> getColors() {
		return colors;
	}

	public String getCategory() {
		return category;
	}

	public String getAmbush() {
		return ambush;
	}

	public String getFight() {
		return fight;
	}

	public String getEscape() {
		return escape;
	}

	public String getPoints() {
		return points;
	}

	public String getHitPoints() {
		return hitPoints;
	}

	public String getSetup() {
		return setup;
	}

	public String getSpecialRules() {
		return specialRules;
	}

	public String getTwist() {
		return twist;
	}

	public String getEvilWins() {
		return evilWins;
	}
	
	public void setColor(int location, String color){
		colors.set(location, color);
	}
}
