package server.definition;

import shared.definitions.SharedVariables;

public interface ServerVariables extends SharedVariables{

	public final static int INITIAL_BYSTANDER_COUNT = 30;
	public final static int INITIAL_OFFICER_COUNT = 30;
	public final static int INITIAL_WOUND_COUNT = 30;
	
	public final static int SERVER_MAX_WAITING_CONNECTIONS = 10;

	public final static boolean MOCK_SERVER = false;
	public final static boolean TESTING = false;
	
	public final static String EXCLAMATION_POINT = "!";
	public final static String BRACKET_OPEN = "[";
	public final static String BRACKET_CLOSE = "]";
	
	public final static String DATABASE_AND = " and ";
	public final static String DATABASE_DRIVER = "org.sqlite.JDBC";
	public final static String DATABASE_EQUAL = "=\"";
	public final static String DATABASE_ERROR = "No connection is open";
	public final static String DATABASE_INSERT_1 = "Insert into ";
	public final static String DATABASE_INSERT_2 = " (";
	public final static String DATABASE_INSERT_3 = ") values(";
	public final static String DATABASE_INSERT_4 = ")";
	public final static String DATABASE_SELECT_1 = "Select ";
	public final static String DATABASE_SELECT_2 = " from ";
	public final static String DATABASE_SELECT_3 = " where ";
	public final static String DATABASE_SELECT_4 = "1=1";
	public final static String DATABASE_QUOTE = "\"";
	public final static String DATABASE_UPDATE_1 = "Update ";
	public final static String DATABASE_UPDATE_2 = " set ";
	public final static String DATABASE_UPDATE_3 = " where ";
	public final static String DATABASE_URL = "jdbc:sqlite:" + RESOURCES_DIRECTORY + "db/legendary.sqlite";
	
	public final static String DATABASE_CLEAR_INFO = RESOURCES_DIRECTORY + "db/clearDB.txt";
	
	public final static String DATABASE_HEADER_AMBUSH = "Ambush";
	public final static String DATABASE_HEADER_ATTACK_POWER = "AttackPower";
	public final static String DATABASE_HEADER_BUY_POWER = "BuyPower";
	public final static String DATABASE_HEADER_CATEGORY = "Category";
	public final static String DATABASE_HEADER_COLOR1 = "Color1";
	public final static String DATABASE_HEADER_COLOR2 = "Color2";
	public final static String DATABASE_HEADER_COLOR3 = "Color3";
	public final static String DATABASE_HEADER_COLOR4 = "Color4";
	public final static String DATABASE_HEADER_DESCRIPTION = "Description";
	public final static String DATABASE_HEADER_ESCAPE = "\"Escape\"";
	public final static String DATABASE_HEADER_EVIL_WINS = "EvilWins";
	public final static String DATABASE_HEADER_FIGHT = "Fight";
	public final static String DATABASE_HEADER_GAME_ID = "GameID";
	public final static String DATABASE_HEADER_HITPOINTS = "HitPoints";
	public final static String DATABASE_HEADER_ID = "ID";
	public final static String DATABASE_HEADER_NAME = "Name";
	public final static String DATABASE_HEADER_PASSWORD = "Password";
	public final static String DATABASE_HEADER_POINTS = "Points";
	public final static String DATABASE_HEADER_PRICE = "Price";
	public final static String DATABASE_HEADER_SETUP = "Setup";
	public final static String DATABASE_HEADER_SPECIAL_RULES = "SpecialRules";
	public final static String DATABASE_HEADER_TWIST = "Twist";
	public final static String DATABASE_HEADER_USERNAME = "Username";
	public final static String DATABASE_HEADER_VERSION = "Version";
	
	public final static String DATABASE_MODEL_DELETE = "DELETE from Model Where ID=?";
	public final static String DATABASE_MODEL_INSERT = "insert into Model (Model, Header) values (?, ?)";
	public final static String DATABASE_MODEL_INSERT_WITH_ID = "insert into Model (ID, Model, Header) values (?, ?, ?)";
	public final static String DATABASE_MODEL_READ_HEADER = "select Header from Model";
	public final static String DATABASE_MODEL_READ_MODEL = "select Model from Model where ID=?";
	
	public final static String DATABASE_TABLE_CARDS = "Cards";
	public final static String DATABASE_TABLE_USERS = "Users";
	public final static String DATABASE_TABLE_VERSIONS = "Versions";
	
	public final static String FUNCTIONS_ATTACK_CARD = "attackCard";
	public final static String FUNCTIONS_END_TURN = "endTurn";
	public final static String FUNCTIONS_GET_MODEL = "getModel";
	
	public final static String LOCATION_ESCAPED = "escaped";
	
	public final static String LOG_ATTACK_CARD = "Attacked card: ";
	public final static String LOG_BUY_CARD = "Bought card: ";
	public final static String LOG_DISCARD_CARD = "Discarded card: ";
	public final static String LOG_DRAW_CARD = "Drew a card";
	public final static String LOG_END_TURN = "Ended turn";
	public final static String LOG_ESCAPE = "Villian escaped: ";
	public final static String LOG_KO = "KOd card: ";
	public final static String LOG_PASS = "Passing Card: ";
	public final static String LOG_VILLIAN_PLAYED = "Villian played: ";
	
	public final static String SERVER_DONE_SETTING_UP = "Done setting up Game";
	
	public final static String TEST_HAND_INFO = RESOURCES_DIRECTORY + "test/hand.txt";
	public final static String TEST_MODEL_INFO_1 = RESOURCES_DIRECTORY + "test/model-";
	public final static String TEST_MODEL_INFO_2 = ".txt";
	public final static String TEST_VILLIAN_INFO = RESOURCES_DIRECTORY + "test/villian.txt";
}
