package server.definition;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ServerLoader implements ServerVariables{

	public static String loadVillian(){
		try{
			File input = new File(TEST_VILLIAN_INFO);
			Scanner scanner = new Scanner(input);
			String villian = scanner.nextLine();
			scanner.close();
			return villian;
		}
		catch(IOException e){
			e.printStackTrace();
		}
		return null;
	}
	public static List<String> loadHand(){
		try{
			File input = new File(TEST_HAND_INFO);
			Scanner scanner = new Scanner(input);
			List<String> hand = new ArrayList<String>();
			while(scanner.hasNextLine()){
				String line = scanner.nextLine();
				if(line.equals(EMPTY)){
					break;
				}
				hand.add(line);
			}			
			scanner.close();
			return hand;
		}
		catch(IOException e){
			e.printStackTrace();
		}
		return null;
	}
}
