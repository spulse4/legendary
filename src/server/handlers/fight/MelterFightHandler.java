package server.handlers.fight;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import server.definition.ServerVariables;
import server.main.ServerManager;

public class MelterFightHandler implements HttpHandler, ServerVariables{
	//Each player reveals the top card of their deck. For each card, you choose to KO it or put it back

	private Gson gson = new Gson();
	@Override
	public void handle(HttpExchange e) throws IOException {	
		InputStream input = e.getRequestBody();
		int len = ZERO;
		
		byte[] buffer = new byte[BYTE_SIZE];
		StringBuilder string = new StringBuilder();
		while(NEGATIVE_ONE != (len = input.read(buffer))){
			string.append(new String(buffer, ZERO, len));
		}
		
		JsonParser parser = new JsonParser();
		JsonObject o = parser.parse(string.toString()).getAsJsonObject();

		int gameID = o.get(GAME_ID).getAsInt();
		int playerIndex = o.get(PLAYER_INDEX).getAsInt();
		boolean ko = o.get(KO).getAsBoolean();
		
		ServerManager.getInstance().getServer().melterFight(gameID, playerIndex, ko);

		e.sendResponseHeaders(HttpURLConnection.HTTP_OK, ZERO);
		OutputStreamWriter out = new OutputStreamWriter(e.getResponseBody());
		out.write(gson.toJson(new JsonObject()));
		out.flush();
		e.getResponseBody().close();
	}
}
