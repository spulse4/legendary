package server.handlers.test;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import server.definition.ServerVariables;
import server.main.ServerManager;

public class TestHandler implements HttpHandler, ServerVariables{

	private Gson gson = new Gson();
	
	@Override
	public void handle(HttpExchange e) throws IOException{	
		//localhost:8081/test?<function>:<gameID>(:<Other Numbers>)
		String uri = e.getRequestURI().toString().substring(FIVE);
		JsonObject obj = new JsonObject();
		if(uri.length() == ZERO){
			obj = ServerManager.getInstance().getServer().getModel(ONE, NEGATIVE_ONE);
		}
		else{
			uri = uri.substring(ONE);
			if(uri.startsWith(FUNCTIONS_END_TURN)){
				int gameID = Integer.valueOf(uri.substring(uri.indexOf(COLON) + ONE, uri.lastIndexOf(COLON)));
				int playerIndex = Integer.valueOf(uri.substring(uri.lastIndexOf(COLON) + ONE));
				ServerManager.getInstance().getServer().endTrun(gameID, playerIndex);
			}
			else if(uri.startsWith(FUNCTIONS_GET_MODEL)){
				int gameID = ONE;
				if(uri.indexOf(COLON) != NEGATIVE_ONE){
					gameID = Integer.valueOf(uri.substring(uri.indexOf(COLON) + ONE));
				}
				obj = ServerManager.getInstance().getServer().getModel(gameID, NEGATIVE_ONE);
			}
			else if(uri.startsWith(FUNCTIONS_ATTACK_CARD)){
				int gameID = ONE;
				int cardIndex = ZERO;
				if(uri.indexOf(COLON) != NEGATIVE_ONE){
					if(uri.indexOf(COLON) == uri.lastIndexOf(COLON)){
						gameID = Integer.valueOf(uri.substring(uri.indexOf(COLON) + ONE));						
					}
					else{
						gameID = Integer.valueOf(uri.substring(uri.indexOf(COLON) + ONE, uri.lastIndexOf(COLON)));
						cardIndex = Integer.valueOf(uri.substring(uri.lastIndexOf(COLON) + ONE));
					}
				}
				ServerManager.getInstance().getServer().attackCard(gameID, cardIndex);
			}
		}		
		
		e.sendResponseHeaders(HttpURLConnection.HTTP_OK, ZERO);
		OutputStreamWriter out = new OutputStreamWriter(e.getResponseBody());
		out.write(gson.toJson(obj));
		out.flush();
		e.getResponseBody().close();
	}
}