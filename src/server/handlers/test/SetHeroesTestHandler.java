package server.handlers.test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.nio.file.Files;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import server.definition.ServerVariables;

public class SetHeroesTestHandler implements HttpHandler, ServerVariables{
	
	@Override
	public void handle(HttpExchange e) throws IOException{
		File check = new File("test/hand.txt");
		if(check.exists()){
			Files.delete(check.toPath());
		}
		InputStream in = e.getRequestBody();
		int len = 0;
		
		byte[] buffer = new byte[1024];
		StringBuilder stringy = new StringBuilder();
		while(-1 != (len = in.read(buffer))){
			stringy.append(new String(buffer, 0, len));
		}
							
		FileWriter saveIt = new FileWriter("test/hand.txt");
		saveIt.write(stringy.toString());
		saveIt.close();
		
		e.sendResponseHeaders(HttpURLConnection.HTTP_OK, ZERO);
		e.getResponseBody().close();
	}
}