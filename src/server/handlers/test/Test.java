package server.handlers.test;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class Test implements HttpHandler{

	private Gson gson = new Gson();
	
	@Override
	public void handle(HttpExchange e) throws IOException{	
		JsonObject obj = new JsonObject();
		e.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
		OutputStreamWriter out = new OutputStreamWriter(e.getResponseBody());
		out.write(gson.toJson(obj));
		out.flush();
		e.getResponseBody().close();
	}
}