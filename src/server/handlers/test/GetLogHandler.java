package server.handlers.test;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import server.definition.ServerVariables;

public class GetLogHandler implements HttpHandler, ServerVariables{

	private String sourceFolder = "log";
		
	@Override
	public void handle(HttpExchange e) throws IOException{
		File log = new File(sourceFolder + File.separator + "logs.zip");
		if(log.exists()){
			Files.delete(log.toPath());
		}
		
		zipLogs();
		
		e.sendResponseHeaders(HttpURLConnection.HTTP_OK, ZERO);

		BufferedOutputStream toServer = new BufferedOutputStream(e.getResponseBody());
		File myFile = new File("log/logs.zip");
		byte[] mybytearray = new byte[(int) myFile.length()];
        FileInputStream fis = new FileInputStream(myFile);
        BufferedInputStream bis = new BufferedInputStream(fis);
        bis.read(mybytearray, 0, mybytearray.length);
        toServer.write(mybytearray, 0, mybytearray.length);
        bis.close();
        toServer.flush();
        toServer.close();
		e.getResponseBody().close();
	}
	
	private void zipLogs(){
		List<String> logs = new ArrayList<String>();
		File top = new File(sourceFolder);
		for(File inner : top.listFiles()){
			if(inner.getName().endsWith(".sp")){
				logs.add(inner.getName());
			}
		}
		zipIt("logs.zip", logs);
	}
	
	private void zipIt(String zipFile, List<String> fileList){
		byte[] buffer = new byte[1024];
		try{
			FileOutputStream fos = new FileOutputStream(sourceFolder + File.separator + zipFile);
			ZipOutputStream zos = new ZipOutputStream(fos);

			for(String file : fileList){
				ZipEntry ze= new ZipEntry(file);
				zos.putNextEntry(ze);

				FileInputStream in = 
						new FileInputStream(sourceFolder + File.separator + file);
				int len;
				while ((len = in.read(buffer)) > 0) {
					zos.write(buffer, 0, len);
				}
				in.close();
			}
			zos.closeEntry();
			zos.close();
		}
		catch(IOException ex){
			ex.printStackTrace();   
		}
	}
}