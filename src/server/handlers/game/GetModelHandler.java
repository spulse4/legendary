package server.handlers.game;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import server.definition.ServerVariables;
import server.main.ServerManager;

public class GetModelHandler implements HttpHandler, ServerVariables{

	private Gson gson = new Gson();
	
	@Override
	public void handle(HttpExchange e) throws IOException {	
		InputStream input = e.getRequestBody();
		int len = ZERO;
		
		byte[] buffer = new byte[BYTE_SIZE];
		StringBuilder string = new StringBuilder();
		while(NEGATIVE_ONE != (len = input.read(buffer))){
			string.append(new String(buffer, ZERO, len));
		}
		
		JsonParser parser = new JsonParser();
		JsonObject o = parser.parse(string.toString()).getAsJsonObject();

		int gameID = o.get(GAME_ID).getAsInt();
		int version = o.get(VERSION_ID).getAsInt();
		
		JsonObject obj = ServerManager.getInstance().getServer().getModel(gameID, version);
		
		e.sendResponseHeaders(HttpURLConnection.HTTP_OK, ZERO);
		OutputStreamWriter out = new OutputStreamWriter(e.getResponseBody());
		out.write(gson.toJson(obj));
		out.flush();
		e.getResponseBody().close();
	}
}