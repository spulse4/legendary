package server.handlers.game;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import server.definition.ServerVariables;
import server.main.ServerManager;

public class SetVilliansHandler implements HttpHandler, ServerVariables{

	private Gson gson = new Gson();
	
	@SuppressWarnings("unchecked")
	@Override
	public void handle(HttpExchange e) throws IOException {	
		InputStream input = e.getRequestBody();
		int len = ZERO;
		
		byte[] buffer = new byte[BYTE_SIZE];
		StringBuilder string = new StringBuilder();
		while(NEGATIVE_ONE != (len = input.read(buffer))){
			string.append(new String(buffer, ZERO, len));
		}
		
		JsonParser parser = new JsonParser();
		JsonObject o = parser.parse(string.toString()).getAsJsonObject();

		int gameID = o.get(GAME_ID).getAsInt();
		ArrayList<Object> listOfObjects = gson.fromJson(o.get(VILLIANS), ArrayList.class);
		List<String> villians = new ArrayList<String>();
		for(Object ob : listOfObjects){
			villians.add((String)ob);
		}
		
		ServerManager.getInstance().getServer().setVillians(gameID, villians);
		
		e.sendResponseHeaders(HttpURLConnection.HTTP_OK, ZERO);
		OutputStreamWriter out = new OutputStreamWriter(e.getResponseBody());
		out.write(gson.toJson(new JsonObject()));
		out.flush();
		e.getResponseBody().close();
	}
}