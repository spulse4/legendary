package server.handlers.game;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import server.definition.ServerVariables;
import server.main.ServerManager;
import shared.definitions.GameHeader;

public class GetGamesHandler implements HttpHandler, ServerVariables{

	private Gson gson = new Gson();
	
	@Override
	public void handle(HttpExchange e) throws IOException{		
		List<GameHeader> games = ServerManager.getInstance().getServer().getGames();
		
		e.sendResponseHeaders(HttpURLConnection.HTTP_OK, ZERO);
		OutputStreamWriter out = new OutputStreamWriter(e.getResponseBody());
		JsonObject obj = new JsonObject();
		obj.add(GAMES, gson.toJsonTree(games));
		out.write(gson.toJson(obj));
		out.flush();
		e.getResponseBody().close();
	}
}
