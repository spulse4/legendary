In order to run this application you must have a java JRE installed.

After unzipping the application follow these steps:
	1. Open 1 terminal for the server, and 1 terminal for every player that wishes to play and navigate all terminals to the extracted folder
	2. In the first terminal, Run the appropriate command (replacing <PORT> with your desired port. Leave blank to use the default port 31415):
		a. Windows Machine: Server.bat <PORT>
		b. Unix Machine: Server.sh <PORT>
	3. In all remaining terminals, run the appropriate command (again, replacing <PORT> with your desired port, and <HOST> with the desired host. Default values are localhost:31415)
		a. Windows Machine: Legendary.bat <HOST> <PORT>
		b. Unix Machine: Legendary.sh <HOST> <PORT>
		note: in order to fill in the port, you must specify a host

When running Server.bat/sh you may encounter a bind conflict if you are running another process on the port specified. In this case, simply choose another port number and run Server.bat/sh again until you find a port that is not being bound to a process.

The first time that you start the server it will print out error messages that it could not drop any tables from the database and that there is no log file for game 1. These will not impede the server from running. Any subsequent reboots of the server will not throw these errors.

Starting the server will erase all game and user data that was previously stored in the database. 
