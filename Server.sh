#!/bin/bash

PORT=31415
if [ ! -z ${1} ]; then
	PORT=${1}
fi

java -cp "./bin:./lib/*" server.main.Server ${PORT}
