#!/bin/bash

HOST="localhost"
PORT=31415

if [ ! -z ${1} ]; then
	HOST=${1}
fi

if [ ! -z ${2} ]; then
	PORT=${2}
fi

java -cp "./bin:./lib/*" client.main.Client ${HOST} ${PORT}
