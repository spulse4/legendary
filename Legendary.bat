@echo off
set HOST="localhost"
set PORT=31415

if "%1"=="" goto GO

set HOST=%1

if "%2"=="" goto GO

set PORT=%2

:GO
java -cp ./lib/*;./bin client.main.Client %HOST% %PORT%

