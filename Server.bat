@echo off
set PORT=31415

if "%1"=="" goto GO

set PORT=%1

:GO
java -cp ./lib/*;./bin server.main.Server %PORT%
